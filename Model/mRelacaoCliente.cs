﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mRelacaoCliente
    {
        public int Id { get; set; }
        public mProduto produto { get; set; }
        public mClientes cliente { get; set; }
    }
}