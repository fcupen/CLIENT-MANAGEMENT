﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mMetas
    {
        public int id { get; set; }
        public string ano { get; set; }
        public mVendedor vendedor { get; set; }
        public mProduto produto { get; set; }
        public decimal Enero { get; set; }
        public decimal Febrero { get; set; }
        public decimal Marzo { get; set; }
        public decimal Abril { get; set; }
        public decimal Mayo { get; set; }
        public decimal Junio { get; set; }
        public decimal Julio { get; set; }
        public decimal Agosto { get; set; }
        public decimal Septiembre { get; set; }
        public decimal Octubre { get; set; }
        public decimal Noviembre { get; set; }
        public decimal Diciembre { get; set; }
        public string Enero_diashabilesdetrabajo { get; set; }
        public string Febrero_diashabilesdetrabajo { get; set; }
        public string Marzo_diashabilesdetrabajo { get; set; }
        public string Abril_diashabilesdetrabajo { get; set; }
        public string Mayo_diashabilesdetrabajo { get; set; }
        public string Junio_diashabilesdetrabajo { get; set; }
        public string Julio_diashabilesdetrabajo { get; set; }
        public string Agosto_diashabilesdetrabajo { get; set; }
        public string Septiembre_diashabilesdetrabajo { get; set; }
        public string Octubre_diashabilesdetrabajo { get; set; }
        public string Noviembre_diashabilesdetrabajo { get; set; }
        public string Diciembre_diashabilesdetrabajo { get; set; }        
    }
}