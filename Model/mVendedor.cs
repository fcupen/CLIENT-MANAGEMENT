﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mVendedor
    {
        public int id { get; set; }
        public DateTime calendario { get; set; }
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string appelido { get; set; }
        public mGrupoVendedores area { get; set; }
        public mProduto seccion { get; set; }
        public decimal meta { get; set; }
        public string email { get; set; }
        public string clave { get; set; }
        public string cargo { get; set; }
        public string cartera_assignada { get; set; }
    }
}