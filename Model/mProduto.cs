﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mProduto
    {
        public int Id { get; set; }
        public string nombre { get; set; }
        public string codigo { get; set; }
        public string horizonte { get; set; }
    }
}