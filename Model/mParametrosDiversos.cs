﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mParametrosDiversos
    {
        public int travardias { get; set; }
        public int maxvisitasdias { get; set; }
        public int maxvisitasagendadas { get; set; }
        public int tkc { get; set; }
        public int metaprometodiaria { get; set; }
    }
}