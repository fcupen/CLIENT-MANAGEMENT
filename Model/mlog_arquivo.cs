﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mlog_arquivo
    {
        public int Id { get; set; }
        public string arquivo { get; set; }
        public string tela { get; set; }
        public DateTime data { get; set; }
        public mVendedor vendedor { get; set; }
    }
}