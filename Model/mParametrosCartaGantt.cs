﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mParametrosCartaGantt
    {
        public int Id { get; set; }
        public string ano { get; set; }
        public string mes { get; set; }
        public string dia { get; set; }
        public string fecha { get; set; }
    }
}