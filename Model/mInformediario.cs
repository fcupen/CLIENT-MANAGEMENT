﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mInformediario
    {
        public int Id { get; set; }
        public DateTime calendario { get; set; }
        public mClientes cliente { get; set; }
        public mVendedor vendedor { get; set; }
        public string contacto { get; set; }
        public string formacontacto { get; set; }
        public string formacontacto2 { get; set; }
        public string acuerdos { get; set; }
        public string ano { get; set; }
        public string mes { get; set; }
        public string d1_valor { get; set; }
        public string d2_valor { get; set; }
        public string d3_valor { get; set; }
        public string d4_valor { get; set; }
        public string d5_valor { get; set; }
        public string d6_valor { get; set; }
        public string d7_valor { get; set; }
        public string d8_valor { get; set; }
        public string d9_valor { get; set; }
        public string d10_valor { get; set; }
        public string d11_valor { get; set; }
        public string d12_valor { get; set; }
        public string d13_valor { get; set; }
        public string d14_valor { get; set; }
        public string d15_valor { get; set; }
        public string d16_valor { get; set; }
        public string d17_valor { get; set; }
        public string d18_valor { get; set; }
        public string d19_valor { get; set; }
        public string d20_valor { get; set; }
        public string d21_valor { get; set; }
        public string d22_valor { get; set; }
        public string d23_valor { get; set; }
        public string d24_valor { get; set; }
        public string d25_valor { get; set; }
        public string d26_valor { get; set; }
        public string d27_valor { get; set; }
        public string d28_valor { get; set; }
        public string d29_valor { get; set; }
        public string d30_valor { get; set; }
        public string d31_valor { get; set; }
    }
}