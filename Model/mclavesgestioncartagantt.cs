﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mclavesgestioncartagantt
    {
        public int Id { get; set; }
        public string ano { get; set; }
        public string mes { get; set; }
        public string clave { get; set; }
        public string fecha { get; set; }
    }
}