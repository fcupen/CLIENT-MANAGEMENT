﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mBDN
    {
        public int Id { get; set; }
        public DateTime fecha { get; set; }
        public string cotizacion { get; set; }
        public mClientes instituicion { get; set; }
        public string contacto { get; set; }
        public string descripcion_productos { get; set; }
        public decimal totalneto { get; set; }
        public string status { get; set; }
        public mProduto produto { get; set; }
        public mVendedor vendedor { get; set; }
    }
}