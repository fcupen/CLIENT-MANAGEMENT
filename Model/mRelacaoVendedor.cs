﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mRelacaoVendedor
    {
        public int Id { get; set; }
        public mVendedor vendedor { get; set; }
        public mClientes cliente { get; set; }

    }
}