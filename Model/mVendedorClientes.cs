﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mVendedorClientes
    {
        public int Id { get; set; }
        public mVendedor vendedor { get; set; }
        public int clientes { get; set; }
        public mProduto produto { get; set; }
    }
}