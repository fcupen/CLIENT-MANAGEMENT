﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mClientes
    {
        public int id { get; set; }
        //public mVendedor vendedor { get; set; }
        public DateTime calendario { get; set; }
        public string rut { get; set; }
        public string razonsocial { get; set; }
        public string direccion { get; set; }
        public string comuna { get; set; }
        public string ciudade { get; set; }
        public string region { get; set; }
        public string sucursal { get; set; }
        public string contato { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }

    }
}