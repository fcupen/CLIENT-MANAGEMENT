﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mBrecha
    {
        public int Id { get; set; }
        public decimal valor { get; set; }
        public mProduto produto { get; set; }
        public mClientes cliente { get; set; }
        public mVendedor vendedor { get; set; }
    }
}