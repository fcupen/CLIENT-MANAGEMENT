﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clients_Management.Model
{
    public class mDiasUteis
    {
        public int id { get; set; }
        public string ano { get; set; }
        public string Enero_diashabilesdetrabajo { get; set; }
        public string Febrero_diashabilesdetrabajo { get; set; }
        public string Marzo_diashabilesdetrabajo { get; set; }
        public string Abril_diashabilesdetrabajo { get; set; }
        public string Mayo_diashabilesdetrabajo { get; set; }
        public string Junio_diashabilesdetrabajo { get; set; }
        public string Julio_diashabilesdetrabajo { get; set; }
        public string Agosto_diashabilesdetrabajo { get; set; }
        public string Septiembre_diashabilesdetrabajo { get; set; }
        public string Octubre_diashabilesdetrabajo { get; set; }
        public string Noviembre_diashabilesdetrabajo { get; set; }
        public string Diciembre_diashabilesdetrabajo { get; set; }   
    }
}