﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Clients_Management.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div id="header"></div>

<div id="section_header">
  <div class="container">
    <h2><span>Login</span></h2>
  </div>
</div>
<div id="services">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <img src="images/service1.jpg" class="img-responsive" alt=" ">
		<h4>FUERZA DE VENTAS</h4>
		<div>
      	<div class="form_details">
          <asp:TextBox ID="txtEmailFuerza" class="form-control input-lg" placeholder="Login"  runat="server"></asp:TextBox>
          <asp:TextBox ID="txtSenhaFuerza" class="form-control input-lg" placeholder="Clave" TextMode="Password" runat="server"></asp:TextBox>
          <div class="clearfix"> </div>
            <asp:Button ID="btnFuerza" runat="server" class="btn btn-success btn-lg" Text="Enviar" OnClick="btnFuerza_Click" />
        </div>
      </div>
		
         </div>
      <div class="col-lg-3">
	  <img src="images/service2.jpg" class="img-responsive" alt=" ">
        <h4>DIRECCION DE VENTAS</h4>
		<div >
        <div class="form_details">
          <asp:TextBox ID="txtUsuarioDirecao" class="form-control input-lg" placeholder="Login"  runat="server"></asp:TextBox>
          <asp:TextBox ID="txtSenhaDirecao" class="form-control input-lg" placeholder="Clave" TextMode="Password" runat="server"></asp:TextBox>
          <div class="clearfix"> </div>
          <asp:Button ID="Button1" runat="server" class="btn btn-success btn-lg" Text="Enviar"  OnClick="Button1_Click" />
        </div>
      </div>
       </div>
      <div class="col-lg-3 service">
	  <img src="images/service3.jpg" class="img-responsive" alt=" ">
        <h4>ADMINISTRACION DE VENTAS</h4>
		<div>
        <div class="form_details">
          <asp:TextBox ID="txtUsuarioAdmin" class="form-control input-lg" placeholder="Login"  runat="server"></asp:TextBox>
          <asp:TextBox ID="txtSenhaAdmin" class="form-control input-lg" placeholder="Clave" TextMode="Password" runat="server"></asp:TextBox>
          <div class="clearfix"> </div>
          <asp:Button ID="Button2" runat="server" class="btn btn-success btn-lg" Text="Enviar" OnClick="Button2_Click" />
        </div>
      </div>
       </div>
	  <div class="col-lg-3">
	  <img src="images/soporte.jpg" class="img-responsive" alt=" ">
        <h4>SOPORTE TECNICO</h4>
		<div>
        <div class="form_details">
          <input type="text" class="form-control input-lg" value="Login">
		  <input type="text" class="form-control input-lg" value="Clave">
          <div class="clearfix"> </div>
          <asp:Button ID="Button3" runat="server" class="btn btn-success btn-lg" Text="Enviar" PostBackUrl="~/View/login_T.aspx" />
        </div>
      </div>
	   </div>
    </div>
    <div class="row"></div>
  </div>
</div>
<!-- Footer -->
<div id="footer"></div>
<!-- Bootstrap core JavaScript --> 
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</asp:Content>
