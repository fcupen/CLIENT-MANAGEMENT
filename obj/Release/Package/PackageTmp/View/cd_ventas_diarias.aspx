﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_ventas_diarias.aspx.cs" Inherits="Clients_Management.View.cd_ventas_diarias" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">    

  <div class="container">
                    <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">CONTROL DE OPERACIONES CONEXAS</a></li>
  <li class="active">INGRESO DE VENTAS</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
         <asp:Label ID="lblNombre" runat="server" Text="Label"></asp:Label>
     </div>
     <p>&nbsp;</p>
     <div class="col-md-12">


          <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">
                <asp:Label ID="lblTitulo" runat="server" Text="Título"></asp:Label>
            </h4>
			
            
            <div style="overflow:auto">
            <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White">
                <Columns>
                    <asp:CommandField DeleteText="Deletar" ShowDeleteButton="false" />
                </Columns>
            </asp:GridView>	
            </div>	
        
        		
		</div>
	</div>
 
</div>
	 

            <div class="form-horizontal well" >
         <h4>Ingrese Venta</h4>
				 <div class="row">
 
					<div class="col-md-12">
				
                   <div class="form-group">
					<div class="rows">
							 <div class="col-md-3"><label>Selecione Vendedor:</label>
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false" ></asp:TextBox>
                                <asp:DropDownList ID="ddlVendedor" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVendedor_SelectedIndexChanged"></asp:DropDownList>
							</div>
					
					</div>
				</div>
				<div class="form-group">
					<div class="rows">
							 <div class="col-md-3"><label>Selecione Producto:</label>
                                <asp:TextBox ID="TextBox1" class="form-control input-lg" runat="server" Visible="false" ></asp:TextBox>
                                <asp:DropDownList ID="ddlProduto" class="form-control input-sm" runat="server"  AutoPostBack="True" OnSelectedIndexChanged="ddlProduto_SelectedIndexChanged" ></asp:DropDownList>
							</div>
					
					</div>
				</div>					
				
                <div class="form-group">
					<div class="rows">
							 <div class="col-md-3"><label>Selecione Cliente:</label>
                                <asp:DropDownList ID="ddlCliente" class="form-control input-sm"  runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged"></asp:DropDownList>
							</div>
					
					</div>
				</div>	
		
				<div class="form-group">
					<div class="rows">
						
							<div class="col-md-2">
                                <asp:Label ID="Label1" runat="server" Text="Año:"></asp:Label>
                                <asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAno_SelectedIndexChanged" >
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                </asp:DropDownList>
							</div>

                            <div class="col-md-2">
                                <asp:Label ID="lblMes" runat="server" Text="Mes:"></asp:Label>
                                <asp:DropDownList ID="ddlMes" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlMes_SelectedIndexChanged" >
                                    <asp:ListItem>Enero</asp:ListItem>
                                    <asp:ListItem>Febrero</asp:ListItem>
                                    <asp:ListItem>Marzo</asp:ListItem>
                                    <asp:ListItem>Abril</asp:ListItem>
                                    <asp:ListItem>Mayo</asp:ListItem>
                                    <asp:ListItem>Junio</asp:ListItem>
                                    <asp:ListItem>Julio</asp:ListItem>
                                    <asp:ListItem>Agosto</asp:ListItem>
                                    <asp:ListItem>Septiembre</asp:ListItem>
                                    <asp:ListItem>Octubre</asp:ListItem>
                                    <asp:ListItem>Noviembre</asp:ListItem>
                                    <asp:ListItem>Diciembre</asp:ListItem>                                    
                                </asp:DropDownList>
							</div>

				
					</div>
				
                
                </div>

                            
                <h4>Dias del Mes </h4>
                 
                     <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld1" runat="server" class="col-md-1 control-label" Text="1:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                            <asp:TextBox ID="txtd1" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld2" runat="server" class="col-md-1 control-label" Text="2:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd2" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld3" runat="server" class="col-md-1 control-label" Text="3:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd3" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld4" runat="server" class="col-md-1 control-label" Text="4:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd4" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld5" runat="server" class="col-md-1 control-label" Text="5:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd5" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>

                <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld6" runat="server" class="col-md-1 control-label" Text="6:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd6" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld7" runat="server" class="col-md-1 control-label" Text="7:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd7" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld8" runat="server" class="col-md-1 control-label" Text="8:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd8" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld9" runat="server" class="col-md-1 control-label" Text="9:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd9" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld10" runat="server" class="col-md-1 control-label" Text="10:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd10" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                                                
                <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld11" runat="server" class="col-md-1 control-label" Text="11:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd11" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld12" runat="server" class="col-md-1 control-label" Text="12:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd12" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld13" runat="server" class="col-md-1 control-label" Text="13:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd13" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld14" runat="server" class="col-md-1 control-label" Text="14:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd14" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld15" runat="server" class="col-md-1 control-label" Text="15:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd15" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
                        
<div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld16" runat="server" class="col-md-1 control-label" Text="16:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd16" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld17" runat="server" class="col-md-1 control-label" Text="17:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd17" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld18" runat="server" class="col-md-1 control-label" Text="18:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd18" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld19" runat="server" class="col-md-1 control-label" Text="19:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd19" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld20" runat="server" class="col-md-1 control-label" Text="20:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd20" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
<div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld21" runat="server" class="col-md-1 control-label" Text="21:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd21" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld22" runat="server" class="col-md-1 control-label" Text="22:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd22" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld23" runat="server" class="col-md-1 control-label" Text="23:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd23" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld24" runat="server" class="col-md-1 control-label" Text="24:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd24" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld25" runat="server" class="col-md-1 control-label" Text="25:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd25" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
<div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld26" runat="server" class="col-md-1 control-label" Text="26:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd26" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld27" runat="server" class="col-md-1 control-label" Text="27:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd27" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld28" runat="server" class="col-md-1 control-label" Text="28:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd28" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld29" runat="server" class="col-md-1 control-label" Text="29:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd29" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld30" runat="server" class="col-md-1 control-label" Text="30:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd30" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
               <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld31" runat="server" class="col-md-1 control-label" Text="31:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd31" class="form-control" runat="server" placeholder="Valor" ClientIDMode="Static"></asp:TextBox>
	                  </div>			
								
				</div>
 
                   <div class="form-group" style="font-size:12px">
					<div class="rows">
						
							<div class="col-md-12" align="center">
                                <asp:Button ID="btnNovo" runat="server" class="btn btn-primary" Text="Limpiar" OnClick="btnNovo_Click" />
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Cadastrar" OnClick="btnSalvar_Click" />                                
							</div>
					
					</div>
				</div>
				</div>	
	   </div>
     </div>
	 </div>
	 
	
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
<div class="row">  
</div>
</div>

</div>
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer --></asp:Content>
