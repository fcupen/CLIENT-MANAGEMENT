﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_claves_gestion_gantt.aspx.cs" Inherits="Clients_Management.View.cd_claves_gestion_gantt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div id="section_header">
  <div class="container">
    <h2><span>CLAVES GESTION GANTT</span></h2>
  </div>
</div>
<div id="Div1"></div>
     
<div id="about_section_1">
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p>Hola NOMBRA ADMINISTRATOR 
     </div>
     <div class="col-md-12">
       <h3 align="center">&nbsp;</h3>
	
       <div class="well">
         <h4>CLAVE GESTION GANTT.</h4>
				 <div class="row">
 
					<div class="col-xs-8">
					
					
					<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="txtid" runat="server" Visible="false"></asp:TextBox>
                                <asp:Label ID="Label1" runat="server" Text="Ano"></asp:Label><br />
                                <asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAno_SelectedIndexChanged" >
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                </asp:DropDownList>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:Label ID="Label2" runat="server" Text="Mes"></asp:Label><br />
                                <asp:DropDownList ID="ddlMes" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAno_SelectedIndexChanged" >
                                    <asp:ListItem>Enero</asp:ListItem>
                                    <asp:ListItem>Febrero</asp:ListItem>
                                    <asp:ListItem>Marzo</asp:ListItem>
                                    <asp:ListItem>Abril</asp:ListItem>
                                    <asp:ListItem>Mayo</asp:ListItem>
                                    <asp:ListItem>Junio</asp:ListItem>
                                    <asp:ListItem>Julio</asp:ListItem>
                                    <asp:ListItem>Agosto</asp:ListItem>
                                    <asp:ListItem>Septiembre</asp:ListItem>
                                    <asp:ListItem>Octubre</asp:ListItem>
                                    <asp:ListItem>Noviembre</asp:ListItem>
                                    <asp:ListItem>Diciembre</asp:ListItem>
                                </asp:DropDownList>
							</div>
						</div>
					</div>
				</div>
			<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:Label ID="Label3" runat="server" Text="Fecha"></asp:Label><br />
                                <asp:DropDownList ID="ddlFecha" class="form-control input-sm" runat="server"  >

                                </asp:DropDownList>
							</div>
						</div>
					</div>
				</div>
					
                <div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:Label ID="Label4" runat="server" Text="Clave"></asp:Label><br />
                                <asp:DropDownList ID="ddlClave" class="form-control input-sm" runat="server" >
                                    <asp:ListItem>0</asp:ListItem>
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>P</asp:ListItem>
                                </asp:DropDownList>
							</div>
						</div>
					</div>
				</div>			
				</div>
 
				</div>	
	   		 		
			

					<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-success btn-lg" Text="Enviar" OnClick="btnSalvar_Click" />                                
							</div>
						</div>
					</div>
				</div>
  
	   
	   
	   	

	  
     </div>
	 	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">Relacion de Grupos</h4>
			<hr width="70%">
			    <asp:GridView ID="gdvDados" runat="server" CssClass="table table-striped" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged">
                <Columns>
                    <asp:CommandField DeleteText="Deletar" ShowDeleteButton="True" />
                </Columns>
            </asp:GridView>		
		</div>
	</div>
 
</div>
   </div>
  
  
<div class="row">  
  

<div class="col-lg-12" align="center">
    <asp:Button ID="btnVoltar" runat="server" class="btn btn-success btn-lg" Text="Volver Planificacion" PostBackUrl="~/View/painel_parametros.aspx" />
</div>
    <p></p>


</div>
  
  
  
  </div>
      </div>
    </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>