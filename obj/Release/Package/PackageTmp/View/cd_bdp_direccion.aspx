﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_bdp_direccion.aspx.cs" Inherits="Clients_Management.View.cd_bdp_direccion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("[id$=calendario]").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercules', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                dayNamesShort: ['Dom', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Out', 'Nov', 'Dec'],


                buttonImage: 'http://www.asksystems.com.br/webgestion/Images/calendario.png'


            });
        });
    </script>

  <div class="container">
          <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">DIRECCION DE VENTAS</a></li>
  <li><a href="#">INFORMES DE GESTION</a></li>
  <li class="active">BASES DE PROYECTOS</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <p>&nbsp;</p>        
	 </div>
	 <div class="form-group">
			<div class="rows">
				<div class="col-md-8">
					<div class="col-lg-12">
                        <h4 align="left" class="col-sm-4"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>: <asp:Label ID="lblData" runat="server" Text="Label"></asp:Label></h4><br />
                        <asp:Label ID="lblTotal" runat="server" Text="Total: 00"></asp:Label>
					</div>
				</div>
			</div>
		</div>

        <div class="form-group">
			<div class="rows">
				<div class="col-md-8">
					<div class="col-lg-12">
                        <asp:Label ID="lblTotalValor" runat="server" Text="Total Neto: 00"></asp:Label>
					</div>
				</div>
			</div>
		</div>
	 <div class="row">
    <div class="col-md-12">
		<div class="well">
            
			
            <asp:RadioButton ID="rdTodos" runat="server" Text="Todos" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdTodos_CheckedChanged" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:RadioButton ID="rdProduto" runat="server" Text="Producto" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdProduto_CheckedChanged" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="ddlFiltro" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFiltro_SelectedIndexChanged"></asp:DropDownList>
			<div class="form-group">
					<div class="rows">
						<div class="col-md-4">
							<div class="col-md-4">
                                <asp:DropDownList ID="ddlfiltros" class="form-control input-sm" runat="server">
                                <asp:ListItem>Todas</asp:ListItem>
                                <asp:ListItem>Proyecto</asp:ListItem>
                                <asp:ListItem>Licitacion</asp:ListItem>
                                <asp:ListItem>Propuesta</asp:ListItem>
                                </asp:DropDownList><br />
                                <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="Buscar" OnClick="Button1_Click"  />                                
							</div> 							
						</div>
					</div>
		</div>
        <!--div class="form-group">
            <div class="rows">
            <div class="col-md-4">
	            <div class="col-md-4">
                    
	            </div>
            </div>
            </div>
        </!--div-->
            <hr width="70%">
            <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White" OnRowDataBound="gdvDados_RowDataBound">
            </asp:GridView>	
                        <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />				
		</div>
        

	</div>
 
</div>

	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
<div class="row">  

</div>

</div>
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
