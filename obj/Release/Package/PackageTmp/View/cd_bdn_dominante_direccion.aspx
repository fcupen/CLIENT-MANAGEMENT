﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_bdn_dominante_direccion.aspx.cs" Inherits="Clients_Management.View.cd_bdn_dominante_direccion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
  

  <div class="container">
	     	    <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">DIRECCION DE VENTAS</a></li>
  <li><a href="#">INFORMES DE GESTION</a></li>
  <li class="active">BDN DOMINANTES</li>
</ol>
	 <div class="row">
              <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
         	     	<div class="form-group">
			<div class="rows">
				<div class="col-md-12">
					<div class="col-md-4">
                        <asp:Label ID="lblTotalCotizacoes" runat="server" Text="Cotizaciones: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblMediaCotizacao" runat="server" Text="Media Cotización: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblTotal" runat="server" Text="Total Cotización: 00"></asp:Label>
					</div>
                    
				</div>
			</div>
		</div>
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">BDN DOMINANTES</h4>
            <asp:RadioButton ID="rdVendedorTodos" runat="server" Text="Todos Vendedores" AutoPostBack="True" GroupName="filtroVendedor" OnCheckedChanged="rdTodosVendedores_CheckedChanged" Checked="True"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:RadioButton ID="rdVendedor" runat="server" Text="Vendedor" AutoPostBack="True" GroupName="filtroVendedor" OnCheckedChanged="rdVendedor_CheckedChanged"  /><br />
            <asp:DropDownList ID="ddlVendedor" runat="server" AutoPostBack="true" class="form-control input-sm" OnSelectedIndexChanged="ddlVendedor_SelectedIndexChanged" >
            </asp:DropDownList><br />
            <hr width="70%">
            <asp:RadioButton ID="rdTodos" runat="server" Text="Todos Product" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdTodos_CheckedChanged" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:RadioButton ID="rdProduto" runat="server" Text="Product" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdProduto_CheckedChanged" /><br />
            <asp:DropDownList ID="ddlFiltro" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFiltro_SelectedIndexChanged"></asp:DropDownList>
			<hr width="70%">
            <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White" OnRowDataBound="gdvDados_RowDataBound1">
                <Columns>
                </Columns>
            </asp:GridView>				
            <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />	
		</div>
        <div class="form-group">
			<div class="rows">
				<div class="col-md-12">
					<div class="col-md-4">
                        <asp:Label ID="lblTotalDominate" runat="server" Text="Total Dominante: 00"></asp:Label>
					</div>
                    <!--div class="col-md-4">
                        <asp:Label ID="lblMediaDominante" runat="server" Text="Média Dominante: 00"></asp:Label>
					</!--div-->
                    <!--div class="col-md-4">
                        <asp:Label ID="lblTotalAnomala" runat="server" Text="Total Anomala: 00"></asp:Label>
					</div-->
				</div>

				</div>
			</div>
	</div>
 
</div>
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
<div class="row">  

</div>

</div>
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
