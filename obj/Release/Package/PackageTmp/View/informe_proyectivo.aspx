﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="informe_proyectivo.aspx.cs" Inherits="Clients_Management.View.informe_proyectivo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container">
       <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">DIRECCION DE VENTAS</a></li>
   <li class="active">INFORMES PROYECTIVO</li>
</ol>
  </div>
</div>
<div id="Div1"></div>
          
<div id="about_section_1">
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     </div>
	 
	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">
                <asp:Label ID="lblTituloRelatorio" runat="server" Text="Relatório"></asp:Label></h4>
            <asp:Label ID="lblAno" runat="server" Text="Ano" Visible="False" ></asp:Label>
            <asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" Visible="False" >
                <asp:ListItem>2015</asp:ListItem>
                <asp:ListItem>2016</asp:ListItem>
                <asp:ListItem>2017</asp:ListItem>
                <asp:ListItem>2018</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblMes" runat="server" Text="Mês" Visible="False" ></asp:Label>
            <asp:DropDownList ID="ddlMes" class="form-control input-sm" runat="server" Visible="False" >
                <asp:ListItem>Enero</asp:ListItem>
                <asp:ListItem>Febrero</asp:ListItem>
                <asp:ListItem>Marzo</asp:ListItem>
                <asp:ListItem>Abril</asp:ListItem>
                <asp:ListItem>Mayo</asp:ListItem>
                <asp:ListItem>Junio</asp:ListItem>
                <asp:ListItem>Julio</asp:ListItem>
                <asp:ListItem>Agosto</asp:ListItem>
                <asp:ListItem>Septiembre</asp:ListItem>
                <asp:ListItem>Octubre</asp:ListItem>
                <asp:ListItem>Noviembre</asp:ListItem>
                <asp:ListItem>Diciembre</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="ddlVendedor" runat="server" AutoPostBack="true" class="form-control input-sm" OnSelectedIndexChanged="ddlVendedor_SelectedIndexChanged" >
            </asp:DropDownList>
            <asp:Button ID="Button1" Visible="False" runat="server" class="btn btn-success btn-lg" Text="Consultar" OnClick="Button1_Click"/>
			
            <asp:GridView ID="dgvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnPageIndexChanging="OnPageIndexChanging">
                <Columns>
                </Columns>
            </asp:GridView>	
            <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />	
		</div>
	</div>
 
</div>
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  

  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
