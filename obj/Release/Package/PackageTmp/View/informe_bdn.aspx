﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="informe_bdn.aspx.cs" Inherits="Clients_Management.View.informe_bdn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-group">
        <div class="rows">
            <div class="col-md-8">                            
                <div class="col-lg-6">
                    <asp:DropDownList ID="ddlVendedores" runat="server" class="form-control input-sm" AutoPostBack="True" OnSelectedIndexChanged="ddlVendedores_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">BDN</h4>
			 <asp:GridView ID="gdvDados" AllowSorting="true" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" >
            </asp:GridView>				
		</div>
	</div>
</asp:Content>
