﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="agenda_visitas.aspx.cs" Inherits="Clients_Management.View.agenda_visitas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
  
  <div class="container">
        <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">FUERZA DE VENTAS</a></li>
  <li><a href="#">OPERACION DE VENTAS</a></li>
  <li class="active">ADMINISTRACION DE PLAN DE COBERTURAS</li>
</ol>
  </div>
</div>
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
       <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>

         <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">PLANIFICACION DE VISITAS MES EN CURSO</h4>
			
            <div style="overflow:auto"> 
                <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged">
                </asp:GridView>	
            <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />	
            </div>				
		</div>
	</div>
 
</div>
     <div class="col-md-12">
      
       <div class="form-horizontal well">         
				 <div class="row">											
					<div class="col-xs-8">
					<p>
					    <h4>Rellene el formulario .</h4>
                        <p>
                        </p>
                        <div class="form-group">
                            <div class="rows">
                                <div class="col-md-8">
                                    <div class="col-lg-12">
                                        <asp:TextBox ID="calendario" runat="server" class="form-control input-lg" Enabled="false" placeholder="Fecha"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="rows">
                                <div align="" class="col-md-8">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtid" runat="server" class="form-control input-lg" visible="false"></asp:TextBox>
                                        <asp:DropDownList ID="ddlCliente" runat="server" AutoPostBack="True" class="form-control input-sm" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtContato" runat="server" class="form-control input-lg" Enabled="false" placeholder="Contacto"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                            
                <h4>Dias del Mes </h4>

                        <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld1" runat="server" class="col-md-1 control-label" Text="1:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                            <asp:TextBox ID="txtd1" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld2" runat="server" class="col-md-1 control-label" Text="2:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd2" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld3" runat="server" class="col-md-1 control-label" Text="3:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd3" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld4" runat="server" class="col-md-1 control-label" Text="4:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd4" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld5" runat="server" class="col-md-1 control-label" Text="5:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd5" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>

                <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld6" runat="server" class="col-md-1 control-label" Text="6:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd6" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld7" runat="server" class="col-md-1 control-label" Text="7:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd7" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld8" runat="server" class="col-md-1 control-label" Text="8:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd8" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld9" runat="server" class="col-md-1 control-label" Text="9:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd9" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld10" runat="server" class="col-md-1 control-label" Text="10:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd10" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                                                
                <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld11" runat="server" class="col-md-1 control-label" Text="11:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd11" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld12" runat="server" class="col-md-1 control-label" Text="12:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd12" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld13" runat="server" class="col-md-1 control-label" Text="13:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd13" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld14" runat="server" class="col-md-1 control-label" Text="14:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd14" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld15" runat="server" class="col-md-1 control-label" Text="15:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd15" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
                        
            <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld16" runat="server" class="col-md-1 control-label" Text="16:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd16" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld17" runat="server" class="col-md-1 control-label" Text="17:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd17" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld18" runat="server" class="col-md-1 control-label" Text="18:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd18" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld19" runat="server" class="col-md-1 control-label" Text="19:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd19" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld20" runat="server" class="col-md-1 control-label" Text="20:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd20" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
            <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld21" runat="server" class="col-md-1 control-label" Text="21:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd21" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld22" runat="server" class="col-md-1 control-label" Text="22:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd22" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld23" runat="server" class="col-md-1 control-label" Text="23:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd23" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld24" runat="server" class="col-md-1 control-label" Text="24:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd24" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld25" runat="server" class="col-md-1 control-label" Text="25:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd25" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
                <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld26" runat="server" class="col-md-1 control-label" Text="26:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd26" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld27" runat="server" class="col-md-1 control-label" Text="27:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd27" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld28" runat="server" class="col-md-1 control-label" Text="28:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd28" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld29" runat="server" class="col-md-1 control-label" Text="29:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd29" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld30" runat="server" class="col-md-1 control-label" Text="30:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd30" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
               <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld31" runat="server" class="col-md-1 control-label" Text="31:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd31" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>			
								
				</div>

                

 
                        <div class="form-group">
                            <div class="rows">
                                <div class="col-md-8">
                                    <div class="col-lg-6">
                                        <asp:Button ID="btnNovo" runat="server" class="btn btn-primary" OnClick="btnNovo_Click" Text="Nuevo" />
                                        <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" OnClick="btnSalvar_Click" Text="Enviar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                        </p>
                        <p>
                        </p>
                        <p>
                        </p>
                        <p>
                        </p>
                        <p>
                        </p>
                    </p>				
				</div>
				 </div>
	</div>
 
  

	 
	 

	 
	 
	 
	 
	 
	 
	 
    </div>
 
 

  
  
  </div>

      </div>
       </div>


  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
                </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
