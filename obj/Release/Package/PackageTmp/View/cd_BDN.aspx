﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_BDN.aspx.cs" 
    Inherits="Clients_Management.View.cd_BDN" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("[id$=calendario]").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercules', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                dayNamesShort: ['Dom', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Out', 'Nov', 'Dec'],
                buttonImage: 'http://www.asksystems.com.br/webgestion/Images/calendario.png'


            });
        });
    </script>

  <div class="container">
        <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">FUERZA DE VENTAS</a></li>
  <li><a href="#">OPERACION DE VENTAS</a></li>
  <li class="active">INFORME BDN</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <p>&nbsp;</p>
     <div class="col-md-12">
               <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">RELACION DE INFORMES DE BDN</h4>
            <asp:Label ID="lblTituloRelatorio" runat="server" Text="Nombre - 2015"></asp:Label>
            <asp:RadioButton ID="rdTodos" runat="server" Text="Todos" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdTodos_CheckedChanged" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:RadioButton ID="rdProduto" runat="server" Text="Producto" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdProduto_CheckedChanged" /><br />
            <asp:DropDownList ID="ddlFiltro" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFiltro_SelectedIndexChanged"></asp:DropDownList>
			<hr width="70%">
            <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White" OnRowDataBound="gdvDados_RowDataBound1">
                <Columns>
                    <asp:CommandField DeleteText="Deletar" ShowDeleteButton="True" />
                </Columns>
            </asp:GridView>				
            <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />				
		</div>
        <div class="form-group">
			<div class="rows">
				<div class="col-md-12">
					<div class="col-md-4">
                        <asp:Label ID="lblTotalDominate" runat="server" Text="Total Dominante: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblMediaDominante" runat="server" Text="Média Dominante: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblTotalAnomala" runat="server" Text="Total Anomala: 00"></asp:Label>
					</div>
				</div>

				</div>
			</div>
	</div>
 
</div>
       <div class="form-horizontal well" >
         <h4>Ingrese Cotizaction</h4>
				 <div class="row">
 
					<div class="col-xs-12">
					
					<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
							 
							<div class="col-md-3">
                                <label>Fecha:</label>
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false" ></asp:TextBox>
                                <asp:TextBox ID="calendario" class="form-control input-lg" runat="server" placeholder="Fecha" ></asp:TextBox>
							</div>

                            <div class="col-md-3">
                                <label>Cotización:</label>
                                <asp:TextBox ID="cotizacion" class="form-control input-lg" runat="server" placeholder="Cotizacion" style="text-align:right;" ></asp:TextBox>
							</div>

						</div>
					</div>
				</div>
				
                  <div class="form-group">
					<div class="rows">
						<div class="col-md-12">
							<div class="col-md-4">
                                <label>Razon Social:</label>
                                <asp:DropDownList ID="ddlClientes" class="form-control input-sm" runat="server"></asp:DropDownList>
							</div>
                            <div class="col-md-3">
                                <label>Producto:</label>
                                <asp:DropDownList ID="ddlProduto" class="form-control input-sm" runat="server"></asp:DropDownList>
							</div>
                            <div class="col-md-3">
                                <label>Contacto:</label>
                                <asp:TextBox ID="contacto" class="form-control input-lg" runat="server" placeholder="Contacto" ></asp:TextBox>
							</div>
						</div>
					</div>
				   </div>
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
							<div class="col-md-4">
                                <label>Descripción:</label>
                                <asp:TextBox ID="descripcion" class="form-control input-lg" runat="server" placeholder="Descripcion Productos" ></asp:TextBox>
							</div>
							<div class="col-md-3">
                                <label>Total Neto:</label>
                                <asp:TextBox ID="totalneto" class="form-control input-lg" runat="server" placeholder="Totalneto" ></asp:TextBox>
							</div>

                            <div class="col-md-3">
                                <label>Status:</label>
                                <asp:DropDownList ID="ddlStatus" class="form-control input-sm" runat="server">
                                    <asp:ListItem>Vigente</asp:ListItem>
                                    <asp:ListItem>Ganada</asp:ListItem>
                                    <asp:ListItem>Perdida</asp:ListItem>
                                </asp:DropDownList>
							</div>

						</div>
					</div>
				</div>
				
				

				<div class="form-group">
					<div class="rows">
					
							<div class="col-md-12" align="center">
                                <asp:Button ID="btnNovo" runat="server" class="btn btn-primary" Text="Limpiar" OnClick="btnNovo_Click" />
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Cadastrar" OnClick="btnSalvar_Click" />                                
							</div>
					
					</div>
				</div>
				</div>
 
				</div>	
	   </div>
     </div>
	 </div>
	     	<div class="form-group">
			<div class="rows">
				<div class="col-md-12">
					<div class="col-md-4">
                        <asp:Label ID="lblTotalCotizacoes" runat="server" Text="Cotizaciones: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblMediaCotizacao" runat="server" Text="Media Cotización: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblTotal" runat="server" Text="Total Cotización: 00"></asp:Label>
					</div>
                    
				</div>
			</div>
		</div>
	
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
<div class="row">  

</div>

</div>
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
