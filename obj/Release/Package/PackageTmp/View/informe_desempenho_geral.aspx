﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="informe_desempenho_geral.aspx.cs" Inherits="Clients_Management.View.informe_desempenho_geral" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
       <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">RESULTADOS E INFORMES</a></li>
   <li class="active">INFORMES OPERATIVOS DE VENDEDORES</li>
</ol>
  </div>
</div>
<div id="Div1"></div>
          
<div id="about_section_1">
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     </div>
	 
	 <div class="row">
    <div class="col-md-12">
                    <div class="well">
                        <asp:Label ID="lbllegendavendedor" runat="server" Text="Vendedor"></asp:Label><br />
                <asp:DropDownList ID="ddlVendedor" runat="server" AutoPostBack="true" class="form-control input-sm" OnSelectedIndexChanged="ddlVendedor_SelectedIndexChanged" Width="394px" >
                </asp:DropDownList>
            </div>
		<div class="well">
			<h4 class="text-center">
                <asp:Label ID="lblVendedor" runat="server" Text="Relatório"></asp:Label></h4>
            <asp:Label ID="Label2" runat="server" Text="Ano"></asp:Label><br />
            <asp:Label ID="lblAno" runat="server" Text="Ano" Visible="False" ></asp:Label>
            <asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" Visible="true" AutoPostBack="True" OnSelectedIndexChanged="ddlAno_SelectedIndexChanged" >
                <asp:ListItem>2015</asp:ListItem>
                <asp:ListItem>2016</asp:ListItem>
                <asp:ListItem>2017</asp:ListItem>
                <asp:ListItem>2018</asp:ListItem>
            </asp:DropDownList>
            <asp:RadioButton ID="rdbProduto" runat="server" AutoPostBack="True" GroupName="produto" Text="Produto" OnCheckedChanged="rdbProduto_CheckedChanged" Checked="True" /><asp:RadioButton ID="rdbTodos" runat="server" AutoPostBack="True" GroupName="produto" Text="Todos Produtos" OnCheckedChanged="rdbTodos_CheckedChanged" />
            <asp:DropDownList ID="ddlProduto" class="form-control input-sm" runat="server" Visible="true" AutoPostBack="True" OnSelectedIndexChanged="ddlproduto_SelectedIndexChanged" >
            </asp:DropDownList>
            <asp:Button ID="Button1" Visible="False" runat="server" class="btn btn-success btn-lg" Text="Consultar" OnClick="Button1_Click"/>
			
            <asp:GridView ID="dgvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDataBound="dgvDados_RowDataBound" >
                <Columns>
                </Columns>
            </asp:GridView>	
		</div>
	</div>
 
</div>
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  

  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
