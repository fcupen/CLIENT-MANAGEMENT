﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_diasuteis.aspx.cs" Inherits="Clients_Management.View.cd_diasuteis" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  
  <div class="container">
      <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">PLANIFICACION</a></li>
  <li class="active">CONTROL DIAS HABILES DEL MES</li>
</ol>
  </div>
</div>
    
  <div class="container">
    <div class="row">
     <div class="col-lg-12">     
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <div class="col-md-12">
            <div class="well">
        
				 <div class="row">
 
					<div class="col-xs-8">
					
					
				<div class="form-group">
					<div class="rows">
                       <div class="col-lg-4">
                                 <label>Selecione el Año:</label>
                                <asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAno_SelectedIndexChanged">
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                </asp:DropDownList>
							</div>
						
					</div>
				</div>
				
				
											
				</div>
 
				</div>	

	  
 	
	<div class="form-group">
     <label for="inputEmail3" class="col-sm-2 control-label">Enero</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="txtid" class="form-control" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="Enero_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Enero"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Febrero</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Febrero_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Febrero"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Marzo</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Marzo_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Marzo"></asp:TextBox>
	  </div>
	    
	</div>
		
	<div class="form-group">
     <label for="inputEmail3" class="col-sm-2 control-label">Abril</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Abril_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Abril"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Mayo</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Mayo_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Mayo"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Junio</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Junio_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Junio"></asp:TextBox>
	  </div>
	    
	</div>
	
	<div class="form-group">
     <label for="inputEmail3" class="col-sm-2 control-label">Julio</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Julio_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Julio"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Agosto</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Agosto_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Agosto"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Septiembre</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Septiembre_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Septiembre"></asp:TextBox>
	  </div>
	    
	</div>
	
	<div class="form-group">
     <label for="inputEmail3" class="col-sm-2 control-label">Octubre</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Octubre_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Octubre"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Noviembre</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Noviembre_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Noviembre"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Diciembre</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Diciembre_diashabilesdetrabajo" class="form-control" runat="server" placeholder="Diciembre"></asp:TextBox>
	  </div>
	    
	</div>

	<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Enviar" OnClick="btnSalvar_Click" />                                
							</div>
						</div>
					</div>
				</div>
  
	   
	   
	   	

	 
    
	 
   
  

         
<div class="row">  
    


</div>
  
 </div>
  
  </div>

        </div>

</asp:Content>
