﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vista_previa.aspx.cs" Inherits="Clients_Management.View.vista_previa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>INFORME DE ACCIONES DE VENTA DIARIAS</title>
    <link runat="server" href="~/css/bootstrap.css" rel="stylesheet">
</head>
<body>
    <form id="Form1" runat="server">
    <div class="container">
        <div class="row">
           <div class="col-xs-6">
  <h1>
    <a href="#">
      Logo
    </a>
  </h1>
</div>
<div class="col-xs-6 text-right">
  <h3>INFORME DE ACCIONES DE VENTA DIARIAS</h3>
  <h4>Departamento de Ventas<h4>
  </div>
<hr>
<div class="row">
        <div class="col-xs-5">
          <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4>Vendedor: <asp:Label ID="lblNomeVendedor" runat="server" Text="Label"></asp:Label></h4>
                  </div>
                  <div class="panel-body">
                    <p>
                        <asp:Label ID="lblFechaVendedor" runat="server" Text="Label"></asp:Label><br>
					  <asp:Label ID="lblCodigoVendedor" runat="server" Text="Label"></asp:Label><br>
                      <asp:Label ID="lblEmailVendedor" runat="server" Text="Label"></asp:Label><br>
                    </p>
                  </div>
                </div>
        </div>
        <!--div class="col-xs-5 col-xs-offset-2 text-right">
          <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4>Para: <asp:Label ID="lblNomeCliente" runat="server" Text="Label"></asp:Label></h4>
                  </div>
                  <div class="panel-body">
                    <p>
                      <asp:Label ID="lblContactoCliente" runat="server" Text="Label"></asp:Label><br>
                      <asp:Label ID="lblTelefonoCliente" runat="server" Text="Label"></asp:Label><br>
                    </p>
                  </div>
                </div>
        </!--div-->
      </div> <!-- / end client details section -->

       <!--table class="table table-bordered">
        <thead>
          <tr>
            <th><h5>INSTITUCION</h5></th>
            <th><h5>CONTACTO</h5></th>
            <th><h5>FORMA DE CONTACTO</h5></th>
            <th><h5>ACUERDOS ALCANZADOS</h5></th>
            <th><h5>PROXIMA ACTIVIDAD</h5></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><asp:Label ID="lblNomeInstituicao" runat="server" Text="Label"></asp:Label></td>
            <td><asp:Label ID="lblContacto" runat="server" Text="Label"></asp:Label></td>
            <td class="text-center">PLAN</td>
             <td class="text-left"><asp:Label ID="lblAcordosAlcancados" runat="server" Text="Label"></asp:Label></td>
              <td class="text-left"><asp:Label ID="lblProximaAtividade" runat="server" Text="Label"></asp:Label></td>
          </tr>
        </tbody>
      </!--table-->
                <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">RELACION DE INFORMES DIARIOS</h4>
			     <div style="overflow:auto"> 
                <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" >
                </asp:GridView>				
            </div>
		</div>
	</div>

 <div class="row">
  <div class="col-xs-5">
    <div class="panel panel-info">
  <div class="panel-heading">
    <h4>Resumen</h4>
  </div>
  <div class="panel-body">
    <p>Nº de visitas realizadas:<asp:Label ID="lblNumeroVisitasRealizadas" runat="server" Text="Label"></asp:Label></p>
    <p>Nº de contactos telefonicas: <asp:Label ID="lblNumerodeVisitasTelefonicas" runat="server" Text="Label"></asp:Label></p>
    <p>Nº de llamadas de seguimiento a negocios: <asp:Label ID="lblNumeroLamadas" runat="server" Text="Label"></asp:Label></p>
      <p>Nº de visitas Abortadas: <asp:Label ID="lblAbortadas" runat="server" Text="Label"></asp:Label></p>
  </div>
</div>
      <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />	
  </div>
  <!--div class="col-xs-7">
    <div class="span7">
  <div class="panel panel-info">
    <div class="panel-heading">
      <h4>Detalles Contacto</h4>
    </div>
    <div class="panel-body">
      <p>
          Email : <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label> <br><br>
          Mobile : <asp:Label ID="lblMobile" runat="server" Text="Label"></asp:Label> <br> <br>
      </p>
     </div>
  </div>
</div>
  </!--div-->
</div>
        </div> <!-- /row -->
    </div> <!-- /container -->
        </form>
</body>
</html>
