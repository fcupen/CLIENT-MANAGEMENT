﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_BDN_view.aspx.cs" Inherits="Clients_Management.View.cd_BDN_view" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div id="about_section_1">
  
        <div class="col-lg-12">        
      <p>Hola NOMBRE DEL VENDEDOR QUE HICE EL LOGIN 
     </div>
        
        <div class="container">
     
	 <div class="row" style="font-size:16px">
    	<div class="form-group">
			<div class="rows">
				<div class="col-md-12">
					<div class="col-md-4">
                        <asp:Label ID="lblTotalCotizacoes" runat="server" Text="Cotizaciones: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblMediaCotizacao" runat="server" Text="Media Cotización: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblTotal" runat="server" Text="Total Cotización: 00"></asp:Label>
					</div>
                    
				</div>
			</div>
		</div>
        
                 
         <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">BDN</h4>
			 <asp:GridView ID="gdvDados" AllowSorting="true" runat="server" class="table table-striped table-bordered table-condensed" OnRowDataBound="gdvDados_RowDataBound" OnSorting="gdvDados_Sorting" Font-Size="11px" BackColor="White" >
            </asp:GridView>				
		</div>
	</div>

    <div class="form-group">
			<div class="rows">
				<div class="col-md-12">
					<div class="col-md-4">
                        <asp:Label ID="lblTotalDominate" runat="server" Text="Total Dominante: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblMediaDominante" runat="server" Text="Média Dominante: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblTotalAnomala" runat="server" Text="Total Anomala: 00"></asp:Label>
					</div>
				</div>

				</div>
			</div>
		</div>
            
              
</div>
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  <p></p>
  
<div class="row">  
<div class="col-lg-12" align="center">

    <asp:Button ID="btnVoltar" runat="server" class="btn btn-success btn-lg" Text="Volver Administrar Cartera de Clientes" PostBackUrl="~/View/adm_parametros.aspx" />
<p></p>
</div>
</div>

</div>
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
