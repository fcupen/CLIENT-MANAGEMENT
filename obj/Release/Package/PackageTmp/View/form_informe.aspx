﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="form_informe.aspx.cs" Inherits="Clients_Management.View.form_informe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
      
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
  
  <div class="container">
   <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">FUERZA DE VENTAS</a></li>
  <li><a href="#">OPERACION DE VENTAS</a></li>
  <li class="active">INFORME DIARIO</li>
</ol>
  </div>
</div>
  <div class="container">
  
<div class="row">
<div class="col-md-12"> 
<p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
</div>
</div>

<div class="well">
       <div class="col-md-12">
		
			<h4 class="text-center">RELACION DE INFORMES DIARIOS</h4>
			     <div style="overflow:auto"> 
                <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged">
                </asp:GridView>	 
                      
            </div>   
           
           <br />  <br />			
		</div>



  <div class="row">
  <div class="col-md-4">
  <label>Fecha:</label>
  <asp:TextBox ID="calendario" runat="server" class="form-control input-lg" Enabled="false" placeholder="Fecha"></asp:TextBox></div>
   <br />  <br />
  </div>

  <div class="row">
  <div class="col-md-6"> 
  <label>Cliente:</label>
  <asp:TextBox ID="txtid" runat="server" class="form-control input-lg" visible="false"></asp:TextBox>
  <asp:DropDownList ID="ddlCliente" runat="server" AutoPostBack="True" class="form-control input-sm" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged">
  </asp:DropDownList>
  <br />  <br />
  </div>
 
 <div class="col-md-6">
 <label>Contato:</label>
 <asp:TextBox ID="txtContato" runat="server" class="form-control input-lg" Enabled="true" placeholder="Contacto"></asp:TextBox>
      <br />  <br />
 </div>
 </div>

  <div class="row">
  <div class="col-md-6">
  <label>Forma de Contato:</label>
  <asp:DropDownList ID="optone" runat="server" AutoPostBack="True" OnSelectedIndexChanged="optone_SelectedIndexChanged">
  <asp:ListItem> </asp:ListItem>
  <asp:ListItem>0: CONTACTO ABORTADO</asp:ListItem>
  <asp:ListItem>2: CONTACTO TELEFONICO</asp:ListItem>
  <asp:ListItem>3: CONTACTO EFECTIVO</asp:ListItem>
  </asp:DropDownList>
     <br />  <br />
  
  </div>
  <div class="col-md-6">
  <label>Acuerdos Alcanzados</label>
  <asp:TextBox ID="comment" TextMode="multiline" Columns="50" Rows="7" runat="server" class="form-control input-lg" placeholder="Acuerdos Alcanzados"></asp:TextBox>
    <br />  <br />
  </div>
  </div>
  
  <div class="row">
  <div class="col-md-12">
 <label>Cotizar:</label> 
 <asp:TextBox ID="txtComentario" TextMode="multiline" Columns="50" Rows="6" runat="server" class="form-control input-lg" placeholder="comentario"></asp:TextBox>
 <br />  <br />

  </div>
  </div>
  
   <div class="row">
   <div class="col-md-12">
   <label>Proxima Actividad:</label> 

 <asp:TextBox ID="TextBox3" TextMode="multiline" Columns="50" Rows="6" runat="server" class="form-control input-lg" placeholder="comentario"></asp:TextBox>
   <br />  <br />
        </div>
   </div>



           <div class="rows">
                                <div class="col-md-8">
                                    <div class="col-lg-6">
                                        <asp:Button ID="btnNovo" runat="server" class="btn btn-primary" OnClick="btnNovo_Click" Text="Nuevo" />
                                        <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" OnClick="btnSalvar_Click" Text="Enviar" />
                                        <asp:Button ID="btnVistaPrevia" runat="server" class="btn btn-primary" Text="Vista Previa" OnClick="btnVistaPrevia_Click" />
										 <a id="A1" runat="server" href="~/ayuda.aspx" class="btn btn-danger" role="button">AYUDA</a>
                                    </div>
                                </div>
                            </div>













 	</div>




  
      <label>Proxima Visita:</label> 
        <div class="row">
    
        <div class="col-md-2"> <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
                                                                        <asp:Label ID="Label1" runat="server" Text="Ano"></asp:Label><br />
                                                                        <asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAnoVisita_SelectedIndexChanged" >
                                                                            <asp:ListItem>2015</asp:ListItem>
                                                                            <asp:ListItem>2016</asp:ListItem>
                                                                            <asp:ListItem>2017</asp:ListItem>
                                                                        </asp:DropDownList>

        </div>
        <div class="col-md-3">Mes

            <asp:Label ID="Label2" runat="server" Text="Mes"></asp:Label><br />
                                                                            <asp:DropDownList ID="ddlMes" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAnoVisita_SelectedIndexChanged" >
                                                                                <asp:ListItem>Enero</asp:ListItem>
                                                                                <asp:ListItem>Febrero</asp:ListItem>
                                                                                <asp:ListItem>Marzo</asp:ListItem>
                                                                                <asp:ListItem>Abril</asp:ListItem>
                                                                                <asp:ListItem>Mayo</asp:ListItem>
                                                                                <asp:ListItem>Junio</asp:ListItem>
                                                                                <asp:ListItem>Julio</asp:ListItem>
                                                                                <asp:ListItem>Agosto</asp:ListItem>
                                                                                <asp:ListItem>Septiembre</asp:ListItem>
                                                                                <asp:ListItem>Octubre</asp:ListItem>
                                                                                <asp:ListItem>Noviembre</asp:ListItem>
                                                                                <asp:ListItem>Diciembre</asp:ListItem>
                                                                            </asp:DropDownList>




        </div>
        <div class="col-md-2">Fecha

            <asp:Label ID="Label3" runat="server" Text="Fecha"></asp:Label><br />
                                                                            <asp:DropDownList ID="ddlFecha" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDiaVisita_SelectedIndexChanged" >

                                                                            </asp:DropDownList>


        </div>
      <div class="col-md-2">Dia

           <asp:Label ID="Label4" runat="server" Text="Dia"></asp:Label><br />
                                      <asp:Label ID="lblDiaVisita" runat="server" Text="Dia"></asp:Label><br />


      </div>
      <div class="col-md-3">Salvar


            <asp:Button ID="btnSalvarVisita" runat="server" class="btn btn-primary"  Text="Salvar" OnClick="btnSalvarVisita_Click" />


      </div>
      </div>
  
  
  
  

  
  
  
 
  
  
  
  
   
     <div class="col-md-12">
      
       <div class="form-horizontal well">         
				 <div class="row">											
					<div class="col-md-12">
                                 <p>
                        </p>
                  

                        
                     
                
                        <br/>
                        <br/>
                                      
                <div class="form-group">
                    <div class="rows">
                        <div class="col-md-8">
                            <div class="col-lg-6">
                                <asp:Label ID="lblDia" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div> 
                        <br/>
                        <div class="form-group">
                            <div class="rows">
                                <div class="col-md-10">
                                    <div class="col-lg-8">
                                         
                                    </div>
                                </div>
                            </div>
                        </div>

                <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld1" runat="server" class="col-md-1 control-label" Text="1:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                            <asp:TextBox ID="txtd1" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld2" runat="server" class="col-md-1 control-label" Text="2:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd2" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld3" runat="server" class="col-md-1 control-label" Text="3:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd3" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld4" runat="server" class="col-md-1 control-label" Text="4:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd4" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld5" runat="server" class="col-md-1 control-label" Text="5:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd5" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>

                <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld6" runat="server" class="col-md-1 control-label" Text="6:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd6" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld7" runat="server" class="col-md-1 control-label" Text="7:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd7" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld8" runat="server" class="col-md-1 control-label" Text="8:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd8" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld9" runat="server" class="col-md-1 control-label" Text="9:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd9" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld10" runat="server" class="col-md-1 control-label" Text="10:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd10" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                                                
                <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld11" runat="server" class="col-md-1 control-label" Text="11:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd11" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld12" runat="server" class="col-md-1 control-label" Text="12:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd12" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld13" runat="server" class="col-md-1 control-label" Text="13:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd13" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld14" runat="server" class="col-md-1 control-label" Text="14:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd14" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld15" runat="server" class="col-md-1 control-label" Text="15:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd15" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
                        
            <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld16" runat="server" class="col-md-1 control-label" Text="16:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd16" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld17" runat="server" class="col-md-1 control-label" Text="17:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd17" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld18" runat="server" class="col-md-1 control-label" Text="18:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd18" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld19" runat="server" class="col-md-1 control-label" Text="19:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd19" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld20" runat="server" class="col-md-1 control-label" Text="20:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd20" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
            <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld21" runat="server" class="col-md-1 control-label" Text="21:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd21" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld22" runat="server" class="col-md-1 control-label" Text="22:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd22" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld23" runat="server" class="col-md-1 control-label" Text="23:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd23" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld24" runat="server" class="col-md-1 control-label" Text="24:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd24" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld25" runat="server" class="col-md-1 control-label" Text="25:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd25" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
                <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld26" runat="server" class="col-md-1 control-label" Text="26:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd26" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld27" runat="server" class="col-md-1 control-label" Text="27:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd27" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	  
	                  <asp:Label ID="lbld28" runat="server" class="col-md-1 control-label" Text="28:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd28" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	    
                      <asp:Label ID="lbld29" runat="server" class="col-md-1 control-label" Text="29:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd29" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
                    <asp:Label ID="lbld30" runat="server" class="col-md-1 control-label" Text="30:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd30" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>
	            </div>
                        
               <div class="form-group" style="font-size:12px">
                     <asp:Label ID="lbld31" runat="server" class="col-md-1 control-label" Text="31:" ClientIDMode="Static"></asp:Label>
  		                  <div class="col-md-1">
                                <asp:TextBox ID="txtd31" CssClass="form-control" class="form-control" runat="server" placeholder="" ClientIDMode="Static"></asp:TextBox>
	                  </div>			
								
				</div>
                    
                          

        
                       
 

				</div>
				 </div>
	</div>
 
  

	 
	 

	 
	 
	 
	 
	 
	 
	 
    </div>
 
 


  
  
  </div>

      </div>
       </div>


  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
