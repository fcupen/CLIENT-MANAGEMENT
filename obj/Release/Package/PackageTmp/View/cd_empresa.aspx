﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_empresa.aspx.cs" Inherits="Clients_Management.View.cd_empresa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("[id$=calendario]").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercules', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                dayNamesShort: ['Dom', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Out', 'Nov', 'Dec'],


                buttonImage: 'http://jqueryui.com/demos/datepicker/images/calendar.gif'


            });
        });
    </script>    
 
  <div class="container">
  <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">DESPLIEGUE GENERAL</a></li>
  <li class="active">CARTERA DE CLIENTES</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <p>&nbsp;</p>
	 	 
	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">CARTERA DE CLIENTES</h4>
            <asp:Label ID="lblVendedor" runat="server" Text="Vendedor"></asp:Label>
          
            <asp:GridView ID="gdvDados" AllowSorting="true" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White"  OnSorting="gdvDados_Sorting">
                <Columns>
                    <asp:CommandField DeleteText="Deletar" ShowDeleteButton="True" />
                </Columns>
            </asp:GridView>		
            <asp:Label ID="lblTotal" runat="server" Text="00"></asp:Label>	
		</div>
	</div>
 
</div>
	 
     <div class="col-md-12">
             <div class="form-horizontal well" >
         <h4>Seleccione el filtro .</h4>
				 <div class="row">
 
					<div class="col-xs-12">
					
                <div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-6">
                                <asp:RadioButton ID="rdTodosClientes" runat="server" Text="Todos Los Clientes" AutoPostBack="True" OnCheckedChanged="rdTodosClientes_CheckedChanged" /><br />
                                <asp:RadioButton ID="rdClientesSinVendedorAsignado" runat="server" Text="Clientes Sin Vendedor Asignado" AutoPostBack="True" OnCheckedChanged="rdClientesSinVendedorAsignado_CheckedChanged" /><br />
                                <asp:RadioButton ID="rdTodos" runat="server" Text="Todos Los Vendedores" AutoPostBack="True" OnCheckedChanged="rdTodos_CheckedChanged" /><br />                                
							</div>
						</div>
					</div>
				</div>

					<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-md-4">
                                <asp:RadioButton ID="rdNome" runat="server" Text="Vendedores por Nombre" AutoPostBack="True" OnCheckedChanged="rdNome_CheckedChanged" /><br />
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false"></asp:TextBox>
                                <asp:DropDownList ID="ddlVendedor" class="form-control input-sm" runat="server"></asp:DropDownList>                                
							</div>
						</div>
					</div>
				</div>


				<!--div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="rut" class="form-control input-lg" runat="server" placeholder="RUT" ></asp:TextBox>
                                <asp:TextBox ID="calendario" class="form-control input-lg" runat="server" placeholder="Fecha" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div-->
 
				<!--div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="razonsocial" class="form-control input-lg" runat="server" placeholder="Razon Social" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div-->
				
				<!--div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="direccion" class="form-control input-lg" runat="server" placeholder="Direccion" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div-->
				
				
				
				<!--div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-6">
                                <asp:TextBox ID="comuna" class="form-control input-lg" runat="server" placeholder="Comuna" ></asp:TextBox>
							</div>
							<div class="col-lg-6">
                                <asp:TextBox ID="ciudad" class="form-control input-lg" runat="server" placeholder="Ciudad" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div-->
				
				<!--div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-6">
                                <asp:TextBox ID="region" class="form-control input-lg" runat="server" placeholder="Region" ></asp:TextBox>
							</div>
							<div class="col-lg-6">
                                <asp:TextBox ID="sucursal" class="form-control input-lg" runat="server" placeholder="Sucursal" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div-->
				
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Buscar" OnClick="btnSalvar_Click" />                                
							</div>
						</div>
					</div>
				</div>
				</div>
 
				</div>	
		 </div>
	   </div>
     </div>
	 

	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
 <div class="row">  
  

</div>
  
  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
 
</asp:Content>
