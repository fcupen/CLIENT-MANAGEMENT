﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_clientes.aspx.cs" Inherits="Clients_Management.View.cd_clientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("[id$=calendario]").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy',
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercules', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                dayNamesShort: ['Dom', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Out', 'Nov', 'Dec'],
                buttonImage: 'http://www.asksystems.com.br/webgestion/Images/calendario.png'


            });
        });
    </script>    
 

    
  <div class="container">
  <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">ADMINISTRACION DE LA CARTERA DE CLIENTES</a></li>
  <li class="active">CREAR CLIENTES</li>
</ol>

    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <p>&nbsp;</p>
	 	 <div class="row">
  <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">CLIENTES</h4>
			            <asp:GridView ID="gdvDados" Visible="true" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White">
                <Columns>
                    <asp:CommandField DeleteText="Deletar" ShowDeleteButton="True" />
                </Columns>
            </asp:GridView>				
		</div>
	</div>
 
</div>
     <div class="col-md-12">
       <h3 align="center">CREAR CLIENTES</h3>
       <div class="form-horizontal well" >
         <h4>Complete datos para generar un cliente</h4>
				 <div class="row">
 
					<div class="col-md-12">				

				<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
							<div class="col-md-2">
                                 <label>RUT:</label>
                                    <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false" ></asp:TextBox>
                                    <asp:TextBox ID="rut" class="form-control input-lg" runat="server" placeholder="RUT" ></asp:TextBox>                                
                                    <ajaxToolkit:MaskedEditExtender ID="rut_MaskedEditExtender" runat="server" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" Mask="??,???,???-?" TargetControlID="rut">
                                     </ajaxToolkit:MaskedEditExtender>
                               </div>

                             <div class="col-md-2">
                                <label>Fecha:</label>
                                <asp:TextBox ID="calendario" class="form-control input-lg" runat="server" placeholder="Fecha" ></asp:TextBox>
							</div>
                           </div>



						</div>
					</div>
				
 
				<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
							<div class="col-md-5">
                                <label>Razon Social:</label>
                                <asp:TextBox ID="razonsocial" class="form-control input-lg" runat="server" placeholder="Razon Social" ></asp:TextBox>
							</div>
						 </div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
							<div class="col-md-4">
                                <label>Direccion:</label>
                                <asp:TextBox ID="direccion" class="form-control input-lg" runat="server" placeholder="Direccion" ></asp:TextBox>
							</div>
                            <div class="col-md-4">
                                <label>Comuna:</label>
                                <asp:TextBox ID="comuna" class="form-control input-lg" runat="server" placeholder="Comuna" ></asp:TextBox>
							</div>
							<div class="col-md-4">
                                <label>Ciudad:</label>
                                <asp:TextBox ID="ciudad" class="form-control input-lg" runat="server" placeholder="Ciudad" ></asp:TextBox>
							</div>


						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-6">
                                <label>Region:</label>
                                <asp:TextBox ID="region" class="form-control input-lg" runat="server" placeholder="Region" ></asp:TextBox>
							</div>
							<div class="col-lg-6">
                                <label>Sucursal:</label>
                                <asp:TextBox ID="sucursal" class="form-control input-lg" runat="server" placeholder="Sucursal" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
				
                <div class="form-group">
					<div class="rows">
						<div class="col-md-12">
							<div class="col-md-4">
                                <label>Contacto:</label>
                                <asp:TextBox ID="contato" class="form-control input-lg" runat="server" placeholder="Contacto" ></asp:TextBox>
							</div>
							<div class="col-md-4">
                                <label>Telefono:</label>
                                <asp:TextBox ID="Telefone" class="form-control input-lg" runat="server" placeholder="Teléfono" ></asp:TextBox>
							</div>
                            <div class="col-md-4">
                                <label>Email:</label>
                                <asp:TextBox ID="email" class="form-control input-lg" runat="server" placeholder="Mail" ></asp:TextBox>
							</div>

						</div>
					</div>
				</div>
				

				<div class="form-group">
					<div class="rows">
						<div class="col-md-12" align="center">
                                <asp:Button ID="btnNovo" runat="server" class="btn btn-primary" Text="Limpiar" OnClick="btnNovo_Click" />
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Cadastrar" OnClick="btnSalvar_Click" />                                
							</div>
				</div>
				</div>
				
          
 
				</div>	
		 </div>
	   </div>
     </div>
	 
	 

	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
 <div class="row">  
  

<p></p>
</div>
  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
