﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="informe_cobertura.aspx.cs" Inherits="Clients_Management.View.informe_cobertura" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
             <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("[id$=calendario]").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercules', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                dayNamesShort: ['Dom', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Out', 'Nov', 'Dec'],
                buttonImage: 'http://www.asksystems.com.br/webgestion/Images/calendario.png'


            });
        });
    </script>

   <div class="container">
       <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">FUERZA DE VENTAS</a></li>
  <li><a href="#">CONTROL DE COBERTURAS</a></li>
  <li class="active">COBERTURAS POR VENDEDOR</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <div class="col-md-12">
      
	  
	  </div>

     </div>
	  <p></p>
	 
	 
				 <div class="row">
    <div class="col-md-12">
		<div class="well">
              <h4 align="center">COBERTURAS POR VENDEDOR</h4>
            <h4 align="center">
                <asp:Label ID="lblData" runat="server" Text="00/00/0000"></asp:Label></h4>
            <asp:Label ID="lblPromovida" runat="server" Text="Promovida"></asp:Label><br />
            <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White">
                <Columns>
                </Columns>
            </asp:GridView>	
                        <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />					
		</div>
	</div>
 
</div>
	<div class="row">
    <div class="col-md-12">
		<div class="well">
            <p><asp:Label ID="lblTitulo" runat="server" Text=""></asp:Label></p
		    <asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" Visible="false" >
                <asp:ListItem>2015</asp:ListItem>
                <asp:ListItem>2016</asp:ListItem>
                <asp:ListItem>2017</asp:ListItem>
                <asp:ListItem>2018</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="ddlMes" class="form-control input-sm" runat="server" Visible="false"  >
                <asp:ListItem>Enero</asp:ListItem>
                <asp:ListItem>Febrero</asp:ListItem>
                <asp:ListItem>Marzo</asp:ListItem>
                <asp:ListItem>Abril</asp:ListItem>
                <asp:ListItem>Mayo</asp:ListItem>
                <asp:ListItem>Junio</asp:ListItem>
                <asp:ListItem>Julio</asp:ListItem>
                <asp:ListItem>Agosto</asp:ListItem>
                <asp:ListItem>Septiembre</asp:ListItem>
                <asp:ListItem>Octubre</asp:ListItem>
                <asp:ListItem>Noviembre</asp:ListItem>
                <asp:ListItem>Diciembre</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="SELECCIONE" OnClick="btnSalvar_Click" Visible ="false"  />                                
            </div>
        </div>
         </div> 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  

  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
 
</asp:Content>
