﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_vendedor.aspx.cs" Inherits="Clients_Management.View.cd_vendedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div id="section_header">
  <div class="container">
    <h2><span>Painel de Control -   ADMINISTRACION DE VENDAS </span></h2>
  </div>
</div>
<div id="about_section_1"></div>

<div id="Div1">
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p>Hola NOMBRA ADMINISTRATOR 
     </div>
     <p>&nbsp;</p>
     <div class="col-md-12">
       <h3>Cadastro Vendedor </h3>
         <div class="form-horizontal well">
         <h4>Preencha o formulario .</h4>
				 <div class="row">
 
					<div class="col-xs-8">
					<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-6">
                                <asp:TextBox ID="fName"  runat="server" class="form-control input-lg" placeholder="Nombre" ></asp:TextBox>
							</div>
 
							<div class="col-lg-6">
                                <asp:TextBox ID="lName"  runat="server" class="form-control input-lg" placeholder="Apellido" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
 
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="email"  runat="server" class="form-control input-lg" placeholder="Email" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
 
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="codvendedor"  runat="server" class="form-control input-lg" placeholder="Codigo Vendedor" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
 
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="password"  runat="server" class="form-control input-lg" placeholder="Novo Password" TextMode="Password" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
				  <div class="rows"></div>
				</div>
 
				<div class="form-group">
				  <div class="rows"></div>
				</div>
 
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-success btn-lg" Text="Registrar" OnClick="btnSalvar_Click" />
							</div>
						</div>
					</div>
				</div>
				</div>
 
				</div>	
	   </div>
     </div>
	 
	 
	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h2 class="text-center">Relacion de Vendedores</h2>
            <asp:GridView ID="gdvDados" runat="server" CssClass="table table-striped" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged"
                >
                <Columns>
                    <asp:CommandField EditText="Editar" SelectText="Editar" ShowSelectButton="True" DeleteText="Deletar" ShowDeleteButton="True" />
                </Columns>
            </asp:GridView>
		</div>
	</div>
 
</div>
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
      </div>
  
  
  
  
 <div class="row">  
  
<div class="col-lg-3">
    <asp:Button ID="btnVoltar" runat="server" class="btn btn-success btn-lg" Text="Voltar Administracion de Ventas" PostBackUrl="~/View/painel_administrator.aspx" />
</div>


</div>
  
  
  
  </div>
</asp:Content>
