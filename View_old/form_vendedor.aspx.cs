﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class form_vendedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
            }
        }
        private void PopularGridGeral(int limite)
        {
            cVendedor engineVendedor = new cVendedor();
            gdvDados.DataSource = engineVendedor.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _Vendedor = new mVendedor();

            _Vendedor.calendario = (calendario.Text!=string.Empty?Convert.ToDateTime(calendario.Text):Convert.ToDateTime("1900-01-01"));
            _Vendedor.codigo = codigo.Text;
            _Vendedor.nombre = nombre.Text;
            _Vendedor.appelido = appelido.Text;
            _Vendedor.area = area.Text;
            _Vendedor.seccion = seccion.Text;
            _Vendedor.meta = (meta.Text!=string.Empty?Convert.ToDecimal(meta.Text):0);
            _Vendedor.id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
            _Vendedor.id = Convert.ToInt32(engineVendedor.Inserir(_Vendedor)); ;
            }
            else
            {
             engineVendedor.Update(_Vendedor);
            }



            limpar();
            PopularGridGeral(100);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            calendario.Text = string.Empty;
            codigo.Text = string.Empty;
            nombre.Text = string.Empty;
            appelido.Text = string.Empty;
            area.Text = string.Empty;
            seccion.Text = string.Empty;
            meta.Text = string.Empty;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtid.Text = gdvDados.SelectedRow.Cells[2].Text;
            calendario.Text = gdvDados.SelectedRow.Cells[3].Text;
            codigo.Text = gdvDados.SelectedRow.Cells[4].Text;
            nombre.Text = gdvDados.SelectedRow.Cells[5].Text;
            appelido.Text = gdvDados.SelectedRow.Cells[6].Text;
            area.Text = gdvDados.SelectedRow.Cells[7].Text;
            seccion.Text = gdvDados.SelectedRow.Cells[8].Text;
            meta.Text = gdvDados.SelectedRow.Cells[9].Text;
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _Vendedor = new mVendedor();
            _Vendedor = engineVendedor.RetornarVendedor(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            engineVendedor.Deletar(_Vendedor);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }


    }
}