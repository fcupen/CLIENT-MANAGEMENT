﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_vendedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(100);
            }
        }

        private void PopularGridGeral(int limite)
        {
            cVendedor engineVendedor = new cVendedor();
            gdvDados.DataSource = engineVendedor.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            //cVendedor engineVendedor = new cVendedor();
            //mVendedor _Vendedor = new mVendedor();

            //_Vendedor.nome = fName.Text;
            //_Vendedor.id = 0;
            //_Vendedor.sobrenome = lName.Text;
            //_Vendedor.email = email.Text;
            //_Vendedor.codigo = codvendedor.Text;
            //_Vendedor.senha = password.Text;
            ////_Vendedor.id = (global.IsNumeric(txtId.Text) ? Convert.ToInt32(txtId.Text) : 0);

            ////if (txtId.Text == string.Empty)
            ////{
            //    _Vendedor.id = Convert.ToInt32(engineVendedor.Inserir(_Vendedor)); ;
            ////}
            ////else
            ////{
            //   // engineVendedor.Update(_Vendedor);
            ////}



            ////limpar();
            //PopularGridGeral(100);
            ////TabContainer1.ActiveTabIndex = 0;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            //preencher campos
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //deletar do banco
        }

    }
}