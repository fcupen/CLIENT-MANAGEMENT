﻿<%@ Page Title="WEB GESTION VENTAS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Clients_Management._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">

    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
   <div id="header"></div>

<!-- Slider -->
<div class="header-banner"> 
  <script src="js/responsiveslides.min.js"></script> 
  <script>
      $(function () {
          $("#slider").responsiveSlides({
              auto: true,
              nav: true,
              speed: 500,
              namespace: "callbacks",
              pager: true,
          });
      });
			 </script>
  <div class="container">
    <div class="slider">
      <div class="callbacks_container">
        <ul class="rslides" id="slider">
          <li> <img src="images/bnr1.jpg" alt="">
            <div class="caption">
              <h1>Informes de Ventas<span>.</span></h1>
              <p>Lorem ipsum dolor sit amet, mea id noster everti. In eos prima necessitatibus, ad duo iudico facilis voluptatum.</p>
              <a runat="server" href="~/services.aspx" class="btn">Leer mas</a> </div>
          </li>
          <li> <img src="images/bnr2.jpg" alt="">
            <div class="caption">
              <h1>Catrasto de Clientes<span>.</span></h1>
              <p>Lorem ipsum dolor sit amet, mea id noster everti. In eos prima necessitatibus, ad duo iudico facilis voluptatum.</p>
              <a runat="server" href="~/services.aspx" class="btn">Leer mas</a> </div>
          </li>
          <li> <img src="images/bnr3.jpg" alt="">
            <div class="caption">
              <h1>Informes Graficos<span>.</span></h1>
              <p>Lorem ipsum dolor sit amet, mea id noster everti. In eos prima necessitatibus, ad duo iudico facilis voluptatum.</p>
              <a runat="server" href="~/services.aspx" class="btn">Leer mas</a> </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- Welcome Section -->
<div id="section_header">
  <h2>Bienvenidos a WEB GESTÍON DE VENTAS - CONSULTOR.CL</h2>
</div>
<div id="welcome">
  <div class="container">
    <div class="col-md-6"> <img class="img-responsive" src="images/about1.jpg" align=""> </div>
    <div class="col-md-6">
      <h3 align="center">Web Gestión de Ventas </h3>
      <p>Lorem ipsum dolor sit amet, quo meis audire placerat eu, te eos porro veniam. An everti maiorum detracto mea. Eu eos dicam voluptaria, erant bonorum albucius et per, ei sapientem accommodare est. Saepe dolorum constituam ei vel. Te sit malorum ceteros repudiandae, ne tritani adipisci vis.</p>
      <p>Lorem ipsum dolor sit amet, quo meis audire placerat eu, te eos porro veniam. An everti maiorum detracto mea. Eu eos dicam voluptaria, erant bonorum albucius et per, ei sapientem accommodare est.</p>
      <a runat="server" href="~/nosotros.aspx" class="btn">Leer mas </a> </div>
  </div>
</div>

<!-- What we do Section -->
<div id="section_header" >
  <h2>Informes del sistema</h2>
</div>
<div id="main-services">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 centered"> <i class="fa fa-gears fa-3x"></i>
        <h3 align="center">Informes de Venta </h3>
        <p>Erat imperdiet dissentias ea usu, alia aliquid corrumpit ea qui. Eu vim oratio conclusionemque, vel at errem nominavi delicatissimi.</p>
        <a href="#" class="btn">Leer mas </a> </div>
      <div class="col-lg-4 centered"> <i class="fa fa-briefcase fa-3x"></i>
        <h3 align="center">Painel de Control K.P.I </h3>
        <p>Erat imperdiet dissentias ea usu, alia aliquid corrumpit ea qui. Eu vim oratio conclusionemque, vel at errem nominavi delicatissimi.</p>
        <a href="#" class="btn">Leer mas</a> </div>
      <div class="col-lg-4 centered"> <i class="fa fa-line-chart fa-3x"></i>
        <h3 align="center">Informes Gráficos</h3>
        <p>Erat imperdiet dissentias ea usu, alia aliquid corrumpit ea qui. Eu vim oratio conclusionemque, vel at errem nominavi delicatissimi.</p>
        <a href="#" class="btn">Leer mas</a> </div>
    </div>
  </div>
</div>
<!-- Our clients Section -->
<!-- Footer -->
<div id="footer"></div>
<!-- Bootstrap core JavaScript --> 
<script type="text/javascript" src="js/bootstrap.min.js"></script>





</asp:Content>
