﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Clients_Management.Contact" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
   <div id="section_header">
  <div class="container">
    <h2><span>Contactenos</span></h2>
  </div>
</div>
<div class="contact">
  <div class="container">
    <div class="col-md-6">
      <h3 align="center">Contactenos</h3>
      <p><span>Dirección:</span>Agustinas 1022 Oficina 514. Santiago, Chile</p>
      <p><span>Correo electrónico:</span> cnanco@consultor.cl</p>
      <p><span>Teléfono:</span>+56-02-5415239</p>
    </div>
    <div class="col-md-6">
      <h3 align="center">Formulario</h3>
      <form>
        <div class="form_details">
          <input type="text" class="text" value="Nombre" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Name';}">
          <input type="text" class="text" value="Email" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Email Address';}">
          <input type="text" class="text" value="Asunto" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Subject';}">
          <textarea value="Message" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Message';}">Mensaje
</textarea>
          <div class="clearfix"> </div>
          <button class="btn" type="submit">Envie Mensaje </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Footer -->
<div id="footer"></div>
<!-- Bootstrap core JavaScript --> 
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</asp:Content>