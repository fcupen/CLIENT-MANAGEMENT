﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["vendedor"] = null;
            Session["admin"] = null;
            Session["vendedor_2"] = null;
        }

        protected void btnFuerza_Click(object sender, EventArgs e)
        {
            //PostBackUrl="~/View/painel_fuerza.aspx" 
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = null;
            if (txtEmailFuerza.Text != string.Empty && txtSenhaFuerza.Text != string.Empty)
            {
                _vendedor = engineVendedor.RetornarVendedorUsuarioSenha(txtEmailFuerza.Text, txtSenhaFuerza.Text);
                if (_vendedor != null)
                {
                    Session["vendedor"] = _vendedor;
                    Session["vendedor_2"] = _vendedor;
                    Response.Redirect("~/View/painel_fuerza.aspx");
                }
                else
                {
                    Alert.Show("Usuario o clave incorretos");
                }
            }
            else
            {
                Alert.Show("Informe su Usuario e clave");
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = null;
            if (txtUsuarioDirecao.Text != string.Empty && txtSenhaDirecao.Text != string.Empty)
            {
                _vendedor = engineVendedor.RetornarVendedorUsuarioSenha(txtUsuarioDirecao.Text, txtSenhaDirecao.Text);
                if (_vendedor != null)
                {
                    Session["vendedor"] = _vendedor;
                    Response.Redirect("~/View/painel_direccion.aspx");
                }
                else
                {
                    Alert.Show("Usuario o clave incorretos");
                }
            }
            else
            {
                Alert.Show("Informe su Usuario e clave");
            }
            

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            //PostBackUrl="~/View/painel_administrator.aspx" 
            //PostBackUrl="~/View/painel_fuerza.aspx" 
            cUsuario engineUsuario = new cUsuario();
            mUsuario _usuario = null;
            Alert.Show("1");
            if (txtUsuarioAdmin.Text != string.Empty && txtSenhaAdmin.Text != string.Empty)
            {
                Alert.Show("2");
                _usuario = engineUsuario.RetornarUsuarioSenha(txtUsuarioAdmin.Text, txtSenhaAdmin.Text);
                if (_usuario != null)
                {
                    Session["admin"] = _usuario;
                    Response.Redirect("~/View/painel_administrator.aspx");
                }
                else
                {
                    Alert.Show("Usuario o clave incorretos");
                }
            }
            else
            {
                Alert.Show("Informe su Usuario e clave");
            }
        }
    }
}