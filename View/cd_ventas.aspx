﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_ventas.aspx.cs" Inherits="Clients_Management.View.cd_ventas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
 
  <div class="container">
  <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">DESPLIEGUE GENERAL</a></li>
  <li class="active">METAS DE VENTAS</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
	 	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">METAS DE VENTAS</h4>
            <asp:Label ID="lblProduto" runat="server" Text="Producto"></asp:Label>
			<div style="overflow:auto">
            <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White" style="text-align:right;" OnRowDataBound="gdvDados_RowDataBound1">
                <Columns>
                </Columns>
            </asp:GridView>	
                </div>	
		</div>
	</div>
 
</div>
     <div class="col-md-12">
       <h3 align="center">METAS DE VENTAS</h3>
	    <h4 align="center">Valores netos en CLP</h4>
         <div class="form-horizontal well" >
         <div class="row">
			<div class="col-md-12">

			    <div class="form-group">
				    <div class="rows">
                        <label>Selecionar el Filtro</label>
					    <div class="col-md-12">
						   <div class="col-md-2">
                             <asp:RadioButton ID="rdSinAssignar" runat="server" Text="SIN ASIGNAR" AutoPostBack="True" OnCheckedChanged="rdSinAssignar_CheckedChanged"  />
                            </div>
                            <div class="col-md-12">
                             <div class="col-md-3">
                             <asp:RadioButton ID="rdTodosLosGrupos" runat="server" Text="TODOS LOS GRUPOS" AutoPostBack="True" OnCheckedChanged="rdTodosLosGrupos_CheckedChanged" />
                            </div>
                                </div>
                         </div>
				    </div>
			    </div>
                
                <div class="form-group">
					<div class="rows">
						<div class="col-md-12">                            
							 <div class="col-md-4">
                                <asp:RadioButton ID="rdPorGrupo" runat="server" Text="POR GRUPO PRODUCTOS" AutoPostBack="True" OnCheckedChanged="rdPorGrupo_CheckedChanged" /><br />                                
                                <asp:DropDownList ID="ddlProduto" runat="server" class="form-control input-sm"></asp:DropDownList>
							</div>

                            <div class="col-md-3">
                             <asp:RadioButton ID="rdPorGrupoVendedores" runat="server" Text="POR GRUPO VENDEDORES" AutoPostBack="True" OnCheckedChanged="rdPorGrupoVendedores_CheckedChanged" /><br />                                
                                <asp:DropDownList ID="ddlGrupoVendedores" runat="server" class="form-control input-sm"></asp:DropDownList> 
                             </div>
                            <div class="col-md-3">
                             <asp:RadioButton ID="rdPorVendedor" runat="server" Text="POR VENDEDOR" AutoPostBack="True" OnCheckedChanged="rdPorVendedor_CheckedChanged" /><br />                                
                                <asp:DropDownList ID="ddlVendedores" runat="server" class="form-control input-sm"></asp:DropDownList>
                            </div>
                               
							</div>
						</div>
					</div>
				</div>
             <div class="col-md-12" align="center">
                        <asp:Button ID="btnNovo" runat="server" class="btn btn-primary" Text="Limpiar" OnClick="btnNovo_Click1"/><br />                                
                        <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Consultar" OnClick="btnSalvar_Click" />                                
				    </div>
            </div>	
        </div>
         <p></p>
	   	

	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
 <div class="row">  
  
<div class="col-lg-12" align="center">
    
    <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="Asignacion de Metas" PostBackUrl="~/View/cd_metas.aspx" />
  
    <p></p>
</div>


</div>
  
  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
 
 
</asp:Content>
