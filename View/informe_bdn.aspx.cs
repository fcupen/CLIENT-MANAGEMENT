﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class informe_bdn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
            if (!Page.IsPostBack)
            {
                PopularVendedores();
                PopularGrid();
            }
        }
        private void PopularVendedores()
        {
            cVendedor engineVendedor = new cVendedor();
            ddlVendedores.DataSource = engineVendedor.PopularGrid(0).Tables[0];
            ddlVendedores.DataValueField = "Id";
            ddlVendedores.DataTextField = "NOMBRE";
            ddlVendedores.DataBind();
        }
        private void PopularGrid()
        {
            cBDN engineBDN = new cBDN();
            DataSet ds = new DataSet();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedores.SelectedValue.ToString());
            ds = engineBDN.PopularGrid_2PorVendedor(_vendedor.id);
            double media = 0;
            double total = 0;
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["ESTADO"].ToString() == "Vigente" && ds.Tables[0].Rows[i]["Expirado"].ToString() == "N")
                    {
                        totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["TOTAL NETO"].ToString());
                        total++;
                    }
                }                
            }

            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add("Cod_Vendedor");
            dtNovo.Columns.Add("Vendedor");
            dtNovo.Columns.Add("BDN");
            dtNovo.Columns.Add("Proyectos");
            
            DataRow dr = dtNovo.NewRow();
            dr["Cod_Vendedor"] = _vendedor.id;
            dr["Vendedor"] = _vendedor.nombre;
            dr["BDN"] = totalvalor.ToString("N0"); ;
            dr["Proyectos"] = total;
            
            dtNovo.Rows.Add(dr);
            gdvDados.DataSource = dtNovo;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();
        }

        protected void ddlVendedores_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGrid();
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            Session["vendedor_codigo"]= gdvDados.SelectedRow.Cells[1].Text;
            Response.Redirect("~/View/bdn_view_direccion.aspx");            
            //Page P = HttpContext.Current.Handler as Page;
            //ScriptManager.RegisterStartupScript(P, P.GetType(), "Wind", "window.open( 'bdn_view_direccion.aspx','newWin', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=800,height=600,left = 400,top = 200');", true);

        }
    }
}