﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class agenda_visitas_vendedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();

                

            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularComboVendedores();
                PopularVendedores();
                PopularGridGeral(0);                                
                calendario.Text = DateTime.Now.ToString("dd/MM/yyyy");
                PreencherDiasDoMes();

                ddlCliente_SelectedIndexChanged(null, null);
            }
            
        }

        private void PopularComboVendedores()
        {
            cVendedor engineVendedor = new cVendedor();
            DataSet ds = engineVendedor.PopularGrid(0);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendedor.DataSource = ds.Tables[0];
                    ddlVendedor.DataValueField = "Id";
                    ddlVendedor.DataTextField = "NOMBRE";
                    ddlVendedor.DataBind();
                }
                else
                {
                    ddlVendedor.DataSource = null;
                    ddlVendedor.Items.Clear();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }


        private void PopularVendedores()
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            cClientes engineCliente = new cClientes();
            DataSet ds = engineCliente.PopularGridPorVendedorSemAgendaCompleta(_vendedor.id);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlCliente.DataSource = ds.Tables[0];
                    ddlCliente.DataValueField = "Id";
                    ddlCliente.DataTextField = "Razon Social";
                    ddlCliente.DataBind();
                }
                else
                {
                    ddlCliente.DataSource = null;
                    ddlCliente.Items.Clear();
                    //ddlCliente.DataBind();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }
        private void PopularGridGeral(int limite)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            cInformediario engineInformediario = new cInformediario();
            DataTable dt = engineInformediario.PopularGridAgenda(limite, DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(),_vendedor.id.ToString()).Tables[0];
            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add("calendario");
            dtNovo.Columns.Add("razonsocial");
            dtNovo.Columns.Add("contacto");
            dtNovo.Columns.Add("telefone");
            for (int i = 1; i <= 31; i++)
            {
                dtNovo.Columns.Add(i.ToString());
            }
            DataRow dr = dtNovo.NewRow();
            dr["calendario"] = "";// DateTime.Now;
            dr["razonsocial"] = "";
            dr["contacto"] = "";
            dr["telefone"] = "";
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dr[(i + 1).ToString()] = ListaParametroCartaGantt[0].dia;
                    }
                    else
                    {
                        dr[(i + 1).ToString()] = "F";
                    }
                }
                else
                {
                    dr[(i + 1).ToString()] = "F";
                }

            }
            dtNovo.Rows.Add(dr);
            foreach (DataRow linha in dt.Rows)
            {
                dr = dtNovo.NewRow();
                dr["calendario"] = linha["calendario"];
                dr["razonsocial"] = linha["razonsocial"];
                dr["contacto"] = linha["contacto"];
                dr["telefone"] = linha["telefone"];
                for (int i = 1; i <= 31; i++)
                {
                    dr[i.ToString()] = linha[i.ToString()];
                }

                dtNovo.Rows.Add(dr);
            }

            gdvDados.DataSource = dtNovo;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();

            int contador = 0;
            foreach (GridViewRow r in gdvDados.Rows)
            {
                if (contador > 0)
                {
                    for (int i = 4; i < 35; i++)
                    {
                        if (r.Cells[i].Text == "F")
                        {
                            r.Cells[i].BackColor = System.Drawing.Color.LightGray;
                            r.Cells[i].Text = "F";
                        }
                        //else if (r.Cells[i].Text == "1" || r.Cells[i].Text == "3")
                        //{
                        //   r.Cells[i].Text = "1";
                        //}
                        //else
                        //{
                        //r.Cells[i].Text = "";
                        //}

                    }
                    contador++;
                }
                else
                {
                    contador++;
                }
            }

        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {

            string nomecontrole = string.Empty;
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cParametrosDiversos engineParametros = new cParametrosDiversos();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());

            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;

            if (DateTime.Now.Day > engineParametros.RetornarParametrosDiversos()[0].travardias)
            {
                Alert.Show("Fora do prazo para planificar");
                return;
            }

            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            for (int i = 1; i <= 31; i++)
            {
                nomecontrole = "txtd" + i.ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                if (TB.Text.Trim() != string.Empty && TB.Text.Trim() != "F" && TB.Text.Trim() != "P" && TB.Enabled)
                {
                    Alert.Show("Somente permitido entrada com P");
                    return;
                }
                else if (TB.Text == "P")
                {
                    if (engineInformediario.RetornaVisitasAgendadas(DateTime.Now.Year.ToString(), mesescrito, _vendedor.id.ToString(), i.ToString()) >= engineParametros.RetornarParametrosDiversos()[0].maxvisitasagendadas)
                    {
                        Alert.Show("Número de visitas por dia no es permitido para o dia " + i.ToString());
                        return;
                    }
                }
            }





            _Informediario.calendario = DateTime.Now;// (calendario.Text != string.Empty ? Convert.ToDateTime(calendario.Text) : Convert.ToDateTime("1900-01-01"));
            _Informediario.cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
            _Informediario.vendedor = _vendedor;
            _Informediario.contacto = txtContato.Text;
            _Informediario.ano = DateTime.Now.Year.ToString();
            _Informediario.mes = mesescrito;
            _Informediario.d1_valor = txtd1.Text;
            _Informediario.d2_valor = txtd2.Text;
            _Informediario.d3_valor = txtd3.Text;
            _Informediario.d4_valor = txtd4.Text;
            _Informediario.d5_valor = txtd5.Text;
            _Informediario.d6_valor = txtd6.Text;
            _Informediario.d7_valor = txtd7.Text;
            _Informediario.d8_valor = txtd8.Text;
            _Informediario.d9_valor = txtd9.Text;
            _Informediario.d10_valor = txtd10.Text;
            _Informediario.d11_valor = txtd11.Text;
            _Informediario.d12_valor = txtd12.Text;
            _Informediario.d13_valor = txtd13.Text;
            _Informediario.d14_valor = txtd14.Text;
            _Informediario.d15_valor = txtd15.Text;
            _Informediario.d16_valor = txtd16.Text;
            _Informediario.d17_valor = txtd17.Text;
            _Informediario.d18_valor = txtd18.Text;
            _Informediario.d19_valor = txtd19.Text;
            _Informediario.d20_valor = txtd20.Text;
            _Informediario.d21_valor = txtd21.Text;
            _Informediario.d22_valor = txtd22.Text;
            _Informediario.d23_valor = txtd23.Text;
            _Informediario.d24_valor = txtd24.Text;
            _Informediario.d25_valor = txtd25.Text;
            _Informediario.d26_valor = txtd26.Text;
            _Informediario.d27_valor = txtd27.Text;
            _Informediario.d28_valor = txtd28.Text;
            _Informediario.d29_valor = txtd29.Text;
            _Informediario.d30_valor = txtd30.Text;
            _Informediario.d31_valor = txtd31.Text;
            _Informediario.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty || txtid.Text == "0")
            {
                _Informediario.Id = Convert.ToInt32(engineInformediario.Inserir(_Informediario)); ;
            }
            else
            {
                engineInformediario.Update(_Informediario);
            }



            limpar();
            PopularGridGeral(0);
            Alert.Show("Sucesso");
            PopularVendedores();
            for (int i = 1; i <= 31; i++)
            {
                nomecontrole = "txtd" + i.ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                if (TB.Enabled)
                    TB.Text = string.Empty;
                else
                    TB.Text = "F";
            }
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            txtContato.Text = string.Empty;
            ddlCliente.SelectedIndex = 0;

        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = engineInformediario.RetornarInformediario(gdvDados.SelectedRow.Cells[2].Text);
            txtid.Text = _Informediario.Id.ToString();
            calendario.Text = _Informediario.calendario.ToString("yyyy-MM-dd");
            txtContato.Text = _Informediario.contacto.ToString();
            ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(_Informediario.cliente.id.ToString()));


        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = new mInformediario();
            _Informediario = engineInformediario.RetornarInformediario(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            engineInformediario.Deletar(_Informediario);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            cClientes engineCliente = new cClientes();
            mClientes _cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
            txtContato.Text = _cliente.contato;
            PreencherDiasDoMes();
        }

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GerarDias();
            PreencherDiasDoMes();
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GerarDias();
            PreencherDiasDoMes();
        }

        public void PreencherDiasDoMes()
        {
            cInformediario engineInformeDiario = new cInformediario();
            mInformediario _informeDiario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            if (ddlCliente.SelectedValue.ToString() == string.Empty)
                return;
            mClientes _cliente = (ddlCliente.SelectedValue != null ? engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);

            IList<mInformediario> lstInformeDiario = engineInformeDiario.RetornarInformediario(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), _vendedor.id.ToString(), _cliente.id.ToString());
            txtid.Text = "0";
            LimparControles();



            if (lstInformeDiario != null)
            {
                if (lstInformeDiario.Count > 0)
                {
                    _informeDiario = lstInformeDiario[0];
                    txtid.Text = lstInformeDiario[0].Id.ToString();
                    txtd1.Text = (_informeDiario.d1_valor != string.Empty ? _informeDiario.d1_valor : txtd1.Text);
                    txtd2.Text = (_informeDiario.d2_valor != string.Empty ? _informeDiario.d2_valor : txtd2.Text);
                    txtd3.Text = (_informeDiario.d3_valor != string.Empty ? _informeDiario.d3_valor : txtd3.Text);
                    txtd4.Text = (_informeDiario.d4_valor != string.Empty ? _informeDiario.d4_valor : txtd4.Text);
                    txtd5.Text = (_informeDiario.d5_valor != string.Empty ? _informeDiario.d5_valor : txtd5.Text);
                    txtd6.Text = (_informeDiario.d6_valor != string.Empty ? _informeDiario.d6_valor : txtd6.Text);
                    txtd7.Text = (_informeDiario.d7_valor != string.Empty ? _informeDiario.d7_valor : txtd7.Text);
                    txtd8.Text = (_informeDiario.d8_valor != string.Empty ? _informeDiario.d8_valor : txtd8.Text);
                    txtd9.Text = (_informeDiario.d9_valor != string.Empty ? _informeDiario.d9_valor : txtd9.Text);
                    txtd10.Text = (_informeDiario.d10_valor != string.Empty ? _informeDiario.d10_valor : txtd10.Text);
                    txtd11.Text = (_informeDiario.d11_valor != string.Empty ? _informeDiario.d11_valor : txtd11.Text);
                    txtd12.Text = (_informeDiario.d12_valor != string.Empty ? _informeDiario.d12_valor : txtd12.Text);
                    txtd13.Text = (_informeDiario.d13_valor != string.Empty ? _informeDiario.d13_valor : txtd13.Text);
                    txtd14.Text = (_informeDiario.d14_valor != string.Empty ? _informeDiario.d14_valor : txtd14.Text);
                    txtd15.Text = (_informeDiario.d15_valor != string.Empty ? _informeDiario.d15_valor : txtd15.Text);
                    txtd16.Text = (_informeDiario.d16_valor != string.Empty ? _informeDiario.d16_valor : txtd16.Text);
                    txtd17.Text = (_informeDiario.d17_valor != string.Empty ? _informeDiario.d17_valor : txtd17.Text);
                    txtd18.Text = (_informeDiario.d18_valor != string.Empty ? _informeDiario.d18_valor : txtd18.Text);
                    txtd19.Text = (_informeDiario.d19_valor != string.Empty ? _informeDiario.d19_valor : txtd19.Text);
                    txtd20.Text = (_informeDiario.d20_valor != string.Empty ? _informeDiario.d20_valor : txtd20.Text);
                    txtd21.Text = (_informeDiario.d21_valor != string.Empty ? _informeDiario.d21_valor : txtd21.Text);
                    txtd22.Text = (_informeDiario.d22_valor != string.Empty ? _informeDiario.d22_valor : txtd22.Text);
                    txtd23.Text = (_informeDiario.d23_valor != string.Empty ? _informeDiario.d23_valor : txtd23.Text);
                    txtd24.Text = (_informeDiario.d24_valor != string.Empty ? _informeDiario.d24_valor : txtd24.Text);
                    txtd25.Text = (_informeDiario.d25_valor != string.Empty ? _informeDiario.d25_valor : txtd25.Text);
                    txtd26.Text = (_informeDiario.d26_valor != string.Empty ? _informeDiario.d26_valor : txtd26.Text);
                    txtd27.Text = (_informeDiario.d27_valor != string.Empty ? _informeDiario.d27_valor : txtd27.Text);
                    txtd28.Text = (_informeDiario.d28_valor != string.Empty ? _informeDiario.d28_valor : txtd28.Text);
                    txtd29.Text = (_informeDiario.d29_valor != string.Empty ? _informeDiario.d29_valor : txtd29.Text);
                    txtd30.Text = (_informeDiario.d30_valor != string.Empty ? _informeDiario.d30_valor : txtd30.Text);
                    txtd31.Text = (_informeDiario.d31_valor != string.Empty ? _informeDiario.d31_valor : txtd31.Text);

                    if (txtd1.Text.Trim() == "1" || txtd1.Text.Trim() == "2" || txtd1.Text.Trim() == "3" || txtd1.Text.Trim() == "4")
                        txtd1.Enabled = false;

                    if (txtd2.Text.Trim() == "1" || txtd2.Text.Trim() == "2" || txtd2.Text.Trim() == "3" || txtd2.Text.Trim() == "4")
                        txtd2.Enabled = false;

                    if (txtd3.Text.Trim() == "1" || txtd3.Text.Trim() == "2" || txtd3.Text.Trim() == "3" || txtd3.Text.Trim() == "4")
                        txtd3.Enabled = false;

                    if (txtd4.Text.Trim() == "1" || txtd4.Text.Trim() == "2" || txtd4.Text.Trim() == "3" || txtd4.Text.Trim() == "4")
                        txtd4.Enabled = false;

                    if (txtd5.Text.Trim() == "1" || txtd5.Text.Trim() == "2" || txtd5.Text.Trim() == "3" || txtd5.Text.Trim() == "4")
                        txtd5.Enabled = false;

                    if (txtd6.Text.Trim() == "1" || txtd6.Text.Trim() == "2" || txtd6.Text.Trim() == "3" || txtd6.Text.Trim() == "4")
                        txtd6.Enabled = false;

                    if (txtd7.Text.Trim() == "1" || txtd7.Text.Trim() == "2" || txtd7.Text.Trim() == "3" || txtd7.Text.Trim() == "4")
                        txtd7.Enabled = false;

                    if (txtd8.Text.Trim() == "1" || txtd8.Text.Trim() == "2" || txtd8.Text.Trim() == "3" || txtd8.Text.Trim() == "4")
                        txtd8.Enabled = false;

                    if (txtd9.Text.Trim() == "1" || txtd9.Text.Trim() == "2" || txtd9.Text.Trim() == "3" || txtd9.Text.Trim() == "4")
                        txtd9.Enabled = false;

                    if (txtd10.Text.Trim() == "1" || txtd10.Text.Trim() == "2" || txtd10.Text.Trim() == "3" || txtd10.Text.Trim() == "4")
                        txtd10.Enabled = false;

                    if (txtd11.Text.Trim() == "1" || txtd11.Text.Trim() == "2" || txtd11.Text.Trim() == "3" || txtd11.Text.Trim() == "4")
                        txtd11.Enabled = false;

                    if (txtd12.Text.Trim() == "1" || txtd12.Text.Trim() == "2" || txtd12.Text.Trim() == "3" || txtd12.Text.Trim() == "4")
                        txtd12.Enabled = false;

                    if (txtd13.Text.Trim() == "1" || txtd13.Text.Trim() == "2" || txtd13.Text.Trim() == "3" || txtd13.Text.Trim() == "4")
                        txtd13.Enabled = false;

                    if (txtd14.Text.Trim() == "1" || txtd14.Text.Trim() == "2" || txtd14.Text.Trim() == "3" || txtd14.Text.Trim() == "4")
                        txtd14.Enabled = false;

                    if (txtd15.Text.Trim() == "1" || txtd15.Text.Trim() == "2" || txtd15.Text.Trim() == "3" || txtd15.Text.Trim() == "4")
                        txtd15.Enabled = false;

                    if (txtd16.Text.Trim() == "1" || txtd16.Text.Trim() == "2" || txtd16.Text.Trim() == "3" || txtd16.Text.Trim() == "4")
                        txtd16.Enabled = false;

                    if (txtd17.Text.Trim() == "1" || txtd17.Text.Trim() == "2" || txtd17.Text.Trim() == "3" || txtd17.Text.Trim() == "4")
                        txtd17.Enabled = false;

                    if (txtd18.Text.Trim() == "1" || txtd18.Text.Trim() == "2" || txtd18.Text.Trim() == "3" || txtd18.Text.Trim() == "4")
                        txtd18.Enabled = false;

                    if (txtd19.Text.Trim() == "1" || txtd19.Text.Trim() == "2" || txtd19.Text.Trim() == "3" || txtd19.Text.Trim() == "4")
                        txtd19.Enabled = false;

                    if (txtd20.Text.Trim() == "1" || txtd20.Text.Trim() == "2" || txtd20.Text.Trim() == "3" || txtd20.Text.Trim() == "4")
                        txtd20.Enabled = false;

                    if (txtd21.Text.Trim() == "1" || txtd21.Text.Trim() == "2" || txtd21.Text.Trim() == "3" || txtd21.Text.Trim() == "4")
                        txtd21.Enabled = false;

                    if (txtd22.Text.Trim() == "1" || txtd22.Text.Trim() == "2" || txtd22.Text.Trim() == "3" || txtd22.Text.Trim() == "4")
                        txtd22.Enabled = false;

                    if (txtd23.Text.Trim() == "1" || txtd23.Text.Trim() == "2" || txtd23.Text.Trim() == "3" || txtd23.Text.Trim() == "4")
                        txtd23.Enabled = false;

                    if (txtd24.Text.Trim() == "1" || txtd24.Text.Trim() == "2" || txtd24.Text.Trim() == "3" || txtd24.Text.Trim() == "4")
                        txtd24.Enabled = false;

                    if (txtd25.Text.Trim() == "1" || txtd25.Text.Trim() == "2" || txtd25.Text.Trim() == "3" || txtd25.Text.Trim() == "4")
                        txtd25.Enabled = false;

                    if (txtd26.Text.Trim() == "1" || txtd26.Text.Trim() == "2" || txtd26.Text.Trim() == "3" || txtd26.Text.Trim() == "4")
                        txtd26.Enabled = false;

                    if (txtd27.Text.Trim() == "1" || txtd27.Text.Trim() == "2" || txtd27.Text.Trim() == "3" || txtd27.Text.Trim() == "4")
                        txtd27.Enabled = false;

                    if (txtd28.Text.Trim() == "1" || txtd28.Text.Trim() == "2" || txtd28.Text.Trim() == "3" || txtd28.Text.Trim() == "4")
                        txtd28.Enabled = false;

                    if (txtd29.Text.Trim() == "1" || txtd29.Text.Trim() == "2" || txtd29.Text.Trim() == "3" || txtd29.Text.Trim() == "4")
                        txtd29.Enabled = false;

                    if (txtd30.Text.Trim() == "1" || txtd30.Text.Trim() == "2" || txtd30.Text.Trim() == "3" || txtd30.Text.Trim() == "4")
                        txtd30.Enabled = false;

                    if (txtd31.Text.Trim() == "1" || txtd31.Text.Trim() == "2" || txtd31.Text.Trim() == "3" || txtd31.Text.Trim() == "4")
                        txtd31.Enabled = false;
                }
            }



        }

        public void LimparControles()
        {
            string nomecontrole = string.Empty;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            int dia = 0;
            int dialimite = 0;
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                nomecontrole = "txtd" + (i + 1).ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        TB.Text = "";// ListaParametroCartaGantt[0].dia + " - ";
                        TB.Enabled = true;
                        _paramDiv = engineParametrosDiversos.RetornarParametrosDiversos("1");
                        dia = DateTime.Now.Day;
                        dialimite = dia - _paramDiv.travardias;
                        if (dia < dialimite)
                            TB.Enabled = false;
                    }
                    else
                    {
                        TB.Text = "F";
                        TB.Enabled = false;
                    }
                }
                else
                {
                    TB.Text = "F";
                    TB.Enabled = false;
                }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Alert.Show("1");
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cParametrosDiversos engineParametros = new cParametrosDiversos();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());

            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;


            string nomecontrole = string.Empty;
            for (int i = 1; i <= 31; i++)
            {
                nomecontrole = "txtd" + i.ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                if (TB.Text.Trim() != string.Empty && TB.Text.Trim() != "F" && TB.Text.Trim() != "P")
                {
                    Alert.Show("Somente permitido entrada com P");
                    return;
                }
                else if (TB.Text == "P")
                {
                    if (engineInformediario.RetornaVisitasAgendadas(DateTime.Now.Year.ToString(), mesescrito, _vendedor.id.ToString(), i.ToString()) >= engineParametros.RetornarParametrosDiversos()[0].maxvisitasagendadas)
                    {
                        Alert.Show("Número de visitas por dia no es permitido para o dia " + i.ToString());
                        return;
                    }
                }
            }
            Alert.Show("5");
            

        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularVendedores();
            PopularGridGeral(0);
            calendario.Text = DateTime.Now.ToString("dd/MM/yyyy");
            PreencherDiasDoMes();
            
        }

        protected void ddlVendedor_PreRender(object sender, EventArgs e)
        {

        }
    }
}