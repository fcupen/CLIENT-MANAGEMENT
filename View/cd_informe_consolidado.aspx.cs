﻿using Clients_Management.Controller;
using Clients_Management.Model;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_informe_consolidado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;

            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                    
            }
        }
        private void CarregarDados()
        {

            //Preenchendo relatório
            #region Preencher relatório

            // Set Processing Mode
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            // Supply data corresponding to each report data source.

            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            rltInformeConsolidado _pesquisa = new rltInformeConsolidado(_vendedor.id.ToString(),txtDataInicial.Text,txtDataFinal.Text);

            ReportViewer1.LocalReport.EnableExternalImages = true;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(
            new ReportDataSource("DataSet1", _pesquisa.GetDados()));


            #endregion
        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            CarregarDados();
        }
    }
}