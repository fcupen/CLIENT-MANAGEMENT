﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Clients_Management.Controller;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace Clients_Management.View
{
    public partial class cd_informe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                {
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
                    lblVendedor.Text = _vendedor.nombre.ToString() + " - " + DateTime.Now.ToString("dd/MM/yyyy");
                }


                if (Request.QueryString["filtro"].ToString() != null)
                {
                    if (Request.QueryString["filtro"].ToString() == "novisitados")
                    {
                        rdNoVisitados.Checked = true;
                        NoVisitados();
                    }
                    if (Request.QueryString["filtro"].ToString() == "sinbdn")
                    {
                        rdSinBDNProyecto.Checked = true; 
                        SinBDN();
                    }
                    if (Request.QueryString["filtro"].ToString() == "cotizacaodominante")
                    {
                        rdCotizacionDominante.Checked = true;
                        CotizacaoDominante();
                    }
                }
            }
        }

        private void NoVisitados()
        {
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            cClientes enginecliente = new cClientes();
            cVendedor engineVendedor = new cVendedor();

            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            DataTable dtNovo = new DataTable("Dados");
            clsConexao banco = new clsConexao();
            string ConsultaSQL = string.Empty;

            ConsultaSQL = "select * from  informediario where mes='" + mesescrito + "' and ano='" + DateTime.Now.Year.ToString()
                            + "' and vendedor_id='" + _vendedor.id.ToString() + "' and (d" + DateTime.Now.Day.ToString() + "_valor='0'"
                            + " or d" + DateTime.Now.Day.ToString() + "_valor='0'" + " or d" + DateTime.Now.Day.ToString() + "_valor='1'"
                            + " or d" + DateTime.Now.Day.ToString() + "_valor='2'" + " or d" + DateTime.Now.Day.ToString() + "_valor='3'"
                             + " or d" + DateTime.Now.Day.ToString() + "_valor='4')";

            DataSet ds = banco.Selecionar(ConsultaSQL);

            dtNovo.Columns.Add("RAZON SOCIAL");
            dtNovo.Columns.Add("CONTACTO");
            dtNovo.Columns.Add("TELEFONO");

            DataSet dsVendedor = engineVendedor.PopularGridComAsignadosOuNo_2("sim");

            foreach (DataRow linha in dsVendedor.Tables[0].Rows)
            {
                bool visitado=false;
                foreach (DataRow linha_2 in ds.Tables[0].Rows)
                {
                    if (linha["cliente_id"].ToString() == linha_2["cliente_id"].ToString())
                        visitado = true;
                }
                if (!visitado)
                {
                    mClientes _cliente = enginecliente.RetornarClientes(linha["cliente_id"].ToString());
                    DataRow dr = dtNovo.NewRow();
                    dr["RAZON SOCIAL"] = _cliente.razonsocial;
                    dr["CONTACTO"] = _cliente.contato;
                    dr["TELEFONO"] = _cliente.telefone;
                    dtNovo.Rows.Add(dr);
                }
            }


            gdvDados.DataSource = dtNovo;
            gdvDados.DataBind();
        }
        protected void rdNoVisitados_CheckedChanged(object sender, EventArgs e)
        {
            NoVisitados();
        }

        private void SinBDN()
        {
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            cClientes enginecliente = new cClientes();

            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            DataTable dtNovo = new DataTable("Dados");
            clsConexao banco = new clsConexao();
            string ConsultaSQL = string.Empty;

            ConsultaSQL = "select * from clientes c left join bdn bdn on c.id=bdn.instituicion left join bdp bdp on c.id=bdp.instituicion where bdn.id is null and bdp.id is null ;";

            DataSet ds = banco.Selecionar(ConsultaSQL);


            dtNovo.Columns.Add("RAZON SOCIAL");
            dtNovo.Columns.Add("CONTACTO");
            dtNovo.Columns.Add("TELEFONO");

            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                //mClientes _cliente = enginecliente.RetornarClientes(linha["cliente_id"].ToString());
                DataRow dr = dtNovo.NewRow();
                dr["RAZON SOCIAL"] = linha["razonsocial"];
                dr["CONTACTO"] = linha["contato"];
                dr["TELEFONO"] = linha["telefone"];
                dtNovo.Rows.Add(dr);
            }


            gdvDados.DataSource = dtNovo;
            gdvDados.DataBind();
        }
        protected void rdSinBDNProyecto_CheckedChanged(object sender, EventArgs e)
        {
            SinBDN();
        }

        private void CotizacaoDominante()
        {
            DataTable dtNovo = new DataTable("Dados");
            cBDN engineBDN = new cBDN();
            DataSet ds = new DataSet();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];

            dtNovo.Columns.Add("RAZON SOCIAL");
            dtNovo.Columns.Add("CONTACTO");
            dtNovo.Columns.Add("TELEFONO");
            

            ds = engineBDN.PopularGrid_2PorVendedor(_vendedor.id);
            double media = 0;
            double total = 0;
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["ESTADO"].ToString() == "Vigente" && ds.Tables[0].Rows[i]["Expirado"].ToString() == "N")
                    {
                        totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["TOTAL NETO"].ToString());
                        total++;
                    }
                }
                media = totalvalor / total;
            }

            double totaldominante = 0;
            cClientes engineCliente = new cClientes();
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                if (r[9].ToString() != string.Empty)
                {
                    double valoratual =  Convert.ToDouble(r[7].ToString());

                    if (r[9].ToString() == "N")
                    {
                        if (valoratual > media)
                        {
                            //r.BackColor = System.Drawing.Color.LightGreen;
                            mBDN _bdn = engineBDN.RetornaBDN(r[0].ToString());
                            totaldominante++;
                            DataRow dr = dtNovo.NewRow();
                            dr["RAZON SOCIAL"] = r[3].ToString();
                            dr["CONTACTO"] = r[4].ToString();
                            dr["TELEFONO"] = _bdn.instituicion.telefone;
                            dtNovo.Rows.Add(dr);

                        }
                    }


                }
                else
                {
                    // r.BackColor = System.Drawing.Color.Orange;
                    // r.Cells[0].BackColor = System.Drawing.Color.Green;
                }
            }
            gdvDados.DataSource = dtNovo;
            gdvDados.DataBind();
        }
        protected void rdCotizacionDominante_CheckedChanged(object sender, EventArgs e)
        {
            CotizacaoDominante();
        }


        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gdvDados.AllowPaging = false;
                if (Request.QueryString["filtro"].ToString() != null)
                {
                    if (Request.QueryString["filtro"].ToString() == "novisitados")
                    {
                        rdNoVisitados.Checked = true;
                        NoVisitados();
                    }
                    if (Request.QueryString["filtro"].ToString() == "sinbdn")
                    {
                        rdSinBDNProyecto.Checked = true;
                        SinBDN();
                    }
                    if (Request.QueryString["filtro"].ToString() == "cotizacaodominante")
                    {
                        rdCotizacionDominante.Checked = true;
                        CotizacaoDominante();
                    }
                }

                gdvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gdvDados.HeaderRow.Cells)
                {
                    cell.BackColor = gdvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gdvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gdvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gdvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gdvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //salvando a cópia local
            string filename = "informe.pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gdvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 8f, 8f, 8f, 8f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            gdvDados.AllowPaging = true;
            gdvDados.DataBind();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                    if (j == 2)
                    {
                        e.Row.Cells[j].HorizontalAlign = HorizontalAlign.Right;
                    }
                }

            }
        }
    }
}