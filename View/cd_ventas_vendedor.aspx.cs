﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_ventas_vendedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();

                PopularGridGeral();
            }

        }

        private void PopularGridGeral()
        {

            cMetas engineMetas = new cMetas();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            gdvDados.AutoGenerateSelectButton = false;

            gdvDados.DataSource = engineMetas.PopularGridFiltros("vendedores", _vendedor.id.ToString());
           

            gdvDados.AutoGenerateColumns = true;

            gdvDados.DataBind();

            //cVentas engineVentas= new cVentas();
            //gdvDados.DataSource = engineVentas.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = true;
            //gdvDados.DataBind();
        }
    }
}