﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class bdn_view_direccion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
            }
        }

        private void PopularGridGeral(int limite)
        {

            cBDN engineBDN = new cBDN();
            DataSet ds = new DataSet();
            ds = engineBDN.PopularGrid_2PorVendedor(Convert.ToInt32(Session["vendedor_codigo"].ToString()));
            double media = 0;
            double total = 0;
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["ESTADO"].ToString() == "Vigente" && ds.Tables[0].Rows[i]["Expirado"].ToString() == "N")
                    {
                        totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["TOTAL NETO"].ToString());
                        total++;
                    }
                }
                media = totalvalor / total;
                lblTotalCotizacoes.Text = "Cotizaciones :" + total;
                lblTotal.Text = "TOTAL :" + totalvalor.ToString("N0");
                lblMediaCotizacao.Text = "Media Cotización :" + (totalvalor / total).ToString("N2");
            }
            
            gdvDados.DataSource = ds.Tables[0];
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();

            double mediadominante = 0;
            double totaldominante = 0;
            double totalanomala = 0;
            double totalanomalavalor = 0;
            double percentagemanomala = (totalvalor * 40) / 100;
            foreach (GridViewRow r in gdvDados.Rows)
            {
                if (r.Cells[8].Text != string.Empty)
                {
                    double valoratual = Convert.ToDouble(r.Cells[8].Text);

                    if (r.Cells[10].Text == "N")
                    {
                        if (valoratual > percentagemanomala)
                        {
                            r.BackColor = System.Drawing.Color.LightYellow;
                            //r.Cells[5].BackColor = System.Drawing.Color.LightBlue;
                            totalanomala++;
                            totalanomalavalor += valoratual;
                        }
                        else if (valoratual > media)
                        {
                            r.BackColor = System.Drawing.Color.LightGreen;
                            //r.Cells[5].BackColor = System.Drawing.Color.LightSeaGreen;
                            totaldominante++;
                            mediadominante += valoratual;
                        }
                    }


                }
                else
                {
                    // r.BackColor = System.Drawing.Color.Orange;
                    // r.Cells[0].BackColor = System.Drawing.Color.Green;
                }
            }


            lblTotalDominate.Text = "Total Cotización Dominante: " + totaldominante;
            lblMediaDominante.Text = "Porcentaje Cotización Dominante:" + (totaldominante > 0 ? ((totalanomalavalor / totalvalor) * 100).ToString("N2") : "0");
            lblTotalAnomala.Text = "Total Anomala:" + totalanomala;



        }

    }
}
