﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_empresa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral();
                PopularVendedores();
            }
        }

        private void PopularVendedores()
        {
            cVendedor engineVendedor = new cVendedor();
            ddlVendedor.DataSource = engineVendedor.PopularGrid(0).Tables[0];
            ddlVendedor.DataValueField = "Id";
            ddlVendedor.DataTextField = "NOMBRE";
            ddlVendedor.DataBind();
        }
        private void PopularGridGeral()
        {
            //cClientes engineClientes = new cClientes();
            //gdvDados.DataSource = engineClientes.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = false;
            //gdvDados.DataBind();
            cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            DataSet ds = null;
            lblVendedor.Text = string.Empty;
            if (rdTodosClientes.Checked)
            {
                ds = engineRelacaoVendedor.PopularGrid_2("todosClientes", string.Empty);
            }
            else if (rdClientesSinVendedorAsignado.Checked)
            {
                ds = engineRelacaoVendedor.PopularGrid_2("clientesSemVendedor", string.Empty);
            }
            else if (rdTodos.Checked)
            {
                ds = engineRelacaoVendedor.PopularGrid_2("todosvendedores", string.Empty);
            }
            else if (rdNome.Checked)
            {
                ds = engineRelacaoVendedor.PopularGrid_2("vendedorpornome", ddlVendedor.SelectedValue);
                cVendedor engineVendedor = new cVendedor();
                mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue);
                lblVendedor.Text = _vendedor.nombre.ToString();
            }
            else
            {
                ds = engineRelacaoVendedor.PopularGrid_2("todosClientes", string.Empty);
            }
            Session["gdvDadosTable"] = ds.Tables[0];
            Session["ordenamento"] = "asc";
            gdvDados.DataSource = ds.Tables[0];
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();
            
            lblTotal.Text = "Total: "+ ds.Tables[0].Rows.Count.ToString();
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            //cClientes engineClientes = new cClientes();
            //mClientes _Cliente = new mClientes();
            //cVendedor engineVendedor = new cVendedor();

            ////if(fName.SelectedValue==null)
            ////{
            ////    Alert.Show("Selecione um vendedor");
            ////    return;
            ////}

            //_Cliente.calendario = (calendario.Text != string.Empty ? Convert.ToDateTime(calendario.Text) : Convert.ToDateTime("1900-01-01"));
            ////_Cliente.vendedor = (fName.SelectedValue != null ? engineVendedor.RetornarVendedor(fName.SelectedValue.ToString()) : null);
            //_Cliente.rut = rut.Text;
            //_Cliente.razonsocial = razonsocial.Text;
            //_Cliente.direccion = direccion.Text;
            //_Cliente.comuna = comuna.Text;
            //_Cliente.ciudade = ciudad.Text;
            //_Cliente.region = region.Text;
            //_Cliente.sucursal = sucursal.Text;
            //_Cliente.id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            //if (txtid.Text == string.Empty)
            //{
            //    _Cliente.id = Convert.ToInt32(engineClientes.Inserir(_Cliente)); ;
            //}
            //else
            //{
            //    engineClientes.Update(_Cliente);
            //}



            //limpar();
            //PopularGridGeral(0);
            PopularGridGeral();
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlVendedor.SelectedIndex = 0;
            //calendario.Text = string.Empty;
            ////fName.SelectedIndex = 0;
            //rut.Text = string.Empty;
            //razonsocial.Text = string.Empty;
            //direccion.Text = string.Empty;
            //comuna.Text = string.Empty;
            //ciudad.Text = string.Empty;
            //region.Text = string.Empty;
            //sucursal.Text = string.Empty;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cClientes engineClientes = new cClientes();
            //mClientes _cliente = engineClientes.RetornarClientes(gdvDados.SelectedRow.Cells[2].Text);
            //txtid.Text = _cliente.id.ToString();
            //calendario.Text = _cliente.calendario.ToString("dd/MM/yyyy");
            ////fName.SelectedIndex = fName.Items.IndexOf(fName.Items.FindByValue(_cliente.vendedor.id.ToString()));
            //rut.Text = _cliente.rut;
            //razonsocial.Text = _cliente.razonsocial;
            //direccion.Text = _cliente.direccion;
            //comuna.Text = _cliente.comuna;
            //ciudad.Text = _cliente.ciudade;
            //region.Text = _cliente.region;
            //sucursal.Text = _cliente.sucursal;

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //cClientes engineClientes = new cClientes();
            //mClientes _clientes = new mClientes();
            //_clientes = engineClientes.RetornarClientes(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            //engineClientes.Deletar(_clientes);
            //limpar();
            //PopularGridGeral(0);
            cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            mRelacaoVendedor _relacaoVendedor = new mRelacaoVendedor();
            _relacaoVendedor = engineRelacaoVendedor.RetornarRelacaoVendedor(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            engineRelacaoVendedor.Deletar(_relacaoVendedor);
            limpar();
            PopularGridGeral();
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void rdTodosClientes_CheckedChanged(object sender, EventArgs e)
        {
            rdClientesSinVendedorAsignado.Checked = false;
            rdTodos.Checked = false;
            rdNome.Checked = false;
        }

        protected void rdClientesSinVendedorAsignado_CheckedChanged(object sender, EventArgs e)
        {
            rdTodosClientes.Checked = false;
            rdTodos.Checked = false;
            rdNome.Checked = false;
        }

        protected void rdTodos_CheckedChanged(object sender, EventArgs e)
        {
            rdClientesSinVendedorAsignado.Checked = false;
            rdTodosClientes.Checked = false;
            rdNome.Checked = false;
        }

        protected void rdNome_CheckedChanged(object sender, EventArgs e)
        {
            rdClientesSinVendedorAsignado.Checked = false;
            rdTodos.Checked = false;
            rdTodosClientes.Checked = false;
        }

       

        protected void gdvDados_Sorting(object sender, GridViewSortEventArgs e)
        {
                //DataTable dataTable = gdvDados.DataSource as DataTable;
            DataTable dataTable = (DataTable)Session["gdvDadosTable"] ;
            
                   if (dataTable != null)
                   {
                      DataView dataView = new DataView(dataTable);
                      //dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);
                      dataView.Sort = e.SortExpression + " " + Session["ordenamento"] ;
                      if (Session["ordenamento"] == "asc")
                          Session["ordenamento"] = "desc";
                      else
                          Session["ordenamento"] = "asc";
                      gdvDados.DataSource = dataView;
                      gdvDados.DataBind();
                   }
        }
        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "asc";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "desc";
                    break;
            }

            return newSortDirection;
        }
    }
}