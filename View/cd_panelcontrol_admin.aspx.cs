﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_panelcontrol_admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PopularComboVendedores();
                CarregarCalculos();
            }

            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }
        private void PopularComboVendedores()
        {
            cVendedor engineVendedor = new cVendedor();

            DataSet ds = engineVendedor.PopularGrid(0);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendedor.DataSource = ds.Tables[0];
                    ddlVendedor.DataValueField = "Id";
                    ddlVendedor.DataTextField = "NOMBRE";
                    ddlVendedor.DataBind();
                }
                else
                {
                    ddlVendedor.DataSource = null;
                    ddlVendedor.Items.Clear();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }

        private void CarregarCalculos()
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());

            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;


            double dias_uteis = 0;
            double dias_uteis_atuais = 0;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dias_uteis++;
                        if (i < (DateTime.Now.Day - 1))
                        {
                            dias_uteis_atuais++;
                        }
                    }
                }
            }
            cDiasUteis engineDiasUteis = new cDiasUteis();
            dias_uteis = Convert.ToDouble(engineDiasUteis.PopularGridPorAno(DateTime.Now.Year.ToString()).Tables[0].Rows[0][DateTime.Now.Month + 1].ToString());

            
            cProduto engineProduto = new cProduto();
            //retornando vendedores e produtos
            DataSet ds;
            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add("Representante de Ventas");//0
            dtNovo.Columns.Add("Productos");//1
            dtNovo.Columns.Add("Rendimiento (%)");//2
            dtNovo.Columns.Add("Desempeño (%)");//3
            dtNovo.Columns.Add("Cobertura (%)");//4
            dtNovo.Columns.Add("Base de Negocios (%)");//5
            dtNovo.Columns.Add("Brecha ($)");//6
            dtNovo.Columns.Add("Envío Informes (%)");//7
            DataRow dr;
            ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT * FROM asignacionmetas where vendedor="+_vendedor.id.ToString());
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                string codigoVendedor = item["vendedor"].ToString();
                string codigoProduto = item["produto"].ToString();
                //verificando se existe o vendedor
                if (banco.Selecionar("select * from vendedor where id=" + codigoVendedor).Tables[0].Rows.Count > 0)
                {

                    //calculando rendimento
                    double rendimento = CalcularRendimento(codigoVendedor, codigoProduto);
                    double desempenho = CalcularDesempenho(codigoVendedor, codigoProduto);
                    double bdn = CalcularBDN(codigoVendedor, codigoProduto);
                    double cobertura = CalcularCobertura(codigoVendedor, dias_uteis_atuais);
                    double informe = CalcularInforme(codigoVendedor, codigoProduto, dias_uteis_atuais, dias_uteis);
                    double brecha = Convert.ToDouble( RetornarBrecha(codigoVendedor, codigoProduto).ToString("N2"));
                    double brechafloor = Math.Floor(brecha);
                    dr = dtNovo.NewRow();
                    dr[0] = engineVendedor.RetornarVendedor(item["vendedor"].ToString()).nombre;
                    dr[1] = engineProduto.RetornarProduto(item["produto"].ToString()).nombre;
                    dr[2] = rendimento.ToString("N2");
                    dr[3] = desempenho.ToString("N2");
                    dr[4] = cobertura.ToString("N2");
                    dr[5] = bdn.ToString("N2"); ;
                    dr[6] = brechafloor.ToString();
                    dr[7] = informe.ToString("N2");
                    dtNovo.Rows.Add(dr);
                }

            }

            dgvDados.DataSource = dtNovo;
            dgvDados.DataBind();

            lblData.Text = DateTime.Now.Day.ToString() + " de " + mesescrito + " de " + DateTime.Now.Year.ToString();



            lblTotalDiasUteis.Text = dias_uteis.ToString();
            lblDiasUteis.Text = dias_uteis_atuais.ToString();
        }

        private double CalcularInforme(string pCodigoVendedor, string pCodigoProduto, double dias_uteis_atuais, double totais_dias_uteis)
        {
            clsConexao banco = new clsConexao();
            DataSet ds = null;
            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;


            //dias informados 
            double dias_informados = 0;
            lblvendas2.Text = string.Empty;
            for (int i = 1; i <= DateTime.Now.Day; i++)
            {
                ds = banco.Selecionar("select * from informediario where ano='" + DateTime.Now.Year.ToString()
                            + "' and mes='" + mesescrito + "' and vendedor_id=" + pCodigoVendedor
                            + " and (d" + i.ToString() + "_valor<>'' and d" + i.ToString() + "_valor<>'P'"
                            + " and d" + i.ToString() + "_valor<>'F') ");

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count >= 1)
                    {
                        dias_informados++;
                        lblvendas2.Text += i.ToString() + ";";
                    }
                }
            }

            double resultado = (((dias_informados / dias_uteis_atuais) * totais_dias_uteis) / totais_dias_uteis) * 100;

            lblvendas2.Text += "--> Dias informados:" + dias_informados;
            lbldiastrabalhados2.Text = "dias_uteis_atuais:" + dias_uteis_atuais.ToString();
            lblmeta2.Text = "total dias uteis:" + totais_dias_uteis.ToString();
            return resultado;
        }

        private double CalcularCobertura(string pCodigoVendedor, double dias_uteis_atuais)
        {
            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;


            double totalvisitas = 0;
            //select count(*) from informediario where vendedor_id=27 and (d1_valor='1' or d1_valor='3');

            clsConexao banco = new clsConexao();
            DataSet ds = null;
            for (int i = 1; i < DateTime.Now.Day; i++)
            {
                ds = banco.Selecionar("select count(*) from informediario where ano='" + DateTime.Now.Year.ToString()
                        + "' and mes='" + mesescrito + "' and vendedor_id=" + pCodigoVendedor
                        + " and (d" + i.ToString() + "_valor='1' or d" + i.ToString() + "_valor='3') ");

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0][0].ToString() != string.Empty)
                            totalvisitas += Convert.ToDouble(ds.Tables[0].Rows[0][0].ToString());
                    }
                }
            }

            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _parametro = engineParametrosDiversos.RetornarParametrosDiversos()[0];
            //dias uteis            
            double resultado = ((totalvisitas / dias_uteis_atuais) / _parametro.metaprometodiaria) * 100;


            return resultado;
        }

        private decimal RetornarBrecha(string pCodigoVendedor, string pCodigoProduto)
        {
            cBrecha engineBrecha = new cBrecha();
            List<mBrecha> _brecha = engineBrecha.RetornarBrecha(Convert.ToInt32(pCodigoVendedor), Convert.ToInt32(pCodigoProduto));
            decimal total = 0;
            if (_brecha != null)
            {
                foreach (mBrecha item in _brecha)
                {
                    total += item.valor;
                }
            }
            return total;

        }

        private double CalcularRendimento(string pCodigoVendedor, string pCodigoProduto)
        {
            //valor total de vendas
            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            DataSet ds = engineVentasDiarias.PopularGridVendedorProduto(pCodigoVendedor, pCodigoProduto,DateTime.Now.Year.ToString(),DateTime.Now.Month.ToString());
            double total = 0;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                for (int i = 1; i <= 31; i++)
                {
                    total += (item[i.ToString()] != string.Empty ? Convert.ToDouble(item[i.ToString()].ToString()) : 0);
                }

            }
            //txtvalorAcumuladoDia.Text = total.ToString();

            //total de dias trabalhados
            double dias_trabalhadoscalc = 0;

            //double d1_valor = 0;
            //double d2_valor = 0;
            //double d3_valor = 0;
            //double d4_valor = 0;
            //double d5_valor = 0;
            //double d6_valor = 0;
            //double d7_valor = 0;
            //double d8_valor = 0;
            //double d9_valor = 0;
            //double d310_valor = 0;
            //double d311_valor = 0;
            //double d312_valor = 0;
            //double d313_valor = 0;
            //double d314_valor = 0;
            //double d315_valor = 0;
            //double d316_valor = 0;
            //double d317_valor = 0;
            //double d318_valor = 0;
            //double d319_valor = 0;
            //double d20_valor = 0;
            //double d21_valor = 0;
            //double d22_valor = 0;
            //double d23_valor = 0;
            //double d24_valor = 0;
            //double d25_valor = 0;
            //double d26_valor = 0;
            //double d27_valor = 0;
            //double d28_valor = 0;
            //double d29_valor = 0;
            //double d30_valor = 0;
            //double d31_valor = 0;
            //cInformediario engineInforme = new cInformediario();
            //List<mInformediario> _ListInforme = engineInforme.RetornarInformediario(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), pCodigoVendedor);
            //if (_ListInforme != null)
            //{
            //    if (_ListInforme.Count > 0)
            //    {
            //        foreach (mInformediario item in _ListInforme)
            //        {
            //            if (item.d1_valor == "0" || item.d1_valor == "1" || item.d1_valor == "2" || item.d1_valor == "3" || item.d1_valor == "4")
            //                d1_valor = 1;
            //            if (item.d2_valor == "0" || item.d2_valor == "1" || item.d2_valor == "2" || item.d2_valor == "3" || item.d2_valor == "4")
            //                d2_valor = 1;
            //            if (item.d3_valor == "0" || item.d3_valor == "1" || item.d3_valor == "2" || item.d3_valor == "3" || item.d3_valor == "4")
            //                d3_valor = 1;
            //            if (item.d4_valor == "0" || item.d4_valor == "1" || item.d4_valor == "2" || item.d4_valor == "3" || item.d4_valor == "4")
            //                d4_valor = 1;
            //            if (item.d5_valor == "0" || item.d5_valor == "1" || item.d5_valor == "2" || item.d5_valor == "3" || item.d5_valor == "4")
            //                d5_valor = 1;
            //            if (item.d6_valor == "0" || item.d6_valor == "1" || item.d6_valor == "2" || item.d6_valor == "3" || item.d6_valor == "4")
            //                d6_valor = 1;
            //            if (item.d7_valor == "0" || item.d7_valor == "1" || item.d7_valor == "2" || item.d7_valor == "3" || item.d7_valor == "4")
            //                d7_valor = 1;
            //            if (item.d8_valor == "0" || item.d8_valor == "1" || item.d8_valor == "2" || item.d8_valor == "3" || item.d8_valor == "4")
            //                d8_valor = 1;
            //            if (item.d9_valor == "0" || item.d9_valor == "1" || item.d9_valor == "2" || item.d9_valor == "3" || item.d9_valor == "4")
            //                d9_valor = 1;
            //            if (item.d10_valor == "0" || item.d10_valor == "1" || item.d10_valor == "2" || item.d10_valor == "3" || item.d10_valor == "4")
            //                d310_valor = 1;
            //            if (item.d11_valor == "0" || item.d11_valor == "1" || item.d11_valor == "2" || item.d11_valor == "3" || item.d11_valor == "4")
            //                d311_valor = 1;
            //            if (item.d12_valor == "0" || item.d12_valor == "1" || item.d12_valor == "2" || item.d12_valor == "3" || item.d12_valor == "4")
            //                d312_valor = 1;
            //            if (item.d13_valor == "0" || item.d13_valor == "1" || item.d13_valor == "2" || item.d13_valor == "3" || item.d13_valor == "4")
            //                d313_valor = 1;
            //            if (item.d14_valor == "0" || item.d14_valor == "1" || item.d14_valor == "2" || item.d14_valor == "3" || item.d14_valor == "4")
            //                d314_valor = 1;
            //            if (item.d15_valor == "0" || item.d15_valor == "1" || item.d15_valor == "2" || item.d15_valor == "3" || item.d15_valor == "4")
            //                d315_valor = 1;
            //            if (item.d16_valor == "0" || item.d16_valor == "1" || item.d16_valor == "2" || item.d16_valor == "3" || item.d16_valor == "4")
            //                d316_valor = 1;
            //            if (item.d17_valor == "0" || item.d17_valor == "1" || item.d17_valor == "2" || item.d17_valor == "3" || item.d17_valor == "4")
            //                d317_valor = 1;
            //            if (item.d18_valor == "0" || item.d18_valor == "1" || item.d18_valor == "2" || item.d18_valor == "3" || item.d18_valor == "4")
            //                d318_valor = 1;
            //            if (item.d19_valor == "0" || item.d19_valor == "1" || item.d19_valor == "2" || item.d19_valor == "3" || item.d19_valor == "4")
            //                d319_valor = 1;
            //            if (item.d20_valor == "0" || item.d20_valor == "1" || item.d20_valor == "2" || item.d20_valor == "3" || item.d20_valor == "4")
            //                d20_valor = 1;
            //            if (item.d21_valor == "0" || item.d21_valor == "1" || item.d21_valor == "2" || item.d21_valor == "3" || item.d21_valor == "4")
            //                d21_valor = 1;
            //            if (item.d22_valor == "0" || item.d22_valor == "1" || item.d22_valor == "2" || item.d22_valor == "3" || item.d22_valor == "4")
            //                d22_valor = 1;
            //            if (item.d23_valor == "0" || item.d23_valor == "1" || item.d23_valor == "2" || item.d23_valor == "3" || item.d23_valor == "4")
            //                d23_valor = 1;
            //            if (item.d24_valor == "0" || item.d24_valor == "1" || item.d24_valor == "2" || item.d24_valor == "3" || item.d24_valor == "4")
            //                d24_valor = 1;
            //            if (item.d25_valor == "0" || item.d25_valor == "1" || item.d25_valor == "2" || item.d25_valor == "3" || item.d25_valor == "4")
            //                d25_valor = 1;
            //            if (item.d26_valor == "0" || item.d26_valor == "1" || item.d26_valor == "2" || item.d26_valor == "3" || item.d26_valor == "4")
            //                d26_valor = 1;
            //            if (item.d27_valor == "0" || item.d27_valor == "1" || item.d27_valor == "2" || item.d27_valor == "3" || item.d27_valor == "4")
            //                d27_valor = 1;
            //            if (item.d28_valor == "0" || item.d28_valor == "1" || item.d28_valor == "2" || item.d28_valor == "3" || item.d28_valor == "4")
            //                d28_valor = 1;
            //            if (item.d29_valor == "0" || item.d29_valor == "1" || item.d29_valor == "2" || item.d29_valor == "3" || item.d29_valor == "4")
            //                d29_valor = 1;
            //            if (item.d30_valor == "0" || item.d30_valor == "1" || item.d30_valor == "2" || item.d30_valor == "3" || item.d30_valor == "4")
            //                d30_valor = 1;
            //            if (item.d31_valor == "0" || item.d31_valor == "1" || item.d31_valor == "2" || item.d31_valor == "3" || item.d31_valor == "4")
            //                d31_valor = 1;
            //        }
            //    }
            //}


            //dias_trabalhadoscalc =
            //    d1_valor +
            //    d2_valor +
            //    d3_valor +
            //    d4_valor +
            //    d5_valor +
            //    d6_valor +
            //    d7_valor +
            //    d8_valor +
            //    d9_valor +
            //    d310_valor +
            //    d311_valor +
            //    d312_valor +
            //    d313_valor +
            //    d314_valor +
            //    d315_valor +
            //    d316_valor +
            //    d317_valor +
            //    d318_valor +
            //    d319_valor +
            //    d20_valor +
            //    d21_valor +
            //    d22_valor +
            //    d23_valor +
            //    d24_valor +
            //    d25_valor +
            //    d26_valor +
            //    d27_valor +
            //    d28_valor +
            //    d29_valor +
            //    d30_valor +
            //    d31_valor;

            //txtdias_trabalhados.Text = dias_trabalhadoscalc.ToString();

            //dias uteis
            //double dias_uteis = 0;
            //cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            //List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            //cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            //mParametrosDiversos _paramDiv = new mParametrosDiversos();
            //for (int i = 0; i < 31; i++)
            //{
            //    ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
            //    if (ListaParametroCartaGantt != null)
            //    {
            //        if (ListaParametroCartaGantt.Count > 0)
            //        {
            //            dias_uteis++;
            //        }
            //    }
            //}

            //txtdiasUteisMes.Text = dias_uteis.ToString();
            double dias_uteis = 0;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dias_uteis++;
                        if (i < (DateTime.Now.Day - 1))
                        {
                            dias_trabalhadoscalc++;
                        }
                    }
                }
            }
            cDiasUteis engineDiasUteis = new cDiasUteis();
            dias_uteis = Convert.ToDouble(engineDiasUteis.PopularGridPorAno(DateTime.Now.Year.ToString()).Tables[0].Rows[0][DateTime.Now.Month + 1].ToString());

            //meta    
            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT sum(" + mesescrito + ") as total FROM asignacionmetas a where a.vendedor=" + pCodigoVendedor + " and produto=" + pCodigoProduto);
            double metacalc = Convert.ToDouble(ds.Tables[0].Rows[0]["total"]);
            //txtmeta.Text = metacalc.ToString();

            double valorAcumuladoDia = Convert.ToDouble(total);
            double dias_trabalhados = Convert.ToDouble(dias_trabalhadoscalc);
            double diasUteisMes = Convert.ToDouble(dias_uteis);
            double meta = Convert.ToDouble(metacalc);



            return (((valorAcumuladoDia / dias_trabalhados) * diasUteisMes) / meta) * 100;
        }

        private double CalcularDesempenho(string pCodigoVendedor, string pCodigoProduto)
        {
            //valor total de vendas
            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            DataSet ds = engineVentasDiarias.PopularGridVendedorProdutoAno(pCodigoVendedor, pCodigoProduto);
            double total = 0;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                for (int i = 1; i <= 31; i++)
                {
                    total += (item[i.ToString()].ToString().Trim() != string.Empty ? Convert.ToDouble(item[i.ToString()].ToString()) : 0);
                }

            }
            //txtvendatMes.Text = total.ToString();

            //mes
            //txtmes.Text = DateTime.Now.Month.ToString();

            //meta
            ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT (sum(enero)+sum(febrero)+ sum(marzo)+sum(abril)+sum(mayo)+sum(junio)+sum(julio)+sum(agosto)+sum(septiembre)+sum(octubre)+sum(noviembre)+sum(diciembre))  as total FROM asignacionmetas a where a.vendedor=" + pCodigoVendedor + " and a.produto=" + pCodigoProduto);
            double metacalc = Convert.ToDouble(ds.Tables[0].Rows[0]["total"]);
            //txtmeta2.Text = metacalc.ToString();

            double venta_mes = Convert.ToDouble(total);
            double mescalc = Convert.ToDouble(DateTime.Now.Month.ToString());
            double meta = Convert.ToDouble(metacalc);
            //lbldesempenho.Text = "Desempenho" + ((((venta_mes / mescalc) * 12) / meta) * 100).ToString();

            //(((4250000/5)*21)/50000000)*100


            return (((venta_mes / mescalc) * 12) / meta) * 100;
        }

        private double CalcularBDN(string pCodigoVendedor, string pCodigoProduto)
        {


            //meta3
            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            DataSet ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT sum(" + mesescrito + ") as total FROM asignacionmetas a where a.vendedor=" + pCodigoVendedor + " and a.produto=" + pCodigoProduto);
            double metacalc = Convert.ToDouble(ds.Tables[0].Rows[0]["total"]);
            //txtmeta3.Text = metacalc.ToString();

            //tkc
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _parametro = engineParametrosDiversos.RetornarParametrosDiversos()[0];
            //txttkc.Text = _parametro.tkc.ToString();

            //bdn 
            cBDN engineBDN = new cBDN();
            ds = new DataSet();

            ds = engineBDN.PopularGrid_2PorVendedorProdutoCalculo(Convert.ToInt32(pCodigoVendedor), Convert.ToInt32(pCodigoProduto));
            double media = 0;
            double total = 0;
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["ESTADO"].ToString() == "Vigente" && ds.Tables[0].Rows[i]["Expirado"].ToString() == "N")
                    {
                        totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["TOTAL NETO"].ToString());
                        total++;
                    }
                }
                //txtbdn.Text = totalvalor.ToString("N0");
            }


            double bdn = Convert.ToDouble(totalvalor);
            double tkc = Convert.ToDouble(_parametro.tkc.ToString());
            double meta = Convert.ToDouble(metacalc);
            //lblbdn.Text = "BDN" + (((bdn * (tkc / 100)) / meta) * 100).ToString();
            return ((bdn * (tkc / 100)) / meta) * 100;
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarCalculos();
        }
    }
}