﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_clientes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }

        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                
            }
        }


        private void PopularGridGeral(int limite)
        {
            cClientes engineClientes = new cClientes();
            gdvDados.DataSource = engineClientes.PopularGrid(limite).Tables[0];
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.Visible = true;
            gdvDados.DataBind();
            //cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            //DataSet ds = engineRelacaoVendedor.PopularGrid_2(codigoVendedor);
            //gdvDados.DataSource = ds.Tables[0];
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = false;
            //gdvDados.DataBind();
            //lblTotal.Text = "Total: " + ds.Tables[0].Rows.Count.ToString();
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cClientes engineClientes = new cClientes();
            mClientes _Cliente = new mClientes();
            //cVendedor engineVendedor = new cVendedor();

            ////if(fName.SelectedValue==null)
            ////{
            ////    Alert.Show("Selecione um vendedor");
            ////    return;
            ////}

            _Cliente.calendario = (calendario.Text != string.Empty ? Convert.ToDateTime(calendario.Text) : Convert.ToDateTime("1900-01-01"));
            //_Cliente.vendedor = (fName.SelectedValue != null ? engineVendedor.RetornarVendedor(fName.SelectedValue.ToString()) : null);
            _Cliente.rut = rut.Text;
            _Cliente.razonsocial = razonsocial.Text;
            _Cliente.direccion = direccion.Text;
            _Cliente.comuna = comuna.Text;
            _Cliente.ciudade = ciudad.Text;
            _Cliente.region = region.Text;
            _Cliente.sucursal = sucursal.Text;
            _Cliente.contato = contato.Text;
            _Cliente.telefone = Telefone.Text;
            _Cliente.email = email.Text;
            _Cliente.id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                _Cliente.id = Convert.ToInt32(engineClientes.Inserir(_Cliente)); ;
            }
            else
            {
                engineClientes.Update(_Cliente);
            }



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            calendario.Text = string.Empty;
            //fName.SelectedIndex = 0;
            rut.Text = string.Empty;
            razonsocial.Text = string.Empty;
            direccion.Text = string.Empty;
            comuna.Text = string.Empty;
            ciudad.Text = string.Empty;
            region.Text = string.Empty;
            sucursal.Text = string.Empty;
            contato.Text = string.Empty;
            Telefone.Text = string.Empty;
            email.Text = string.Empty;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cClientes engineClientes = new cClientes();
            mClientes _cliente = engineClientes.RetornarClientes(gdvDados.SelectedRow.Cells[2].Text);
            txtid.Text = _cliente.id.ToString();
            calendario.Text = _cliente.calendario.ToString("MM/dd/yyyy");
            //fName.SelectedIndex = fName.Items.IndexOf(fName.Items.FindByValue(_cliente.vendedor.id.ToString()));
            rut.Text = _cliente.rut;
            razonsocial.Text = _cliente.razonsocial;
            direccion.Text = _cliente.direccion;
            comuna.Text = _cliente.comuna;
            ciudad.Text = _cliente.ciudade;
            region.Text = _cliente.region;
            sucursal.Text = _cliente.sucursal;
            contato.Text = _cliente.contato;
            Telefone.Text = _cliente.telefone;
            email.Text = _cliente.email;

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cClientes engineClientes = new cClientes();
            mClientes _clientes = new mClientes();
            _clientes = engineClientes.RetornarClientes(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            engineClientes.Deletar(_clientes);
            limpar();
            PopularGridGeral(0);
            //cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            //mRelacaoVendedor _relacaoVendedor = new mRelacaoVendedor();
            //_relacaoVendedor = engineRelacaoVendedor.RetornarRelacaoVendedor(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            //engineRelacaoVendedor.Deletar(_relacaoVendedor);
            //limpar();
            //PopularGridGeral(ddlVendedor.SelectedValue);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }
    }
}