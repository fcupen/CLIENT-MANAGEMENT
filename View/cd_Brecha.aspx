﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_Brecha.aspx.cs" Inherits="Clients_Management.View.cd_Brecha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

  <div class="container">
         <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">FUERZA DE VENTAS</a></li>
  <li><a href="#">CONSULTAS E INFORMES</a></li>
   <li class="active">BRECHA</li>
</ol>
</div>

  <div class="container">
        <div class="row">
     <div class="col-lg-12">        
      <asp:Label ID="lblNombre" runat="server" Text="Vendedor"></asp:Label>
     </div>
     <p>&nbsp;</p>
     <div class="col-md-12">
       <div class="form-horizontal well" >
         <h4>Rellene el formulario .</h4>

           	 
	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">BRECHA</h4>
            <asp:Label ID="lblTituloRelatorio" runat="server" Text="Label"></asp:Label>
			<hr width="70%">
			    <asp:GridView ID="gdvDados" runat="server" CssClass="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" OnRowDataBound="gdvDados_RowDataBound" style="text-align:right;">
                <Columns>
                    <asp:CommandField DeleteText="Deletar" ShowDeleteButton="false" />
                </Columns>
            </asp:GridView>	
            <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />						
		</div>
	</div>
 
</div>
				 <div class="row">
 
					<div class="col-xs-12">
					
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:Label ID="Label2" runat="server" Text="Vendedor"></asp:Label><br/>
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false" ></asp:TextBox>
                                <asp:DropDownList ID="ddlVendedor" class="form-control input-sm" runat="server" ></asp:DropDownList> 
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">              
                                <asp:Label ID="Label3" runat="server" Text="Producto"></asp:Label><br/>     
                                <br /><asp:DropDownList ID="ddlProduto" class="form-control input-sm" runat="server" ></asp:DropDownList> 
							</div>
						</div>
					</div>
				</div>

                <div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">             
                                <asp:Label ID="Label1" runat="server" Text="Cliente"></asp:Label><br/>      
                                <asp:DropDownList ID="ddlCliente" class="form-control input-sm" runat="server" AutoPostBack="false" ></asp:DropDownList>
							</div>
						</div>
					</div>
				</div>
                         
                        					
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="txtValor" class="form-control" runat="server" placeholder="valor" ClientIDMode="Static"></asp:TextBox>
							</div>
						</div>
					</div>
				</div>				                               
                      				<div class="form-group">
					<div class="rows">
					
							<div class="col-md-12" align="center">
                                <asp:Button ID="btnNovo" runat="server" class="btn btn-primary" Text="Limpiar" OnClick="btnNovo_Click" />
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Cadastrar" OnClick="btnSalvar_Click" />                                
							</div>
					
					</div>
				</div>  
                        
				</div>
 
				</div>	
		 </div>
	   </div>
     </div>
	 

	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
<div class="row">  

</div>
  
  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
