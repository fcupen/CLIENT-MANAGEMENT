﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_informe.aspx.cs" Inherits="Clients_Management.View.cd_informe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

  <div class="container">
       <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">FUERZA DE VENTAS</a></li>
  <li><a href="#">CONTROL DE COBERTURAS</a></li>
            <li><a href="#">INFORMES PERIODICOS</a></li>
   <li class="active">ANALISIS CRITICO POR CLIENTE</li>
</ol>
   
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     
     <div class="col-md-12">
           <div class="form-horizontal well" >
          <div class="row">
 
					<div class="col-xs-12">
					
							
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							
                            <div class="col-lg-8">
                                <label>Seleccione el filtro:</label><br/>
                                <asp:RadioButton ID="rdNoVisitados" runat="server" GroupName="vendedor" Text="No Visitados" AutoPostBack="True" OnCheckedChanged="rdNoVisitados_CheckedChanged" />
                                &nbsp;&nbsp; <asp:RadioButton ID="rdSinBDNProyecto" runat="server" GroupName="vendedor" Text="Sin BDN ni Proyectos" AutoPostBack="True" OnCheckedChanged="rdSinBDNProyecto_CheckedChanged" />
                                &nbsp;&nbsp; <asp:RadioButton ID="rdCotizacionDominante" runat="server" GroupName="vendedor" Text="Cotizaciones Dominantes" AutoPostBack="True" OnCheckedChanged="rdCotizacionDominante_CheckedChanged" Visible="false" />
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false"></asp:TextBox>
							</div>
						</div>
					</div>
				</div>                       
 

				</div>
 
				</div>	
	   </div>
     </div>
	 
	 	 <div class="row">
    <div class="col-md-12">
		<div class="well">
		    <asp:Label ID="lblVendedor" runat="server" Text="Vendedor"></asp:Label>
			<hr width="70%">
            <asp:GridView ID="gdvDados" runat="server" CssClass="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDataBound="gdvDados_RowDataBound">
            </asp:GridView>		
            <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />				
		</div>
	</div>
 
</div>

 <div class="row">  

</div>

</div>
      </div>
    </div>
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
