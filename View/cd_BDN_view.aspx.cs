﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_BDN_view : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
            }
        }

        private void PopularGridGeral(int limite)
        {
            cBDN engineBDN = new cBDN();
            DataSet ds = new DataSet();
            ds = engineBDN.PopularGrid_2(0);
            double media = 0;
            double total = 0;
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {                
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["ESTADO"].ToString() == "Vigente" && ds.Tables[0].Rows[i]["Expirado"].ToString() == "N")
                    {
                        totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["TOTAL NETO"].ToString());
                        total++;
                    }
                }
                media = totalvalor / total;
                lblTotalCotizacoes.Text = "Cotizaciones :" + total;
                lblTotal.Text = "TOTAL :" + totalvalor.ToString("N0");
                lblMediaCotizacao.Text = "Media Cotización :" + (totalvalor/total).ToString("N2");
            }

            gdvDados.DataSource = engineBDN.PopularGrid_2(0);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();


            double mediadominante = 0;
            double totaldominante = 0;
            double totalanomala = 0;
            double totalanomalavalor = 0;
            double percentagemanomala = (totalvalor * 40) / 100;
            foreach (GridViewRow r in gdvDados.Rows)
            {
                if (r.Cells[5].Text !=string.Empty)
                {
                    double valoratual = Convert.ToDouble(r.Cells[5].Text);

                    if (r.Cells[7].Text == "N")
                    {
                        if (valoratual > percentagemanomala) 
                        {
                            r.BackColor = System.Drawing.Color.LightYellow;
                            //r.Cells[5].BackColor = System.Drawing.Color.LightBlue;
                            totalanomala++;
                            totalanomalavalor += valoratual;
                        }
                        else if (valoratual > media)
                        {
                            r.BackColor = System.Drawing.Color.LightGreen;
                            //r.Cells[5].BackColor = System.Drawing.Color.LightSeaGreen;
                            totaldominante++;
                            mediadominante += valoratual;
                        }
                    }

 
                }
                else
                {
                    // r.BackColor = System.Drawing.Color.Orange;
                    // r.Cells[0].BackColor = System.Drawing.Color.Green;
                }
            }


            lblTotalDominate.Text = "Total Cotización Dominante: " + totaldominante;
            lblMediaDominante.Text = "Porcentaje Cotización Dominante:" + (totaldominante > 0 ? ((totalanomalavalor/totalvalor)*100).ToString("N2") : "0");
            lblTotalAnomala.Text = "Total Anomala:" + totalanomala;

            Session["gdvDadosTable"] = ds.Tables[0];
            Session["ordenamento"] = "asc";
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // you only want to check DataRow type, and not headers, footers etc.
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // you already know you're looking at this row, so check your cell text
            //    if (e.Row.Cells(1).Text == "C6N")
            //    {
            //        e.Row.BackColor = System.Drawing.Color.Red;
            //    }
                e.Row.Cells[5].Text = Convert.ToDouble(e.Row.Cells[5].Text).ToString("N0");
                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
            }
        }

        protected void gdvDados_Sorting(object sender, GridViewSortEventArgs e)
        {
            //DataTable dataTable = gdvDados.DataSource as DataTable;
            DataTable dataTable = (DataTable)Session["gdvDadosTable"];

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                //dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);
                dataView.Sort = e.SortExpression + " " + Session["ordenamento"];
                if (Session["ordenamento"] == "asc")
                    Session["ordenamento"] = "desc";
                else
                    Session["ordenamento"] = "asc";
                gdvDados.DataSource = dataView;
                gdvDados.DataBind();
            }
        }
    }
}