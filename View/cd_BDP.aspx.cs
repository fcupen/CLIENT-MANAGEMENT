﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_BDP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
            }

        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                PopularClientes();
                PopularProdutos();
            }
        }
        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            ddlProduto.DataSource = engineProduto.PopularGridVendedor(_vendedor.id).Tables[0];
            ddlProduto.DataValueField = "Id";
            ddlProduto.DataTextField = "NOMBRE";
            ddlProduto.DataBind();

            ddlFiltro.DataSource = engineProduto.PopularGridVendedor(_vendedor.id).Tables[0];
            ddlFiltro.DataValueField = "Id";
            ddlFiltro.DataTextField = "NOMBRE";
            ddlFiltro.DataBind();
        }
        private void PopularClientes()
        {
            cClientes engineClientes = new cClientes();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            if (engineClientes.PopularGridPorVendedor(_vendedor.id) != null)
            {
                ddlClientes.DataSource = engineClientes.PopularGridPorVendedor(_vendedor.id).Tables[0];
                ddlClientes.DataValueField = "Id";
                ddlClientes.DataTextField = "Razon Social";
                ddlClientes.DataBind();
            }
            else
            {
                Alert.Show("Não há clientes para este vendedor");
            }
        }

        private void PopularGridGeral(int limite)
        {
            cBDP engineBDP = new cBDP();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            lblTituloRelatorio.Text = _vendedor.nombre + " - " + DateTime.Now.Year.ToString();
            
            DataSet ds = engineBDP.PopularGrid(0,_vendedor.id.ToString());


            
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["total neto"].ToString());
                }
            }
            lblTotal.Text = "Total: " + ds.Tables[0].Rows.Count;
            lblTotalValor.Text = "Total Neto: " + totalvalor.ToString("N2");
            gdvDados.DataSource = ds;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cBDP engineBDP = new cBDP();
            mBDP _BDP = new mBDP();
            cClientes engineClientes = new cClientes();
            cProduto engineProduto = new cProduto();

            if (ddlClientes.SelectedIndex == -1)
            {
                Alert.Show("Selecione um cliente");
                return;
            }
            if (ddlProduto.SelectedIndex == -1)
            {
                Alert.Show("Selecione um producto");
                return;
            }

            _BDP.fecha = (calendario.Text != string.Empty ? Convert.ToDateTime(calendario.Text) : Convert.ToDateTime("1900-01-01"));
            _BDP.instituicion = (ddlClientes.SelectedValue != null ? engineClientes.RetornarClientes(ddlClientes.SelectedValue.ToString()) : null);
            _BDP.produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
            _BDP.cotizacion = cotizacion.Text;
            _BDP.contacto = contacto.Text;
            _BDP.descripcion_productos = descripcion.Text;
            _BDP.status = ddlStatus.Text;
            _BDP.totalneto = (totalneto.Text != string.Empty ? Convert.ToDecimal(totalneto.Text) : 0);
            _BDP.vendedor = (mVendedor)Session["vendedor"];

            _BDP.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                if (_BDP.status.ToLower() == "bdn")
                {
                    cBDN engineBDN = new cBDN();
                    mBDN _BDN = new mBDN();
                    _BDN.fecha = (calendario.Text != string.Empty ? Convert.ToDateTime(calendario.Text) : Convert.ToDateTime("1900-01-01"));
                    _BDN.instituicion = (ddlClientes.SelectedValue != null ? engineClientes.RetornarClientes(ddlClientes.SelectedValue.ToString()) : null);
                    _BDN.produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
                    _BDN.cotizacion = cotizacion.Text;
                    _BDN.contacto = contacto.Text;
                    _BDN.descripcion_productos = descripcion.Text;
                    _BDN.status = "Vigente";
                    _BDN.totalneto = (totalneto.Text != string.Empty ? Convert.ToDecimal(totalneto.Text) : 0);
                    _BDN.vendedor = _BDP.vendedor;
                    _BDN.Id = Convert.ToInt32(engineBDN.Inserir(_BDN));
                }
                else
                {
                    _BDP.Id = Convert.ToInt32(engineBDP.Inserir(_BDP));
                }
            }
            else
            {

                if (_BDP.status.ToLower() == "bdn")
                {
                    cBDN engineBDN = new cBDN();
                    mBDN _BDN = new mBDN();
                    _BDN.fecha = (calendario.Text != string.Empty ? Convert.ToDateTime(calendario.Text) : Convert.ToDateTime("1900-01-01"));
                    _BDN.instituicion = (ddlClientes.SelectedValue != null ? engineClientes.RetornarClientes(ddlClientes.SelectedValue.ToString()) : null);
                    _BDN.produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
                    _BDN.cotizacion = cotizacion.Text;
                    _BDN.contacto = contacto.Text;
                    _BDN.descripcion_productos = descripcion.Text;
                    _BDN.status = "Vigente";
                    _BDN.totalneto = (totalneto.Text != string.Empty ? Convert.ToDecimal(totalneto.Text) : 0);
                    _BDN.vendedor = _BDP.vendedor;
                    _BDN.Id = Convert.ToInt32(engineBDN.Inserir(_BDN));                    
                    engineBDP.Deletar(_BDP);
                }
                else
                {
                    engineBDP.Update(_BDP);
                }
            }



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            calendario.Text = string.Empty;
            ddlClientes.SelectedIndex = 0;
            ddlStatus.SelectedIndex = 0;
            cotizacion.Text = string.Empty;
            contacto.Text = string.Empty;
            descripcion.Text = string.Empty;
            totalneto.Text = string.Empty;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cBDP engineBDP = new cBDP();
            mBDP _BDP = engineBDP.RetornaBDP(gdvDados.SelectedRow.Cells[2].Text);
            txtid.Text = _BDP.Id.ToString();
            calendario.Text = _BDP.fecha.ToString("dd/MM/yyyy");
            ddlClientes.SelectedIndex = ddlClientes.Items.IndexOf(ddlClientes.Items.FindByValue(_BDP.instituicion.id.ToString()));
            ddlStatus.Text = _BDP.status;
            cotizacion.Text = _BDP.cotizacion;
            contacto.Text = _BDP.contacto;
            descripcion.Text = _BDP.descripcion_productos;
            totalneto.Text = _BDP.totalneto.ToString("N2");
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cBDP engineBDP = new cBDP();
            mBDP _BDP = new mBDP();
            _BDP = engineBDP.RetornaBDP(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            engineBDP.Deletar(_BDP);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);

                    if (j == 9)
                        e.Row.HorizontalAlign = HorizontalAlign.Right;
                }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            cBDP engineBDP = new cBDP();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            DataSet ds = engineBDP.PopularGridFiltros(ddlfiltros.Text,_vendedor.id.ToString());


            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["total neto"].ToString());
                }
            }
            lblTotal.Text = "Total: " + ds.Tables[0].Rows.Count;
            lblTotalValor.Text = "Total Neto: " + totalvalor.ToString("N2");
            gdvDados.DataSource = ds;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();
        }

        protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        public void PopularGridProduto()
        {
            if (!rdProduto.Checked)
                return;
            cBDP engineBDP = new cBDP();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            cProduto engineProduto = new cProduto();
            mProduto _producto = engineProduto.RetornarProduto(ddlFiltro.SelectedValue.ToString());
            DataSet ds = engineBDP.PopularGrid(_vendedor.id.ToString(),_producto.Id);


            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["totalneto"].ToString());
                }
            }
            lblTotal.Text = "Total: " + ds.Tables[0].Rows.Count;
            lblTotalValor.Text = "Total Neto: " + totalvalor.ToString("N2");
            gdvDados.DataSource = ds;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();
        }

        protected void rdTodos_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridGeral(0);
        }

        protected void rdProduto_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gdvDados.AllowPaging = false;
                Button1_Click(null,null);

                gdvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gdvDados.HeaderRow.Cells)
                {
                    cell.BackColor = gdvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gdvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gdvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gdvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gdvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //salvando a cópia local
            string filename = "informe.pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gdvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 8f, 8f, 8f, 8f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            gdvDados.AllowPaging = true;
            gdvDados.DataBind();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}