﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_produtos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
            }
        }
        private void PopularGridGeral(int limite)
        {
            cProduto engineProduto = new cProduto();
            gdvDados.DataSource = engineProduto.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cProduto engineProduto = new cProduto();
            mProduto _Produto = new mProduto();
            string horizonte = string.Empty;
            if (rd1mes.Checked)
                horizonte = "1";
            if(rd3meses.Checked)
                horizonte = "3";
            if (rd6meses.Checked)
                horizonte = "6";
            if (rd9meses.Checked)
                horizonte = "9";
            if (rd12meses.Checked)
                horizonte = "12";
            if (rd18meses.Checked)
                horizonte = "18";
            if (rd24meses.Checked)
                horizonte = "24";

            _Produto.codigo = grupo.Text;
            _Produto.nombre = nomegrupoprodutos.Text;
            _Produto.horizonte = horizonte;
            _Produto.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                _Produto.Id = Convert.ToInt32(engineProduto.Inserir(_Produto)); ;
            }
            else
            {
                engineProduto.Update(_Produto);
            }



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            grupo.Text = string.Empty;
            nomegrupoprodutos.Text = string.Empty;
            rd1mes.Checked=false;
            rd3meses.Checked=false;
            rd6meses.Checked=false;
            rd9meses.Checked=false;
            rd12meses.Checked=false;
            rd18meses.Checked=false;
            rd24meses.Checked = false;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            limpar();

            cProduto engineProduto = new cProduto();
            mProduto _produto = engineProduto.RetornarProduto(gdvDados.SelectedRow.Cells[1].Text);
            txtid.Text = _produto.Id.ToString();
            grupo.Text = _produto.codigo;
            nomegrupoprodutos.Text = _produto.nombre;

            if (_produto.horizonte=="1")
                rd1mes.Checked = true;
            if (_produto.horizonte == "3")
                rd3meses.Checked = true;
            if (_produto.horizonte == "6")
                rd6meses.Checked = true;
            if (_produto.horizonte == "9")
                rd9meses.Checked = true;
            if (_produto.horizonte == "12")
                rd12meses.Checked = true;
            if (_produto.horizonte == "18")
                rd18meses.Checked = true;
            if (_produto.horizonte == "24")
                rd24meses.Checked = true;

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cProduto engineProduto = new cProduto();
            mProduto _Produto = new mProduto();
            _Produto = engineProduto.RetornarProduto(gdvDados.Rows[e.RowIndex].Cells[1].Text);
            engineProduto.Deletar(_Produto);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }
    }
}