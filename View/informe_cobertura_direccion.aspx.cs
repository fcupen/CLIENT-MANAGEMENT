﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class informe_cobertura_direccion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();

            }
        }

        private void PopularComboVendedores()
        {
            cVendedor engineVendedor = new cVendedor();

            DataSet ds = engineVendedor.PopularGrid(0);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendedor.DataSource = ds.Tables[0];
                    ddlVendedor.DataValueField = "Id";
                    ddlVendedor.DataTextField = "NOMBRE";
                    ddlVendedor.DataBind();
                }
                else
                {
                    ddlVendedor.DataSource = null;
                    ddlVendedor.Items.Clear();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }
        
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                PopularComboVendedores();
                PopularGridGeral();
            }
        }

        private void PopularGridGeral()
        {

            cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            List<mRelacaoVendedor> listaClientes = null;
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            cInformediario engineInforme = new cInformediario();


            gdvDados.AutoGenerateSelectButton = false;


            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add("Nº");              //0
            dtNovo.Columns.Add("Cliente");         //1
            dtNovo.Columns.Add("Visitas");         //2
            dtNovo.Columns.Add(" ");               //3
            dtNovo.Columns.Add("  ");              //4
            dtNovo.Columns.Add("   ");             //5
            dtNovo.Columns.Add("    ");            //6
            dtNovo.Columns.Add("     ");           //7
            dtNovo.Columns.Add("      ");          //8
            dtNovo.Columns.Add("        ");        //9
            dtNovo.Columns.Add("         ");       //10
            dtNovo.Columns.Add("          ");      //11
            dtNovo.Columns.Add("            ");    //12
            dtNovo.Columns.Add("              ");  //13
            dtNovo.Columns.Add("Total");  //13


            
            //totalizadores
            int[] totalvisitasmes= new int[12];


            //trazer todos cliente
            DataRow dr;
            int contador=0;
            listaClientes = engineRelacaoVendedor.RetornarRelacaoVendedorPorVendedor(_vendedor.id.ToString());
            if (listaClientes != null)
            {
                 dr = dtNovo.NewRow();
                    dr[0] = "";
                    dr[1] = "";
                    dr[2] = "Enero";
                    dr[3] = "Febrero";
                    dr[4] = "Marzo";
                    dr[5] = "Abril";
                    dr[6] = "Mayo";
                    dr[7] = "Junio";
                    dr[8] = "Julio";
                    dr[9] = "Agosto";
                    dr[10] = "Septiembre";
                    dr[11] = "Octubre";
                    dr[12] = "Noviembre";
                    dr[13] = "Diciembre";
                    dr[14] = "Total";
                    dtNovo.Rows.Add(dr);


                    int indice = 2;
                    int totalvisitas = 0;
                    string ConsultaSQL = string.Empty;
                    DataSet ds = new DataSet();
                    clsConexao banco = new clsConexao();
                    string mesescrito = string.Empty;

                    for (int i = 0; i <= 11; i++)
                    {
                        totalvisitasmes[i] = 0;
                    }

                    foreach (mRelacaoVendedor item in listaClientes)
                    {
                        //contabilizar visitas de cada cliente, vendedor, ano e mes


                        contador++;
                        dr = dtNovo.NewRow();
                        dr[0] = contador.ToString();
                        dr[1] = item.cliente.razonsocial.ToString();

                        indice = 2;
                        totalvisitas = 0;
                        int totalvisitascliente = 0;
                        for (int mes = 1; mes <= 12; mes++)
                        {                            
                            if (mes == 1)
                                mesescrito = "Enero";
                            else if (mes == 2)
                                mesescrito = "Febrero";
                            else if (mes == 3)
                                mesescrito = "Marzo";
                            else if (mes == 4)
                                mesescrito = "Abril";
                            else if (mes == 5)
                                mesescrito = "Mayo";
                            else if (mes == 6)
                                mesescrito = "Junio";
                            else if (mes == 7)
                                mesescrito = "Julio";
                            else if (mes == 8)
                                mesescrito = "Agosto";
                            else if (mes == 9)
                                mesescrito = "Septiembre";
                            else if (mes == 10)
                                mesescrito = "Octubre";
                            else if (mes == 11)
                                mesescrito = "Noviembre";
                            else if (mes == 12)
                                mesescrito = "Diciembre";
                            else
                                mesescrito = mes.ToString();

                            totalvisitas = 0;                            
                            for (int dia = 1; dia <= 31; dia++)
                            {
                                ConsultaSQL = "select * from informediario where mes='" + mesescrito + "' and ano='" + DateTime.Now.Year.ToString() +
                                    "' and cliente_id=" + item.cliente.id.ToString() + " and vendedor_id=" + _vendedor.id.ToString() + " and "
                                    + "(d" + dia.ToString() + "_valor='1' or d" + dia.ToString() + "_valor='3')";

                                ds = banco.Selecionar(ConsultaSQL);
                                if (ds != null)
                                    totalvisitas += ds.Tables[0].Rows.Count;

                            }

                            totalvisitasmes[mes-1] += totalvisitas;
                            totalvisitascliente += totalvisitas;
                            //totalvisitasmes
                            dr[indice] = totalvisitas;
                            indice++;
                        }
                        dr[indice] = totalvisitascliente.ToString();
                        dtNovo.Rows.Add(dr);
                    }

                    dr = dtNovo.NewRow();
                    dr[0] = contador.ToString();
                    dr[1] = "Total";
                    indice = 2;
                    for (int i = 0; i <= 11; i++)
                    {
                        dr[indice] = totalvisitasmes[i].ToString();
                        indice++;
                    }
                    dr[indice] = totalvisitasmes.Sum().ToString();
                    dtNovo.Rows.Add(dr);
            }
            


            gdvDados.DataSource = dtNovo;


            gdvDados.AutoGenerateColumns = true;

            gdvDados.DataBind();

        }


        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridGeral();
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition",
            // "attachment;filename=GridViewExport.pdf");
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);
            //dgvDados.AllowPaging = false;
            //dgvDados.DataBind();
            //dgvDados.RenderControl(hw);
            //StringReader sr = new StringReader(sw.ToString());
            //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //pdfDoc.Open();
            //htmlparser.Parse(sr);
            //pdfDoc.Close();
            //Response.Write(pdfDoc);
            //Response.End();

            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            string filename = "informe_cobertura_direccion.pdf";
            string pasta = Server.MapPath("~/pdf");
            //if(!Directory.Exists(pasta +"//"+ DateTime.Now.ToString("dd_MM_yyyy")))
            //    Directory.CreateDirectory(pasta + "//"+ DateTime.Now.ToString("dd_MM_yyyy"));

            //pasta += "//"+ DateTime.Now.ToString("dd_MM_yyyy");

            clog_arquivos enginelogArquivos = new clog_arquivos();
            mlog_arquivo _logArquivos = null;
            string tela = "Informe operativo vendedores";
            List<mlog_arquivo> _listaLogArquivos = enginelogArquivos.Retornarlog_arquivoPorData(_vendedor.id, DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), tela);
            if (_listaLogArquivos != null)
            {
                if (_listaLogArquivos.Count > 0)
                {
                    _logArquivos = _listaLogArquivos[0];
                }
            }

            if (_logArquivos == null)
            {
                _logArquivos = new mlog_arquivo();
                _logArquivos.data = DateTime.Now;
                _logArquivos.tela = tela;
                _logArquivos.vendedor = _vendedor;
                _logArquivos.arquivo = string.Empty;

                _logArquivos.Id = Convert.ToInt32(enginelogArquivos.Inserir(_logArquivos));

                filename = tela + "_";
                filename += DateTime.Now.ToString("yyyy_MM_dd");
                filename += "_" + _logArquivos.Id.ToString();
                filename += "_" + _vendedor.id.ToString();
                filename += ".pdf";

                _logArquivos.arquivo = filename;
                //enginelogArquivos.Update(_logArquivos);
            }
            else
            {
                filename = tela + "_";
                filename += DateTime.Now.ToString("yyyy_MM_dd");
                filename += "_" + _logArquivos.Id.ToString();
                filename += "_" + _vendedor.id.ToString();
                filename += ".pdf";

                _logArquivos.arquivo = filename;
                //enginelogArquivos.Update(_logArquivos);
            }


            //if(File.Exists(


            //Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition", "attachment;filename=informe_consolidado.pdf");
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);
            //dgvDados.RenderControl(hw);
            //StringReader sr = new StringReader(sw.ToString());
            //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //pdfDoc.Open();
            //htmlparser.Parse(sr);
            //pdfDoc.Close();
            //Response.Write(pdfDoc);
            //Response.End();
            //dgvDados.AllowPaging = true;
            //dgvDados.DataBind();

            //salvando a cópia local
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gdvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            gdvDados.AllowPaging = true;
            gdvDados.DataBind();
        }

        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gdvDados.PageIndex = e.NewPageIndex;
            PopularGridGeral();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gdvDados.AllowPaging = false;
                PopularGridGeral();

                gdvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gdvDados.HeaderRow.Cells)
                {
                    cell.BackColor = gdvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gdvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gdvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gdvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gdvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the
            /* specified ASP.NET server control at run time. */
        }
       
    }
}