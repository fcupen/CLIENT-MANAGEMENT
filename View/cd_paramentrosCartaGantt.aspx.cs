﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_paramentrosCartaGantt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }

            
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);


                string mes = DateTime.Now.Month.ToString();
                string mesescrito = string.Empty;
                if (mes == "1")
                    mesescrito = "Enero";
                else if (mes == "2")
                    mesescrito = "Febrero";
                else if (mes == "3")
                    mesescrito = "Marzo";
                else if (mes == "4")
                    mesescrito = "Abril";
                else if (mes == "5")
                    mesescrito = "Mayo";
                else if (mes == "6")
                    mesescrito = "Junio";
                else if (mes == "7")
                    mesescrito = "Julio";
                else if (mes == "8")
                    mesescrito = "Agosto";
                else if (mes == "9")
                    mesescrito = "Septiembre";
                else if (mes == "10")
                    mesescrito = "Octubre";
                else if (mes == "11")
                    mesescrito = "Noviembre";
                else if (mes == "12")
                    mesescrito = "Diciembre";
                else
                    mesescrito = mes;
                ddlMesFiltro.Text = mesescrito;
                PopularGridGeralFiltro(ddlAnoFiltro.Text, mesescrito);
            }
        }
        private void PopularGridGeral(int limite)
        {
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            gdvDados.DataSource = engineParametrosCartaGantt.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }

        private void PopularGridGeralFiltro(string ano,string mes)
        {
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            gdvDados.DataSource = engineParametrosCartaGantt.PopularGridAnoMes(ano,mes);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            mParametrosCartaGantt _ParametrosCartaGantt = new mParametrosCartaGantt();

            _ParametrosCartaGantt.ano = ddlAno.Text;
            _ParametrosCartaGantt.mes = ddlMes.Text;
            _ParametrosCartaGantt.fecha = ddlFecha.Text;
            _ParametrosCartaGantt.dia = ddlDia.Text;
            _ParametrosCartaGantt.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                _ParametrosCartaGantt.Id = Convert.ToInt32(engineParametrosCartaGantt.Inserir(_ParametrosCartaGantt)); ;
            }
            else
            {
                engineParametrosCartaGantt.Update(_ParametrosCartaGantt);
            }



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlAno.SelectedIndex = 0;
            ddlMes.SelectedIndex = 0;
            ddlDia.SelectedIndex = 0;
            ddlFecha.SelectedIndex = 0;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            mParametrosCartaGantt _parametrosCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(gdvDados.SelectedRow.Cells[1].Text);
            txtid.Text = _parametrosCartaGantt.Id.ToString();

            ddlAno.Text = _parametrosCartaGantt.ano.ToString();
            ddlMes.Text = _parametrosCartaGantt.mes;
            ddlDia.Text = _parametrosCartaGantt.dia.ToString();
            ddlFecha.Text = _parametrosCartaGantt.fecha.ToString();
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            mParametrosCartaGantt _parametrosCartaGantt = new mParametrosCartaGantt();
            _parametrosCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(gdvDados.Rows[e.RowIndex].Cells[1].Text);
            engineParametrosCartaGantt.Deletar(_parametrosCartaGantt);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void ddlAnoFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridGeralFiltro(ddlAnoFiltro.Text, ddlMesFiltro.Text);
        }
    }
}