﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_carta_gatt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();

            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral();
            }
        }
        private void PopularGridGeral()
        {

            //cMetas engineMetas = new cMetas();
            string sql = "SELECT   razonsocial,contato,telefone,comuna FROM clientes c"
                     + " order by razonsocial ;";
            dgvCliente.AutoGenerateSelectButton = true;


            //gdvDados.DataSource = engineMetas.PopularGridFiltros3("vendedores", ((mVendedor)Session["Vendedor"]).id.ToString());
            clsConexao conexa = new clsConexao();
            dgvCliente.DataSource = conexa.Selecionar(sql);

            dgvCliente.AutoGenerateColumns = true;

            dgvCliente.DataBind();

             sql = "SELECT   dia,fecha FROM parametrosCartaGantt"
         + " order by fecha;";
            dgvDias.AutoGenerateSelectButton = false;


            //gdvDados.DataSource = engineMetas.PopularGridFiltros3("vendedores", ((mVendedor)Session["Vendedor"]).id.ToString());            
            dgvDias.DataSource = conexa.Selecionar(sql);

            dgvDias.AutoGenerateColumns = true;

            dgvDias.DataBind();
            DataTable dt = conexa.Selecionar(sql).Tables[0];

            DataTable dt2 = new DataTable();
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dt2.Columns.Add();
            }
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                dt2.Rows.Add();
                dt2.Rows[i][0] = dt.Columns[i].ColumnName;
            }
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    dt2.Rows[i][j + 1] = dt.Rows[j][i];
                }
            }
            BindGrid(dt2, true);

        }


        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            //if (dgvCliente.Rows.Count > 0)
            //{
            //    dgvCliente.UseAccessibleHeader = true;
            //    dgvCliente.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
        }
        private void BindGrid(DataTable dt, bool rotate)
        {
            dgvDias.ShowHeader = !rotate;
            dgvDias.DataSource = dt;
            dgvDias.DataBind();
            if (rotate)
            {
                foreach (GridViewRow row in dgvDias.Rows)
                {
                    row.Cells[0].CssClass = "header";
                }
            }
        }
    }
}