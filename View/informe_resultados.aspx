﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="informe_resultados.aspx.cs" Inherits="Clients_Management.View.informe_resultados" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <script src="Scripts/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>  
     <div class="container">
       <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">DIRECCION DE VENTAS</a></li>
   <li class="active">RESULTADOS</li>
</ol>
  </div>
</div>
<div id="Div1"></div>
          
<div id="about_section_1">
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     </div>
	 
	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">
                <asp:Label ID="lblTituloRelatorio" runat="server" Text="Relatório"></asp:Label></h4>
            <asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" Visible="true" AutoPostBack="True" OnSelectedIndexChanged="ddlAno_SelectedIndexChanged" >
                <asp:ListItem>2015</asp:ListItem>
                <asp:ListItem>2016</asp:ListItem>
                <asp:ListItem>2017</asp:ListItem>
                <asp:ListItem>2018</asp:ListItem>
            </asp:DropDownList>
            <asp:RadioButton ID="rdTodos" runat="server" Text="Todos" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdTodos_CheckedChanged" Checked="True" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:RadioButton ID="rdProduto" runat="server" Text="PRODUCTO" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdProduto_CheckedChanged" /><br />
            <asp:DropDownList ID="ddlFiltro" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFiltro_SelectedIndexChanged"></asp:DropDownList>
			<hr width="70%">
			<div class="well">
                <asp:DropDownList ID="ddlVendedor" runat="server" AutoPostBack="true" class="form-control input-sm" OnSelectedIndexChanged="ddlVendedor_SelectedIndexChanged" >
                </asp:DropDownList>
            </div>
            <asp:GridView ID="dgvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDataBound="dgvDados_RowDataBound" >
                <Columns>
                </Columns>
            </asp:GridView>
            <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />				
            <asp:Literal ID="ltScripts" runat="server"></asp:Literal> 
            <div id="chart_div" style="width: 860px; height: 400px;">  
                <div id="chart_div_2" style="width: 660px; height: 400px;"> 
        </div>  

		</div>
	</div>
 
</div>
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  

  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
