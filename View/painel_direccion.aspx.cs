﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class painel_direccion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //mUsuario _usuario = null;
            //if (Session["admin"] != null)
            //{
            //    _usuario = (mUsuario)Session["admin"];
            //}
            //else
            //{
            //    Response.Redirect("~/login.aspx");
            //}
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
            }
        }
    }
}