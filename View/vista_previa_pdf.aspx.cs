﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class vista_previa_pdf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["vistaPrevia"] != null)
            {
                mVendedor _vendedor = (mVendedor)Session["vendedor"];

                lblNomeVendedor.Text = _vendedor.nombre;
                lblFechaVendedor.Text = DateTime.Now.ToString("dd/MM/yyyy");
                lblCodigoVendedor.Text = _vendedor.codigo;
                lblEmailVendedor.Text = _vendedor.email;
               

                cInformediario engineInformediario = new cInformediario();
                //DataTable dt = engineInformediario.PopularGridVistaPrevia(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString()).Tables[0];
                //gdvDados.DataSource = dt;
                //gdvDados.AutoGenerateColumns = true;
                //gdvDados.AutoGenerateSelectButton = false;
                //gdvDados.DataBind();


                lblNumeroVisitasRealizadas.Text = "0";
                lblNumerodeVisitasTelefonicas.Text = "0";
                lblNumeroLamadas.Text = "0";


                DataSet dsCalculos = null;
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtro(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), "1");
                if (dsCalculos != null)
                    lblNumeroVisitasRealizadas.Text = dsCalculos.Tables[0].Rows.Count.ToString();

                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtro(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), "3");
                if (dsCalculos != null)
                    lblNumeroVisitasRealizadas.Text = (Convert.ToInt32(lblNumeroVisitasRealizadas.Text) + Convert.ToInt32(dsCalculos.Tables[0].Rows.Count)).ToString();


                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtro(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), "2");
                if (dsCalculos != null)
                    lblNumerodeVisitasTelefonicas.Text = dsCalculos.Tables[0].Rows.Count.ToString();

                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtro(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), "4");
                if (dsCalculos != null)
                    lblNumeroLamadas.Text = dsCalculos.Tables[0].Rows.Count.ToString();

                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtro(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), "0");
                if (dsCalculos != null)
                    lblAbortadas.Text = dsCalculos.Tables[0].Rows.Count.ToString();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ExportarPDF();
            }

        }
        private void ExportarPDF()
        {

            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            string filename = "informe_consolidado.pdf";
            string pasta = Server.MapPath("~/pdf");

            clog_arquivos enginelogArquivos = new clog_arquivos();
            mlog_arquivo _logArquivos = null;
            string tela = "vista previa";
            List<mlog_arquivo> _listaLogArquivos = enginelogArquivos.Retornarlog_arquivoPorData(_vendedor.id, DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), tela);
            if (_listaLogArquivos != null)
            {
                if (_listaLogArquivos.Count > 0)
                {
                    _logArquivos = _listaLogArquivos[0];
                }
            }

            if (_logArquivos == null)
            {
                _logArquivos = new mlog_arquivo();
                _logArquivos.data = DateTime.Now;
                _logArquivos.tela = tela;
                _logArquivos.vendedor = _vendedor;
                _logArquivos.arquivo = string.Empty;

                _logArquivos.Id = Convert.ToInt32(enginelogArquivos.Inserir(_logArquivos));

                filename = tela + "_";
                filename += DateTime.Now.ToString("yyyy_MM_dd");
                filename += "_" + _logArquivos.Id.ToString();
                filename += "_" + _vendedor.id.ToString();
                filename += ".pdf";

                _logArquivos.arquivo = filename;
                enginelogArquivos.Update(_logArquivos);
            }
            else
            {
                filename = tela + "_";
                filename += DateTime.Now.ToString("yyyy_MM_dd");
                filename += "_" + _logArquivos.Id.ToString();
                filename += "_" + _vendedor.id.ToString();
                filename += ".pdf";

                _logArquivos.arquivo = filename;
                enginelogArquivos.Update(_logArquivos);
            }


            //salvando a cópia local
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Page.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();

        }
    }
}