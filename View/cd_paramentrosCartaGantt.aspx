﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_paramentrosCartaGantt.aspx.cs" Inherits="Clients_Management.View.cd_paramentrosCartaGantt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
   
  <div class="container">
      <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">PLANIFICACION</a></li>
  <li class="active">CONTROL AGENDA ANUAL</li>
</ol>
  </div>
</div>
         

  <div class="container"> 
  <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
    <div class="row">
     <div class="col-lg-12">        

     </div>
	 
	 
	 <div class="well">
	 
	          <div class="row">
 
					<div class="col-md-12">
                        
					
					<div class="form-horizontal" >
                        <h4 class="text-center">CONTROL AGENDA ANUAL</h4>
					<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
                            <div class="col-md-3">
                            <label>Seleccione el Año:</label>
							 <asp:DropDownList ID="ddlAnoFiltro" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAnoFiltro_SelectedIndexChanged" >
                <asp:ListItem>2015</asp:ListItem>
                <asp:ListItem>2016</asp:ListItem>
                <asp:ListItem>2017</asp:ListItem>
            </asp:DropDownList> 
							</div>
                            <div class="col-md-3">
                             <label>Seleccione el Mes:</label>
							 <asp:DropDownList ID="ddlMesFiltro" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAnoFiltro_SelectedIndexChanged">
                <asp:ListItem>Enero</asp:ListItem>
                <asp:ListItem>Febrero</asp:ListItem>
                <asp:ListItem>Marzo</asp:ListItem>
                <asp:ListItem>Abril</asp:ListItem>
                <asp:ListItem>Mayo</asp:ListItem>
                <asp:ListItem>Junio</asp:ListItem>
                <asp:ListItem>Julio</asp:ListItem>
                <asp:ListItem>Agosto</asp:ListItem>
                <asp:ListItem>Septiembre</asp:ListItem>
                <asp:ListItem>Octubre</asp:ListItem>
                <asp:ListItem>Noviembre</asp:ListItem>
                <asp:ListItem>Diciembre</asp:ListItem>
            </asp:DropDownList>
							</div>

						</div>
					</div>
				</div>




  
  </div>

  
  
  
  </div>
             </div>
	 
	 
	 	 	 <div class="row">
    <div class="col-md-12">
             <div class="row">
					
			    <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged">
            </asp:GridView>		
		</div>
	</div>
 
</div>
</div>

     <div class="col-md-12">
     
       <div class="well">
          <h4 class="text-center">ALTERACION DE DATOS</h4><br />
                         <h5 class="text-center">Selecione SELECT, haga los cambios y click ENVIAR</h5>
				 <div class="row">
 
			<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
                            <div class="col-md-3">
                            <label>Año:</label>
                                <asp:TextBox ID="txtid" runat="server" Visible="false"></asp:TextBox>
                               
							<asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" AutoPostBack="True" >
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                </asp:DropDownList>
							</div>
                            <div class="col-md-3">
                             <label>Mes:</label>
                                
							<asp:DropDownList ID="ddlMes" class="form-control input-sm" runat="server" AutoPostBack="True" >
                                    <asp:ListItem>Enero</asp:ListItem>
                                    <asp:ListItem>Febrero</asp:ListItem>
                                    <asp:ListItem>Marzo</asp:ListItem>
                                    <asp:ListItem>Abril</asp:ListItem>
                                    <asp:ListItem>Mayo</asp:ListItem>
                                    <asp:ListItem>Junio</asp:ListItem>
                                    <asp:ListItem>Julio</asp:ListItem>
                                    <asp:ListItem>Agosto</asp:ListItem>
                                    <asp:ListItem>Septiembre</asp:ListItem>
                                    <asp:ListItem>Octubre</asp:ListItem>
                                    <asp:ListItem>Noviembre</asp:ListItem>
                                    <asp:ListItem>Diciembre</asp:ListItem>
                                </asp:DropDownList>
							</div>
                            <div class="col-md-3">
                             <label>Dia:</label>
							
                                <asp:DropDownList ID="ddlDia" class="form-control input-sm" runat="server" AutoPostBack="True" >
                                    <asp:ListItem>L</asp:ListItem>
                                    <asp:ListItem>M</asp:ListItem>
                                    <asp:ListItem>M</asp:ListItem>
                                    <asp:ListItem>J</asp:ListItem>
                                    <asp:ListItem>V</asp:ListItem>
                                    <asp:ListItem>S</asp:ListItem>
                                </asp:DropDownList>
							</div>
                            <div class="col-md-3">
                             <label>Fecha:</label>
						
                                <asp:DropDownList ID="ddlFecha" class="form-control input-sm" runat="server" AutoPostBack="True">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>11</asp:ListItem>
                                    <asp:ListItem>12</asp:ListItem>
                                    <asp:ListItem>13</asp:ListItem>
                                    <asp:ListItem>14</asp:ListItem>
                                    <asp:ListItem>15</asp:ListItem>
                                    <asp:ListItem>16</asp:ListItem>
                                    <asp:ListItem>17</asp:ListItem>
                                    <asp:ListItem>18</asp:ListItem>
                                    <asp:ListItem>19</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>21</asp:ListItem>
                                    <asp:ListItem>22</asp:ListItem>
                                    <asp:ListItem>23</asp:ListItem>
                                    <asp:ListItem>24</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>26</asp:ListItem>
                                    <asp:ListItem>27</asp:ListItem>
                                    <asp:ListItem>28</asp:ListItem>
                                    <asp:ListItem>29</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>31</asp:ListItem>
                                </asp:DropDownList>
							</div>
						</div>
					</div>
				</div>
			
			<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                                 	<br />		
		
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Enviar" OnClick="btnSalvar_Click" />                                
							</div>
						</div>
					</div>
				</div>
  
	   
	   
	   	</div>	

	  
     </div>




</div>

</div></div>
</asp:Content>
