﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class form_vendedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                //PopularGrupoProdutos();
                PopularGrupoVendedores();
            }
        }

        private void PopularGrupoVendedores()
        {
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();
            if (engineGrupoVendedores.PopularGrid(0) != null)
            {
                ddlGrupoVendas.DataSource = engineGrupoVendedores.PopularGrid(0).Tables[0];
                ddlGrupoVendas.DataValueField = "id";
                ddlGrupoVendas.DataTextField = "nomegrupovendedores";
                ddlGrupoVendas.DataBind();
            }
            else
            {
                Alert.Show("Não há grupo de vendedores");
            }
        }

        //private void PopularGrupoProdutos()
        //{
        //    cProduto engineProduto = new cProduto();
        //    if (engineProduto.PopularGrid(0) != null)
        //    {
        //        ddlGrupoProdutos.DataSource = engineProduto.PopularGrid(0).Tables[0];
        //        ddlGrupoProdutos.DataValueField = "id";
        //        ddlGrupoProdutos.DataTextField = "nombre";
        //        ddlGrupoProdutos.DataBind();
        //    }
        //    else
        //    {
        //        Alert.Show("Não há grupo de produtos");
        //    }
        //}
        private void PopularGridGeral(int limite)
        {
            cVendedor engineVendedor = new cVendedor();
            gdvDados.DataSource = engineVendedor.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _Vendedor = new mVendedor();
            cProduto engineProduto = new cProduto();
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();

            _Vendedor.calendario = (calendario.Text!=string.Empty? Convert.ToDateTime(calendario.Text):Convert.ToDateTime("1900-01-01"));
            _Vendedor.codigo = codigo.Text;
            _Vendedor.nombre = nombre.Text;
            _Vendedor.appelido = appelido.Text;
            _Vendedor.area = engineGrupoVendedores.RetornarGrupoVendedores(ddlGrupoVendas.SelectedValue.ToString());
            //_Vendedor.seccion = engineProduto.RetornarProduto(ddlGrupoProdutos.SelectedValue.ToString());
            _Vendedor.meta = 0;// (meta.Text != string.Empty ? Convert.ToDecimal(meta.Text) : 0);
            _Vendedor.email = txtEmail.Text;
            _Vendedor.cargo = txtCargo.Text;
            _Vendedor.clave = txtClave.Text;
            _Vendedor.cartera_assignada = "N";// (txtCartera.Text != string.Empty ? txtCartera.Text.Substring(0, 1) : "");
            _Vendedor.id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
            _Vendedor.id = Convert.ToInt32(engineVendedor.Inserir(_Vendedor)); ;
            }
            else
            {
             engineVendedor.Update(_Vendedor);
            }



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            calendario.Text = string.Empty;
            codigo.Text = string.Empty;
            nombre.Text = string.Empty;
            appelido.Text = string.Empty;
            //ddlGrupoProdutos.SelectedIndex =0;
            ddlGrupoVendas.SelectedIndex = 0;
            //meta.Text = string.Empty;
            txtEmail.Text=string.Empty;
            txtCargo.Text=string.Empty;
            txtClave.Text=string.Empty;
            //txtCartera.Text = string.Empty;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(gdvDados.SelectedRow.Cells[2].Text);
            txtid.Text = _vendedor.id.ToString();
            calendario.Text = _vendedor.calendario.ToString("MM/dd/yyyy");
            codigo.Text = _vendedor.codigo;
            nombre.Text = _vendedor.nombre;
            appelido.Text = _vendedor.appelido;
            //area.Text = _vendedor.area;
            //seccion.Text = _vendedor.seccion;
            if(_vendedor.area!=null)
                ddlGrupoVendas.SelectedIndex = ddlGrupoVendas.Items.IndexOf(ddlGrupoVendas.Items.FindByValue(_vendedor.area.Id.ToString()));

            //if (_vendedor.seccion != null)
            //    ddlGrupoProdutos.SelectedIndex = ddlGrupoProdutos.Items.IndexOf(ddlGrupoProdutos.Items.FindByValue(_vendedor.seccion.Id.ToString()));
            //meta.Text = _vendedor.meta.ToString("N2");
            txtEmail.Text = _vendedor.email;
            txtClave.Text = _vendedor.clave;
            txtCargo.Text = _vendedor.cargo;
            //txtCartera.Text = _vendedor.cartera_assignada;
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _Vendedor = new mVendedor();
            _Vendedor = engineVendedor.RetornarVendedor(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            engineVendedor.Deletar(_Vendedor);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }


    }
}