﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GrupoVendedores.aspx.cs" Inherits="Clients_Management.View.GrupoVendedores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <div class="container">
                    <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">ADMINISTRACION DE OPERACIONES DE VENTAS</a></li>
  <li><a href="#">ESTRATEGIA OPERATIVA</a></li>
  <li class="active">CREAR GRUPOS DE VENTAS</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <p>&nbsp;</p>
	 
	 
	  <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">Relacion de Grupos</h4>
			<hr width="70%">
			    <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged">
            </asp:GridView>		
		</div>
	</div>
 
</div>

     <div class="col-md-12">
    
       <div class="form-horizontal well" >
         <h4>Rellene el formulario .</h4>
		 
		 			 <div class="row">
 
					<div class="col-xs-12">
					
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false" ></asp:TextBox>
                                <asp:TextBox ID="grupo" class="form-control input-lg" runat="server" placeholder="Codigo Grupo" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
 
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="nomegrupoprodutos" class="form-control input-lg" runat="server" placeholder="Nombre do Grupo" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
								<asp:Button ID="btnNovo" runat="server" class="btn btn-primary" Text="Nuevo" OnClick="btnNovo_Click" />
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Enviar" OnClick="btnSalvar_Click" />                                
							</div>
						</div>
					</div>
				</div>
				</div>
 
				</div>	
		 </div>
	   </div>
     </div>
	 
	 
	
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
 <div class="row">  
  

     <br /> <br />

</div>
  
  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
