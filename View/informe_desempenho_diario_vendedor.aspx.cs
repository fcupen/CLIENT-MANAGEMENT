﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class informe_desempenho_diario_vendedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
            }

            if (!Page.IsPostBack)
            {
                
                ddlAno.Text = DateTime.Now.Year.ToString();
                ddlMes.SelectedIndex = DateTime.Now.Month - 1;
                lblTituloRelatorio.Text = _vendedor.nombre.ToString() + " - " + ddlMes.Text + " - " + DateTime.Now.Year.ToString();
                Button1_Click(null, null);
            }
        }

        

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = _vendedor = (mVendedor)Session["vendedor"];
            clog_arquivos engineLogArquivos = new clog_arquivos();
            string tela = "vista previa";
            string dataInicial = ddlAno.Text + "-" + (ddlMes.SelectedIndex + 1).ToString() + "-01";
            string dataFinal = ddlAno.Text + "-" + (ddlMes.SelectedIndex + 1).ToString() + "-31";
            dgvDados.DataSource = engineLogArquivos.Retornarlog_arquivoPorData(_vendedor.id, dataInicial, dataFinal, tela);
            dgvDados.DataBind();
        }

        protected void dgvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink myLink = new HyperLink();
                myLink.Text = e.Row.Cells[1].Text;
                e.Row.Cells[1].Controls.Add(myLink);
                //myLink.NavigateUrl = Server.MapPath("~/pdf") + "/" + e.Row.Cells[1].Text;
                myLink.NavigateUrl = "~/pdf/" + e.Row.Cells[1].Text;
            }
        }
    }
}