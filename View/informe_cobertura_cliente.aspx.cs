﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class informe_cobertura_cliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {

                PopularVendedores();
                PopularClientes();

                    if (_usuario != null)
                        lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }

        protected void ddlVendedores_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularClientes();
        }

        private void PopularClientes()
        {
            cClientes engineClientes = new cClientes();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedores.SelectedValue.ToString());
            if (engineClientes.PopularGridPorVendedor(_vendedor.id) != null)
            {
                ddlCliente.DataSource = engineClientes.PopularGridPorVendedor(_vendedor.id).Tables[0];
                ddlCliente.DataValueField = "Id";
                ddlCliente.DataTextField = "Razon Social";
                ddlCliente.DataBind();
            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }

        private void PopularVendedores()
        {

            cVendedor engineVendedor = new cVendedor();
            DataSet ds = engineVendedor.PopularGrid(0);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendedores.DataSource = ds.Tables[0];
                    ddlVendedores.DataValueField = "Id";
                    ddlVendedores.DataTextField = "NOMBRE";
                    ddlVendedores.DataBind();
                }
                else
                {
                    ddlVendedores.DataSource = null;
                    ddlVendedores.Items.Clear();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            string mesescrito = ddlMes.Text.ToString();
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            cInformediario engineInformediario = new cInformediario();
            cClientes engineClientes = new cClientes();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedores.SelectedValue.ToString());
            mClientes _cliente = engineClientes.RetornarClientes(ddlCliente.SelectedValue.ToString());
            DataSet dsClientes = new DataSet();
            DataSet dsCalculos;


            DataSet ds = engineParametrosCartaGantt.PopularGridAnoMesOrdenado(ddlAno.Text, mesescrito);

            int[] totais_dias = new int[ds.Tables[0].Rows.Count + 1];

            DataTable dtNovo = new DataTable("Dados");

            dtNovo.Columns.Add("Dia");//0
            int indice = 1;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                DataColumn dc = new DataColumn();
                dc.Caption = linha["dia"].ToString();
                dc.ColumnName = linha["dia"].ToString();
                int space = 1;
                string nome = linha["dia"].ToString();
                while (dtNovo.Columns.Contains(nome))
                {
                    nome = linha["dia"].ToString() + new string(' ', space);
                    dc.ColumnName = nome;
                    space++;
                }

                dc.DataType = System.Type.GetType("System.String");
                dtNovo.Columns.Add(dc);
                indice++;
            }
            dtNovo.Columns.Add(" ");

            DataRow dr;
            dr = dtNovo.NewRow();
            dr[0] = "Fecha";
            int contador = 1;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dr[contador] = linha["fecha"].ToString();
                totais_dias[contador - 1] = 0;
                contador++;
            }
            totais_dias[contador - 1] = 0;

            dr[contador] = "Total";
            dtNovo.Rows.Add(dr);


            //Contacto Abortado
            dr = dtNovo.NewRow();
            dr[0] = "Contacto Abortado";
            contador = 1;
            int total_abortado = 0;
            int abortados = 0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedorCliente(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex+1).ToString(), linha["fecha"].ToString(), "0", _vendedor.id.ToString(),_cliente.id.ToString());
                if (dsCalculos != null)
                    abortados = dsCalculos.Tables[0].Rows.Count;
                else
                    abortados = 0;

                dr[contador] = abortados;
                total_abortado += abortados;
                //totais_dias[contador - 1] += abortados;
                contador++;
            }
            //totais_dias[contador - 1] += abortados;
            dr[contador] = abortados.ToString();
            dtNovo.Rows.Add(dr);

            //Contacto no Planificado
            dr = dtNovo.NewRow();
            dr[0] = "Contacto no Planificado";
            contador = 1;
            int total_noplanificado = 0;
            int noplanificado = 0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedorCliente(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex + 1).ToString(), linha["fecha"].ToString(), "1", _vendedor.id.ToString(), _cliente.id.ToString());
                if (dsCalculos != null)
                    noplanificado = dsCalculos.Tables[0].Rows.Count;
                else
                    noplanificado = 0;

                dr[contador] = noplanificado;
                total_noplanificado += noplanificado;
                totais_dias[contador - 1] += noplanificado;
                contador++;
            }
            totais_dias[contador - 1] += noplanificado;
            dr[contador] = total_noplanificado.ToString();
            dtNovo.Rows.Add(dr);

            //Contacto Telefónico
            dr = dtNovo.NewRow();
            dr[0] = "Contacto Telefónico";
            contador = 1;
            int total_telefonico = 0;
            int telefonico = 0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedorCliente(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex + 1).ToString(), linha["fecha"].ToString(), "2", _vendedor.id.ToString(), _cliente.id.ToString());
                if (dsCalculos != null)
                    telefonico = dsCalculos.Tables[0].Rows.Count;
                else
                    telefonico = 0;

                dr[contador] = telefonico;
                total_telefonico += telefonico;
                //totais_dias[contador - 1] += telefonico;
                contador++;
            }
            //totais_dias[contador - 1] += telefonico;
            dr[contador] = total_telefonico.ToString();
            dtNovo.Rows.Add(dr);

            //Contacto Efectivo
            dr = dtNovo.NewRow();
            dr[0] = "Contacto Efectivo";
            contador = 1;
            int total_efectivo = 0;
            int efectivo = 0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedorCliente(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex + 1).ToString(), linha["fecha"].ToString(), "3", _vendedor.id.ToString(), _cliente.id.ToString());
                if (dsCalculos != null)
                    efectivo = dsCalculos.Tables[0].Rows.Count;
                else
                    efectivo = 0;

                dr[contador] = efectivo;
                total_efectivo += efectivo;
                totais_dias[contador - 1] += efectivo;
                contador++;
            }
            totais_dias[contador - 1] += efectivo;
            dr[contador] = total_efectivo.ToString();
            dtNovo.Rows.Add(dr);

            //Follow up
            dr = dtNovo.NewRow();
            dr[0] = "Contacto Follow up";
            contador = 1;
            int total_followup = 0;
            int followup = 0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedorCliente(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex + 1).ToString(), linha["fecha"].ToString(), "4", _vendedor.id.ToString(), _cliente.id.ToString());
                if (dsCalculos != null)
                    followup = dsCalculos.Tables[0].Rows.Count;
                else
                    followup = 0;

                dr[contador] = followup;
                total_followup += followup;
                //totais_dias[contador - 1] += followup;
                contador++;
            }
            //totais_dias[contador - 1] += followup;
            dr[contador] = total_followup.ToString();
            dtNovo.Rows.Add(dr);

            //total
            dr = dtNovo.NewRow();
            dr[0] = "Total";
            contador = 1;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dr[contador] = totais_dias[contador - 1].ToString();
                contador++;
            }
            totais_dias[contador - 1] = total_noplanificado + total_efectivo;
            dr[contador] = totais_dias[contador - 1].ToString();
            dtNovo.Rows.Add(dr);

            gdvDados.DataSource = dtNovo;
            gdvDados.DataBind();

            int div = ds.Tables[0].Rows.Count;
            if (div == 0)
                div = 1;

            double promovida = total_noplanificado + total_efectivo;
            promovida = promovida / ds.Tables[0].Rows.Count;
            lblPromovida.Text = "PROMVDIA: " + promovida.ToString("N2");
        }
    }
}