﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_bdp_direccion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
            }

            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;
            lblData.Text = DateTime.Now.Day.ToString() + " de " + mesescrito + " de " + DateTime.Now.Year.ToString();

        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                PopularProdutos();
            }
        }
        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];

            ddlFiltro.DataSource = engineProduto.PopularGridVendedor(_vendedor.id).Tables[0];
            ddlFiltro.DataValueField = "Id";
            ddlFiltro.DataTextField = "NOMBRE";
            ddlFiltro.DataBind();
        }
       

        private void PopularGridGeral(int limite)
        {
            cBDP engineBDP = new cBDP();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            DataSet ds = engineBDP.PopularGrid(0, _vendedor.id.ToString());


            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["total neto"].ToString());
                }
            }
            lblTotal.Text = "Total: " + ds.Tables[0].Rows.Count;
            lblTotalValor.Text = "Total Neto: " + totalvalor.ToString("N2");
            gdvDados.DataSource = ds;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();

        }
        

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);

                    if (j == 7)
                    {
                        double valorfloor = Convert.ToDouble(e.Row.Cells[j].Text);
                        e.Row.Cells[j].Text = valorfloor.ToString();
                        e.Row.HorizontalAlign = HorizontalAlign.Right;
                    }
                }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            cBDP engineBDP = new cBDP();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            DataSet ds = engineBDP.PopularGridFiltros(ddlfiltros.Text, _vendedor.id.ToString());


            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["total neto"].ToString());
                }
            }
            lblTotal.Text = "Total: " + ds.Tables[0].Rows.Count;
            lblTotalValor.Text = "Total Neto: " + totalvalor.ToString("N2");
            gdvDados.DataSource = ds;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();
        }

        protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        public void PopularGridProduto()
        {
            if (!rdProduto.Checked)
                return;
            cBDP engineBDP = new cBDP();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            cProduto engineProduto = new cProduto();
            mProduto _producto = engineProduto.RetornarProduto(ddlFiltro.SelectedValue.ToString());
            DataSet ds = engineBDP.PopularGrid(_vendedor.id.ToString(), _producto.Id);


            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["total neto"].ToString());
                }
            }
            lblTotal.Text = "Total: " + ds.Tables[0].Rows.Count;
            lblTotalValor.Text = "Total Neto: " + totalvalor.ToString("N2");
            gdvDados.DataSource = ds;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();
        }

        protected void rdTodos_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridGeral(0);
        }

        protected void rdProduto_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition",
            // "attachment;filename=GridViewExport.pdf");
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);
            //dgvDados.AllowPaging = false;
            //dgvDados.DataBind();
            //dgvDados.RenderControl(hw);
            //StringReader sr = new StringReader(sw.ToString());
            //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //pdfDoc.Open();
            //htmlparser.Parse(sr);
            //pdfDoc.Close();
            //Response.Write(pdfDoc);
            //Response.End();

            cVendedor engineVendedor = new cVendedor();
            //mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            string filename = "informe_bdp_direccion.pdf";
            string pasta = Server.MapPath("~/pdf");
            //if(!Directory.Exists(pasta +"//"+ DateTime.Now.ToString("dd_MM_yyyy")))
            //    Directory.CreateDirectory(pasta + "//"+ DateTime.Now.ToString("dd_MM_yyyy"));

            //pasta += "//"+ DateTime.Now.ToString("dd_MM_yyyy");

            clog_arquivos enginelogArquivos = new clog_arquivos();
            mlog_arquivo _logArquivos = null;
            string tela = "Informe operativo vendedores";
            List<mlog_arquivo> _listaLogArquivos = enginelogArquivos.Retornarlog_arquivoPorData(_vendedor.id, DateTime.Now.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), tela);
            if (_listaLogArquivos != null)
            {
                if (_listaLogArquivos.Count > 0)
                {
                    _logArquivos = _listaLogArquivos[0];
                }
            }

            if (_logArquivos == null)
            {
                _logArquivos = new mlog_arquivo();
                _logArquivos.data = DateTime.Now;
                _logArquivos.tela = tela;
                _logArquivos.vendedor = _vendedor;
                _logArquivos.arquivo = string.Empty;

                _logArquivos.Id = Convert.ToInt32(enginelogArquivos.Inserir(_logArquivos));

                filename = tela + "_";
                filename += DateTime.Now.ToString("yyyy_MM_dd");
                filename += "_" + _logArquivos.Id.ToString();
                filename += "_" + _vendedor.id.ToString();
                filename += ".pdf";

                _logArquivos.arquivo = filename;
                //enginelogArquivos.Update(_logArquivos);
            }
            else
            {
                filename = tela + "_";
                filename += DateTime.Now.ToString("yyyy_MM_dd");
                filename += "_" + _logArquivos.Id.ToString();
                filename += "_" + _vendedor.id.ToString();
                filename += ".pdf";

                _logArquivos.arquivo = filename;
                //enginelogArquivos.Update(_logArquivos);
            }


            //if(File.Exists(


            //Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition", "attachment;filename=informe_consolidado.pdf");
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);
            //dgvDados.RenderControl(hw);
            //StringReader sr = new StringReader(sw.ToString());
            //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //pdfDoc.Open();
            //htmlparser.Parse(sr);
            //pdfDoc.Close();
            //Response.Write(pdfDoc);
            //Response.End();
            //dgvDados.AllowPaging = true;
            //dgvDados.DataBind();

            //salvando a cópia local
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gdvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            gdvDados.AllowPaging = true;
            gdvDados.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gdvDados.AllowPaging = false;
                Button1_Click(null, null);

                gdvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gdvDados.HeaderRow.Cells)
                {
                    cell.BackColor = gdvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gdvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gdvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gdvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gdvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the
            /* specified ASP.NET server control at run time. */
        }
    }
}