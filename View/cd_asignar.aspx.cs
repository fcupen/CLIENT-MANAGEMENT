﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_assignar_new : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {

                //PopularGridGeral();
                PopularClientes("todos");
                PopularVendedores("todos");
                if (Session["vendedor_id"] == null)
                    Session["vendedor_id"] = 0;

                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            PopularGridGeral();
            if (Session["vendedor_id"] != null)
            {
                if (Session["vendedor_id"].ToString() != "0")
                {
                    ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(Convert.ToInt32(Session["vendedor_id"].ToString()).ToString()));
                    PopularGridGeral();
                }
            }
        }
        private void PopularVendedores(string asignados)
        {
            cVendedor engineVendedor = new cVendedor();
            if (engineVendedor.PopularGridComAsignadosOuNo(asignados) != null)
            {
                ddlVendedor.DataSource = engineVendedor.PopularGridComAsignadosOuNo(asignados).Tables[0];
                ddlVendedor.DataValueField = "Id";
                ddlVendedor.DataTextField = "NOMBRE";
                ddlVendedor.DataBind();
            }
            else
            {
                Alert.Show("Não há vendedores");
            }
        }

        private void PopularClientes(string asignados)
        {
            cClientes engineClientes = new cClientes();
            if (engineClientes.PopularGridAsignados(asignados) != null)
            {
                ddlCliente.DataSource = engineClientes.PopularGridAsignados(asignados).Tables[0];
                ddlCliente.DataValueField = "Id";
                ddlCliente.DataTextField = "Razon Social";
                ddlCliente.DataBind();
            }
            else
            {
                Alert.Show("Não há vendedores");
            }
        }
        private void PopularGridGeral()
        {
            //cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            //gdvDados.DataSource = engineRelacaoVendedor.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = false;
            //gdvDados.DataBind();    
            cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            lblTotal.Text = "Total: 0 " ;
            DataSet ds = null;
            if (ddlVendedor.SelectedValue != "")
            {
                ds =engineRelacaoVendedor.PopularGrid_2("vendedorpornome", ddlVendedor.SelectedValue);
                gdvDados.DataSource = ds.Tables[0];
                lblTotal.Text = ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                gdvDados.DataSource = null;
            }
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();
            cVendedor enginevendedor = new cVendedor();
            if (ddlVendedor.SelectedValue != "")
            {
                mVendedor _vendedor = enginevendedor.RetornarVendedor(ddlVendedor.SelectedValue);
                lblVendedor.Text = _vendedor.nombre;
            }
            else
            {
                lblVendedor.Text = "";
            }
            
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            cClientes engineCliente = new cClientes();
            mRelacaoVendedor _RelacaoVendedor = new mRelacaoVendedor();
            cVendedor engineVendedor = new cVendedor();
            cClientes engineClientes = new cClientes();

            if (ddlVendedor.SelectedValue == null)
            {
                Alert.Show("Selecione um vendedor");
                return;
            }

            if (ddlCliente.SelectedValue == null)
            {
                Alert.Show("Selecione um cliente");
                return;
            }

            //verificar se existe já o registro pelo produto e vendedor e ano
            mClientes _cliente = (ddlCliente.SelectedValue != null ? engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);
            mVendedor _vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            if (_cliente != null && _vendedor != null)
            {
                mRelacaoVendedor _relacao = engineRelacaoVendedor.RetornarRelacaoVendedorPorVendedorCliente(_vendedor.id.ToString(), _cliente.id.ToString());
                if (_relacao != null)
                {
                    _RelacaoVendedor = _relacao;
                    txtid.Text = _RelacaoVendedor.Id.ToString();
                }
            }

            _RelacaoVendedor.vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            _RelacaoVendedor.cliente = (ddlCliente.SelectedValue != null ? engineClientes.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);
            _RelacaoVendedor.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                _RelacaoVendedor.Id = Convert.ToInt32(engineRelacaoVendedor.Inserir(_RelacaoVendedor)); ;
            }
            else
            {
                engineRelacaoVendedor.Update(_RelacaoVendedor);
            }


            limpar();
            PopularGridGeral();
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlCliente.SelectedIndex = 0;
            ddlVendedor.SelectedIndex = 0;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            mRelacaoVendedor _relacaoVendedor = engineRelacaoVendedor.RetornarRelacaoVendedor(gdvDados.SelectedRow.Cells[1].Text);
            txtid.Text = _relacaoVendedor.Id.ToString();
            PopularVendedores("todos");            
            ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_relacaoVendedor.vendedor.id.ToString()));
            PopularClientes("todos");
            ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(_relacaoVendedor.cliente.id.ToString()));
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            mRelacaoVendedor _relacaoVendedor = new mRelacaoVendedor();
            _relacaoVendedor = engineRelacaoVendedor.RetornarRelacaoVendedor(gdvDados.Rows[e.RowIndex].Cells[1].Text);
            engineRelacaoVendedor.Deletar(_relacaoVendedor);
            limpar();
            PopularGridGeral();
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridGeral();
            Session["vendedor_id"] = ddlVendedor.SelectedValue.ToString();
        }

        protected void rdVendedorNoAsignados_CheckedChanged(object sender, EventArgs e)
        {
            PopularVendedores("nao");  
        }

        protected void rdVendedorAsignar_CheckedChanged(object sender, EventArgs e)
        {
            PopularVendedores("sim");  
        }

        protected void rdClienteAsignar_CheckedChanged(object sender, EventArgs e)
        {
            PopularClientes("sim");  

        }

        protected void rdClienteNoAsignados_CheckedChanged(object sender, EventArgs e)
        {
            PopularClientes("nao");  
        }
    }
}