﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_assignar_cliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
            if (!Page.IsPostBack)
            {

                //PopularGridGeral();
                PopularClientes("todos");
                PopularProdutos("todos");
                //if (Session["vendedor_id"] == null)
                //    Session["vendedor_id"] = 0;
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            PopularGridGeral();
            //if (Session["vendedor_id"] != null)
            //{
            //    if (Session["vendedor_id"].ToString() != "0")
            //    {
            //        ddlProduto.SelectedIndex = ddlProduto.Items.IndexOf(ddlProduto.Items.FindByValue(Convert.ToInt32(Session["vendedor_id"].ToString()).ToString()));
            //        PopularGridGeral();
            //    }
            //}
        }
        private void PopularProdutos(string asignados)
        {
            cProduto engineProduto = new cProduto();
            if (engineProduto.PopularGrid(0) != null)
            {
                ddlProduto.DataSource = engineProduto.PopularGrid(0).Tables[0];
                ddlProduto.DataValueField = "Id";
                ddlProduto.DataTextField = "NOMBRE";
                ddlProduto.DataBind();
            }
            else
            {
                Alert.Show("Não há produtos");
            }
        }

        private void PopularClientes(string asignados)
        {
            cClientes engineClientes = new cClientes();
            if (engineClientes.PopularGridAsignados(asignados) != null)
            {
                ddlCliente.DataSource = engineClientes.PopularGridAsignados(asignados).Tables[0];
                ddlCliente.DataValueField = "Id";
                ddlCliente.DataTextField = "Razon Social";
                ddlCliente.DataBind();
            }
            else
            {
                Alert.Show("Não há vendedores");
            }
        }
        private void PopularGridGeral()
        {
            //cRelacaoCliente engineRelacaoVendedor = new cRelacaoCliente();
            //gdvDados.DataSource = engineRelacaoVendedor.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = false;
            //gdvDados.DataBind();    
            cRelacaoCliente engineRelacaoVendedor = new cRelacaoCliente();
            //if (ddlVendedor.SelectedValue != "")
            //{
                gdvDados.DataSource = engineRelacaoVendedor.PopularGrid(0);
            //}
            //else
            //{
            //    gdvDados.DataSource = null;
            //}
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();
            cVendedor enginevendedor = new cVendedor();
            //if (ddlVendedor.SelectedValue != "")
            //{
            //    mVendedor _vendedor = enginevendedor.RetornarVendedor(ddlVendedor.SelectedValue);
            //    lblVendedor.Text = _vendedor.nombre;
            //}
            //else
            //{
            //    lblVendedor.Text = "";
            //}

        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cRelacaoCliente engineRelacaoCliente = new cRelacaoCliente();
            cClientes engineCliente = new cClientes();
            mRelacaoCliente _RelacaoCliente = new mRelacaoCliente();
            cProduto engineProduto = new cProduto();
            cClientes engineClientes = new cClientes();

            if (ddlProduto.SelectedValue == null)
            {
                Alert.Show("Selecione um producto");
                return;
            }

            if (ddlCliente.SelectedValue == null)
            {
                Alert.Show("Selecione um cliente");
                return;
            }

            //verificar se existe já o registro pelo produto e vendedor e ano
            mClientes _cliente = (ddlCliente.SelectedValue != null ? engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);
            mProduto _produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
            if (_cliente != null && _produto != null)
            {
                mRelacaoCliente _relacao = engineRelacaoCliente.RetornarRelacaoVendedorPorProdutoCliente(_produto.Id.ToString(), _cliente.id.ToString());
                if (_relacao != null)
                {
                    _RelacaoCliente = _relacao;
                    txtid.Text = _RelacaoCliente.Id.ToString();
                }
            }

            _RelacaoCliente.produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
            _RelacaoCliente.cliente = (ddlCliente.SelectedValue != null ? engineClientes.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);
            _RelacaoCliente.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                _RelacaoCliente.Id = Convert.ToInt32(engineRelacaoCliente.Inserir(_RelacaoCliente)); ;
            }
            else
            {
                engineRelacaoCliente.Update(_RelacaoCliente);
            }


            limpar();
            PopularGridGeral();
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlCliente.SelectedIndex = 0;
            ddlProduto.SelectedIndex = 0;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cRelacaoCliente engineRelacaoCliente = new cRelacaoCliente();
            mRelacaoCliente _relacaoVendedor = engineRelacaoCliente.Retornarrelacaocliente(gdvDados.SelectedRow.Cells[2].Text);
            txtid.Text = _relacaoVendedor.Id.ToString();
            PopularProdutos("todos");
            ddlProduto.SelectedIndex = ddlProduto.Items.IndexOf(ddlProduto.Items.FindByValue(_relacaoVendedor.produto.Id.ToString()));
            PopularClientes("todos");
            ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(_relacaoVendedor.cliente.id.ToString()));
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cRelacaoCliente engineRelacaoCliente = new cRelacaoCliente();
            mRelacaoCliente _relacaoVendedor = new mRelacaoCliente();
            _relacaoVendedor = engineRelacaoCliente.Retornarrelacaocliente(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            engineRelacaoCliente.Deletar(_relacaoVendedor);
            limpar();
            PopularGridGeral();
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            //PopularGridGeral();
            //Session["vendedor_id"] = ddlVendedor.SelectedValue.ToString();
        }

        protected void rdVendedorNoAsignados_CheckedChanged(object sender, EventArgs e)
        {
            //PopularProdutos("nao");
        }

        protected void rdVendedorAsignar_CheckedChanged(object sender, EventArgs e)
        {
            //PopularProdutos("sim");
        }

        protected void rdClienteAsignar_CheckedChanged(object sender, EventArgs e)
        {
            //PopularClientes("sim");

        }

        protected void rdClienteNoAsignados_CheckedChanged(object sender, EventArgs e)
        {
            //PopularClientes("nao");
        }
    }
}