﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_vendedor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                //PopularVendedores();
                //PopularClientes();
            }
        }

        //private void PopularVendedores()
        //{
        //    cVendedor engineVendedor = new cVendedor();
        //    if (engineVendedor.PopularGrid(0) != null)
        //    {
        //        ddlVendedor.DataSource = engineVendedor.PopularGrid(0).Tables[0];
        //        ddlVendedor.DataValueField = "Id";
        //        ddlVendedor.DataTextField = "NOMBRE";
        //        ddlVendedor.DataBind();
        //    }
        //    else
        //    {
        //        Alert.Show("Não há vendedores");
        //    }
        //}

        //private void PopularClientes()
        //{
        //    cClientes engineClientes = new cClientes();
        //    if (engineClientes.PopularGrid(0) != null)
        //    {
        //        ddlCliente.DataSource = engineClientes.PopularGrid(0).Tables[0];
        //        ddlCliente.DataValueField = "Id";
        //        ddlCliente.DataTextField = "Razon Social";
        //        ddlCliente.DataBind();
        //    }
        //    else
        //    {
        //        Alert.Show("Não há vendedores");
        //    }
        //}
        private void PopularGridGeral(int limite)
        {
            //cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            //gdvDados.DataSource = engineRelacaoVendedor.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = false;
            //gdvDados.DataBind();
            cVendedor engineVendedor = new cVendedor();
            gdvDados.DataSource = engineVendedor.PopularGrid_2();
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            //cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            //mRelacaoVendedor _RelacaoVendedor = new mRelacaoVendedor();
            //cVendedor engineVendedor = new cVendedor();
            //cClientes engineClientes = new cClientes();

            //if (ddlVendedor.SelectedValue == null)
            //{
            //    Alert.Show("Selecione um vendedor");
            //    return;
            //}

            //if (ddlCliente.SelectedValue == null)
            //{
            //    Alert.Show("Selecione um cliente");
            //    return;
            //}

           
            //_RelacaoVendedor.vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            //_RelacaoVendedor.cliente = (ddlCliente.SelectedValue != null ? engineClientes.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);           
            //_RelacaoVendedor.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            //if (txtid.Text == string.Empty)
            //{
            //    _RelacaoVendedor.Id = Convert.ToInt32(engineRelacaoVendedor.Inserir(_RelacaoVendedor)); ;
            //}
            //else
            //{
            //    engineRelacaoVendedor.Update(_RelacaoVendedor);
            //}



            //limpar();
            //PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            //ddlCliente.SelectedIndex = 0;
            //ddlVendedor.SelectedIndex = 0;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            //mRelacaoVendedor _relacaoVendedor = engineRelacaoVendedor.RetornarRelacaoVendedor(gdvDados.SelectedRow.Cells[2].Text);
            //txtid.Text = _relacaoVendedor.Id.ToString();
            //ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_relacaoVendedor.vendedor.id.ToString()));
            //ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(_relacaoVendedor.cliente.id.ToString()));            

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            //mRelacaoVendedor _relacaoVendedor = new mRelacaoVendedor();
            //_relacaoVendedor = engineRelacaoVendedor.RetornarRelacaoVendedor(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            //engineRelacaoVendedor.Deletar(_relacaoVendedor);
            //limpar();
            //PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void gdvDados_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);            
            if (e.CommandName == "meta")
            {
                Session["vendedor_id"] = id.ToString();
                Response.Redirect("~/View/cd_metas_view.aspx");                
            }
            else if (e.CommandName == "cartera")
            {
                Session["vendedor_id"] = id.ToString();
                Response.Redirect("~/View/cd_asignar.aspx");
            }
        }

    }
}