﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_metas.aspx.cs" Inherits="Clients_Management.View.cd_metas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
 
  <div class="container">
       <ol class="breadcrumb">
  <li><a id="A2" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">PLANIFICACION</a></li>
  <li class="active">ASIGNACION DE METAS</li>
</ol>
  </div>
</div>
<div id="Div1"></div>
     
<div id="about_section_1">
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     
          
        
				 <div class="row">
 
					<div class="col-md-12">
					
					<div class="form-horizontal well" >
					<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
                            <div class="col-md-3">
                            <label>Selecione Vendedor:</label>
							 <asp:DropDownList ID="ddlVendedor" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVendedor_SelectedIndexChanged"></asp:DropDownList> 
							</div>
                            <div class="col-md-3">
                             <label>Selecione Producto:</label>
							 <asp:DropDownList ID="ddlProduto" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlProduto_SelectedIndexChanged"></asp:DropDownList>
							</div>

						</div>
					</div>
				</div>
				
				
				
			   
	   
	    <label>Digite Metas Mensales:</label>
 	
	 <div class="form-group">

     <label for="inputEmail3" class="col-md-2 control-label">Enero:</label>
  		  <div class="col-sm-2">
            <asp:TextBox ID="txtid" class="form-control" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="Enero" class="form-control" runat="server" placeholder="Enero"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Febrero:</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Febrero" class="form-control" runat="server" placeholder="Febrero"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Marzo:</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Marzo" class="form-control" runat="server" placeholder="Marzo"></asp:TextBox>
	  </div>
        
	    
	</div>
	
	
	<div class="form-group">
     
	   <label for="inputEmail3" class="col-sm-2 control-label">Abril:</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Abril" class="form-control" runat="server" placeholder="Abril"></asp:TextBox>
	  </div>
	  <label for="inputEmail3" class="col-sm-2 control-label">Mayo:</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Mayo" class="form-control" runat="server" placeholder="Mayo"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Junio:</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Junio" class="form-control" runat="server" placeholder="Junio"></asp:TextBox>
	  </div>
	    
	</div>
	
	
	<div class="form-group">
     <label for="inputEmail3" class="col-sm-2 control-label">Julio:</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Julio" class="form-control" runat="server" placeholder="Julio"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Agosto:</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Agosto" class="form-control" runat="server" placeholder="Agosto"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Septiembre:</label>
  		  <div class="col-sm-2">
                <asp:TextBox ID="Septiembre" class="form-control" runat="server" placeholder="Septiembre"></asp:TextBox>
	  </div>
	    
	</div>
	
	
	<div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Octubre:</label>     
  		  <div class="col-sm-2">
                <asp:TextBox ID="Octubre" class="form-control" runat="server" placeholder="Octubre"></asp:TextBox>
	  </div>	          
  		<label for="inputEmail3" class="col-sm-2 control-label">Noviembre:</label>     
          <div class="col-sm-2">
      <asp:TextBox ID="Noviembre" class="form-control" runat="server" placeholder="Noviembre"></asp:TextBox>
	  </div>
	  
	  <label for="inputEmail3" class="col-sm-2 control-label">Diciembre:</label>        
  		  <div class="col-sm-2">
                <asp:TextBox ID="Diciembre" class="form-control" runat="server" placeholder="Diciembre"></asp:TextBox>
	  </div>
	    
	</div>
	
					<div class="form-group">
					<div class="rows">
						
							<div class="col-lg-12" align="center">
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Cadastrar" OnClick="btnSalvar_Click" />                                
							</div>
						
					</div>
				</div>
  
	   
	   
	   	

	  
     </div>
	 
   </div>

       </div>
  
  
<div class="row">  
  

<div class="col-lg-12" align="center">
    <asp:Button ID="btnVoltar" runat="server" class="btn btn-primary" Text="Volver Planificacion" PostBackUrl="~/View/planificacion.aspx" /> &nbsp;&nbsp;&nbsp;&nbsp;
    <a id="A1" runat="server" href="~/View/cd_ventas.aspx" target="_self" class="btn btn-primary">Metas de Ventas </a>&nbsp;
</div>
    <p></p>


</div>
  
  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
