﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_bdn_dominante_direccion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                PopularVendedores();
                PopularGridGeral(0);
                PopularProdutos();
                rdTodos.Checked = true;
            }
        }
        private void PopularVendedores()
        {

            cVendedor engineVendedor = new cVendedor();
            DataSet ds = engineVendedor.PopularGrid(0);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendedor.DataSource = ds.Tables[0];
                    ddlVendedor.DataValueField = "Id";
                    ddlVendedor.DataTextField = "NOMBRE";
                    ddlVendedor.DataBind();
                }
                else
                {
                    ddlVendedor.DataSource = null;
                    ddlVendedor.Items.Clear();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }

        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];

            ddlFiltro.DataSource = engineProduto.PopularGridVendedor(_vendedor.id).Tables[0];
            ddlFiltro.DataValueField = "Id";
            ddlFiltro.DataTextField = "NOMBRE";
            ddlFiltro.DataBind();
        }

        private void PopularGridGeral(int limite)
        {
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            cVendedor engineVendedor= new cVendedor();

            if (rdVendedorTodos.Checked)
                _vendedor = new mVendedor();

            if(rdVendedor.Checked)
                _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());

            //cBDN engineBDN = new cBDN();            
            //gdvDados.DataSource = engineBDN.PopularGrid(limite,_vendedor.id.ToString());
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = true;
            //gdvDados.DataBind();

            cBDN engineBDN = new cBDN();
            DataSet ds = new DataSet();
            ds = engineBDN.PopularGrid_2PorVendedor(_vendedor.id);
            double media = 0;
            double total = 0;
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["ESTADO"].ToString() == "Vigente" && ds.Tables[0].Rows[i]["Expirado"].ToString() == "N")
                    {
                        totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["TOTAL NETO"].ToString());
                        total++;
                    }
                }
                media = totalvalor / total;
                lblTotalCotizacoes.Text = "Cotizaciones :" + total;
                lblTotal.Text = "TOTAL :" + totalvalor.ToString("N0");
                lblMediaCotizacao.Text = "Media Cotización :" + (totalvalor / total).ToString("N2");
            }

            
            gdvDados.DataSource = engineBDN.PopularGrid_2PorVendedorDominanteDireccion(_vendedor.id, media);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();


            double mediadominante = 0;
            double totaldominante = 0;
            double totalanomala = 0;
            double totalanomalavalor = 0;
            double percentagemanomala = (totalvalor * 40) / 100;
            foreach (GridViewRow r in gdvDados.Rows)
            {
                if (r.Cells[8].Text != string.Empty)
                {
                    double valoratual = Convert.ToDouble(r.Cells[8].Text);

                    if (r.Cells[10].Text == "N")
                    {
                        if (valoratual > percentagemanomala)
                        {
                            r.BackColor = System.Drawing.Color.LightYellow;
                            //r.Cells[5].BackColor = System.Drawing.Color.LightBlue;
                            totalanomala++;
                            totalanomalavalor += valoratual;
                        }
                        else if (valoratual > media)
                        {
                            r.BackColor = System.Drawing.Color.LightSalmon;
                            //r.Cells[5].BackColor = System.Drawing.Color.LightSeaGreen;
                            totaldominante++;
                            mediadominante += valoratual;
                        }
                    }


                }
                else
                {
                    // r.BackColor = System.Drawing.Color.Orange;
                    // r.Cells[0].BackColor = System.Drawing.Color.Green;
                }
            }


            lblTotalDominate.Text = "Total Cotización Dominante: " + totaldominante;
            lblMediaDominante.Text = "Porcentaje Cotización Dominante:" + (totaldominante > 0 ? ((totalanomalavalor / totalvalor) * 100).ToString("N2") : "0");
            lblTotalAnomala.Text = "Total Anomala:" + totalanomala;

            Session["gdvDadosTable"] = ds.Tables[0];
            Session["ordenamento"] = "asc";

        }



        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gdvDados_RowDataBound1(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }

            // you only want to check DataRow type, and not headers, footers etc.
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // you already know you're looking at this row, so check your cell text
                //    if (e.Row.Cells(1).Text == "C6N")
                //    {
                //        e.Row.BackColor = System.Drawing.Color.Red;
                //    }
                e.Row.Cells[8].Text = Convert.ToDouble(e.Row.Cells[8].Text).ToString("N0");
                e.Row.Cells[8].HorizontalAlign = HorizontalAlign.Right;
            }
        }

        protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        public void PopularGridProduto()
        {
            //if (!rdProduto.Checked)
            //    return;
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            cVendedor engineVendedor = new cVendedor();
            if (rdVendedorTodos.Checked)
                _vendedor = new mVendedor();

            if (rdVendedor.Checked)
                _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());

            //cBDN engineBDN = new cBDN();            
            //gdvDados.DataSource = engineBDN.PopularGrid(limite,_vendedor.id.ToString());
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = true;
            //gdvDados.DataBind();

            cBDN engineBDN = new cBDN();
            DataSet ds = new DataSet();
            cProduto engineProduto = new cProduto();
            mProduto _producto = engineProduto.RetornarProduto(ddlFiltro.SelectedValue.ToString());
            if (rdTodos.Checked)
                _producto = new mProduto();
            ds = engineBDN.PopularGrid_2PorVendedorProduto(_vendedor.id, _producto.Id);
            double media = 0;
            double total = 0;
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["ESTADO"].ToString() == "Vigente" && ds.Tables[0].Rows[i]["Expirado"].ToString() == "N")
                    {
                        totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["TOTAL NETO"].ToString());
                        total++;
                    }
                }
                media = totalvalor / total;
                lblTotalCotizacoes.Text = "Cotizaciones :" + total;
                lblTotal.Text = "TOTAL :" + totalvalor.ToString("N0");
                lblMediaCotizacao.Text = "Media Cotización :" + (totalvalor / total).ToString("N2");
            }

            gdvDados.DataSource = engineBDN.PopularGrid_2PorVendedorProdutoDominanteDireccion(_vendedor.id, _producto.Id, media);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();


            double mediadominante = 0;
            double totaldominante = 0;
            double totalanomala = 0;
            double totalanomalavalor = 0;
            double percentagemanomala = (totalvalor * 40) / 100;
            foreach (GridViewRow r in gdvDados.Rows)
            {
                if (r.Cells[8].Text != string.Empty)
                {
                    double valoratual = Convert.ToDouble(r.Cells[8].Text);

                    if (r.Cells[10].Text == "N")
                    {
                        if (valoratual > percentagemanomala)
                        {
                            r.BackColor = System.Drawing.Color.LightYellow;
                            //r.Cells[5].BackColor = System.Drawing.Color.LightBlue;
                            totalanomala++;
                            totalanomalavalor += valoratual;
                        }
                        else if (valoratual > media)
                        {
                            r.BackColor = System.Drawing.Color.LightGreen;
                            //r.Cells[5].BackColor = System.Drawing.Color.LightSeaGreen;
                            totaldominante++;
                            mediadominante += valoratual;
                        }
                    }


                }
                else
                {
                    // r.BackColor = System.Drawing.Color.Orange;
                    // r.Cells[0].BackColor = System.Drawing.Color.Green;
                }
            }


            lblTotalDominate.Text = "Total Cotización Dominante: " + totaldominante;
            lblMediaDominante.Text = "Porcentaje Cotización Dominante:" + (totaldominante > 0 ? ((totalanomalavalor / totalvalor) * 100).ToString("N2") : "0");
            lblTotalAnomala.Text = "Total Anomala:" + totalanomala;

            Session["gdvDadosTable"] = ds.Tables[0];
            Session["ordenamento"] = "asc";
        }

        protected void rdTodos_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridGeral(0);
        }

        protected void rdProduto_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        protected void rdTodosVendedores_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        protected void rdVendedor_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }


        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gdvDados.AllowPaging = false;


                gdvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gdvDados.HeaderRow.Cells)
                {
                    cell.BackColor = gdvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gdvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gdvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gdvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gdvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string filename = "panel_control_direccion.pdf";
            //salvando a cópia local
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gdvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            gdvDados.AllowPaging = true;
            gdvDados.DataBind();
        }
    }
}