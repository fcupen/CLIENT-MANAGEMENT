﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_claves_gestion_gantt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                PopularGridGeralFiltro(ddlAno.Text, ddlMes.Text);
            }
        }
        private void PopularGridGeral(int limite)
        {
            cclavesgestioncartagantt engineclavesgestioncartagantt = new cclavesgestioncartagantt();
            gdvDados.DataSource = engineclavesgestioncartagantt.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }

        private void PopularGridGeralFiltro(string ano, string mes)
        {
            cParametrosCartaGantt engineclavesgestioncartagantt = new cParametrosCartaGantt();
            ddlFecha.DataSource = engineclavesgestioncartagantt.PopularGridAnoMes(ano, mes);
            ddlFecha.DataValueField = "fecha";
            ddlFecha.DataTextField = "fecha";
            ddlFecha.DataBind();

        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cclavesgestioncartagantt engineclavesgestioncartagantt = new cclavesgestioncartagantt();
            mclavesgestioncartagantt _clavesgestioncartagantt = new mclavesgestioncartagantt();

            _clavesgestioncartagantt.ano = ddlAno.Text;
            _clavesgestioncartagantt.mes = ddlMes.Text;
            _clavesgestioncartagantt.fecha = ddlFecha.Text;
            _clavesgestioncartagantt.clave = ddlClave.Text;
            _clavesgestioncartagantt.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                _clavesgestioncartagantt.Id = Convert.ToInt32(engineclavesgestioncartagantt.Inserir(_clavesgestioncartagantt)); ;
            }
            else
            {
                engineclavesgestioncartagantt.Update(_clavesgestioncartagantt);
            }



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlAno.SelectedIndex = 0;
            ddlMes.SelectedIndex = 0;
            ddlClave.SelectedIndex = 0;
            ddlFecha.SelectedIndex = 0;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cclavesgestioncartagantt engineclavesgestioncartagantt = new cclavesgestioncartagantt();
            mclavesgestioncartagantt _clavesgestioncartagantt = engineclavesgestioncartagantt.Retornarclavesgestioncartagantt(gdvDados.SelectedRow.Cells[2].Text);
            txtid.Text = _clavesgestioncartagantt.Id.ToString();

            ddlAno.Text = _clavesgestioncartagantt.ano.ToString();
            ddlMes.Text = _clavesgestioncartagantt.mes;
            PopularGridGeralFiltro(ddlAno.Text, ddlMes.Text);
            ddlClave.Text = _clavesgestioncartagantt.clave.ToString();
            ddlFecha.Text = _clavesgestioncartagantt.fecha.ToString();
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cclavesgestioncartagantt engineclavesgestioncartagantt = new cclavesgestioncartagantt();
            mclavesgestioncartagantt _clavesgestioncartagantt = new mclavesgestioncartagantt();
            _clavesgestioncartagantt = engineclavesgestioncartagantt.Retornarclavesgestioncartagantt(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            engineclavesgestioncartagantt.Deletar(_clavesgestioncartagantt);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void ddlAnoFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            //PopularGridGeralFiltro(ddlAnoFiltro.Text, ddlMesFiltro.Text);
        }

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridGeralFiltro(ddlAno.Text, ddlMes.Text);
        }
    }
}