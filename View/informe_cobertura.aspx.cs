﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class informe_cobertura : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();

                ddlMes.SelectedIndex = DateTime.Now.Month - 1;
                btnSalvar_Click(null,null);
                //PopularClientes();

                lblData.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
        }

        //private void PopularClientes()
        //{
        //    cClientes engineClientes = new cClientes();
        //    mVendedor _vendedor = (mVendedor)Session["vendedor"];
        //    if (engineClientes.PopularGridPorVendedor(_vendedor.id) != null)
        //    {
        //        ddlClientes.DataSource = engineClientes.PopularGridPorVendedor(_vendedor.id).Tables[0];
        //        ddlClientes.DataValueField = "Id";
        //        ddlClientes.DataTextField = "Razon Social";
        //        ddlClientes.DataBind();
        //    }
        //    else
        //    {
        //        Alert.Show("Não há clientes");
        //    }
        //}

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            string mesescrito = ddlMes.Text;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            cInformediario engineInformediario = new cInformediario();
            cClientes engineClientes = new cClientes();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            DataSet dsClientes = new DataSet();
            DataSet dsCalculos;


            DataSet ds= engineParametrosCartaGantt.PopularGridAnoMesOrdenado(ddlAno.Text, mesescrito);

            int[] totais_dias = new int[ds.Tables[0].Rows.Count +1];

            DataTable dtNovo = new DataTable("Dados");

            dtNovo.Columns.Add("Dia");//0
            int indice = 1;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                DataColumn dc = new DataColumn();
                dc.Caption = linha["dia"].ToString();
                dc.ColumnName = linha["dia"].ToString();
                int space=1;
                string nome = linha["dia"].ToString();
                while(dtNovo.Columns.Contains(nome))
                {
                    nome = linha["dia"].ToString() + new string(' ', space);
                    dc.ColumnName = nome;
                    space++;
                }

                dc.DataType = System.Type.GetType("System.String");
                dtNovo.Columns.Add(dc);
                indice++;
            }
            dtNovo.Columns.Add(" ");
            
            DataRow dr;
            dr = dtNovo.NewRow();
            dr[0] = "Fecha";
            int contador = 1;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dr[contador]=linha["fecha"].ToString();
                totais_dias[contador-1] = 0;
                contador++;
            }
            totais_dias[contador - 1] = 0;

            dr[contador] = "Total";
            dtNovo.Rows.Add(dr);


            //Contacto Abortado
            dr = dtNovo.NewRow();
            dr[0] = "Contacto Abortado";
            contador = 1;
            int total_abortado = 0;
            int abortados=0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedor(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex+1).ToString(), linha["fecha"].ToString(), "0",_vendedor.id.ToString());
                if (dsCalculos != null)                
                    abortados = dsCalculos.Tables[0].Rows.Count;
                else
                    abortados=0;
                
                dr[contador]=abortados;
                total_abortado += abortados;
                //totais_dias[contador-1] += abortados;
                contador++;
            }
            //totais_dias[contador-1] += abortados;
            dr[contador] = abortados.ToString();
            dtNovo.Rows.Add(dr);

            //Contacto no Planificado
            dr = dtNovo.NewRow();
            dr[0] = "Contacto no Planificado";
            contador = 1;
            int total_noplanificado = 0;
            int noplanificado = 0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedor(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex + 1).ToString(), linha["fecha"].ToString(), "1", _vendedor.id.ToString());
                if (dsCalculos != null)
                    noplanificado = dsCalculos.Tables[0].Rows.Count;
                else
                    noplanificado = 0;

                dr[contador] = noplanificado;
                total_noplanificado += noplanificado;
                totais_dias[contador-1] += noplanificado;
                contador++;
            }
            totais_dias[contador-1] += noplanificado;
            dr[contador] = total_noplanificado.ToString();
            dtNovo.Rows.Add(dr);

            //Contacto Telefónico
            dr = dtNovo.NewRow();
            dr[0] = "Contacto Telefónico";
            contador = 1;
            int total_telefonico = 0;
            int telefonico = 0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedor(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex + 1).ToString(), linha["fecha"].ToString(), "2", _vendedor.id.ToString());
                if (dsCalculos != null)
                    telefonico = dsCalculos.Tables[0].Rows.Count;
                else
                    telefonico = 0;

                dr[contador] = telefonico;
                total_telefonico += telefonico;
               // totais_dias[contador-1] += telefonico;
                contador++;
            }
            //totais_dias[contador-1] += telefonico;
            dr[contador] = total_telefonico.ToString();
            dtNovo.Rows.Add(dr);

            //Contacto Efectivo
            dr = dtNovo.NewRow();
            dr[0] = "Contacto Efectivo";
            contador = 1;
            int total_efectivo = 0;
            int efectivo = 0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedor(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex + 1).ToString(), linha["fecha"].ToString(), "3", _vendedor.id.ToString());
                if (dsCalculos != null)
                    efectivo = dsCalculos.Tables[0].Rows.Count;
                else
                    efectivo = 0;

                dr[contador] = efectivo;
                total_efectivo += efectivo;
                totais_dias[contador-1] += efectivo;
                contador++;
            }
            totais_dias[contador-1] += efectivo;
            dr[contador] = total_efectivo.ToString();
            dtNovo.Rows.Add(dr);

            //Follow up
            dr = dtNovo.NewRow();
            dr[0] = "Contacto Follow up";
            contador = 1;
            int total_followup = 0;
            int followup = 0;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dsCalculos = new DataSet();
                dsCalculos = engineInformediario.RetornarVisitasPorStatusSemOtroPorVendedor(ddlAno.Text, Convert.ToInt32(ddlMes.SelectedIndex + 1).ToString(), linha["fecha"].ToString(), "4", _vendedor.id.ToString());
                if (dsCalculos != null)
                    followup = dsCalculos.Tables[0].Rows.Count;
                else
                    followup = 0;

                dr[contador] = followup;
                total_followup += followup;
                //totais_dias[contador-1] += followup;
                contador++;
            }
            //totais_dias[contador-1] += followup;
            dr[contador] = total_followup.ToString();
            dtNovo.Rows.Add(dr);

            //total
            dr = dtNovo.NewRow();
            dr[0] = "Total";
            contador = 1;
            foreach (DataRow linha in ds.Tables[0].Rows)
            {
                dr[contador] = totais_dias[contador - 1].ToString();
                contador++;
            }
            totais_dias[contador - 1] = total_noplanificado+total_efectivo;
            dr[contador] = totais_dias[contador-1].ToString();
            dtNovo.Rows.Add(dr);

            gdvDados.DataSource = dtNovo;
            gdvDados.DataBind();


            double dias_uteis = 0;
            double dias_uteis_atuais = 0;
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), (ddlMes.SelectedIndex + 1).ToString(), (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dias_uteis++;
                        if (i < (DateTime.Now.Day - 1))
                        {
                            dias_uteis_atuais++;
                        }
                    }
                }
            }
            double promovida = totais_dias[contador - 1] / dias_uteis_atuais;
            //double promovida = total_noplanificado + total_efectivo;
            //promovida = promovida / ds.Tables[0].Rows.Count;
            lblPromovida.Text = "PROMVDIA: " + promovida.ToString("N2");
            lblTitulo.Text = _vendedor.nombre + " - " + mesescrito + " - " +ddlAno.Text;

        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gdvDados.AllowPaging = false;
                btnSalvar_Click(null, null);

                gdvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gdvDados.HeaderRow.Cells)
                {
                    cell.BackColor = gdvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gdvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gdvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gdvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gdvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //salvando a cópia local
            string filename = "informe.pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gdvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 8f, 8f, 8f, 8f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            gdvDados.AllowPaging = true;
            gdvDados.DataBind();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}