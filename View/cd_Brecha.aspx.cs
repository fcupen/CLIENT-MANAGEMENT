﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_Brecha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                PopularVendedores("todos");
                PopularProdutos();
                PopularClientes();
            }

            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                {
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
                    lblTituloRelatorio.Text = _vendedor.nombre + " - " + DateTime.Now.Year.ToString();
                }

            }
        }
        private void PopularClientes()
        {
            cClientes engineClientes = new cClientes();
            ddlCliente.DataSource = engineClientes.PopularGrid(0).Tables[0];
            ddlCliente.DataValueField = "Id";
            ddlCliente.DataTextField = "Razon Social";
            ddlCliente.DataBind();
        }
        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            ddlProduto.DataSource = engineProduto.PopularGrid(0).Tables[0];
            ddlProduto.DataValueField = "Id";
            ddlProduto.DataTextField = "NOMBRE";
            ddlProduto.DataBind();
        }
        private void PopularGridGeral(int limite)
        {
            cBrecha engineBrecha = new cBrecha();
            gdvDados.DataSource = engineBrecha.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }

        private void PopularVendedores(string asignados)
        {
            cVendedor engineVendedor = new cVendedor();
            if (engineVendedor.PopularGridComAsignadosOuNo(asignados) != null)
            {
                ddlVendedor.DataSource = engineVendedor.PopularGridComAsignadosOuNo(asignados).Tables[0];
                ddlVendedor.DataValueField = "Id";
                ddlVendedor.DataTextField = "NOMBRE";
                ddlVendedor.DataBind();
            }
            else
            {
                Alert.Show("Não há vendedores");
            }
        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cBrecha engineBrecha = new cBrecha();
            mBrecha _brecha = new mBrecha();
            cVendedor engineVendedor = new cVendedor();
            cClientes engineCliente = new cClientes();
            cProduto engineProduto = new cProduto();

            if (ddlVendedor.SelectedValue != null)
            {
                if (ddlVendedor.SelectedValue != "")
                {
                    _brecha.vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
                    //if (rdProduto.Checked)
                        _brecha.produto = engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString());
                    //else
                    //    _brecha.produto = null;

                    //if (rdCliente.Checked)
                        _brecha.cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString());
                    //else
                    //    _brecha.cliente = null;

                    _brecha.valor = Convert.ToDecimal(txtValor.Text);
                    _brecha.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);
                    if (txtid.Text == string.Empty)
                    {
                        _brecha.Id = Convert.ToInt32(engineBrecha.Inserir(_brecha)); ;
                    }
                    else
                    {
                        engineBrecha.Update(_brecha);
                    }

                }
                else
                {
                    Alert.Show("Selecione um vendedor");
                }
            }
            else
            {
                Alert.Show("Selecione um vendedor");
            }





            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlVendedor.SelectedIndex = 0;
            ddlProduto.SelectedIndex = 0;
            txtValor.Text = "0";
            //txtClientesMes.Text = "0";
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cBrecha engineBrecha = new cBrecha();
            mBrecha _brecha = engineBrecha.RetornarBrecha(gdvDados.SelectedRow.Cells[2].Text);
            txtid.Text = _brecha.Id.ToString();
            //txtClientesMes.Text = _vendedorCliente.clientes.ToString();
            ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_brecha.vendedor.id.ToString()));
            if(_brecha.produto!=null)
                ddlProduto.SelectedIndex = ddlProduto.Items.IndexOf(ddlProduto.Items.FindByValue(_brecha.produto.Id.ToString()));
            
            if(_brecha.cliente!=null)
                ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(_brecha.cliente.id.ToString()));

            txtValor.Text = _brecha.valor.ToString("N2");
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cBrecha engineBrecha = new cBrecha();
            mBrecha _brecha = new mBrecha();
            _brecha = engineBrecha.RetornarBrecha(gdvDados.Rows[e.RowIndex].Cells[0].Text);
            engineBrecha.Deletar(_brecha);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                    if (j == 6)
                    {
                        double valor = 0;
                        double valorformatado=0;
                        valor = Convert.ToDouble(e.Row.Cells[j].Text);
                        valorformatado = Math.Floor(valor);
                        e.Row.Cells[j].Text =valorformatado.ToString();
                    }
                }

            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gdvDados.AllowPaging = false;
                PopularGridGeral(0);

                gdvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gdvDados.HeaderRow.Cells)
                {
                    cell.BackColor = gdvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gdvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gdvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gdvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gdvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //salvando a cópia local
            string filename = "informe.pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gdvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 8f, 8f, 8f, 8f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            gdvDados.AllowPaging = true;
            gdvDados.DataBind();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

       
    }
}