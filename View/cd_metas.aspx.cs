﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_metas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
            if (!Page.IsPostBack)
            {
                if (Session["vendedor_id"] == null)
                    Session["vendedor_id"] = 0;
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                PopularVendedores();
                PopularProdutos();
            }

            if (Session["vendedor_id"].ToString() != "0")
            {
                ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(Convert.ToInt32(Session["vendedor_id"].ToString()).ToString()));
                CarregarMetas();
            }
        }

        private void PopularVendedores()
        {
            cVendedor engineVendedor = new cVendedor();
            ddlVendedor.DataSource = engineVendedor.PopularGrid(0).Tables[0];
            ddlVendedor.DataValueField = "Id";
            ddlVendedor.DataTextField = "NOMBRE";
            ddlVendedor.DataBind();
        }

        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            ddlProduto.DataSource = engineProduto.PopularGrid(0).Tables[0];
            ddlProduto.DataValueField = "Id";
            ddlProduto.DataTextField = "NOMBRE";
            ddlProduto.DataBind();
        }
        private void PopularGridGeral(int limite)
        {
            //cMetas engineMetas= new cMetas();
            //gdvDados.DataSource = engineMetas.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = true;
            //gdvDados.DataBind();
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cMetas engineMetas = new cMetas();
            mMetas _Metas = new mMetas();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();

            if (ddlVendedor.SelectedValue == null)
            {
                Alert.Show("Selecione um vendedor");
                return;
            }

            if (ddlProduto.SelectedValue == null)
            {
                Alert.Show("Selecione um produto");
                return;
            }


            

            //verificar se existe já o registro pelo produto e vendedor e ano
            mProduto _produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
            mVendedor _vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            if (_produto != null && _vendedor != null )
            {
                IList<mMetas> list = engineMetas.Asignacionmetas(_vendedor.id, _produto.Id);
                if (list != null)
                {
                    _Metas = list[0];
                    txtid.Text = _Metas.id.ToString();
                }
            }
            _Metas.vendedor = _vendedor;
            _Metas.produto = _produto;
            _Metas.Enero = (Enero.Text != string.Empty ? Convert.ToDecimal(Enero.Text) : 0);
            
            _Metas.Febrero = (Febrero.Text != string.Empty ? Convert.ToDecimal(Febrero.Text) : 0);

            _Metas.Marzo = (Marzo.Text != string.Empty ? Convert.ToDecimal(Marzo.Text) : 0);

            _Metas.Abril = (Abril.Text != string.Empty ? Convert.ToDecimal(Abril.Text) : 0);

            _Metas.Mayo = (Mayo.Text != string.Empty ? Convert.ToDecimal(Mayo.Text) : 0);

            _Metas.Junio = (Junio.Text != string.Empty ? Convert.ToDecimal(Junio.Text) : 0);

            _Metas.Julio = (Julio.Text != string.Empty ? Convert.ToDecimal(Julio.Text) : 0);

            _Metas.Agosto = (Agosto.Text != string.Empty ? Convert.ToDecimal(Agosto.Text) : 0);

            _Metas.Septiembre = (Septiembre.Text != string.Empty ? Convert.ToDecimal(Septiembre.Text) : 0);

            _Metas.Octubre = (Octubre.Text != string.Empty ? Convert.ToDecimal(Octubre.Text) : 0);

            _Metas.Noviembre = (Noviembre.Text != string.Empty ? Convert.ToDecimal(Noviembre.Text) : 0);


            _Metas.Diciembre = (Diciembre.Text != string.Empty ? Convert.ToDecimal(Diciembre.Text) : 0);
            

            _Metas.id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                _Metas.id = Convert.ToInt32(engineMetas.Inserir(_Metas)); ;
            }
            else
            {
                engineMetas.Update(_Metas);
            }



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlVendedor.SelectedIndex = 0;
            ddlProduto.SelectedIndex = 0;
            Enero.Text = string.Empty;
            Febrero.Text = string.Empty;
            Marzo.Text = string.Empty;
            Abril.Text = string.Empty;
            Mayo.Text = string.Empty;
            Junio.Text = string.Empty;
            Julio.Text = string.Empty;
            Agosto.Text = string.Empty;
            Septiembre.Text = string.Empty;
            Octubre.Text = string.Empty;
            Noviembre.Text = string.Empty;
            Diciembre.Text = string.Empty;            
            
            //mes.SelectedIndex = 0;
            //diashabilesdetrabajodelmes.Text = string.Empty;
            //meta.Text = string.Empty;
            
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cMetas engineMetas = new cMetas();
            //mMetas _metas = engineMetas.Asignacionmetas(gdvDados.SelectedRow.Cells[2].Text);
            //txtid.Text = _metas.id.ToString();
            //ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_metas.vendedor.id.ToString()));
            //ano.Text = _metas.ano;

            ////mes.Text = _metas.mes;
            ////diashabilesdetrabajodelmes.Text = _metas.diashabilesdetrabajodelmes;
            ////meta.Text = _metas.meta.ToString();

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //cMetas engineMetas = new cMetas();
            //mMetas _metas = new mMetas();
            //_metas = engineMetas.Asignacionmetas(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            //engineMetas.Deletar(_metas);
            //limpar();
            //PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            //if (gdvDados.Rows.Count > 0)
            //{
            //    gdvDados.UseAccessibleHeader = true;
            //    gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    for (int j = 1; j < e.Row.Cells.Count; j++)
            //    {
            //        string encoded = e.Row.Cells[j].Text;
            //        e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
            //    }

            //}
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarMetas();
        }

        protected void ddlProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarMetas();
        }
        private void CarregarMetas()
        {
            cMetas engineMetas = new cMetas();
            mMetas _Metas = new mMetas();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();

            if (ddlVendedor.SelectedValue == null)
            {
                Alert.Show("Selecione um vendedor");
                return;
            }

            if (ddlProduto.SelectedValue == null)
            {
                Alert.Show("Selecione um produto");
                return;
            }





            //verificar se existe já o registro pelo produto e vendedor e ano
            mProduto _produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
            mVendedor _vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            if (_produto != null && _vendedor != null )
            {
                IList<mMetas> list = engineMetas.Asignacionmetas(_vendedor.id, _produto.Id);
                if (list != null)
                {
                    _Metas = list[0];
                    txtid.Text = _Metas.id.ToString();
                }
            }
            _Metas.vendedor = _vendedor;
            _Metas.produto = _produto;
            ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_vendedor.id.ToString()));
            ddlProduto.SelectedIndex = ddlProduto.Items.IndexOf(ddlProduto.Items.FindByValue(_produto.Id.ToString()));
            Enero.Text = (_Metas.Enero.ToString() != string.Empty ? _Metas.Enero.ToString("N2") : "0");

            Febrero.Text = (_Metas.Febrero.ToString() != string.Empty ? _Metas.Febrero.ToString("N2") : "0");

            Marzo.Text = (_Metas.Marzo.ToString() != string.Empty ? _Metas.Marzo.ToString("N2") : "0");

            Abril.Text = (_Metas.Abril.ToString() != string.Empty ? _Metas.Abril.ToString("N2") : "0");

            Mayo.Text = (_Metas.Mayo.ToString() != string.Empty ? _Metas.Mayo.ToString("N2") : "0");

            Junio.Text = (_Metas.Junio.ToString() != string.Empty ? _Metas.Junio.ToString("N2") : "0");

            Julio.Text = (_Metas.Julio.ToString() != string.Empty ? _Metas.Julio.ToString("N2") : "0");

            Agosto.Text = (_Metas.Agosto.ToString() != string.Empty ? _Metas.Agosto.ToString("N2") : "0");

            Septiembre.Text = (_Metas.Septiembre.ToString() != string.Empty ? _Metas.Septiembre.ToString("N2") : "0");

            Octubre.Text = (_Metas.Octubre.ToString() != string.Empty ? _Metas.Octubre.ToString("N2") : "0");

            Noviembre.Text = (_Metas.Noviembre.ToString() != string.Empty ? _Metas.Noviembre.ToString("N2") : "0");

            Diciembre.Text = (_Metas.Diciembre.ToString() != string.Empty ? _Metas.Diciembre.ToString("N2") : "0");
        }
    }
}