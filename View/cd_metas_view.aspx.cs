﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_metas_view : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }

        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral();
                //PopularVendedores();
                //PopularProdutos();
            }
        }

        private void PopularGridGeral()
        {

            cMetas engineMetas = new cMetas();
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataSource = engineMetas.PopularGridFiltros("vend", string.Empty);
            gdvDados.AutoGenerateColumns = true;

            gdvDados.DataBind();

        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {            

            //limpar();
            PopularGridGeral();
        }

        private void limpar()
        {            

        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {           

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void rdPorGrupo_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void rdTodosLosGrupos_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void rdSinAssignar_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void btnNovo_Click1(object sender, EventArgs e)
        {
            Session["vendedor_id"] = gdvDados.SelectedRow.Cells[1].Text;
            Response.Redirect("~/View/cd_metas.aspx");
        }
    }
}