﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_ventas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }

        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral();
                //PopularVendedores();
                PopularProdutos();
                PopularGrupoVendedores();
                PopularVendedores();
            }
        }

        //private void PopularVendedores()
        //{
        //    cVendedor engineVendedor = new cVendedor();
        //    ddlVendedor.DataSource = engineVendedor.PopularGrid(0).Tables[0];
        //    ddlVendedor.DataValueField = "Id";
        //    ddlVendedor.DataTextField = "NOMBRE";
        //    ddlVendedor.DataBind();
        //}

        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            ddlProduto.DataSource = engineProduto.PopularGrid(0).Tables[0];
            ddlProduto.DataValueField = "Id";
            ddlProduto.DataTextField = "NOMBRE";
            ddlProduto.DataBind();
        }
        private void PopularGrupoVendedores()
        {
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();
            ddlGrupoVendedores.DataSource = engineGrupoVendedores.PopularGrid(0).Tables[0];
            ddlGrupoVendedores.DataValueField = "id";
            ddlGrupoVendedores.DataTextField = "nomegrupovendedores";
            ddlGrupoVendedores.DataBind();
        }
        private void PopularVendedores()
        {
            cVendedor engineVendedor = new cVendedor();
            ddlVendedores.DataSource = engineVendedor.PopularGrid(0).Tables[0];
            ddlVendedores.DataValueField = "Id";
            ddlVendedores.DataTextField = "NOMBRE";
            ddlVendedores.DataBind();
        }
        private void PopularGridGeral()
        {

            cMetas engineMetas = new cMetas();
            btnNovo.Visible = false;
            gdvDados.AutoGenerateSelectButton = false;

            if (rdTodosLosGrupos.Checked)
            {
                gdvDados.DataSource = engineMetas.PopularGridFiltros("todos", string.Empty);
            }
            else if (rdPorGrupo.Checked)
            {
                gdvDados.DataSource = engineMetas.PopularGridFiltros("grupoprodutos", ddlProduto.SelectedValue.ToString());
            }
            else if (rdPorGrupoVendedores.Checked)
            {
                gdvDados.DataSource = engineMetas.PopularGridFiltros("grupovendedores", ddlGrupoVendedores.SelectedValue.ToString());
            }
            else if (rdPorVendedor.Checked)
            {
                gdvDados.DataSource = engineMetas.PopularGridFiltros("vendedores", ddlVendedores.SelectedValue.ToString());
            }
            else if (rdSinAssignar.Checked)
            {
                gdvDados.DataSource = engineMetas.PopularGridFiltros("semmeta", string.Empty);
                gdvDados.AutoGenerateSelectButton = true;
                btnNovo.Visible = true;
            }
            else
            {
                gdvDados.DataSource = engineMetas.PopularGridFiltros("todos", string.Empty);
            }
            
            gdvDados.AutoGenerateColumns = true;
            
            gdvDados.DataBind();
            lblProduto.Text = (ddlProduto.SelectedItem != null ? "Vendedor: " + ddlProduto.SelectedItem.Text : ""); ;

            //cVentas engineVentas= new cVentas();
            //gdvDados.DataSource = engineVentas.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = true;
            //gdvDados.DataBind();
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            //cVentas engineVentas = new cVentas();
            //mVentas _Ventas = new mVentas();
            //cVendedor engineVendedor = new cVendedor();
            //cProduto engineProduto = new cProduto();

            //if (ddlVendedor.SelectedValue == null)
            //{
            //    Alert.Show("Selecione um vendedor");
            //    return;
            //}

            //if (ddlProduto.SelectedValue == null)
            //{
            //    Alert.Show("Selecione um produto");
            //    return;
            //}

            //_Ventas.vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            //_Ventas.produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
            //_Ventas.Enero = Enero.Text;
            //_Ventas.Febrero = Febrero.Text;
            //_Ventas.Marzo = Marzo.Text;
            //_Ventas.Abril = Abril.Text;
            //_Ventas.Mayo = Mayo.Text;
            //_Ventas.Junio = Junio.Text;
            //_Ventas.Julio = Julio.Text;
            //_Ventas.Agosto = Agosto.Text;
            //_Ventas.Septiembre = Septiembre.Text;
            //_Ventas.Octubre = Octubre.Text;
            //_Ventas.Noviembre = Noviembre.Text;
            //_Ventas.Diciembre = Diciembre.Text;
            //_Ventas.id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            //if (txtid.Text == string.Empty)
            //{
            //    _Ventas.id = Convert.ToInt32(engineVentas.Inserir(_Ventas)); ;
            //}
            //else
            //{
            //    engineVentas.Update(_Ventas);
            //}



            //limpar();
            PopularGridGeral();
        }

        private void limpar()
        {
            //txtid.Text = string.Empty;
            //ddlVendedor.SelectedIndex = 0;
            //ddlProduto.SelectedIndex = 0;
            //Enero.Text = string.Empty;
            //Febrero.Text = string.Empty;
            //Marzo.Text = string.Empty;
            //Abril.Text = string.Empty;
            //Mayo.Text = string.Empty;
            //Junio.Text = string.Empty;
            //Julio.Text = string.Empty;
            //Agosto.Text = string.Empty;
            //Septiembre.Text = string.Empty;
            //Octubre.Text = string.Empty;
            //Noviembre.Text = string.Empty;
            //Diciembre.Text = string.Empty;

        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cVentas engineVentas = new cVentas();
            //mVentas _ventas = engineVentas.RetornarVentas(gdvDados.SelectedRow.Cells[2].Text);
            //txtid.Text = _ventas.id.ToString();
            //ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_ventas.vendedor.id.ToString()));
            //ddlProduto.SelectedIndex = ddlProduto.Items.IndexOf(ddlProduto.Items.FindByValue(_ventas.produto.Id.ToString()));
            //Enero.Text = _ventas.Enero;
            //Febrero.Text = _ventas.Febrero;
            //Marzo.Text = _ventas.Marzo;
            //Abril.Text = _ventas.Abril;
            //Mayo.Text = _ventas.Mayo;
            //Junio.Text = _ventas.Junio;
            //Julio.Text = _ventas.Julio;
            //Agosto.Text = _ventas.Agosto;
            //Septiembre.Text = _ventas.Septiembre;
            //Octubre.Text = _ventas.Octubre;
            //Noviembre.Text = _ventas.Noviembre;
            //Diciembre.Text = _ventas.Diciembre;

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //cVentas engineVentas = new cVentas();
            //mVentas _ventas = new mVentas();
            //_ventas = engineVentas.RetornarVentas(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            //engineVentas.Deletar(_ventas);
            //limpar();
            //PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }

        protected void rdPorGrupo_CheckedChanged(object sender, EventArgs e)
        {
            rdTodosLosGrupos.Checked = false;
            rdSinAssignar.Checked = false;
            rdPorVendedor.Checked = false;
            rdPorGrupoVendedores.Checked = false;
        }

        protected void rdTodosLosGrupos_CheckedChanged(object sender, EventArgs e)
        {
            rdPorGrupo.Checked = false;
            rdSinAssignar.Checked = false;
            rdPorVendedor.Checked = false;
            rdPorGrupoVendedores.Checked = false;
        }

        protected void rdSinAssignar_CheckedChanged(object sender, EventArgs e)
        {
            rdTodosLosGrupos.Checked = false;
            rdPorGrupo.Checked = false;
            rdPorVendedor.Checked = false;
            rdPorGrupoVendedores.Checked = false;
        }

        protected void btnNovo_Click1(object sender, EventArgs e)
        {
            Session["vendedor_id"] = gdvDados.SelectedRow.Cells[1].Text;
            Response.Redirect("~/View/cd_metas.aspx");
        }

        protected void rdPorGrupoVendedores_CheckedChanged(object sender, EventArgs e)
        {
            rdSinAssignar.Checked = false;
            rdTodosLosGrupos.Checked = false;
            rdPorGrupo.Checked = false;
            rdPorVendedor.Checked = false;
        }

        protected void rdPorVendedor_CheckedChanged(object sender, EventArgs e)
        {
            rdSinAssignar.Checked = false;
            rdTodosLosGrupos.Checked = false;
            rdPorGrupo.Checked = false;
            rdPorGrupoVendedores.Checked = false;
        }

        protected void gdvDados_RowDataBound1(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int contador = 3; contador < 28; contador += 2)
                {
                    //if (e.Row.Cells[contador].Text != string.Empty)
                    //{
                    //    double valor = Convert.ToDouble(e.Row.Cells[contador].Text);
                    //    double valorlfloor = Math.Floor(valor);
                    //    e.Row.Cells[contador].Text = valorlfloor.ToString();
                    //}
                }
            }
            
        }
    }
}