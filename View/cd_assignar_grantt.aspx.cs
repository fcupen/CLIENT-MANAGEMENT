﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_assignar_grantt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                PopularVendedores("todos");
                PopularProdutos();

                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }
        private void PopularProdutos()
        {
            //cProduto engineProduto = new cProduto();
            //ddlProduto.DataSource = engineProduto.PopularGrid(0).Tables[0];
            //ddlProduto.DataValueField = "Id";
            //ddlProduto.DataTextField = "NOMBRE";
            //ddlProduto.DataBind();
        }
        private void PopularGridGeral(int limite)
        {
            cVendedorClientes engineVendedorClientes = new cVendedorClientes();
            gdvDados.DataSource = engineVendedorClientes.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }

        private void PopularVendedores(string asignados)
        {
            cVendedor engineVendedor = new cVendedor();
            if (engineVendedor.PopularGridComAsignadosOuNo(asignados) != null)
            {
                ddlVendedor.DataSource = engineVendedor.PopularGridComAsignadosOuNo(asignados).Tables[0];
                ddlVendedor.DataValueField = "Id";
                ddlVendedor.DataTextField = "NOMBRE";
                ddlVendedor.DataBind();
            }
            else
            {
                Alert.Show("Não há vendedores");
            }
        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cVendedorClientes engineVendedorClientes = new cVendedorClientes();
            mVendedorClientes _vendedoresClientes = new mVendedorClientes();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto= new cProduto();
            mVendedorClientes _vendedoresClientestmp = new mVendedorClientes();

            if (ddlVendedor.SelectedValue != null)
            {
                if (ddlVendedor.SelectedValue != "")
                {
                    _vendedoresClientes.vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
                    _vendedoresClientes.produto = null;// engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString());
                    _vendedoresClientes.clientes = (txtClientesMes.Text != string.Empty ? Convert.ToInt32(txtClientesMes.Text) : 0);
                    _vendedoresClientestmp=engineVendedorClientes.RetornarGrupoVendedorPorVendedor(ddlVendedor.SelectedValue.ToString());

                    if (txtClientesMes.Text != string.Empty)
                    {
                        int valor = Convert.ToInt32(txtClientesMes.Text);

                        if (valor < 0 || valor > 400)
                        {
                            Alert.Show("Minimo entre 0 e maximo 400");
                            return;
                        }
                    }
                    else
                    {
                        Alert.Show("Informe Clientes");
                    }

                    if ( _vendedoresClientestmp != null)
                    {
                        txtid.Text = _vendedoresClientestmp.Id.ToString();
                    }
                        _vendedoresClientes.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);
                        if (txtid.Text == string.Empty)
                        {
                            _vendedoresClientes.Id = Convert.ToInt32(engineVendedorClientes.Inserir(_vendedoresClientes)); ;
                        }
                        else
                        {
                            engineVendedorClientes.Update(_vendedoresClientes);
                        }
                    
                }
                else
                {
                    Alert.Show("Selecione um vendedor");
                }
            }
            else
            {
                Alert.Show("Selecione um vendedor");
            }





            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlVendedor.SelectedIndex = 0;
            //ddlProduto.SelectedIndex = 0;
            txtClientesMes.Text = "0";
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cVendedorClientes engineVendedorCliente = new cVendedorClientes();
            mVendedorClientes _vendedorCliente = engineVendedorCliente.RetornarGrupoVendedores(gdvDados.SelectedRow.Cells[1].Text);            
            txtid.Text = _vendedorCliente.Id.ToString();
            txtClientesMes.Text = _vendedorCliente.clientes.ToString();
            ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_vendedorCliente.vendedor.id.ToString()));
            //ddlProduto.SelectedIndex = ddlProduto.Items.IndexOf(ddlProduto.Items.FindByValue(_vendedorCliente.produto.Id.ToString()));
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cVendedorClientes engineGrupoVendedores = new cVendedorClientes();
            mVendedorClientes _GrupoVendedores = new mVendedorClientes();
            _GrupoVendedores = engineGrupoVendedores.RetornarGrupoVendedores(gdvDados.Rows[e.RowIndex].Cells[1].Text);
            engineGrupoVendedores.Deletar(_GrupoVendedores);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }
    }
}