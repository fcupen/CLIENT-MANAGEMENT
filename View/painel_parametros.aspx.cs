﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class painel_parametros : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
            }
        }
        private void PopularGridGeral(int limite)
        {
            //cPainel_parametros enginePainel_parametros = new cPainel_parametros();
            //gdvDados.DataSource = enginePainel_parametros.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = true;
            //gdvDados.DataBind();

        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            //cPainel_parametros enginePainel_parametros = new cPainel_parametros();
            //mPainel_parametros _Painel_parametros = new mPainel_parametros();

            //_Painel_parametros.horizontes_control = horizontecontrol.Text;
            ////_Painel_parametros.parametrosbdn = parametrosBDN.Text;
            //_Painel_parametros.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            //if (txtid.Text == string.Empty)
            //{
            //    _Painel_parametros.Id = Convert.ToInt32(enginePainel_parametros.Inserir(_Painel_parametros)); ;
            //}
            //else
            //{
            //    enginePainel_parametros.Update(_Painel_parametros);
            //}



            //limpar();
            //PopularGridGeral(0);
        }

        private void limpar()
        {
            //txtid.Text = string.Empty;
            ////parametrosBDN.Text = string.Empty;
            //horizontecontrol.Text = string.Empty;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cPainel_parametros enginePainel_parametros = new cPainel_parametros();
            //mPainel_parametros _Painel_parametros = enginePainel_parametros.RetornarPainel_parametros(gdvDados.SelectedRow.Cells[2].Text);
            //txtid.Text = _Painel_parametros.Id.ToString();
            ////parametrosBDN.Text = _Painel_parametros.parametrosbdn;
            //horizontecontrol.Text = _Painel_parametros.horizontes_control;
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //cPainel_parametros enginePainel_parametros = new cPainel_parametros();
            //mPainel_parametros _Painel_parametros = new mPainel_parametros();
            //_Painel_parametros = enginePainel_parametros.RetornarPainel_parametros(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            //enginePainel_parametros.Deletar(_Painel_parametros);
            //limpar();
            //PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            //if (gdvDados.Rows.Count > 0)
            //{
            //    gdvDados.UseAccessibleHeader = true;
            //    gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }
    }
}