﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_vendedor.aspx.cs" Inherits="Clients_Management.View.cd_vendedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
 <div class="container">
       <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">DESPLIEGUE GENERAL</a></li>
  <li class="active">EQUIPO DE VENTAS</li>
</ol>

       <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>

	 
	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">EQUIPO DE VENTAS Y ASIGNACIONES</h4>
		
            <asp:TextBox ID="txtid" Visible="false" runat="server"></asp:TextBox>
            <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White" OnRowCommand="gdvDados_RowCommand">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <span style="color: rgb(85, 85, 85); font-family: Lato, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: bold; letter-spacing: normal; line-height: 17.142858505249px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(249, 249, 249);">Meta View</span>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName="meta" Height="30px" ImageUrl="~/Images/lupa.png" Width="31px" CommandArgument='<%# Eval("Id" )%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <span style="color: rgb(85, 85, 85); font-family: Lato, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: bold; letter-spacing: normal; line-height: 17.142858505249px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 1; word-spacing: 0px; -webkit-text-stroke-width: 0px; display: inline !important; float: none; background-color: rgb(249, 249, 249);">Cartera Assignada View</span>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton2" runat="server" CommandName="cartera" Height="30px" ImageUrl="~/Images/lupa.png" Width="31px" CommandArgument='<%# Eval("Id" )%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>					
		</div>
	</div>
 
</div>
	 	 	
	 
	 
	 
	 
	 
    </div>
  
  <div class="row">  
<div class="col-lg-12" align="center">
    <a id="A2" runat="server" href="~/View/form_vendedor.aspx" target="_self" class="btn btn-primary">Crear Vendedor</a>&nbsp;
    <a id="A1" runat="server" href="~/View/cd_asignar.aspx" target="_self" class="btn btn-primary">Asigna la cartera de clientes</a>&nbsp;
    <a id="A3" runat="server" href="~/View/cd_metas.aspx" class="btn btn-primary">Asignacion de Metas</a></br></br>
<p></p>
</div>
</div>
  
  
<div class="row">  

</div>

</div>
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
 
</asp:Content>
