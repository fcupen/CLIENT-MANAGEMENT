﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_informe_consolidado.aspx.cs" Inherits="Clients_Management.View.cd_informe_consolidado" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="lblDataInicial" runat="server" Text="Data Inicial"></asp:Label><br />
    <asp:TextBox ID="txtDataInicial" runat="server"></asp:TextBox>
    <ajaxToolkit:CalendarExtender ID="txtDataInicial_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtDataInicial" Format="dd/MM/yyyy">
    </ajaxToolkit:CalendarExtender>
    <br />
    <asp:Label ID="lblDataFinal" runat="server" Text="Data Final"></asp:Label><br />
    <asp:TextBox ID="txtDataFinal" runat="server"></asp:TextBox>
    <ajaxToolkit:CalendarExtender ID="txtDataFinal_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtDataFinal" Format="dd/MM/yyyy">
    </ajaxToolkit:CalendarExtender>
    <br />
    <asp:Button ID="btnConsultar" runat="server" Text="Consultar" OnClick="btnConsultar_Click" /><br />
     <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
         WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" AsyncRendering="False">
        <LocalReport ReportPath="View\rptInformeConsolidado.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="dsRelatoriosTableAdapters."></asp:ObjectDataSource>
    <br />
</asp:Content>
