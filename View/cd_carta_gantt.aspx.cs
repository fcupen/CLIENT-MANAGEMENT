﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_carta_gantt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularVendedores();

                PopularGridGeral(0);
                PopularClientes();
                calendario.Text = DateTime.Now.ToString("dd/MM/yyyy");
                PreencherDiasDoMes();
                
            }
        }

        private void PopularClientes()
        {
            cVendedor enginevendedor = new cVendedor();
            mVendedor _vendedor = enginevendedor.RetornarVendedor(ddlVendedor.SelectedValue);
            cClientes engineCliente = new cClientes();
            if (engineCliente.PopularGridPorVendedor(_vendedor.id) != null)
            {
                ddlCliente.DataSource = engineCliente.PopularGridPorVendedor(_vendedor.id).Tables[0];
                ddlCliente.DataValueField = "Id";
                ddlCliente.DataTextField = "Razon Social";
                ddlCliente.DataBind();
            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }
        private void PopularVendedores()
        {
            cVendedor engineVendedor = new cVendedor();
            if (engineVendedor.PopularGrid(0) != null)
            {
                ddlVendedor.DataSource = engineVendedor.PopularGrid(0).Tables[0];
                ddlVendedor.DataValueField = "Id";
                ddlVendedor.DataTextField = "NOMBRE";
                ddlVendedor.DataBind();
            }
            else
            {
                Alert.Show("Não há vendedores");
            }
        }

        private void PopularGridGeral(int limite)
        {
            cInformediario engineInformediario = new cInformediario();
            DataTable dt = engineInformediario.PopularGridCartaGantt(limite, ddlAno.Text,ddlMes.Text).Tables[0];
            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add("id");
            dtNovo.Columns.Add("calendario");
            dtNovo.Columns.Add("vendedor");
            dtNovo.Columns.Add("razonsocial");
            dtNovo.Columns.Add("contacto");
            dtNovo.Columns.Add("telefone");
            dtNovo.Columns.Add("ano");
            dtNovo.Columns.Add("mes");
            for (int i = 1; i <= 31; i++)
            {
                dtNovo.Columns.Add(i.ToString());
            }
            DataRow dr = dtNovo.NewRow();
            dr["id"] = "";// DateTime.Now;
            dr["calendario"] = "";// DateTime.Now;
            dr["vendedor"] = "";
            dr["razonsocial"] = "";
            dr["contacto"] = "";
            dr["telefone"] = "";
            dr["ano"] = "";
            dr["mes"] = "";
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(ddlAno.Text, ddlMes.Text, (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dr[(i + 1).ToString()] = ListaParametroCartaGantt[0].dia;
                    }
                    else
                    {
                        dr[(i + 1).ToString()] = "F";
                    }
                }
                else
                {
                    dr[(i + 1).ToString()] = "F";
                }

            }
            dtNovo.Rows.Add(dr);
            foreach (DataRow linha in dt.Rows)
            {
                dr = dtNovo.NewRow();
                dr["id"] = linha["id"];
                dr["calendario"] = linha["calendario"];
                dr["vendedor"] = linha["vendedor"];
                dr["razonsocial"] = linha["razonsocial"];
                dr["contacto"] = linha["contacto"];
                dr["telefone"] = linha["telefone"];
                dr["ano"] = linha["ano"];
                dr["mes"] = linha["mes"];
                for (int i = 1; i <= 31; i++)
                {
                    dr[i.ToString()] = linha[i.ToString()];
                }

                dtNovo.Rows.Add(dr);
            }

            gdvDados.DataSource = dtNovo;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

            int contador = 0;
            foreach (GridViewRow r in gdvDados.Rows)
            {
                if (contador > 0)
                {
                    for (int i = 10; i < 41; i++)
                    {
                        if (r.Cells[i].Text == "F")
                        {
                            r.Cells[i].BackColor = System.Drawing.Color.LightGray;
                            r.Cells[i].Text = "F";
                        }
                        //else if (r.Cells[i].Text == "1" || r.Cells[i].Text == "3")
                        //{
                        //    r.Cells[i].Text = "1";
                        //}
                        else
                        {
                            //r.Cells[i].Text = "";
                        }

                    }
                    contador++;
                }
                else
                {
                    contador++;
                }
            }

        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = new mInformediario();
            cClientes engineCliente = new cClientes();


            //string nomecontrole = string.Empty;
            //for (int i = 0; i < 31; i++)
            //{
            //    nomecontrole = "txtd" + (i + 1).ToString();
            //    TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
            //    if (TB.Text != string.Empty && TB.Text != "F" && TB.Text != "P")
            //    {
            //        Alert.Show("Somente permitido entrada com P");
            //        return;
            //    }
            //}

            
            string mesescrito = ddlMes.Text;
            cVendedor enginevendedor = new cVendedor();
            mVendedor _vendedor = enginevendedor.RetornarVendedor(ddlVendedor.SelectedValue);

            _Informediario.calendario = (calendario.Text != string.Empty ? Convert.ToDateTime(calendario.Text) : Convert.ToDateTime("1900-01-01"));
            _Informediario.cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
            _Informediario.vendedor = _vendedor;
            _Informediario.contacto = txtContato.Text;
            _Informediario.ano = ddlAno.Text;
            _Informediario.mes = mesescrito;
            _Informediario.d1_valor = txtd1.Text;
            _Informediario.d2_valor = txtd2.Text;
            _Informediario.d3_valor = txtd3.Text;
            _Informediario.d4_valor = txtd4.Text;
            _Informediario.d5_valor = txtd5.Text;
            _Informediario.d6_valor = txtd6.Text;
            _Informediario.d7_valor = txtd7.Text;
            _Informediario.d8_valor = txtd8.Text;
            _Informediario.d9_valor = txtd9.Text;
            _Informediario.d10_valor = txtd10.Text;
            _Informediario.d11_valor = txtd11.Text;
            _Informediario.d12_valor = txtd12.Text;
            _Informediario.d13_valor = txtd13.Text;
            _Informediario.d14_valor = txtd14.Text;
            _Informediario.d15_valor = txtd15.Text;
            _Informediario.d16_valor = txtd16.Text;
            _Informediario.d17_valor = txtd17.Text;
            _Informediario.d18_valor = txtd18.Text;
            _Informediario.d19_valor = txtd19.Text;
            _Informediario.d20_valor = txtd20.Text;
            _Informediario.d21_valor = txtd21.Text;
            _Informediario.d22_valor = txtd22.Text;
            _Informediario.d23_valor = txtd23.Text;
            _Informediario.d24_valor = txtd24.Text;
            _Informediario.d25_valor = txtd25.Text;
            _Informediario.d26_valor = txtd26.Text;
            _Informediario.d27_valor = txtd27.Text;
            _Informediario.d28_valor = txtd28.Text;
            _Informediario.d29_valor = txtd29.Text;
            _Informediario.d30_valor = txtd30.Text;
            _Informediario.d31_valor = txtd31.Text;
            _Informediario.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty || txtid.Text == "0")
            {
                _Informediario.Id = Convert.ToInt32(engineInformediario.Inserir(_Informediario)); ;
            }
            else
            {
                engineInformediario.Update(_Informediario);
            }



            limpar();
            PopularGridGeral(0);
            Alert.Show("Sucesso");
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            txtContato.Text = string.Empty;
            ddlCliente.SelectedIndex = 0;

        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = engineInformediario.RetornarInformediario(gdvDados.SelectedRow.Cells[2].Text);
            txtid.Text = _Informediario.Id.ToString();
            calendario.Text = _Informediario.calendario.ToString("yyyy-MM-dd");
            txtContato.Text = _Informediario.contacto.ToString();
            ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_Informediario.vendedor.id.ToString()));
            PopularClientes();
            ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(_Informediario.cliente.id.ToString()));

            txtd1.Text = (_Informediario.d1_valor != string.Empty ? _Informediario.d1_valor : txtd1.Text);
            txtd2.Text = (_Informediario.d2_valor != string.Empty ? _Informediario.d2_valor : txtd2.Text);
            txtd3.Text = (_Informediario.d3_valor != string.Empty ? _Informediario.d3_valor : txtd3.Text);
            txtd4.Text = (_Informediario.d4_valor != string.Empty ? _Informediario.d4_valor : txtd4.Text);
            txtd5.Text = (_Informediario.d5_valor != string.Empty ? _Informediario.d5_valor : txtd5.Text);
            txtd6.Text = (_Informediario.d6_valor != string.Empty ? _Informediario.d6_valor : txtd6.Text);
            txtd7.Text = (_Informediario.d7_valor != string.Empty ? _Informediario.d7_valor : txtd7.Text);
            txtd8.Text = (_Informediario.d8_valor != string.Empty ? _Informediario.d8_valor : txtd8.Text);
            txtd9.Text = (_Informediario.d9_valor != string.Empty ? _Informediario.d9_valor : txtd9.Text);
            txtd10.Text = (_Informediario.d10_valor != string.Empty ? _Informediario.d10_valor : txtd10.Text);
            txtd11.Text = (_Informediario.d11_valor != string.Empty ? _Informediario.d11_valor : txtd11.Text);
            txtd12.Text = (_Informediario.d12_valor != string.Empty ? _Informediario.d12_valor : txtd12.Text);
            txtd13.Text = (_Informediario.d13_valor != string.Empty ? _Informediario.d13_valor : txtd13.Text);
            txtd14.Text = (_Informediario.d14_valor != string.Empty ? _Informediario.d14_valor : txtd14.Text);
            txtd15.Text = (_Informediario.d15_valor != string.Empty ? _Informediario.d15_valor : txtd15.Text);
            txtd16.Text = (_Informediario.d16_valor != string.Empty ? _Informediario.d16_valor : txtd16.Text);
            txtd17.Text = (_Informediario.d17_valor != string.Empty ? _Informediario.d17_valor : txtd17.Text);
            txtd18.Text = (_Informediario.d18_valor != string.Empty ? _Informediario.d18_valor : txtd18.Text);
            txtd19.Text = (_Informediario.d19_valor != string.Empty ? _Informediario.d19_valor : txtd19.Text);
            txtd20.Text = (_Informediario.d20_valor != string.Empty ? _Informediario.d20_valor : txtd20.Text);
            txtd21.Text = (_Informediario.d21_valor != string.Empty ? _Informediario.d21_valor : txtd21.Text);
            txtd22.Text = (_Informediario.d22_valor != string.Empty ? _Informediario.d22_valor : txtd22.Text);
            txtd23.Text = (_Informediario.d23_valor != string.Empty ? _Informediario.d23_valor : txtd23.Text);
            txtd24.Text = (_Informediario.d24_valor != string.Empty ? _Informediario.d24_valor : txtd24.Text);
            txtd25.Text = (_Informediario.d25_valor != string.Empty ? _Informediario.d25_valor : txtd25.Text);
            txtd26.Text = (_Informediario.d26_valor != string.Empty ? _Informediario.d26_valor : txtd26.Text);
            txtd27.Text = (_Informediario.d27_valor != string.Empty ? _Informediario.d27_valor : txtd27.Text);
            txtd28.Text = (_Informediario.d28_valor != string.Empty ? _Informediario.d28_valor : txtd28.Text);
            txtd29.Text = (_Informediario.d29_valor != string.Empty ? _Informediario.d29_valor : txtd29.Text);
            txtd30.Text = (_Informediario.d30_valor != string.Empty ? _Informediario.d30_valor : txtd30.Text);
            txtd31.Text = (_Informediario.d31_valor != string.Empty ? _Informediario.d31_valor : txtd31.Text);
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = new mInformediario();
            _Informediario = engineInformediario.RetornarInformediario(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            engineInformediario.Deletar(_Informediario);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            cClientes engineCliente = new cClientes();
            mClientes _cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
            txtContato.Text = _cliente.contato;
            PreencherDiasDoMes();
        }

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GerarDias();
            PreencherDiasDoMes();
            PopularGridGeral(0);
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GerarDias();
            PreencherDiasDoMes();
            PopularGridGeral(0);
        }
        public void PreencherDiasDoMes()
        {
            cInformediario engineInformeDiario = new cInformediario();
            mInformediario _informeDiario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            mClientes _cliente = (ddlCliente.SelectedValue != null ? engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);

            IList<mInformediario> lstInformeDiario = engineInformeDiario.RetornarInformediario(ddlAno.Text, ddlMes.Text, _vendedor.id.ToString(), _cliente.id.ToString());
            txtid.Text = "0";
            LimparControles();



            if (lstInformeDiario != null)
            {
                if (lstInformeDiario.Count > 0)
                {
                    _informeDiario = lstInformeDiario[0];
                    txtid.Text = lstInformeDiario[0].Id.ToString();
                    txtd1.Text = (_informeDiario.d1_valor != string.Empty ? _informeDiario.d1_valor : txtd1.Text);
                    txtd2.Text = (_informeDiario.d2_valor != string.Empty ? _informeDiario.d2_valor : txtd2.Text);
                    txtd3.Text = (_informeDiario.d3_valor != string.Empty ? _informeDiario.d3_valor : txtd3.Text);
                    txtd4.Text = (_informeDiario.d4_valor != string.Empty ? _informeDiario.d4_valor : txtd4.Text);
                    txtd5.Text = (_informeDiario.d5_valor != string.Empty ? _informeDiario.d5_valor : txtd5.Text);
                    txtd6.Text = (_informeDiario.d6_valor != string.Empty ? _informeDiario.d6_valor : txtd6.Text);
                    txtd7.Text = (_informeDiario.d7_valor != string.Empty ? _informeDiario.d7_valor : txtd7.Text);
                    txtd8.Text = (_informeDiario.d8_valor != string.Empty ? _informeDiario.d8_valor : txtd8.Text);
                    txtd9.Text = (_informeDiario.d9_valor != string.Empty ? _informeDiario.d9_valor : txtd9.Text);
                    txtd10.Text = (_informeDiario.d10_valor != string.Empty ? _informeDiario.d10_valor : txtd10.Text);
                    txtd11.Text = (_informeDiario.d11_valor != string.Empty ? _informeDiario.d11_valor : txtd11.Text);
                    txtd12.Text = (_informeDiario.d12_valor != string.Empty ? _informeDiario.d12_valor : txtd12.Text);
                    txtd13.Text = (_informeDiario.d13_valor != string.Empty ? _informeDiario.d13_valor : txtd13.Text);
                    txtd14.Text = (_informeDiario.d14_valor != string.Empty ? _informeDiario.d14_valor : txtd14.Text);
                    txtd15.Text = (_informeDiario.d15_valor != string.Empty ? _informeDiario.d15_valor : txtd15.Text);
                    txtd16.Text = (_informeDiario.d16_valor != string.Empty ? _informeDiario.d16_valor : txtd16.Text);
                    txtd17.Text = (_informeDiario.d17_valor != string.Empty ? _informeDiario.d17_valor : txtd17.Text);
                    txtd18.Text = (_informeDiario.d18_valor != string.Empty ? _informeDiario.d18_valor : txtd18.Text);
                    txtd19.Text = (_informeDiario.d19_valor != string.Empty ? _informeDiario.d19_valor : txtd19.Text);
                    txtd20.Text = (_informeDiario.d20_valor != string.Empty ? _informeDiario.d20_valor : txtd20.Text);
                    txtd21.Text = (_informeDiario.d21_valor != string.Empty ? _informeDiario.d21_valor : txtd21.Text);
                    txtd22.Text = (_informeDiario.d22_valor != string.Empty ? _informeDiario.d22_valor : txtd22.Text);
                    txtd23.Text = (_informeDiario.d23_valor != string.Empty ? _informeDiario.d23_valor : txtd23.Text);
                    txtd24.Text = (_informeDiario.d24_valor != string.Empty ? _informeDiario.d24_valor : txtd24.Text);
                    txtd25.Text = (_informeDiario.d25_valor != string.Empty ? _informeDiario.d25_valor : txtd25.Text);
                    txtd26.Text = (_informeDiario.d26_valor != string.Empty ? _informeDiario.d26_valor : txtd26.Text);
                    txtd27.Text = (_informeDiario.d27_valor != string.Empty ? _informeDiario.d27_valor : txtd27.Text);
                    txtd28.Text = (_informeDiario.d28_valor != string.Empty ? _informeDiario.d28_valor : txtd28.Text);
                    txtd29.Text = (_informeDiario.d29_valor != string.Empty ? _informeDiario.d29_valor : txtd29.Text);
                    txtd30.Text = (_informeDiario.d30_valor != string.Empty ? _informeDiario.d30_valor : txtd30.Text);
                    txtd31.Text = (_informeDiario.d31_valor != string.Empty ? _informeDiario.d31_valor : txtd31.Text);
                }
            }



        }

        public void LimparControles()
        {
            string nomecontrole = string.Empty;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            int dia = 0;
            int dialimite = 0;
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                nomecontrole = "txtd" + (i + 1).ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        TB.Text = "";// ListaParametroCartaGantt[0].dia + " - ";
                        TB.Enabled = true;
                        _paramDiv = engineParametrosDiversos.RetornarParametrosDiversos("1");
                        dia = DateTime.Now.Day;
                        dialimite = dia - _paramDiv.travardias;
                        if (dia < dialimite)
                            TB.Enabled = false;
                    }
                    else
                    {
                        TB.Text = "F";
                        TB.Enabled = false;
                    }
                }
                else
                {
                    TB.Text = "F";
                    TB.Enabled = false;
                }

            }
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularClientes();
            cClientes engineCliente = new cClientes();
            mClientes _cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
            txtContato.Text = _cliente.contato;
            PreencherDiasDoMes();
        }
    }
}