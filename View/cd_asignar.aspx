﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_asignar.aspx.cs" Inherits="Clients_Management.View.cd_assignar_new" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
 
   <div class="container">
      <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">ADMINISTRACION DE LA CARTERA DE CLIENTES</a></li>
  <li class="active">ASIGNAR CARTERA DE CLIENTES</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     
	 	 	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">CARTERA ASIGNADA</h4>
            <label>Vendedor: </label>
            <b><asp:Label ID="lblVendedor" runat="server" Text="Vendedor"></asp:Label></b>
			         <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White">
            </asp:GridView>		
            <asp:Label ID="lblTotal" runat="server" Text="Total"></asp:Label>			
		</div>
	</div>
 
</div>

     <div class="col-md-12">
            <div class="form-horizontal well" >
          <div class="row">
 
					<div class="col-xs-12">
					
							
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							
                            <div class="col-md-4">
                                <label>Seleccione Vendedor:</label><br/>
                                <asp:RadioButton ID="rdVendedorAsignar" runat="server" GroupName="vendedor" Text="Asignados" AutoPostBack="True" OnCheckedChanged="rdVendedorAsignar_CheckedChanged" />&nbsp;&nbsp; <asp:RadioButton ID="rdVendedorNoAsignados" runat="server" GroupName="vendedor" Text="No Asignados" AutoPostBack="True" OnCheckedChanged="rdVendedorNoAsignados_CheckedChanged" />
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false"></asp:TextBox>
								<asp:DropDownList ID="ddlVendedor" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVendedor_SelectedIndexChanged"></asp:DropDownList>
							</div>
						</div>
					</div>
				</div>

                        <div class="form-group">
					<div class="rows">
                        
						<div class="col-md-8">
							
                            <div class="col-md-4"><label>Seleccione o Cliente:</label><br />
                                <asp:RadioButton ID="rdClienteAsignar" runat="server" GroupName="cliente" Text="Asignados" AutoPostBack="True" OnCheckedChanged="rdClienteAsignar_CheckedChanged" />&nbsp;&nbsp; <asp:RadioButton ID="rdClienteNoAsignados" runat="server" GroupName="cliente" Text="No Asignados" AutoPostBack="True" OnCheckedChanged="rdClienteNoAsignados_CheckedChanged"  />
                               <asp:DropDownList ID="ddlCliente" class="form-control input-sm" runat="server" ></asp:DropDownList>
							</div>
						</div>
					</div>
				</div>
 
 
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
								<asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Asignar" OnClick="btnSalvar_Click" />                                
							</div>
						</div>
					</div>
				</div>
				</div>
 
				</div>	
	   </div>
     </div>
	 




</div>
      </div>
    </div>
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
