﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class informe_criticos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
                PopularComboVendedores();
                ddlAno.Text = DateTime.Now.Year.ToString();
                ddlMes.SelectedIndex = DateTime.Now.Month - 1;                
                Button1_Click(null, null);
            }
        }

        private void PopularComboVendedores()
        {
            cVendedor engineVendedor = new cVendedor();

            DataSet ds = engineVendedor.PopularGrid(0);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendedor.DataSource = ds.Tables[0];
                    ddlVendedor.DataValueField = "Id";
                    ddlVendedor.DataTextField = "NOMBRE";
                    ddlVendedor.DataBind();
                }
                else
                {
                    ddlVendedor.DataSource = null;
                    ddlVendedor.Items.Clear();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string _comando = string.Empty;
            clsConexao conexao = new clsConexao();
            DataSet ds = new DataSet();
            DataSet dscalculos = new DataSet();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            string datainicial = ddlAno.Text + "-" + ((ddlMes.SelectedIndex) + 1).ToString() + "-01";
            string datafinal = string.Empty;// ddlAno.Text + "-" + ((ddlMes.SelectedIndex) + 1).ToString() + "-31";
            DateTime endOfMonth = new DateTime(Convert.ToInt32(ddlAno.Text), ((ddlMes.SelectedIndex) + 1), 1).AddMonths(1).AddDays(-1);
            datafinal = endOfMonth.ToString("yyyy-MM-dd");
            lblTituloRelatorio.Text = _vendedor.nombre.ToString() + " - " + ddlMes.Text + " - " + DateTime.Now.Year.ToString();
            _comando = "select "
                        + " c.id as id"
                        + " ,c.razonsocial"
                        + " from clientes c"
                         + " inner join relacaovendedor r on c.id = r.cliente "
                         + " inner join vendedor v on v.id = r.vendedor "
                         + " where r.vendedor=" + _vendedor.id.ToString()
                         + " order by c.razonsocial asc ";

            ds = conexao.Selecionar(_comando);

            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add("Nº");//0
            dtNovo.Columns.Add("Cliente");//1
            dtNovo.Columns.Add("BDN");//2
            dtNovo.Columns.Add("BRECHA");//3
            dtNovo.Columns.Add("BDP");//4
            for (int i = ((ddlMes.SelectedIndex) + 1); i <= ((ddlMes.SelectedIndex) + 1); i++)
            {
                if (i == 1)
                    dtNovo.Columns.Add("Visitas");//5

                if (i == 2)
                    dtNovo.Columns.Add("Visitas");//6

                if (i == 3)
                    dtNovo.Columns.Add("Visitas");//7

                if (i == 4)
                    dtNovo.Columns.Add("Visitas");//8

                if (i == 5)
                    dtNovo.Columns.Add("Visitas");//9

                if (i == 6)
                    dtNovo.Columns.Add("Visitas");//10

                if (i == 7)
                    dtNovo.Columns.Add("Visitas");//11

                if (i == 8)
                    dtNovo.Columns.Add("Visitas");//12

                if (i == 9)
                    dtNovo.Columns.Add("Visitas");//13

                if (i == 10)
                    dtNovo.Columns.Add("Visitas");//14

                if (i == 11)
                    dtNovo.Columns.Add("Visitas");//15

                if (i == 12)
                    dtNovo.Columns.Add("Visitas");//16
            }

            for (int i = ((ddlMes.SelectedIndex) + 1); i <= ((ddlMes.SelectedIndex) + 1); i++)
            {
                if (i == 1)
                    dtNovo.Columns.Add("Facturacion");//17

                if (i == 2)
                    dtNovo.Columns.Add("Facturacion");//18

                if (i == 3)
                    dtNovo.Columns.Add("Facturacion");//19

                if (i == 4)
                    dtNovo.Columns.Add("Facturacion");//20

                if (i == 5)
                    dtNovo.Columns.Add("Facturacion");//21

                if (i == 6)
                    dtNovo.Columns.Add("Facturacion");//22

                if (i == 7)
                    dtNovo.Columns.Add("Facturacion");//23

                if (i == 8)
                    dtNovo.Columns.Add("Facturacion");//24

                if (i == 9)
                    dtNovo.Columns.Add("Facturacion");//25

                if (i == 10)
                    dtNovo.Columns.Add("Facturacion");//26

                if (i == 11)
                    dtNovo.Columns.Add("Facturacion");//27

                if (i == 12)
                    dtNovo.Columns.Add("Facturacion");//28
            }
            DataRow dr;

            if (ds.Tables[0].Rows.Count > 0)
            {
                int contador = 1;
                //double totalcotizacion = 0;
                //foreach (DataRow linha in ds.Tables[0].Rows)
                //{
                //    //soma de cotizações trimestre
                //    dscalculos = conexao.Selecionar("SELECT sum(cotizacion) FROM bdn b where b.vendedor="
                //        + _vendedor.id.ToString()
                //        + " and b.fecha between '" + datainicial + " 00:00:00' and '" + datafinal + " 23:59:59' ");
                //    if (dscalculos != null)
                //    {
                //        if (dscalculos.Tables[0].Rows.Count > 0)
                //        {
                //            totalcotizacion += (dscalculos.Tables[0].Rows[0][0].ToString() != string.Empty ? Convert.ToDouble(dscalculos.Tables[0].Rows[0][0].ToString()) : 0);
                //        }
                //    }
                //}

                //soma de licitacão no trimestre
                //                double totallicitacao = 0;
                //foreach (DataRow linha in ds.Tables[0].Rows)
                //{
                //    dscalculos = conexao.Selecionar("SELECT sum(cotizacion) FROM bdp b where b.vendedor="
                //        + _vendedor.id.ToString() + " and instituicion=" + linha["id"].ToString()
                //        + " and b.fecha between '" + datainicial + " 00:00:00' and '" + datafinal + " 23:59:59' ");
                //    if (dscalculos != null)
                //    {
                //        if (dscalculos.Tables[0].Rows.Count > 0)
                //        {
                //            totallicitacao = (dscalculos.Tables[0].Rows[0][0].ToString() != string.Empty ? Convert.ToDouble(dscalculos.Tables[0].Rows[0][0].ToString()) : 0);
                //        }
                //    }
                //}
                foreach (DataRow linha in ds.Tables[0].Rows)
                {

                    //soma de cotizações trimestre
                    double totalcotizacion = 0;
                    //dscalculos = conexao.Selecionar("SELECT sum(totalneto) FROM bdn b where b.vendedor="
                    //    + _vendedor.id.ToString() + " and instituicion=" + linha["id"].ToString()
                    //    + " and b.fecha between '" + datainicial + " 00:00:00' and '" + datafinal + " 23:59:59' ");
                    dscalculos = conexao.Selecionar("SELECT sum(totalneto) FROM bdn b where b.vendedor="
                        + _vendedor.id.ToString() + " and instituicion=" + linha["id"].ToString()
                        + " and status='Vigente'");
                    if (dscalculos != null)
                    {
                        if (dscalculos.Tables[0].Rows.Count > 0)
                        {
                            totalcotizacion = (dscalculos.Tables[0].Rows[0][0].ToString() != string.Empty ? Convert.ToDouble(dscalculos.Tables[0].Rows[0][0].ToString()) : 0);
                        }
                    }

                    //soma de brechas no trimestre
                    cBrecha engineBrecha = new cBrecha();
                    List<mBrecha> _listaBrecha = engineBrecha.RetornarBrechaPorData(Convert.ToInt32(_vendedor.id.ToString()), datainicial, datafinal, Convert.ToInt32(linha["id"].ToString()));
                    double totalBrecha = 0;
                    if (_listaBrecha != null)
                    {
                        foreach (mBrecha item in _listaBrecha)
                        {
                            totalBrecha += Convert.ToDouble(item.valor.ToString("N2"));
                        }
                    }


                    double totallicitacao = 0;
                    dscalculos = conexao.Selecionar("SELECT sum(totalneto) FROM bdp b where b.vendedor="
                          + _vendedor.id.ToString() + " and instituicion=" + linha["id"].ToString()
                          + " and b.fecha between '" + datainicial + " 00:00:00' and '" + datafinal + " 23:59:59' ");
                    if (dscalculos != null)
                    {
                        if (dscalculos.Tables[0].Rows.Count > 0)
                        {
                            totallicitacao = (dscalculos.Tables[0].Rows[0][0].ToString() != string.Empty ? Convert.ToDouble(dscalculos.Tables[0].Rows[0][0].ToString()) : 0);
                        }
                    }

                    //somando visitas
                    int janeiro_visitas = 0;
                    int fevereiro_visitas = 0;
                    int marco_visitas = 0;
                    int abril_visitas = 0;
                    int maio_visitas = 0;
                    int junho_visitas = 0;
                    int julho_visitas = 0;
                    int agosto_visitas = 0;
                    int setembro_visitas = 0;
                    int outubro_visitas = 0;
                    int novembro_visitas = 0;
                    int dezembro = 0;
                    //obtendo o intervalo de meses
                    DateTime dtInicial = Convert.ToDateTime(datainicial);
                    DateTime dtfinal = Convert.ToDateTime(datafinal);
                    for (int i = dtInicial.Month; i <= dtInicial.Month; i++)
                    {
                        if (i == 1)
                            janeiro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 2)
                            fevereiro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 3)
                            marco_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 4)
                            abril_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 5)
                            maio_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 6)
                            junho_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 7)
                            julho_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 8)
                            agosto_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 9)
                            setembro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 10)
                            outubro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 11)
                            novembro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 12)
                            dezembro = somarVisitasMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                    }



                    //somand faturados
                    double janeiro_faturado = 0;
                    double fevereiro_faturado = 0;
                    double marco_faturado = 0;
                    double abril_faturado = 0;
                    double maio_faturado = 0;
                    double junho_faturado = 0;
                    double julho_faturado = 0;
                    double agosto_faturado = 0;
                    double setembro_faturado = 0;
                    double outubro_faturado = 0;
                    double novembro_faturado = 0;
                    double dezembro_faturado = 0;
                    for (int i = dtInicial.Month; i <= dtInicial.Month; i++)
                    {
                        if (i == 1)
                            janeiro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 2)
                            fevereiro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 3)
                            marco_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 4)
                            abril_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 5)
                            maio_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 6)
                            junho_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 7)
                            julho_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 8)
                            agosto_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 9)
                            setembro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 10)
                            outubro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 11)
                            novembro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                        if (i == 12)
                            dezembro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), _vendedor.id.ToString());
                    }

                    int totalvisita = 0;
                    for (int i = dtInicial.Month; i <= dtInicial.Month; i++)
                    {
                        if (i == 1)
                            totalvisita= janeiro_visitas;

                        if (i == 2)
                            totalvisita = fevereiro_visitas;

                        if (i == 3)
                            totalvisita = marco_visitas;


                        if (i == 4)
                            totalvisita= abril_visitas;

                        if (i == 5)
                            totalvisita = maio_visitas;


                        if (i == 6)
                            totalvisita = junho_visitas;


                        if (i == 7)
                            totalvisita = julho_visitas;


                        if (i == 8)
                            totalvisita = agosto_visitas;


                        if (i == 9)
                            totalvisita = setembro_visitas;

                        if (i == 10)
                            totalvisita = outubro_visitas;

                        if (i == 11)
                            totalvisita = novembro_visitas;


                        if (i == 12)
                            totalvisita = dezembro;
                    }
                    double totalfaturacion = 0;
                    for (int i = dtInicial.Month; i <= dtInicial.Month; i++)
                    {
                        if (i == 1)
                            totalfaturacion = janeiro_faturado;


                        if (i == 2)
                            totalfaturacion = fevereiro_faturado;


                        if (i == 3)
                            totalfaturacion = marco_faturado;

                        if (i == 4)
                            totalfaturacion = abril_faturado;

                        if (i == 5)
                            totalfaturacion = maio_faturado;

                        if (i == 6)
                            totalfaturacion = junho_faturado;

                        if (i == 7)
                            totalfaturacion = julho_faturado;

                        if (i == 8)
                            totalfaturacion = agosto_faturado;

                        if (i == 9)
                            totalfaturacion = setembro_faturado;

                        if (i == 10)
                            totalfaturacion = outubro_faturado;

                        if (i == 11)
                            totalfaturacion = novembro_faturado;

                        if (i == 12)
                            totalfaturacion = dezembro_faturado;

                    }

                    if (totalcotizacion == 0 && totalBrecha == 0 && totallicitacao == 0 && totalfaturacion == 0 && totalvisita == 0)
                    {
                        int indicetabela = 0;
                        dr = dtNovo.NewRow();
                        dr[indicetabela] = contador.ToString();
                        indicetabela++;
                        dr[indicetabela] = linha["razonsocial"].ToString();
                        indicetabela++;
                        dr[indicetabela] = totalcotizacion.ToString("N2");
                        indicetabela++;
                        dr[indicetabela] = totalBrecha.ToString("N2");
                        indicetabela++;
                        dr[indicetabela] = totallicitacao.ToString("N2");
                        indicetabela++;

                        for (int i = dtInicial.Month; i <= dtInicial.Month; i++)
                        {
                            if (i == 1)
                            {
                                dr[indicetabela] = janeiro_visitas.ToString(); ;
                                indicetabela++;
                            }

                            if (i == 2)
                            {
                                dr[indicetabela] = fevereiro_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 3)
                            {
                                dr[indicetabela] = marco_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 4)
                            {
                                dr[indicetabela] = abril_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 5)
                            {
                                dr[indicetabela] = maio_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 6)
                            {
                                dr[indicetabela] = junho_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 7)
                            {
                                dr[indicetabela] = julho_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 8)
                            {
                                dr[indicetabela] = agosto_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 9)
                            {
                                dr[indicetabela] = setembro_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 10)
                            {
                                dr[indicetabela] = outubro_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 11)
                            {
                                dr[indicetabela] = novembro_visitas.ToString();
                                indicetabela++;
                            }

                            if (i == 12)
                            {
                                dr[indicetabela] = dezembro.ToString();
                                indicetabela++;
                            }
                        }
                        for (int i = dtInicial.Month; i <= dtInicial.Month; i++)
                        {
                            if (i == 1)
                            {
                                dr[indicetabela] = janeiro_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 2)
                            {
                                dr[indicetabela] = fevereiro_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 3)
                            {
                                dr[indicetabela] = marco_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 4)
                            {
                                dr[indicetabela] = abril_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 5)
                            {
                                dr[indicetabela] = maio_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 6)
                            {
                                dr[indicetabela] = junho_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 7)
                            {
                                dr[indicetabela] = julho_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 8)
                            {
                                dr[indicetabela] = agosto_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 9)
                            {
                                dr[indicetabela] = setembro_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 10)
                            {
                                dr[indicetabela] = outubro_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 11)
                            {
                                dr[indicetabela] = novembro_faturado.ToString();
                                indicetabela++;
                            }

                            if (i == 12)
                            {
                                dr[indicetabela] = dezembro_faturado.ToString();
                                indicetabela++;
                            }
                        }
                        dtNovo.Rows.Add(dr);
                        contador++;
                    }
                }
            }
            dgvDados.DataSource = dtNovo;
            dgvDados.DataBind();
        }
        private double somarFaturadosMes(string mes, string pCliente, string pVendedor)
        {
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            string ConsultaSQL = string.Empty;
            DataSet ds = null;
            clsConexao banco = new clsConexao();
            double totalvisitas = 0;
            for (int i = 1; i < 31; i++)
            {
                ConsultaSQL = "select  sum(d" + i.ToString() + "_valor) as total from ventas_dias where mes='"
                    + mesescrito + "' and ano='" + ddlAno.Text +
                    "' and cliente_id=" + pCliente + " and vendedor_id=" + pVendedor;

                ds = banco.Selecionar(ConsultaSQL);
                if (ds != null)
                    totalvisitas += Convert.ToDouble((ds.Tables[0].Rows[0][0].ToString() != string.Empty ? ds.Tables[0].Rows[0][0] : 0));
            }
            return totalvisitas;
        }

        private int somarVisitasMes(string mes, string pCliente, string pVendedor)
        {
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            string ConsultaSQL = string.Empty;
            DataSet ds = null;
            clsConexao banco = new clsConexao();
            int totalvisitas = 0;
            for (int i = 1; i < 31; i++)
            {
                ConsultaSQL = "select * from informediario where mes='" + mesescrito + "' and ano='" + ddlAno.Text +
                    "' and cliente_id=" + pCliente + " and vendedor_id=" + pVendedor + " and "
                    + "(d" + i.ToString() + "_valor='1' or d" + i.ToString() + "_valor='3')";

                ds = banco.Selecionar(ConsultaSQL);
                if (ds != null)
                    totalvisitas += ds.Tables[0].Rows.Count;
            }
            return totalvisitas;
        }

        //protected void Button2_Click(object sender, EventArgs e)
        //{

        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        //    Response.Charset = "";
        //    Response.ContentType = "application/vnd.ms-excel";
        //    using (StringWriter sw = new StringWriter())
        //    {
        //        HtmlTextWriter hw = new HtmlTextWriter(sw);

        //        //To Export all pages
        //        dgvDados.AllowPaging = false;
        //        Button1_Click(null, null);

        //        dgvDados.HeaderRow.BackColor = Color.White;
        //        foreach (TableCell cell in dgvDados.HeaderRow.Cells)
        //        {
        //            cell.BackColor = dgvDados.HeaderStyle.BackColor;
        //        }
        //        foreach (GridViewRow row in dgvDados.Rows)
        //        {
        //            row.BackColor = Color.White;
        //            foreach (TableCell cell in row.Cells)
        //            {
        //                if (row.RowIndex % 2 == 0)
        //                {
        //                    cell.BackColor = dgvDados.AlternatingRowStyle.BackColor;
        //                }
        //                else
        //                {
        //                    cell.BackColor = dgvDados.RowStyle.BackColor;
        //                }
        //                cell.CssClass = "textmode";
        //            }
        //        }

        //        dgvDados.RenderControl(hw);

        //        //style to format numbers to string
        //        string style = @"<style> .textmode { } </style>";
        //        Response.Write(style);
        //        Response.Output.Write(sw.ToString());
        //        Response.Flush();
        //        Response.End();
        //    }
        //}

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvDados.PageIndex = e.NewPageIndex;
            Button1_Click(null, null);
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                dgvDados.AllowPaging = false;
                Button1_Click(null, null);

                dgvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in dgvDados.HeaderRow.Cells)
                {
                    cell.BackColor = dgvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in dgvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = dgvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = dgvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                dgvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string filename = "panel_control_direccion.pdf";
            //salvando a cópia local
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            dgvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            dgvDados.AllowPaging = true;
            dgvDados.DataBind();
        }
    }
}