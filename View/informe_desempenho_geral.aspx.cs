﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class informe_desempenho_geral : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
            if (_usuario != null)
                lblNombre.Text = "Hola " + _usuario.nombre;

            if (!Page.IsPostBack)
            {
                PopularComboVendedores();
                PopularProdutos();

                cVendedor engineVendedor = new cVendedor();
                mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
                ddlAno.Text = DateTime.Now.Year.ToString();
                lblVendedor.Text = _vendedor.nombre.ToString() + " - " + DateTime.Now.Year.ToString();
                Button1_Click(null, null);
            }
        }

        private void PopularComboVendedores()
        {
            cVendedor engineVendedor = new cVendedor();

            DataSet ds = engineVendedor.PopularGrid(0);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendedor.DataSource = ds.Tables[0];
                    ddlVendedor.DataValueField = "Id";
                    ddlVendedor.DataTextField = "NOMBRE";
                    ddlVendedor.DataBind();
                }
                else
                {
                    ddlVendedor.DataSource = null;
                    ddlVendedor.Items.Clear();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }

        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            ddlProduto.DataSource = engineProduto.PopularGrid(0).Tables[0];
            ddlProduto.DataValueField = "Id";
            ddlProduto.DataTextField = "NOMBRE";
            ddlProduto.DataBind();
        }

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            lblVendedor.Text = _vendedor.nombre + " - " + DateTime.Now.Year.ToString();

            cProduto engineProduto = new cProduto();
            mProduto _produto = engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString());

            clog_arquivos engineLogArquivos = new clog_arquivos();
            //string dataInicial = ddlAno.Text + "-" + (ddlMes.SelectedIndex + 1).ToString() + "-01";
            //string dataFinal = ddlAno.Text + "-" + (ddlMes.SelectedIndex + 1).ToString() + "-31";
                   
            
            DataSet ds = new DataSet();            
            clsConexao banco = new clsConexao();
            string _sql = "SELECT * FROM produto";

            if (rdbProduto.Checked)
                _sql += " where id=" + _produto.Id.ToString();

            ds = banco.Selecionar(_sql);
            double[] totalmeta = new double[13];
            double[] totalvenda = new double[13];
            double[] totalRendimento = new double[13];
            double[] dmeta = new double[13];
            double[] dvenda = new double[13];

            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add(" ");//0
            dtNovo.Columns.Add("  ");//1
            dtNovo.Columns.Add("   ");//2
            dtNovo.Columns.Add("    ");//3
            dtNovo.Columns.Add("     ");//4
            dtNovo.Columns.Add("      ");//5
            dtNovo.Columns.Add("       ");//6
            dtNovo.Columns.Add("        ");//7
            dtNovo.Columns.Add("         ");//8
            dtNovo.Columns.Add("          ");//9
            dtNovo.Columns.Add("           ");//10
            dtNovo.Columns.Add("            ");//11
            dtNovo.Columns.Add("             ");//12

            

                  
            DataRow dr;

            if (ds != null)
            {
                foreach (DataRow linha in ds.Tables[0].Rows)
                {
                    for (int i = 0; i < 13; i++)
                    {
                        dmeta[i] = 0;
                        dvenda[i] = 0;
                    }

                    dr = dtNovo.NewRow();
                    dr[0] = "Producto";//0
                    dr[1] =linha["nombre"].ToString();//1
                    dr[2] = " ";//2
                    dr[3] = " ";//3
                    dr[4] = " ";//4
                    dr[5] = " ";//5
                    dr[6] = " ";//6
                    dr[7] = " ";//7
                    dr[8] = " ";//8
                    dr[9] = " ";//9
                    dr[10] = " ";//10
                    dr[11] = " ";//11
                    dr[12] = " ";//12
                    dtNovo.Rows.Add(dr); 

                    dr = dtNovo.NewRow();
                    dr[0]="Mes";//0
                    dr[1] = "Enero";//1
                    dr[2] = "Febrero";//2
                    dr[3] = "Marzo";//3
                    dr[4] = "Abril";//4
                    dr[5] = "Mayo";//5
                    dr[6] = "Junio";//6
                    dr[7] = "Julio";//7
                    dr[8] = "Agosto";//8
                    dr[9] = "Septiembre";//9
                    dr[10] = "Octubre";//10
                    dr[11] = "Noviembre";//11
                    dr[12] = "Diciembre";//12
                    dtNovo.Rows.Add(dr);      


                    dr = dtNovo.NewRow();
                    dr[0] = "Meta";
                    double meta = 0;
                    double metafloor=0;
                    for (int i = 1; i <=12; i++)
                    {
                        meta =CalcularMeta(linha["id"].ToString(), _vendedor.id.ToString(), i);    
                        metafloor = Math.Floor(meta);                        
                        totalmeta[i] += meta;
                        if (double.IsInfinity(meta) || double.IsNaN(meta))
                            dmeta[i] = 0;
                        else
                            dmeta[i] = meta;
                        //dmeta[i] = meta;
                        dr[i] = metafloor.ToString();
                    }                                                                                
                    dtNovo.Rows.Add(dr);

                    dr = dtNovo.NewRow();
                    dr[0] = "Venta";
                    double venta = 0;
                    double ventafloor = 0;
                    for (int i = 1; i <= 12; i++)
                    {
                        venta=CalcularVenta(_vendedor.id.ToString(), linha["id"].ToString(), i.ToString());
                        ventafloor = Math.Floor(venta);
                        totalvenda[i] += venta;
                        if (double.IsInfinity(venta) || double.IsNaN(venta))
                            dvenda[i] = 0;
                        else
                            dvenda[i] = venta;
                        dr[i] = ventafloor.ToString();
                    }
                    dtNovo.Rows.Add(dr);

                    dr = dtNovo.NewRow();
                    dr[0] = "Rendimento";
                    for (int i = 1; i <= 12; i++)
                    {
                        //dr[i] = CalcularRendimento(_vendedor.id.ToString(), linha["id"].ToString(), i.ToString()).ToString("N2");
                        double rendimento = (dvenda[i] / dmeta[i]) * 100;
                        double rendimentofllor = Math.Floor(rendimento);
                        if (double.IsInfinity(rendimento) || double.IsNaN(rendimento))
                        {
                            dr[i] = 0;
                            totalRendimento[i] = 0;
                        }
                        else
                        {
                            dr[i] = rendimentofllor.ToString();
                            totalRendimento[i] = rendimentofllor;
                        }
                    }
                    dtNovo.Rows.Add(dr);

                 

                    //dr = dtNovo.NewRow();
                    //dr[0] = "BDN";
                    //for (int i = 1; i <= 12; i++)
                    //{
                    //    dr[i] = CalcularBDN(_vendedor.id.ToString(), linha["id"].ToString(), i.ToString()).ToString("N2");
                    //}
                    //dtNovo.Rows.Add(dr);      
                }

                dr = dtNovo.NewRow();
                dr[0] = "Total Meta";//0
                dr[1] = totalmeta[1].ToString();//1
                dr[2] = totalmeta[2].ToString();//2
                dr[3] = totalmeta[3].ToString();//3
                dr[4] = totalmeta[4].ToString();//4
                dr[5] = totalmeta[5].ToString();//5
                dr[6] = totalmeta[6].ToString();//6
                dr[7] = totalmeta[7].ToString();//7
                dr[8] = totalmeta[8].ToString();//8
                dr[9] = totalmeta[9].ToString();//9
                dr[10] = totalmeta[10].ToString();//10
                dr[11] = totalmeta[11].ToString();//11
                dr[12] = totalmeta[12].ToString();//12
                dtNovo.Rows.Add(dr);

                dr = dtNovo.NewRow();
                dr[0] = "Total Venda";//0
                dr[1] = totalvenda[1].ToString();//1
                dr[2] = totalvenda[2].ToString();//2
                dr[3] = totalvenda[3].ToString();//3
                dr[4] = totalvenda[4].ToString();//4
                dr[5] = totalvenda[5].ToString();//5
                dr[6] = totalvenda[6].ToString();//6
                dr[7] = totalvenda[7].ToString();//7
                dr[8] = totalvenda[8].ToString();//8
                dr[9] = totalvenda[9].ToString();//9
                dr[10] = totalvenda[10].ToString();//10
                dr[11] = totalvenda[11].ToString();//11
                dr[12] = totalvenda[12].ToString();//12
                dtNovo.Rows.Add(dr);

                dr = dtNovo.NewRow();
                dr[0] = "Total Rendimento";//0
                dr[1] = totalRendimento[1].ToString();//1
                dr[2] = totalRendimento[2].ToString();//2
                dr[3] = totalRendimento[3].ToString();//3
                dr[4] = totalRendimento[4].ToString();//4
                dr[5] = totalRendimento[5].ToString();//5
                dr[6] = totalRendimento[6].ToString();//6
                dr[7] = totalRendimento[7].ToString();//7
                dr[8] = totalRendimento[8].ToString();//8
                dr[9] = totalRendimento[9].ToString();//9
                dr[10] = totalRendimento[10].ToString();//10
                dr[11] = totalRendimento[11].ToString();//11
                dr[12] = totalRendimento[12].ToString();//12
                dtNovo.Rows.Add(dr);   
            }

            dgvDados.DataSource = dtNovo;
            dgvDados.DataBind();
        }


        private double CalcularMeta(string produto_id,string vendedor_id,int mes)
        {
            DataSet ds = new DataSet();
            clsConexao banco = new clsConexao();
            double meta = 0;
            string _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor";

            if (mes == 1)
                _sql += " ,sum(a.Enero) as Meta";
            if (mes == 2)
                _sql += " ,sum(a.Febrero) as Meta";
            if (mes == 3)
                _sql += " ,sum(a.Marzo) as Meta";
            if (mes == 4)
                _sql += " ,sum(a.Abril) as Meta";
            if (mes == 5)
                _sql += " ,sum(a.Mayo) as Meta";
            if (mes == 6)
                _sql += " ,sum(a.Junio) as Meta";
            if (mes == 7)
                _sql += " ,sum(a.Julio) as Meta";
            if (mes == 8)
                _sql += " ,sum(a.Agosto) as Meta";
            if (mes == 9)
                _sql += " ,sum(a.Septiembre) as Meta";
            if (mes == 10)
                _sql += " ,sum(a.Octubre) as Meta";
            if (mes == 11)
                _sql += " ,sum(a.Noviembre) as Meta";
            if (mes == 12)
                _sql += " ,sum(a.Diciembre) as Meta";


            _sql += " from asignacionmetas a"
                        + " inner join vendedor v on a.vendedor = v.id"
                        + " inner join produto p on a.produto = p.id"
                        + " inner join dias_uteis d"
                        + " where v.id=" + vendedor_id
                        + " and produto=" + produto_id
                        + " order by v.nombre";
            ds = banco.Selecionar(_sql);

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if(ds.Tables[0].Rows[0]["Meta"].ToString()!=string.Empty)
                        meta = Convert.ToDouble(ds.Tables[0].Rows[0]["Meta"].ToString());
                }
            }

            if (double.IsInfinity(meta) || double.IsNaN(meta))
                return 0;
            else
                return meta; 

        }
        private double CalcularVenta(string pCodigoVendedor, string pCodigoProduto, string mes)
        {
            string _sql = string.Empty;
            clsConexao banco = new clsConexao();
            DataSet ds = null;
            double total = 0;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;
            _sql = "SELECT"
                            + " (d1_valor + d2_valor+d3_valor+d4_valor+d5_valor+d6_valor"
                            + " +d7_valor + d8_valor+d9_valor+d10_valor+d11_valor+d12_valor"
                            +"+d13_valor+d14_valor+d15_valor+d16_valor+d17_valor+d18_valor+d19_valor"
                            + "+d20_valor+d21_valor+d22_valor+d23_valor+d24_valor+d25_valor+d26_valor"
                            + "+d27_valor+d28_valor+d29_valor+d30_valor+d31_valor)as total"
                            + " FROM ventas_dias where ano='" + ddlAno.Text + "' and mes = '" + mesescrito
                            + " ' and grupo_dia=" + pCodigoProduto
                            + " and vendedor_id=" + pCodigoVendedor;

            ds = banco.Selecionar(_sql);
            
            if (ds != null)
            {
                foreach (DataRow linha2 in ds.Tables[0].Rows)
                {
                    if (linha2["total"].ToString() != string.Empty)
                        total += Convert.ToDouble(linha2["total"].ToString());
                }
            }

            if (double.IsInfinity(total) || double.IsNaN(total))
                return 0;
            else
                return total;
        }
        private double CalcularRendimento(string pCodigoVendedor, string pCodigoProduto,string mes)
        {
            //valor total de vendas
            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            DataSet ds = engineVentasDiarias.PopularGridVendedorProduto(pCodigoVendedor, pCodigoProduto);
            double total = 0;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                for (int i = 1; i <= 31; i++)
                {
                    total += (item[i.ToString()] != string.Empty ? Convert.ToDouble(item[i.ToString()].ToString()) : 0);
                }

            }
            //txtvalorAcumuladoDia.Text = total.ToString();

            //total de dias trabalhados
            double dias_trabalhadoscalc = 0;

            //double d1_valor = 0;
            //double d2_valor = 0;
            //double d3_valor = 0;
            //double d4_valor = 0;
            //double d5_valor = 0;
            //double d6_valor = 0;
            //double d7_valor = 0;
            //double d8_valor = 0;
            //double d9_valor = 0;
            //double d310_valor = 0;
            //double d311_valor = 0;
            //double d312_valor = 0;
            //double d313_valor = 0;
            //double d314_valor = 0;
            //double d315_valor = 0;
            //double d316_valor = 0;
            //double d317_valor = 0;
            //double d318_valor = 0;
            //double d319_valor = 0;
            //double d20_valor = 0;
            //double d21_valor = 0;
            //double d22_valor = 0;
            //double d23_valor = 0;
            //double d24_valor = 0;
            //double d25_valor = 0;
            //double d26_valor = 0;
            //double d27_valor = 0;
            //double d28_valor = 0;
            //double d29_valor = 0;
            //double d30_valor = 0;
            //double d31_valor = 0;
            //cInformediario engineInforme = new cInformediario();
            //List<mInformediario> _ListInforme = engineInforme.RetornarInformediario(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), pCodigoVendedor);
            //if (_ListInforme != null)
            //{
            //    if (_ListInforme.Count > 0)
            //    {
            //        foreach (mInformediario item in _ListInforme)
            //        {
            //            if (item.d1_valor == "0" || item.d1_valor == "1" || item.d1_valor == "2" || item.d1_valor == "3" || item.d1_valor == "4")
            //                d1_valor = 1;
            //            if (item.d2_valor == "0" || item.d2_valor == "1" || item.d2_valor == "2" || item.d2_valor == "3" || item.d2_valor == "4")
            //                d2_valor = 1;
            //            if (item.d3_valor == "0" || item.d3_valor == "1" || item.d3_valor == "2" || item.d3_valor == "3" || item.d3_valor == "4")
            //                d3_valor = 1;
            //            if (item.d4_valor == "0" || item.d4_valor == "1" || item.d4_valor == "2" || item.d4_valor == "3" || item.d4_valor == "4")
            //                d4_valor = 1;
            //            if (item.d5_valor == "0" || item.d5_valor == "1" || item.d5_valor == "2" || item.d5_valor == "3" || item.d5_valor == "4")
            //                d5_valor = 1;
            //            if (item.d6_valor == "0" || item.d6_valor == "1" || item.d6_valor == "2" || item.d6_valor == "3" || item.d6_valor == "4")
            //                d6_valor = 1;
            //            if (item.d7_valor == "0" || item.d7_valor == "1" || item.d7_valor == "2" || item.d7_valor == "3" || item.d7_valor == "4")
            //                d7_valor = 1;
            //            if (item.d8_valor == "0" || item.d8_valor == "1" || item.d8_valor == "2" || item.d8_valor == "3" || item.d8_valor == "4")
            //                d8_valor = 1;
            //            if (item.d9_valor == "0" || item.d9_valor == "1" || item.d9_valor == "2" || item.d9_valor == "3" || item.d9_valor == "4")
            //                d9_valor = 1;
            //            if (item.d10_valor == "0" || item.d10_valor == "1" || item.d10_valor == "2" || item.d10_valor == "3" || item.d10_valor == "4")
            //                d310_valor = 1;
            //            if (item.d11_valor == "0" || item.d11_valor == "1" || item.d11_valor == "2" || item.d11_valor == "3" || item.d11_valor == "4")
            //                d311_valor = 1;
            //            if (item.d12_valor == "0" || item.d12_valor == "1" || item.d12_valor == "2" || item.d12_valor == "3" || item.d12_valor == "4")
            //                d312_valor = 1;
            //            if (item.d13_valor == "0" || item.d13_valor == "1" || item.d13_valor == "2" || item.d13_valor == "3" || item.d13_valor == "4")
            //                d313_valor = 1;
            //            if (item.d14_valor == "0" || item.d14_valor == "1" || item.d14_valor == "2" || item.d14_valor == "3" || item.d14_valor == "4")
            //                d314_valor = 1;
            //            if (item.d15_valor == "0" || item.d15_valor == "1" || item.d15_valor == "2" || item.d15_valor == "3" || item.d15_valor == "4")
            //                d315_valor = 1;
            //            if (item.d16_valor == "0" || item.d16_valor == "1" || item.d16_valor == "2" || item.d16_valor == "3" || item.d16_valor == "4")
            //                d316_valor = 1;
            //            if (item.d17_valor == "0" || item.d17_valor == "1" || item.d17_valor == "2" || item.d17_valor == "3" || item.d17_valor == "4")
            //                d317_valor = 1;
            //            if (item.d18_valor == "0" || item.d18_valor == "1" || item.d18_valor == "2" || item.d18_valor == "3" || item.d18_valor == "4")
            //                d318_valor = 1;
            //            if (item.d19_valor == "0" || item.d19_valor == "1" || item.d19_valor == "2" || item.d19_valor == "3" || item.d19_valor == "4")
            //                d319_valor = 1;
            //            if (item.d20_valor == "0" || item.d20_valor == "1" || item.d20_valor == "2" || item.d20_valor == "3" || item.d20_valor == "4")
            //                d20_valor = 1;
            //            if (item.d21_valor == "0" || item.d21_valor == "1" || item.d21_valor == "2" || item.d21_valor == "3" || item.d21_valor == "4")
            //                d21_valor = 1;
            //            if (item.d22_valor == "0" || item.d22_valor == "1" || item.d22_valor == "2" || item.d22_valor == "3" || item.d22_valor == "4")
            //                d22_valor = 1;
            //            if (item.d23_valor == "0" || item.d23_valor == "1" || item.d23_valor == "2" || item.d23_valor == "3" || item.d23_valor == "4")
            //                d23_valor = 1;
            //            if (item.d24_valor == "0" || item.d24_valor == "1" || item.d24_valor == "2" || item.d24_valor == "3" || item.d24_valor == "4")
            //                d24_valor = 1;
            //            if (item.d25_valor == "0" || item.d25_valor == "1" || item.d25_valor == "2" || item.d25_valor == "3" || item.d25_valor == "4")
            //                d25_valor = 1;
            //            if (item.d26_valor == "0" || item.d26_valor == "1" || item.d26_valor == "2" || item.d26_valor == "3" || item.d26_valor == "4")
            //                d26_valor = 1;
            //            if (item.d27_valor == "0" || item.d27_valor == "1" || item.d27_valor == "2" || item.d27_valor == "3" || item.d27_valor == "4")
            //                d27_valor = 1;
            //            if (item.d28_valor == "0" || item.d28_valor == "1" || item.d28_valor == "2" || item.d28_valor == "3" || item.d28_valor == "4")
            //                d28_valor = 1;
            //            if (item.d29_valor == "0" || item.d29_valor == "1" || item.d29_valor == "2" || item.d29_valor == "3" || item.d29_valor == "4")
            //                d29_valor = 1;
            //            if (item.d30_valor == "0" || item.d30_valor == "1" || item.d30_valor == "2" || item.d30_valor == "3" || item.d30_valor == "4")
            //                d30_valor = 1;
            //            if (item.d31_valor == "0" || item.d31_valor == "1" || item.d31_valor == "2" || item.d31_valor == "3" || item.d31_valor == "4")
            //                d31_valor = 1;
            //        }
            //    }
            //}


            //dias_trabalhadoscalc =
            //    d1_valor +
            //    d2_valor +
            //    d3_valor +
            //    d4_valor +
            //    d5_valor +
            //    d6_valor +
            //    d7_valor +
            //    d8_valor +
            //    d9_valor +
            //    d310_valor +
            //    d311_valor +
            //    d312_valor +
            //    d313_valor +
            //    d314_valor +
            //    d315_valor +
            //    d316_valor +
            //    d317_valor +
            //    d318_valor +
            //    d319_valor +
            //    d20_valor +
            //    d21_valor +
            //    d22_valor +
            //    d23_valor +
            //    d24_valor +
            //    d25_valor +
            //    d26_valor +
            //    d27_valor +
            //    d28_valor +
            //    d29_valor +
            //    d30_valor +
            //    d31_valor;

            //txtdias_trabalhados.Text = dias_trabalhadoscalc.ToString();

            //dias uteis
            //double dias_uteis = 0;
            //cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            //List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            //cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            //mParametrosDiversos _paramDiv = new mParametrosDiversos();
            //for (int i = 0; i < 31; i++)
            //{
            //    ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
            //    if (ListaParametroCartaGantt != null)
            //    {
            //        if (ListaParametroCartaGantt.Count > 0)
            //        {
            //            dias_uteis++;
            //        }
            //    }
            //}

            //txtdiasUteisMes.Text = dias_uteis.ToString();
            double dias_uteis = 0;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dias_uteis++;
                        if (i <= DateTime.Now.Day)
                            dias_trabalhadoscalc++;
                    }
                }
            }
            cDiasUteis engineDiasUteis = new cDiasUteis();
            dias_uteis = Convert.ToDouble(engineDiasUteis.PopularGridPorAno(DateTime.Now.Year.ToString()).Tables[0].Rows[0][DateTime.Now.Month + 1].ToString());

            //meta    
            //string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT sum(" + mesescrito + ") as total FROM asignacionmetas a where a.vendedor=" + pCodigoVendedor + " and produto=" + pCodigoProduto);
            double metacalc = 0;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if(ds.Tables[0].Rows[0]["total"].ToString()!=string.Empty)
                        metacalc = Convert.ToDouble(ds.Tables[0].Rows[0]["total"]);
                }
            }

            //txtmeta.Text = metacalc.ToString();

            double valorAcumuladoDia = Convert.ToDouble(total);
            double dias_trabalhados = Convert.ToDouble(dias_trabalhadoscalc);
            double diasUteisMes = Convert.ToDouble(dias_uteis);
            double meta = Convert.ToDouble(metacalc);
            double resultado = 0;
            resultado = (((valorAcumuladoDia / dias_trabalhados) * diasUteisMes) / meta) * 100;

            if (double.IsInfinity(resultado) || double.IsNaN(resultado))
                return 0;
            else
                return resultado;
        }

        private double CalcularBDN(string pCodigoVendedor, string pCodigoProduto,string mes)
        {

            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            DataSet ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT sum(" + mesescrito + ") as total FROM asignacionmetas a where a.vendedor=" + pCodigoVendedor + " and a.produto=" + pCodigoProduto);
            double metacalc = (ds.Tables[0].Rows[0]["total"].ToString()==string.Empty?0: Convert.ToDouble(ds.Tables[0].Rows[0]["total"]));
            //txtmeta3.Text = metacalc.ToString();

            //tkc
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _parametro = engineParametrosDiversos.RetornarParametrosDiversos()[0];
            //txttkc.Text = _parametro.tkc.ToString();

            //bdn 
            cBDN engineBDN = new cBDN();
            ds = new DataSet();

            ds = engineBDN.PopularGrid_2PorVendedorProdutoCalculo(Convert.ToInt32(pCodigoVendedor), Convert.ToInt32(pCodigoProduto));
            double media = 0;
            double total = 0;
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["ESTADO"].ToString() == "Vigente" && ds.Tables[0].Rows[i]["Expirado"].ToString() == "N")
                    {
                        totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["TOTAL NETO"].ToString());
                        total++;
                    }
                }
                //txtbdn.Text = totalvalor.ToString("N0");
            }


            double bdn = Convert.ToDouble(totalvalor);
            double tkc = Convert.ToDouble(_parametro.tkc.ToString());
            double meta = Convert.ToDouble(metacalc);
            //lblbdn.Text = "BDN" + (((bdn * (tkc / 100)) / meta) * 100).ToString();
            double resultado = ((bdn * (tkc / 100)) / meta) * 100;

            if (double.IsNaN(resultado) || double.IsInfinity(resultado))
                return 0;

            return resultado;

        }

        protected void dgvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        protected void rdbTodos_CheckedChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void rdbProduto_CheckedChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);

        }

        protected void ddlproduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }
    }
}