﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class informe_desempenho_diario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                PopularComboVendedores();

                cVendedor engineVendedor = new cVendedor();
                mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
                ddlAno.Text = DateTime.Now.Year.ToString();
                ddlMes.SelectedIndex = DateTime.Now.Month - 1;
                lblTituloRelatorio.Text = _vendedor.nombre.ToString() + " - " + ddlMes.Text + " - " + DateTime.Now.Year.ToString();
                Button1_Click(null, null);

                if (!Page.IsPostBack)
                {
                    if (_usuario != null)
                        lblNombre.Text = "Hola " + _usuario.nombre.ToString();
                }
            }
        }

        private void PopularComboVendedores()
        {
            cVendedor engineVendedor = new cVendedor();

            DataSet ds = engineVendedor.PopularGrid(0);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendedor.DataSource = ds.Tables[0];
                    ddlVendedor.DataValueField = "Id";
                    ddlVendedor.DataTextField = "NOMBRE";
                    ddlVendedor.DataBind();
                }
                else
                {
                    ddlVendedor.DataSource = null;
                    ddlVendedor.Items.Clear();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            clog_arquivos engineLogArquivos = new clog_arquivos();
            string dataInicial=ddlAno.Text+"-"+(ddlMes.SelectedIndex+1).ToString()+"-01";
            string dataFinal=ddlAno.Text+"-"+(ddlMes.SelectedIndex+1).ToString()+"-31";
            dgvDados.DataSource = engineLogArquivos.Retornarlog_arquivoPorData(_vendedor.id, dataInicial, dataFinal, "vista previa");
            dgvDados.DataBind();
        }

        protected void dgvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink myLink = new HyperLink();
                myLink.Text = e.Row.Cells[1].Text;
                e.Row.Cells[1].Controls.Add(myLink);
                //myLink.NavigateUrl = Server.MapPath("~/pdf") + "/" + e.Row.Cells[1].Text;
                myLink.NavigateUrl =  "~/pdf/" + e.Row.Cells[1].Text;
            }
        }
    }
}

