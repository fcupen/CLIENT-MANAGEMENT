﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vista_previa_pdf.aspx.cs" Inherits="Clients_Management.View.vista_previa_pdf" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
    <meta charset="utf-8">
  <title></title>
  <style type="text/css">
<!--
.style1 {
	font-size: 14px;
	font-weight: bold;
}
.style2 {font-size: 14px}
.style3 {font-size: 12px}
-->
  </style>
 </head>
<body>
<br>
<br>
<br>
<img src="http://www.asksystems.com.br/webgestion/images/tit_informe.jpg" id="Img1" height="75" width="567">
<table width="620" border="0" align="left" cellpadding="18" cellspacing="0">
  <tr>
    <td width="226" bgcolor="#E5E5E5"><span class="style1">Vendedor:</span>
      <asp:Label ID="lblNomeVendedor" runat="server" Text="Label"></asp:Label></td>
    <td width="334"><b><span class="style2">Fecha</span>:</b><asp:Label ID="lblFechaVendedor" runat="server" Text="Label"></asp:Label></td>
  </tr>
  <tr>
    <td height="40" colspan="2"><div align="left"><b><span class="style2">Codigo Vendedor</span>:</b>
        <asp:Label ID="lblCodigoVendedor" runat="server" Text="Label"></asp:Label> 
        -
<b> <span class="style2">Email</span>:</b>
<asp:Label ID="lblEmailVendedor" runat="server" Text="Label"></asp:Label>
</div></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
  <tr bgcolor="#E5E5E5" >
    <td colspan="2" align="center"><span class="style2"><strong>INFORME DE ACCIONES DE VENTA DIARIAS</strong></span></td>
  </tr>
  <tr>
    <td colspan="2"><table width="87%" border="1" cellpadding="1" cellspacing="1" bordercolor="#E5E5E5" style="text-align: left; width: 100%;">
        <tbody>
          <tr>
            <td><span class="style3">CLIENTE</span></td>
            <td><span class="style3">Contacto</span></td>
            <td><span class="style3">ACUERDOS ALCANZADOS</span></td>
            <td><span class="style3">Proxima Actividad</span></td>
          </tr>
          <%
                Clients_Management.Controller.cInformediario engineInformediario = new Clients_Management.Controller.cInformediario();
                System.Data.DataTable dt = engineInformediario.PopularGridVistaPrevia(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString()).Tables[0];
                foreach (System.Data.DataRow item in dt.Rows)
                {
                    string Instituicion = item[0].ToString();
                    string Contacto = item[1].ToString();
                    string Acuerdos = item[2].ToString();
                    string proxima = item[3].ToString();
            %>
          <tr>
            <td><p class="style3"><%=Instituicion %></p></td>
            <td><p class="style3"><%=Contacto %></p></td>
            <td><p class="style3"><%=Acuerdos %></p></td>
            <td><p class="style3"><%=proxima %></p></td>
          </tr>
          <%
                }
             %>
        </tbody>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#E5E5E5"><span class="style2"><strong>Resumen</strong></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><span class="style3">N&ordm; de visitas realizadas:
          <asp:Label ID="lblNumeroVisitasRealizadas" runat="server" Text="Label"></asp:Label>
          <p></p>
      </span>
        <p class="style3">N&ordm; de contactos telefonicas:
            <asp:Label ID="lblNumerodeVisitasTelefonicas" runat="server" Text="Label"></asp:Label>
        </p>
        <p class="style3">N&ordm; de llamadas de seguimiento a negocios:
            <asp:Label ID="lblNumeroLamadas" runat="server" Text="Label"></asp:Label>
        </p>
        <p class="style3">N&ordm; de visitas Abortadas:
            <asp:Label ID="lblAbortadas" runat="server" Text="Label"></asp:Label>
      </p></td>
  </tr>
  <tr>
    <td><form name="form1" method="post" action="">
        <input type="submit" name="Submit" value="Enviar">
    </form></td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
