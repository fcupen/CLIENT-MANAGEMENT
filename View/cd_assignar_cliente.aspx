﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_assignar_cliente.aspx.cs" Inherits="Clients_Management.View.cd_assignar_cliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div id="header"></div>

<div id="section_header">
  <div class="container">
    <h2><span>ASIGNA CARTERA DE CLIENTES COM PRODUCTO </span></h2>
  </div>
</div>
<div id="about_section_1">
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p>Hola NOMBRA ADMINISTRATOR 
     </div>
     
     <div class="col-md-12">
       <h3 align="center">Asigna Cartera de Cliente com Producto</h3>
       <div class="form-horizontal well" >
          <div class="row">
 
					<div class="col-xs-12">
					
							
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							
                            <div class="col-lg-6">
                                <label>Selecione o Producto:</label><br/>
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false"></asp:TextBox>
								<asp:DropDownList ID="ddlProduto" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVendedor_SelectedIndexChanged"></asp:DropDownList>
							</div>
						</div>
					</div>
				</div>

                        <div class="form-group">
					<div class="rows">
                        
						<div class="col-md-8">
							
                            <div class="col-lg-6"><label>Selecione o Cliente:</label><br />
                               <asp:DropDownList ID="ddlCliente" class="form-control input-sm" runat="server" ></asp:DropDownList>
							</div>
						</div>
					</div>
				</div>
 
 
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
								<asp:Button ID="btnSalvar" runat="server" class="btn btn-success btn-lg" Text="Asignar" OnClick="btnSalvar_Click" />                                
							</div>
						</div>
					</div>
				</div>
				</div>
 
				</div>	
	   </div>
     </div>
	 
	 	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">EQUIPO DE VENTAS Y ASIGNACIONES</h4>
            <asp:Label ID="lblVendedor" runat="server" Text="Vendedor"></asp:Label>
			<hr width="70%">
            <asp:GridView ID="gdvDados" runat="server" CssClass="table table-striped" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px">
                <Columns>
                    <asp:CommandField DeleteText="Deletar" ShowDeleteButton="True" />
                </Columns>
            </asp:GridView>					
		</div>
	</div>
 
</div>

 <div class="row">  
<div class="col-lg-12" align="center">
<asp:Button ID="btnVoltar" runat="server" class="btn btn-success btn-lg" Text="Volver Administrar Cartera de Clientes" PostBackUrl="~/View/adm_parametros.aspx" />
</div>
<p></p>
</div>

</div>
      </div>
    </div>
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
