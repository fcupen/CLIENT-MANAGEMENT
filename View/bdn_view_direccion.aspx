﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="bdn_view_direccion.aspx.cs" Inherits="Clients_Management.View.bdn_view_direccion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    					
    <div class="form-group">
			<div class="rows">
				<div class="col-md-12">
                    <div class="col-md-4">
                        <asp:Label ID="lblVendedor" runat="server" Text="Nome Vendedor"></asp:Label>
                    </div>
					<div class="col-md-4">
                        <asp:Label ID="lblTotalCotizacoes" runat="server" Text="Cotizaciones: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblMediaCotizacao" runat="server" Text="Media Cotización: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblTotal" runat="server" Text="Total Cotización: 00"></asp:Label>
					</div>
                    
				</div>
			</div>
		</div>
    <div class="well">
			<h4 class="text-center">RELACION DE INFORMES DE BDN</h4>
			<hr width="70%">
            <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" >
                <Columns>
                    <asp:CommandField DeleteText="Deletar" ShowDeleteButton="True" />
                </Columns>
            </asp:GridView>				
		</div>    	     	
            <div class="form-group">
			<div class="rows">
				<div class="col-md-12">
					<div class="col-md-4">
                        <asp:Label ID="lblTotalDominate" runat="server" Text="Total Dominante: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblMediaDominante" runat="server" Text="Média Dominante: 00"></asp:Label>
					</div>
                    <div class="col-md-4">
                        <asp:Label ID="lblTotalAnomala" runat="server" Text="Total Anomala: 00"></asp:Label>
					</div>
				</div>

				</div>
			</div>

      
<div class="row">  
<div class="col-lg-12" align="center">

    <asp:Button ID="btnVoltar" runat="server" class="btn btn-success btn-lg" Text="Volver INFORME BDN" PostBackUrl="~/View/informe_bdn.aspx" />
<p></p>
</div>
</div>
</asp:Content>
