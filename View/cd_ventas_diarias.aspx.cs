﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_ventas_diarias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();

                string mes = DateTime.Now.Month.ToString();
                string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            lblTitulo.Text = "VENTAS HISTORICAS - " + mesescrito;
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                PopularProdutos();                
                PopularVendedores();
                PopularClientes();
                PopularGridGeral(0);                
                GerarDias();
                PreencherDiasDoMes();
                
            }
        }

        private void PopularClientes()
        {
            cClientes engineClientes = new cClientes();
            //ddlCliente.DataSource = engineClientes.PopularGrid(0).Tables[0];
            ddlCliente.DataSource = engineClientes.PopularGridPorVendedor(Convert.ToInt32(ddlVendedor.SelectedValue)).Tables[0];
            ddlCliente.DataValueField = "Id";
            ddlCliente.DataTextField = "Razon Social";
            ddlCliente.DataBind();
        }

        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            ddlProduto.DataSource = engineProduto.PopularGrid(0).Tables[0];
            ddlProduto.DataValueField = "id";
            ddlProduto.DataTextField = "nombre";
            ddlProduto.DataBind();
        }

        private void PopularVendedores()
        {
            cVendedor engineVendedor = new cVendedor();
            if (engineVendedor.PopularGrid(0) != null)
            {
                ddlVendedor.DataSource = engineVendedor.PopularGrid(0).Tables[0];
                ddlVendedor.DataValueField = "Id";
                ddlVendedor.DataTextField = "nombre";
                ddlVendedor.DataBind();
            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }

       
        private void PopularGridGeral(int limite)
        {
            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            DataSet ds = engineVentasDiarias.PopularGrid("0", DateTime.Now.Month.ToString());
            //DataSet ds = engineVentasDiarias.PopularGrid(_vendedor.id.ToString(),DateTime.Now.Month.ToString());
            gdvDados.DataSource = ds;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();


        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            mVentasDias _ventasDiarias = new mVentasDias();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();

            mVendedor _vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            mProduto _produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
            mClientes _cliente = (ddlCliente.SelectedValue != null ?engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);
            IList<mVentasDias> lstVendasDias = engineVentasDiarias.VentasDias(Convert.ToInt32(ddlAno.Text), ddlMes.Text, (_vendedor != null ? _vendedor.id : 0), (_produto != null ? _produto.Id : 0), (_cliente!= null ? _cliente.id : 0));
            if (txtid.Text == string.Empty || txtid.Text == "0")
            {
                if (lstVendasDias != null)
                {
                    if (lstVendasDias.Count > 0)
                    {
                        _ventasDiarias.id = lstVendasDias[0].id;
                        txtid.Text = _ventasDiarias.id.ToString();
                    }
                }
            }
            else
            {
                _ventasDiarias.id = Convert.ToInt32(txtid.Text);
            }


            _ventasDiarias.ano = ddlAno.Text;
            _ventasDiarias.mes = ddlMes.Text;
            _ventasDiarias.vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            _ventasDiarias.produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
            _ventasDiarias.cliente = (ddlCliente.SelectedValue != null ? engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);
            _ventasDiarias.d1_valor = txtd1.Text;
            _ventasDiarias.d2_valor = txtd2.Text;
            _ventasDiarias.d3_valor = txtd3.Text;
            _ventasDiarias.d4_valor = txtd4.Text;
            _ventasDiarias.d5_valor = txtd5.Text;
            _ventasDiarias.d6_valor = txtd6.Text;
            _ventasDiarias.d7_valor = txtd7.Text;
            _ventasDiarias.d8_valor = txtd8.Text;
            _ventasDiarias.d9_valor = txtd9.Text;
            _ventasDiarias.d10_valor = txtd10.Text;
            _ventasDiarias.d11_valor = txtd11.Text;
            _ventasDiarias.d12_valor = txtd12.Text;
            _ventasDiarias.d13_valor = txtd13.Text;
            _ventasDiarias.d14_valor = txtd14.Text;
            _ventasDiarias.d15_valor = txtd15.Text;
            _ventasDiarias.d16_valor = txtd16.Text;
            _ventasDiarias.d17_valor = txtd17.Text;
            _ventasDiarias.d18_valor = txtd18.Text;
            _ventasDiarias.d19_valor = txtd19.Text;
            _ventasDiarias.d20_valor = txtd20.Text;
            _ventasDiarias.d21_valor = txtd21.Text;
            _ventasDiarias.d22_valor = txtd22.Text;
            _ventasDiarias.d23_valor = txtd23.Text;
            _ventasDiarias.d24_valor = txtd24.Text;
            _ventasDiarias.d25_valor = txtd25.Text;
            _ventasDiarias.d26_valor = txtd26.Text;
            _ventasDiarias.d27_valor = txtd27.Text;
            _ventasDiarias.d28_valor = txtd28.Text;
            _ventasDiarias.d29_valor = txtd29.Text;
            _ventasDiarias.d30_valor = txtd30.Text;
            _ventasDiarias.d31_valor = txtd31.Text;

            _ventasDiarias.id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                _ventasDiarias.id = Convert.ToInt32(engineVentasDiarias.Inserir(_ventasDiarias)); ;
            }
            else
            {
                engineVentasDiarias.Update(_ventasDiarias);
            }




            PopularGridGeral(0);
            
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlAno.SelectedIndex = 0;
            ddlMes.SelectedIndex = 0;
            ddlVendedor.SelectedIndex = 0;
            ddlProduto.SelectedIndex = 0;
            LimparControles();
            //calendario.Text = string.Empty;
            //ddlClientes.SelectedIndex = 0;
            //ddlProduto.SelectedIndex = 0;
            //ddlStatus.SelectedIndex = 0;
            //cotizacion.Text = string.Empty;
            //contacto.Text = string.Empty;
            //descripcion.Text = string.Empty;
            //totalneto.Text = string.Empty;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cVentasDiarias engineVendas = new cVentasDiarias();
            mVentasDias _ventasDiarias = engineVendas.VentasDias(gdvDados.SelectedRow.Cells[2].Text);
            txtid.Text = _ventasDiarias.id.ToString();

            ddlAno.Text=_ventasDiarias.ano  ;
             ddlMes.Text=_ventasDiarias.mes ;
            ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_ventasDiarias.vendedor.id.ToString()));
            ddlProduto.SelectedIndex = ddlProduto.Items.IndexOf(ddlProduto.Items.FindByValue(_ventasDiarias.produto.Id.ToString()));
            ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(_ventasDiarias.cliente.id.ToString()));
            txtd1.Text=_ventasDiarias.d1_valor ;
            txtd2.Text=_ventasDiarias.d2_valor ;
             txtd3.Text=_ventasDiarias.d3_valor ;
             txtd4.Text = _ventasDiarias.d4_valor;
             txtd5.Text=_ventasDiarias.d5_valor;
             txtd6.Text=_ventasDiarias.d6_valor ;
             txtd7.Text=_ventasDiarias.d7_valor ;
             txtd8.Text=_ventasDiarias.d8_valor ;
             txtd9.Text=_ventasDiarias.d9_valor ;
             txtd10.Text=_ventasDiarias.d10_valor ;
             txtd11.Text=_ventasDiarias.d11_valor ;
             txtd12.Text=_ventasDiarias.d12_valor ;
             txtd13.Text=_ventasDiarias.d13_valor ;
            txtd14.Text=_ventasDiarias.d14_valor ;
             txtd15.Text=_ventasDiarias.d15_valor ;
             txtd16.Text=_ventasDiarias.d16_valor ;
             txtd17.Text=_ventasDiarias.d17_valor ;
             txtd18.Text=_ventasDiarias.d18_valor ;
             txtd19.Text=_ventasDiarias.d19_valor ;
             txtd20.Text=_ventasDiarias.d20_valor ;
             txtd21.Text=_ventasDiarias.d21_valor ;
             txtd22.Text=_ventasDiarias.d22_valor ;
             txtd23.Text=_ventasDiarias.d23_valor ;
             txtd24.Text=_ventasDiarias.d24_valor ;
             txtd25.Text=_ventasDiarias.d25_valor ;
            txtd26.Text=_ventasDiarias.d26_valor ;
            txtd27.Text=_ventasDiarias.d27_valor ;
             txtd28.Text=_ventasDiarias.d28_valor ;
            txtd29.Text=_ventasDiarias.d29_valor ;
            txtd30.Text=_ventasDiarias.d30_valor ;
            txtd31.Text=_ventasDiarias.d31_valor ;
        }

        public void PreencherDiasDoMes()
        {
            cVentasDiarias engineVentasDiaria = new cVentasDiarias();
            mVentasDias _VentasDias = new mVentasDias();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            mVendedor _vendedor = (ddlVendedor.SelectedValue != null ? engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString()) : null);
            mProduto _produto = (ddlProduto.SelectedValue != null ? engineProduto.RetornarProduto(ddlProduto.SelectedValue.ToString()) : null);
            IList<mVentasDias> lstVendasDias = engineVentasDiaria.VentasDias(Convert.ToInt32(ddlAno.Text),ddlMes.Text,_vendedor.id,_produto.Id);
            if(lstVendasDias!=null)
            {
                if(lstVendasDias.Count>0)
                {
                    _VentasDias = lstVendasDias[0];

                    txtd1.Text = _VentasDias.d1_valor;
                    txtd2.Text = _VentasDias.d2_valor;
                    txtd3.Text = _VentasDias.d3_valor;
                    txtd4.Text = _VentasDias.d4_valor;
                    txtd5.Text = _VentasDias.d5_valor;
                    txtd6.Text = _VentasDias.d6_valor;
                    txtd7.Text = _VentasDias.d7_valor;
                    txtd8.Text = _VentasDias.d8_valor;
                    txtd9.Text = _VentasDias.d9_valor;
                    txtd10.Text = _VentasDias.d10_valor;
                    txtd11.Text = _VentasDias.d11_valor;
                    txtd12.Text = _VentasDias.d12_valor;
                    txtd13.Text = _VentasDias.d13_valor;
                    txtd14.Text = _VentasDias.d14_valor;
                    txtd15.Text = _VentasDias.d15_valor;
                    txtd16.Text = _VentasDias.d16_valor;
                    txtd17.Text = _VentasDias.d17_valor;
                    txtd18.Text = _VentasDias.d18_valor;
                    txtd19.Text = _VentasDias.d19_valor;
                    txtd20.Text = _VentasDias.d20_valor;
                    txtd21.Text = _VentasDias.d21_valor;
                    txtd22.Text = _VentasDias.d22_valor;
                    txtd23.Text = _VentasDias.d23_valor;
                    txtd24.Text = _VentasDias.d24_valor;
                    txtd25.Text = _VentasDias.d25_valor;
                    txtd26.Text = _VentasDias.d26_valor;
                    txtd27.Text = _VentasDias.d27_valor;
                    txtd28.Text = _VentasDias.d28_valor;
                    txtd29.Text = _VentasDias.d29_valor;
                    txtd30.Text = _VentasDias.d30_valor;
                    txtd31.Text = _VentasDias.d31_valor;
                }
            }
            


        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cVentasDiarias engineVentasDiaria = new cVentasDiarias();
            mVentasDias _VentasDias = new mVentasDias();
            _VentasDias = engineVentasDiaria.VentasDias(gdvDados.Rows[e.RowIndex].Cells[1].Text);
            engineVentasDiaria.Deletar(_VentasDias);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            GerarDias();
            PreencherDiasDoMes();
        }

        public void LimparControles()
        {
            string nomecontrole = string.Empty;
            for (int i = 0; i < 31; i++)
            {
                nomecontrole = "txtd" + (i + 1).ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                nomecontrole = "lbld" + (i + 1).ToString();
                Label LB = Master.FindControl("MainContent").FindControl(nomecontrole) as Label;
                TB.Text = string.Empty;
                TB.Visible = false;
                LB.Visible = false;
            }
        }
        public void GerarDias()
        {
            //int days = DateTime.DaysInMonth(Convert.ToInt32(ddlAno.Text), Convert.ToInt32(ddlMes.SelectedIndex + 1));
            int days = 0;
            string nomecontrole = string.Empty;
            //TextBox txt = null;
            LimparControles();

            cDiasUteis engineDiasUteis = new cDiasUteis();
            mDiasUteis _diaUtil = null;
            IList<mDiasUteis> _ListdiasUteis = engineDiasUteis.AsignaciondiasUteis(Convert.ToInt32(ddlAno.Text));

            if (_ListdiasUteis != null)
            {
                if (_ListdiasUteis.Count > 0)
                {
                    _diaUtil = _ListdiasUteis[0];


                    if (ddlMes.SelectedIndex == 0)
                    {
                        if (_diaUtil.Enero_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Enero_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 1)
                    {
                        if (_diaUtil.Febrero_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Febrero_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 2)
                    {
                        if (_diaUtil.Marzo_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Marzo_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 3)
                    {
                        if (_diaUtil.Abril_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Abril_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 4)
                    {
                        if (_diaUtil.Mayo_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Mayo_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 5)
                    {
                        if (_diaUtil.Junio_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Junio_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 6)
                    {
                        if (_diaUtil.Julio_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Julio_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 7)
                    {
                        if (_diaUtil.Agosto_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Agosto_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 8)
                    {
                        if (_diaUtil.Septiembre_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Septiembre_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 9)
                    {
                        if (_diaUtil.Octubre_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Octubre_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 10)
                    {
                        if (_diaUtil.Noviembre_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Noviembre_diashabilesdetrabajo);
                        }
                    }
                    if (ddlMes.SelectedIndex == 11)
                    {
                        if (_diaUtil.Diciembre_diashabilesdetrabajo != string.Empty)
                        {
                            days = Convert.ToInt32(_diaUtil.Diciembre_diashabilesdetrabajo);
                        }
                    }



                    for (int i = 0; i < days; i++)
                    {
                        nomecontrole = "txtd" + (i + 1).ToString();
                        TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                        nomecontrole = "lbld" + (i + 1).ToString();
                        Label LB = Master.FindControl("MainContent").FindControl(nomecontrole) as Label;
                        TB.Visible = true;
                        LB.Visible = true;
                    }
                }
            }
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            GerarDias();
            PreencherDiasDoMes();
            PopularClientes();
        }

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            GerarDias();
            PreencherDiasDoMes();
        }

        protected void ddlProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            GerarDias();
            PreencherDiasDoMes();
        }

        protected void rdCliente_CheckedChanged(object sender, EventArgs e)
        {
            ddlCliente.Enabled = true;
            ddlProduto.Enabled = false;
        }

        protected void rdProduto_CheckedChanged(object sender, EventArgs e)
        {
            ddlProduto.Enabled = true;
            ddlCliente.Enabled = false;
        }

        protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            GerarDias();
            PreencherDiasDoMes();
        }
    }
}
