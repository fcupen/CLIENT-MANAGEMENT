﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_assignar_grantt.aspx.cs" Inherits="Clients_Management.View.cd_assignar_grantt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
   <div class="container">
      <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">ADMINISTRACION DE LA CARTERA DE CLIENTES</a></li>
  <li class="active">FIJAR TAMAÑO CARTERA DE CLIENTES</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <p>&nbsp;</p>
	 	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">TAMAÑO CARTERA DE CLIENTES</h4>
             		
			    <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged">
            </asp:GridView>		
		</div>
	</div>
 
</div>
     <div class="col-md-12">
          <div class="form-horizontal well" >
         <h4>Rellene el formulario .</h4>
				 <div class="row">
 
					<div class="col-xs-12">
					
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <label>Seleccione Vendedor:</label>
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false" ></asp:TextBox>
                                <asp:DropDownList ID="ddlVendedor" class="form-control input-sm" runat="server" ></asp:DropDownList> 
							</div>
						</div>
					</div>
				</div>

                         
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="txtClientesMes" class="form-control input-lg" runat="server" placeholder="Clientes Mes" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
								<asp:Button ID="btnNovo" runat="server" class="btn btn-primary" Text="Nuevo" OnClick="btnNovo_Click" />
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Enviar" OnClick="btnSalvar_Click" />                                
							</div>
						</div>
					</div>
				</div>
				</div>
 
				</div>	
		 </div>
	   </div>
     </div>
	 
	 

	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
<div class="row">  

</div>
</div>
  
  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
