﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_produtos.aspx.cs" Inherits="Clients_Management.View.cd_produtos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
     

  <div class="container">
           <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">ADMINISTRACION DE OPERACIONES DE VENTAS</a></li>
  <li><a href="#">ESTRATEGIA OPERATIVA</a></li>
  <li class="active">CREAR GRUPOS DE PRODUCTO</li>
</ol>

    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <p>&nbsp;</p>
	 
	 	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">Relacion de Grupos</h4>
			<asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged">
            </asp:GridView>		
		</div>
	</div>
 
</div>

     <div class="col-md-12">
     
       <div class="form-horizontal well" >
         <h4>Rellene el formulario .</h4>
				 <div class="row">
 
					<div class="col-xs-12">
					
				
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                
                                <asp:TextBox ID="txtid" class="form-control input-lg" runat="server" Visible="false" ></asp:TextBox>
                                <asp:TextBox ID="grupo" class="form-control input-lg" runat="server" placeholder="Codigo Grupo" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
 
				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
                                <asp:TextBox ID="nomegrupoprodutos" class="form-control input-lg" runat="server" placeholder="Nombre do Grupo" ></asp:TextBox>
							</div>
						</div>
					</div>
				</div>
				
                <div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<label>Horizontes de Control - Relacionados ao Grupo, cada grupo tem um horizonte</label>
                            <div class="col-lg-12">
                                <asp:RadioButton ID="rd1mes" runat="server" Text="1 Mes" GroupName="g1" AutoPostBack="true" />
                                <asp:RadioButton ID="rd3meses" runat="server" Text="3 Meses" GroupName="g1" AutoPostBack="true" />
                                <asp:RadioButton ID="rd6meses" runat="server" Text="6 Meses" GroupName="g1" AutoPostBack="true"/>
                                <asp:RadioButton ID="rd9meses" runat="server" Text="9 Meses" GroupName="g1" AutoPostBack="true"/>
                                <asp:RadioButton ID="rd12meses" runat="server" Text="12 Meses" GroupName="g1" AutoPostBack="true"/>
                                <asp:RadioButton ID="rd18meses" runat="server" Text="18 Meses" GroupName="g1" AutoPostBack="true"/>
                                <asp:RadioButton ID="rd24meses" runat="server" Text="24 Meses" GroupName="g1" AutoPostBack="true"/>                                                                                               
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="rows">
						<div class="col-md-8">
							<div class="col-lg-12">
								<asp:Button ID="btnNovo" runat="server" class="btn btn-primary" Text="Nuevo" OnClick="btnNovo_Click" />
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Enviar" OnClick="btnSalvar_Click" />                                
							</div>
						</div>
					</div>
				</div>
				</div>
 
				</div>	
		 </div>
	   </div>
     </div>
	 
	 

	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
 <div class="row">  
  

<p></p>
</div>
  
  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
 
</asp:Content>
