﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_panelcontrol_direccion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
                lblNombre.Text = "Hola " + _vendedor.nombre;
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                ddlMes.SelectedIndex = (DateTime.Now.Month - 1);
            }

            PopularGridGeral();
        }

        private void PopularGridGeral()
        {
            //string mes = DateTime.Now.Month.ToString();

            string mes = (ddlMes.SelectedIndex + 1).ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            //mesescrito = ddlMes.Text;

            double dias_uteis = 0;
            double dias_uteis_atuais = 0;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), (ddlMes.SelectedIndex + 1).ToString(), (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dias_uteis++;
                        if (i < (DateTime.Now.Day - 1))
                        {
                            dias_uteis_atuais++;
                        }
                    }
                }
            }
            cDiasUteis engineDiasUteis = new cDiasUteis();
            dias_uteis = Convert.ToDouble(engineDiasUteis.PopularGridPorAno(DateTime.Now.Year.ToString()).Tables[0].Rows[0][ddlMes.SelectedIndex + 2].ToString());

            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            //retornando vendedores e produtos
            DataSet ds;
            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add("Representante de Ventas");//0
            dtNovo.Columns.Add("Productos");//1
            dtNovo.Columns.Add("Rendimiento (%)");//2
            dtNovo.Columns.Add("Desempeño (%)");//3
            dtNovo.Columns.Add("Cobertura (%)");//4
            dtNovo.Columns.Add("Base de Negocios (%)");//5
            dtNovo.Columns.Add("Brecha ($)");//6
            dtNovo.Columns.Add("Envío Informes (%)");//7
            DataRow dr;
            ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT * FROM asignacionmetas ");
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                string codigoVendedor = item["vendedor"].ToString();
                string codigoProduto = item["produto"].ToString();
                //verificando se existe o vendedor
                if (banco.Selecionar("select * from vendedor where id=" + codigoVendedor).Tables[0].Rows.Count > 0)
                {

                    //calculando rendimento
                    double rendimento = CalcularRendimento(codigoVendedor, codigoProduto);
                    double rendimentofloor = Math.Floor(rendimento);
                    double desempenho = CalcularDesempenho(codigoVendedor, codigoProduto);
                    double desempenhofloor = Math.Floor(desempenho);
                    double bdn = CalcularBDN(codigoVendedor, codigoProduto);
                    double bdnfloor = Math.Floor(bdn);
                    double cobertura = CalcularCobertura(codigoVendedor, dias_uteis_atuais);
                    double coberturafloor = Math.Floor(cobertura);
                    double informe = CalcularInforme(codigoVendedor, codigoProduto, dias_uteis_atuais, dias_uteis);
                    double informefloor = Math.Floor(informe);
                    decimal brecha = RetornarBrecha(codigoVendedor, codigoProduto);
                    decimal brechafloor = Math.Floor(brecha);
                    dr = dtNovo.NewRow();
                    dr[0] = engineVendedor.RetornarVendedor(item["vendedor"].ToString()).nombre;
                    dr[1] = engineProduto.RetornarProduto(item["produto"].ToString()).nombre;
                    dr[2] = rendimento.ToString("N2");
                    dr[3] = desempenho.ToString("N2");
                    dr[4] = cobertura.ToString("N2");
                    dr[5] = bdn.ToString("N2");
                    dr[6] = brechafloor.ToString();
                    dr[7] = informe.ToString("N2");
                    dtNovo.Rows.Add(dr);
                }
            }

            dgvDados.DataSource = dtNovo;
            dgvDados.DataBind();

            //lblData.Text = DateTime.Now.Day.ToString() + " de " + mesescrito + " de " + DateTime.Now.Year.ToString();
            lblData.Text = mesescrito + " de " + DateTime.Now.Year.ToString(); ;


            lblTotalDiasUteis.Text = dias_uteis.ToString();
            if (Convert.ToInt32(mes) < DateTime.Now.Month)
                lblDiasUteis.Text = dias_uteis.ToString();
            else
                lblDiasUteis.Text = dias_uteis_atuais.ToString();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                dgvDados.AllowPaging = false;
                PopularGridGeral();

                dgvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in dgvDados.HeaderRow.Cells)
                {
                    cell.BackColor = dgvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in dgvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = dgvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = dgvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                dgvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string filename = "panel_control_direccion.pdf";
            //salvando a cópia local
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            dgvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            dgvDados.AllowPaging = true;
            dgvDados.DataBind();
        }

        private double CalcularInforme(string pCodigoVendedor, string pCodigoProduto, double dias_uteis_atuais, double totais_dias_uteis)
        {
            clsConexao banco = new clsConexao();
            DataSet ds = null;
            //string mes = DateTime.Now.Month.ToString();
            string mes = (ddlMes.SelectedIndex + 1).ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;


            //dias informados 
            double dias_informados = 0;
            lblvendas2.Text = string.Empty;
            for (int i = 1; i <= DateTime.Now.Day; i++)
            {
                ds = banco.Selecionar("select * from informediario where ano='" + DateTime.Now.Year.ToString()
                            + "' and mes='" + mesescrito + "' and vendedor_id=" + pCodigoVendedor
                            + " and (d" + i.ToString() + "_valor<>'' and d" + i.ToString() + "_valor<>'P'"
                            + " and d" + i.ToString() + "_valor<>'F') ");

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count >= 1)
                    {
                        dias_informados++;
                        lblvendas2.Text += i.ToString() + ";";
                    }
                }
            }

            try
            {
                double resultado = (((dias_informados / dias_uteis_atuais) * totais_dias_uteis) / totais_dias_uteis) * 100;
                if (double.IsNaN(resultado) || double.IsInfinity(resultado))
                    return 0;

                lblvendas2.Text += "--> Dias informados:" + dias_informados;
                lbldiastrabalhados2.Text = "dias_uteis_atuais:" + dias_uteis_atuais.ToString();
                lblmeta2.Text = "total dias uteis:" + totais_dias_uteis.ToString();
                return resultado;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private double CalcularCobertura(string pCodigoVendedor, double dias_uteis_atuais)
        {
            //string mes = DateTime.Now.Month.ToString();
            string mes = (ddlMes.SelectedIndex + 1).ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;


            double totalvisitas = 0;
            //select count(*) from informediario where vendedor_id=27 and (d1_valor='1' or d1_valor='3');

            clsConexao banco = new clsConexao();
            DataSet ds = null;
            for (int i = 1; i < DateTime.Now.Day; i++)
            {
                ds = banco.Selecionar("select count(*) from informediario where ano='" + DateTime.Now.Year.ToString()
                        + "' and mes='" + mesescrito + "' and vendedor_id=" + pCodigoVendedor
                        + " and (d" + i.ToString() + "_valor='1' or d" + i.ToString() + "_valor='3') ");

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0][0].ToString() != string.Empty)
                            totalvisitas += Convert.ToDouble(ds.Tables[0].Rows[0][0].ToString());
                    }
                }
            }

            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _parametro = engineParametrosDiversos.RetornarParametrosDiversos()[0];
            //dias uteis            
            double resultado = ((totalvisitas / dias_uteis_atuais) / _parametro.metaprometodiaria) * 100;
            if (double.IsNaN(resultado) || double.IsInfinity(resultado))
                return 0;

            return resultado;
        }

        private decimal RetornarBrecha(string pCodigoVendedor, string pCodigoProduto)
        {
            cBrecha engineBrecha = new cBrecha();
            List<mBrecha> _brecha = engineBrecha.RetornarBrecha(Convert.ToInt32(pCodigoVendedor), Convert.ToInt32(pCodigoProduto));
            decimal total = 0;
            if (_brecha != null)
            {
                foreach (mBrecha item in _brecha)
                {
                    total += item.valor;
                }
            }
            return total;
        }

        private double CalcularRendimento(string pCodigoVendedor, string pCodigoProduto)
        {
            //valor total de vendas
            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            DataSet ds = engineVentasDiarias.PopularGridVendedorProduto(pCodigoVendedor, pCodigoProduto, DateTime.Now.Year.ToString(), ddlMes.Text.ToString());
            double total = 0;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                for (int i = 1; i <= 31; i++)
                {
                    total += (item[i.ToString()] != string.Empty ? Convert.ToDouble(item[i.ToString()].ToString()) : 0);
                }

            }
            //txtvalorAcumuladoDia.Text = total.ToString();

            //total de dias trabalhados
            double dias_trabalhadoscalc = 0;

            double dias_uteis = 0;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dias_uteis++;
                        if (i < (DateTime.Now.Day - 1))
                        {
                            dias_trabalhadoscalc++;
                        }
                    }
                }
            }
            cDiasUteis engineDiasUteis = new cDiasUteis();
            dias_uteis = Convert.ToDouble(engineDiasUteis.PopularGridPorAno(DateTime.Now.Year.ToString()).Tables[0].Rows[0][ddlMes.SelectedIndex + 2].ToString());

            //meta    
            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT sum(" + mesescrito + ") as total FROM asignacionmetas a where a.vendedor=" + pCodigoVendedor + " and produto=" + pCodigoProduto);
            double metacalc = Convert.ToDouble(ds.Tables[0].Rows[0]["total"]);
            //txtmeta.Text = metacalc.ToString();

            double valorAcumuladoDia = Convert.ToDouble(total);
            double dias_trabalhados = Convert.ToDouble(dias_trabalhadoscalc);
            double diasUteisMes = Convert.ToDouble(dias_uteis);
            double meta = Convert.ToDouble(metacalc);

            double resultado = (((valorAcumuladoDia / dias_trabalhados) * diasUteisMes) / meta) * 100;
            if (double.IsNaN(resultado) || double.IsInfinity(resultado))
                return 0;

            return resultado;
        }

        private double CalcularDesempenho(string pCodigoVendedor, string pCodigoProduto)
        {
            //valor total de vendas
            //string mes = DateTime.Now.Month.ToString();
            string mes = (ddlMes.SelectedIndex + 1).ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            DataSet ds = engineVentasDiarias.PopularGridVendedorProdutoAno(pCodigoVendedor, pCodigoProduto);
            double total = 0;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                for (int i = 1; i <= 31; i++)
                {
                    total += (item[i.ToString()].ToString().Trim() != string.Empty ? Convert.ToDouble(item[i.ToString()].ToString()) : 0);
                }
            }
            //txtvendatMes.Text = total.ToString();

            //mes
            //txtmes.Text = DateTime.Now.Month.ToString();

            //meta
            ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT (sum(enero)+sum(febrero)+ sum(marzo)+sum(abril)+sum(mayo)+sum(junio)+sum(julio)+sum(agosto)+sum(septiembre)+sum(octubre)+sum(noviembre)+sum(diciembre))  as total FROM asignacionmetas a where a.vendedor=" + pCodigoVendedor + " and a.produto=" + pCodigoProduto);
            double metacalc = Convert.ToDouble(ds.Tables[0].Rows[0]["total"]);
            //txtmeta2.Text = metacalc.ToString();

            double venta_mes = Convert.ToDouble(total);
            double mescalc = Convert.ToDouble((ddlMes.SelectedIndex + 1));
            double meta = Convert.ToDouble(metacalc);
            //lbldesempenho.Text = "Desempenho" + ((((venta_mes / mescalc) * 12) / meta) * 100).ToString();

            //(((4250000/5)*21)/50000000)*100

            double resultado = (((venta_mes / mescalc) * 12) / meta) * 100;

            if (double.IsNaN(resultado) || double.IsInfinity(resultado))
                return 0;

            return resultado;
        }

        private double CalcularBDN(string pCodigoVendedor, string pCodigoProduto)
        {


            //meta3
            //string mes = DateTime.Now.Month.ToString();
            string mes = (ddlMes.SelectedIndex + 1).ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            DataSet ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT sum(" + mesescrito + ") as total FROM asignacionmetas a where a.vendedor=" + pCodigoVendedor + " and a.produto=" + pCodigoProduto);
            double metacalc = Convert.ToDouble(ds.Tables[0].Rows[0]["total"]);
            //txtmeta3.Text = metacalc.ToString();

            //tkc
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _parametro = engineParametrosDiversos.RetornarParametrosDiversos()[0];
            //txttkc.Text = _parametro.tkc.ToString();

            //bdn 
            cBDN engineBDN = new cBDN();
            ds = new DataSet();

            ds = engineBDN.PopularGrid_2PorVendedorProdutoCalculo(Convert.ToInt32(pCodigoVendedor), Convert.ToInt32(pCodigoProduto));
            double media = 0;
            double total = 0;
            double totalvalor = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["ESTADO"].ToString() == "Vigente" && ds.Tables[0].Rows[i]["Expirado"].ToString() == "N")
                    {
                        totalvalor += Convert.ToDouble(ds.Tables[0].Rows[i]["TOTAL NETO"].ToString());
                        total++;
                    }
                }
                //txtbdn.Text = totalvalor.ToString("N0");
            }


            double bdn = Convert.ToDouble(totalvalor);
            double tkc = Convert.ToDouble(_parametro.tkc.ToString());
            double meta = Convert.ToDouble(metacalc);
            //lblbdn.Text = "BDN" + (((bdn * (tkc / 100)) / meta) * 100).ToString();
            double resultado = ((bdn * (tkc / 100)) / meta) * 100;

            if (double.IsNaN(resultado) || double.IsInfinity(resultado))
                return 0;

            return resultado;

        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void dgvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                    if (j >= 2)
                    {
                        e.Row.Cells[j].HorizontalAlign = HorizontalAlign.Right;
                    }
                }

            }
        }
    }
}