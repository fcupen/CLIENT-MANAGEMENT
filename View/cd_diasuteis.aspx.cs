﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_diasuteis : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }

        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                CarregarMetas();
            }

        }
                
        private void PopularGridGeral(int limite)
        {
            //cMetas engineMetas= new cMetas();
            //gdvDados.DataSource = engineMetas.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = true;
            //gdvDados.DataBind();
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cDiasUteis engineDiasUteis = new cDiasUteis();
            mDiasUteis _diasUteis = new mDiasUteis();
           

            if (ddlAno.SelectedValue == null)
            {
                Alert.Show("Selecione um ano");
                return;
            }

            //verificar se existe já o registro pelo produto e vendedor e ano
            if ( ddlAno.Text != string.Empty)
            {
                IList<mDiasUteis> list = engineDiasUteis.AsignaciondiasUteis( Convert.ToInt32(ddlAno.Text));
                if (list != null)
                {
                    _diasUteis = list[0];
                    txtid.Text = _diasUteis.id.ToString();
                }
            }
            _diasUteis.ano = ddlAno.Text;
            _diasUteis.Enero_diashabilesdetrabajo = Enero_diashabilesdetrabajo.Text;

            _diasUteis.Febrero_diashabilesdetrabajo = Febrero_diashabilesdetrabajo.Text;

            _diasUteis.Marzo_diashabilesdetrabajo = Marzo_diashabilesdetrabajo.Text;

            _diasUteis.Abril_diashabilesdetrabajo = Abril_diashabilesdetrabajo.Text;

            _diasUteis.Mayo_diashabilesdetrabajo = Mayo_diashabilesdetrabajo.Text;

            _diasUteis.Junio_diashabilesdetrabajo = Junio_diashabilesdetrabajo.Text;

            _diasUteis.Julio_diashabilesdetrabajo = Julio_diashabilesdetrabajo.Text;

            _diasUteis.Agosto_diashabilesdetrabajo = Agosto_diashabilesdetrabajo.Text;

            _diasUteis.Septiembre_diashabilesdetrabajo = Septiembre_diashabilesdetrabajo.Text;

            _diasUteis.Octubre_diashabilesdetrabajo = Octubre_diashabilesdetrabajo.Text;

            _diasUteis.Noviembre_diashabilesdetrabajo = Noviembre_diashabilesdetrabajo.Text;

            _diasUteis.Diciembre_diashabilesdetrabajo = Diciembre_diashabilesdetrabajo.Text;


            _diasUteis.id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                
                _diasUteis.id = Convert.ToInt32(engineDiasUteis.Inserir(_diasUteis)); ;
            }
            else
            {
                engineDiasUteis.Update(_diasUteis);
            }



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            ddlAno.SelectedIndex = 0;
            Enero_diashabilesdetrabajo.Text = string.Empty;
            Febrero_diashabilesdetrabajo.Text = string.Empty;
            Marzo_diashabilesdetrabajo.Text = string.Empty;
            Abril_diashabilesdetrabajo.Text = string.Empty;
            Mayo_diashabilesdetrabajo.Text = string.Empty;
            Junio_diashabilesdetrabajo.Text = string.Empty;
            Julio_diashabilesdetrabajo.Text = string.Empty;
            Agosto_diashabilesdetrabajo.Text = string.Empty;
            Septiembre_diashabilesdetrabajo.Text = string.Empty;
            Octubre_diashabilesdetrabajo.Text = string.Empty;
            Noviembre_diashabilesdetrabajo.Text = string.Empty;
            Diciembre_diashabilesdetrabajo.Text = string.Empty;            

            //mes.SelectedIndex = 0;
            //diashabilesdetrabajodelmes.Text = string.Empty;
            //meta.Text = string.Empty;

        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cMetas engineMetas = new cMetas();
            //mMetas _metas = engineMetas.Asignacionmetas(gdvDados.SelectedRow.Cells[2].Text);
            //txtid.Text = _metas.id.ToString();
            //ddlVendedor.SelectedIndex = ddlVendedor.Items.IndexOf(ddlVendedor.Items.FindByValue(_metas.vendedor.id.ToString()));
            //ano.Text = _metas.ano;

            ////mes.Text = _metas.mes;
            ////diashabilesdetrabajodelmes.Text = _metas.diashabilesdetrabajodelmes;
            ////meta.Text = _metas.meta.ToString();

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //cMetas engineMetas = new cMetas();
            //mMetas _metas = new mMetas();
            //_metas = engineMetas.Asignacionmetas(gdvDados.Rows[e.RowIndex].Cells[2].Text);
            //engineMetas.Deletar(_metas);
            //limpar();
            //PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            //if (gdvDados.Rows.Count > 0)
            //{
            //    gdvDados.UseAccessibleHeader = true;
            //    gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    for (int j = 1; j < e.Row.Cells.Count; j++)
            //    {
            //        string encoded = e.Row.Cells[j].Text;
            //        e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
            //    }

            //}
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarMetas();
        }

        protected void ddlProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarMetas();
        }
        private void CarregarMetas()
        {
            cDiasUteis engineDiasUteis = new cDiasUteis();
            mDiasUteis _diasUteis = new mDiasUteis();


            if (ddlAno.SelectedValue == null)
            {
                Alert.Show("Selecione um ano");
                return;
            }




            //verificar se existe já o registro pelo produto e vendedor e ano
            if (ddlAno.Text != string.Empty)
            {
                IList<mDiasUteis> list = engineDiasUteis.AsignaciondiasUteis(Convert.ToInt32(ddlAno.Text));
                if (list != null)
                {
                    _diasUteis = list[0];
                    txtid.Text = _diasUteis.id.ToString();

                    Enero_diashabilesdetrabajo.Text = _diasUteis.Enero_diashabilesdetrabajo;

                    Febrero_diashabilesdetrabajo.Text = _diasUteis.Febrero_diashabilesdetrabajo;

                    Marzo_diashabilesdetrabajo.Text = _diasUteis.Marzo_diashabilesdetrabajo;

                    Abril_diashabilesdetrabajo.Text = _diasUteis.Abril_diashabilesdetrabajo;

                    Mayo_diashabilesdetrabajo.Text = _diasUteis.Mayo_diashabilesdetrabajo;

                    Junio_diashabilesdetrabajo.Text = _diasUteis.Junio_diashabilesdetrabajo;

                    Julio_diashabilesdetrabajo.Text = _diasUteis.Julio_diashabilesdetrabajo;

                    Agosto_diashabilesdetrabajo.Text = _diasUteis.Agosto_diashabilesdetrabajo;

                    Septiembre_diashabilesdetrabajo.Text = _diasUteis.Septiembre_diashabilesdetrabajo;

                    Octubre_diashabilesdetrabajo.Text = _diasUteis.Octubre_diashabilesdetrabajo;

                    Noviembre_diashabilesdetrabajo.Text = _diasUteis.Noviembre_diashabilesdetrabajo;

                    Diciembre_diashabilesdetrabajo.Text = _diasUteis.Diciembre_diashabilesdetrabajo;
                }
                else
                {
                    txtid.Text = string.Empty;

                    Enero_diashabilesdetrabajo.Text = string.Empty;

                    Febrero_diashabilesdetrabajo.Text =string.Empty;

                    Marzo_diashabilesdetrabajo.Text = string.Empty;

                    Abril_diashabilesdetrabajo.Text = string.Empty;

                    Mayo_diashabilesdetrabajo.Text = string.Empty;

                    Junio_diashabilesdetrabajo.Text = string.Empty;

                    Julio_diashabilesdetrabajo.Text = string.Empty;

                    Agosto_diashabilesdetrabajo.Text = string.Empty;

                    Septiembre_diashabilesdetrabajo.Text = string.Empty;

                    Octubre_diashabilesdetrabajo.Text = string.Empty;

                    Noviembre_diashabilesdetrabajo.Text = string.Empty;

                    Diciembre_diashabilesdetrabajo.Text = string.Empty;
                }
            }
           
        }

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarMetas();
        }
    }
}