﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class parametros_gerais : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
            }
        }
        private void PopularGridGeral(int limite)
        {
            cParametrosDiversos engineGrupoVendedores = new cParametrosDiversos();
            gdvDados.DataSource = engineGrupoVendedores.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cParametrosDiversos engineGrupoVendedores = new cParametrosDiversos();
            mParametrosDiversos _GrupoVendedores = new mParametrosDiversos();

            _GrupoVendedores.travardias = Convert.ToInt32( txtdiaTrava.Text);
            _GrupoVendedores.maxvisitasdias = Convert.ToInt32(txtmaxvisitasdias.Text);
            _GrupoVendedores.maxvisitasagendadas = Convert.ToInt32(txtmaxvisitasmes.Text);
            _GrupoVendedores.tkc = Convert.ToInt32(txttkc.Text);
            _GrupoVendedores.metaprometodiaria = Convert.ToInt32(txtMetaPromedioDiaria.Text);

                engineGrupoVendedores.Update(_GrupoVendedores);



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            txtdiaTrava.Text = string.Empty;
            txtmaxvisitasdias.Text = string.Empty;
            txtmaxvisitasmes.Text = string.Empty;
            txttkc.Text = string.Empty;
            txtMetaPromedioDiaria.Text = string.Empty;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cParametrosDiversos engineGrupoVendedores = new cParametrosDiversos();
            mParametrosDiversos _GrupoVendedores = engineGrupoVendedores.RetornarParametrosDiversos(gdvDados.SelectedRow.Cells[1].Text);
            txtdiaTrava.Text = _GrupoVendedores.travardias.ToString();
            txtmaxvisitasdias.Text = _GrupoVendedores.maxvisitasdias.ToString();
            txtmaxvisitasmes.Text = _GrupoVendedores.maxvisitasagendadas.ToString();
            txttkc.Text = _GrupoVendedores.tkc.ToString();
            txtMetaPromedioDiaria.Text = _GrupoVendedores.metaprometodiaria.ToString();
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cParametrosDiversos engineGrupoVendedores = new cParametrosDiversos();
            mParametrosDiversos _GrupoVendedores = new mParametrosDiversos();
            _GrupoVendedores = engineGrupoVendedores.RetornarParametrosDiversos(gdvDados.Rows[e.RowIndex].Cells[1].Text);
            engineGrupoVendedores.Deletar(_GrupoVendedores);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }
    }
}