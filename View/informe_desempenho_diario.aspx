﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="informe_desempenho_diario.aspx.cs" Inherits="Clients_Management.View.informe_desempenho_diario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
     <div class="container">
       <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">INFORMES Y RESULTADOS</a></li>
   <li class="active">INFORMES DIARIOS DE VENTAS</li>
</ol>
  </div>
</div>
<div id="Div1"></div>
          
<div id="about_section_1">
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     </div>
	 
	 <div class="row">
    <div class="col-md-12">
                    <div class="well">
					<div class="row">
					 <div class="col-md-3">
					<label>Seleccione Vendedor:</label>
                <asp:DropDownList ID="ddlVendedor" runat="server" AutoPostBack="true" class="form-control input-sm" OnSelectedIndexChanged="ddlVendedor_SelectedIndexChanged" >
                </asp:DropDownList>
            </div>
			    </div>
			</div>
		<div class="well">
			<h4 class="text-center">
            <asp:Label ID="lblTituloRelatorio" runat="server" Text="Relatório"></asp:Label></h4>
            <div class="row">
					 <div class="col-md-3">
			<label>Seleccione Año:</label>
			<asp:Label ID="lblAno" runat="server" Text="Ano" Visible="False" ></asp:Label>
            <asp:DropDownList ID="ddlAno" class="form-control input-sm" runat="server" Visible="true" AutoPostBack="True" OnSelectedIndexChanged="ddlAno_SelectedIndexChanged" >
                <asp:ListItem>2015</asp:ListItem>
                <asp:ListItem>2016</asp:ListItem>
                <asp:ListItem>2017</asp:ListItem>
                <asp:ListItem>2018</asp:ListItem>
            </asp:DropDownList><br>
<label>Seleccione Mes:</label>           
		   <asp:Label ID="lblMes" runat="server" Text="Mês" Visible="False" ></asp:Label>
            <asp:DropDownList ID="ddlMes" class="form-control input-sm" runat="server" Visible="true" AutoPostBack="True" OnSelectedIndexChanged="ddlMes_SelectedIndexChanged" >
                <asp:ListItem>Enero</asp:ListItem>
                <asp:ListItem>Febrero</asp:ListItem>
                <asp:ListItem>Marzo</asp:ListItem>
                <asp:ListItem>Abril</asp:ListItem>
                <asp:ListItem>Mayo</asp:ListItem>
                <asp:ListItem>Junio</asp:ListItem>
                <asp:ListItem>Julio</asp:ListItem>
                <asp:ListItem>Agosto</asp:ListItem>
                <asp:ListItem>Septiembre</asp:ListItem>
                <asp:ListItem>Octubre</asp:ListItem>
                <asp:ListItem>Noviembre</asp:ListItem>
                <asp:ListItem>Diciembre</asp:ListItem>
            </asp:DropDownList>
			</div></div>
            <asp:Button ID="Button1" Visible="False" runat="server" class="btn btn-success btn-lg" Text="Consultar" OnClick="Button1_Click"/>
			<br>
            <asp:GridView ID="dgvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDataBound="dgvDados_RowDataBound" >
                <Columns>
                </Columns>
            </asp:GridView>	
		</div>
	</div>
 
</div>
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  

  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
</asp:Content>
