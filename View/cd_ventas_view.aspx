﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_ventas_view.aspx.cs" Inherits="Clients_Management.View.cd_ventas_view" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        
<div id="section_header">
  <div class="container">
    <h2><span>METAS DE VENTAS</span></h2>
  </div>
</div>
<div id="Div1"></div>
          
<div id="about_section_1">
  <div class="container">
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     <div class="col-md-12">
       <h3 align="center">METAS DE VENTAS</h3>
	    <h4 align="center">Valores netos en CLP</h4>
	  </div>

     </div>
	  <p></p>
	 
	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">METAS DE VENTAS</h4>
            <asp:Label ID="lblTitulo" runat="server" Text="Label"></asp:Label>
            <asp:RadioButton ID="rdTodos" runat="server" Text="Todos" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdTodos_CheckedChanged" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:RadioButton ID="rdProduto" runat="server" Text="Product" AutoPostBack="True" GroupName="filtro" OnCheckedChanged="rdProduto_CheckedChanged" /><br />
            <asp:DropDownList ID="ddlFiltro" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFiltro_SelectedIndexChanged"></asp:DropDownList>
			<hr width="60%">
            <asp:GridView ID="gdvDados" runat="server" CssClass="table table-striped" Font-Size="12px" OnRowDataBound="gdvDados_RowDataBound">
                <Columns>
                </Columns>
            </asp:GridView>	
                                    <asp:Button ID="Button2" Visible="true" runat="server" class="btn btn-primary" Text="Exportar Excel" OnClick="Button2_Click" />	
            <asp:Button ID="Button3" Visible="true" runat="server" class="btn btn-primary" Text="Exportar PDF" OnClick="Button3_Click" />						
		</div>
	</div>
 
</div>
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    </div>
  
  
  
  
 <div class="row">  
  
<div class="col-lg-12" align="center">
    <asp:Button ID="btnVoltar" runat="server" class="btn btn-success btn-lg" Text="Volver Fuerza de Ventas" PostBackUrl="~/View/painel_fuerza.aspx" />
    <p></p>
</div>


</div>
  
  
  
  </div>




  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
 
 
</asp:Content>
