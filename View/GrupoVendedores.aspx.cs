﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class GrupoVendedores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mUsuario _usuario = null;
            if (Session["admin"] != null)
            {
                _usuario = (mUsuario)Session["admin"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_usuario != null)
                    lblNombre.Text = "Hola " + _usuario.nombre.ToString();
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
            }
        }
        private void PopularGridGeral(int limite)
        {
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();
            gdvDados.DataSource = engineGrupoVendedores.PopularGrid(limite);
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = true;
            gdvDados.DataBind();

        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }
        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();
            mGrupoVendedores _GrupoVendedores = new mGrupoVendedores();

            _GrupoVendedores.codigo = grupo.Text;
            _GrupoVendedores.nombre = nomegrupoprodutos.Text;
            _GrupoVendedores.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            if (txtid.Text == string.Empty)
            {
                _GrupoVendedores.Id = Convert.ToInt32(engineGrupoVendedores.Inserir(_GrupoVendedores)); ;
            }
            else
            {
                engineGrupoVendedores.Update(_GrupoVendedores);
            }



            limpar();
            PopularGridGeral(0);
        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            grupo.Text = string.Empty;
            nomegrupoprodutos.Text = string.Empty;
        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();
            mGrupoVendedores _GrupoVendedores = engineGrupoVendedores.RetornarGrupoVendedores(gdvDados.SelectedRow.Cells[1].Text);
            txtid.Text = _GrupoVendedores.Id.ToString();
            grupo.Text = _GrupoVendedores.codigo;
            nomegrupoprodutos.Text = _GrupoVendedores.nombre;
        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();
            mGrupoVendedores _GrupoVendedores = new mGrupoVendedores();
            _GrupoVendedores = engineGrupoVendedores.RetornarGrupoVendedores(gdvDados.Rows[e.RowIndex].Cells[1].Text);
            engineGrupoVendedores.Deletar(_GrupoVendedores);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }
    }
}