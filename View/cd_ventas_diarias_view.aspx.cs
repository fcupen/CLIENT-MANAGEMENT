﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_ventas_diarias_view : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }


            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();

                PopularProdutos();
                PopularGridGeral();
            }

        }
        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            ddlFiltro.DataSource = engineProduto.PopularGrid(0).Tables[0];
            ddlFiltro.DataValueField = "id";
            ddlFiltro.DataTextField = "nombre";
            ddlFiltro.DataBind();
        }
        private void PopularGridGeral()
        {

            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            DataSet ds = engineVentasDiarias.PopularGrid( _vendedor.id.ToString(),ddlMes.Text);
            gdvDados.AutoGenerateSelectButton = false;

            gdvDados.DataSource = ds;


            gdvDados.AutoGenerateColumns = true;

            gdvDados.DataBind();

            double total = 0;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                for (int i = 1; i <= 31; i++)
                {
                    total += (item[i.ToString()] != string.Empty ? Convert.ToDouble(item[i.ToString()].ToString()) : 0);
                }                                

            }
            lblTotal.Text = "Total: " + total.ToString();
            //cVentas engineVentas= new cVentas();
            //gdvDados.DataSource = engineVentas.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = true;
            //gdvDados.DataBind();
        }

        protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        public void PopularGridProduto()
        {
            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            cProduto engineProduto = new cProduto();
            mProduto _producto = engineProduto.RetornarProduto(ddlFiltro.SelectedValue.ToString());
            DataSet ds = null;
            if(rdProduto.Checked)
                ds=engineVentasDiarias.PopularGrid(_vendedor.id.ToString(), _producto.Id, ddlMes.Text.ToString());
            else
                ds = engineVentasDiarias.PopularGrid(_vendedor.id.ToString(), ddlMes.Text.ToString());

            gdvDados.AutoGenerateSelectButton = false;

            gdvDados.DataSource = ds;


            gdvDados.AutoGenerateColumns = true;

            gdvDados.DataBind();
            double total = 0;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                for (int i = 1; i <= 31; i++)
                {
                    total += (item[i.ToString()] != string.Empty ? Convert.ToDouble(item[i.ToString()].ToString()) : 0);
                }                 

            }
            lblTotal.Text ="Total: "+ total.ToString();

        }

        protected void rdTodos_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridGeral();
        }

        protected void rdProduto_CheckedChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridProduto();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gdvDados.AllowPaging = false;
                PopularGridGeral();

                gdvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gdvDados.HeaderRow.Cells)
                {
                    cell.BackColor = gdvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gdvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gdvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gdvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gdvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //salvando a cópia local
            string filename = "informe.pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gdvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 8f, 8f, 8f, 8f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            gdvDados.AllowPaging = true;
            gdvDados.DataBind();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}