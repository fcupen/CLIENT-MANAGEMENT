﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="form_vendedor.aspx.cs" Inherits="Clients_Management.View.form_vendedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("[id$=calendario]").datepicker({
                showOn: 'button',
                buttonImageOnly: true,
                dateFormat: 'mm/dd/yy',
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercules', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                dayNamesShort: ['Dom', 'L', 'M', 'M', 'J', 'V', 'S', 'D'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Out', 'Nov', 'Dec'],
                buttonImage: 'http://www.asksystems.com.br/webgestion/Images/calendario.png'


            });
        });
    </script>

 <!--<div id="section_header">
  <div class="container">
    <h2><span>CREAR VENDEDOR</span></h2>
  </div>
</div>-->

  <div class="container">
  <ol class="breadcrumb">
  <li><a id="A1" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">ADMINISTRACION DE VENTAS</a></li>
  <li><a href="#">ADMINISTRACION DE LA CARTERA DE CLIENTES</a></li>
  <li class="active">CREAR VENDEDOR</li>
</ol>
    <div class="row">
     <div class="col-lg-12">        
      <p><asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
     </div>
     	 <div class="row">
    <div class="col-md-12">
		<div class="well">
			<h4 class="text-center">RELACION DE VENDEDORES</h4>
			            <asp:GridView ID="gdvDados" runat="server" class="table table-striped table-bordered table-condensed" OnRowDeleting="gdvDados_RowDeleting" OnSelectedIndexChanged="gdvDados_SelectedIndexChanged" Font-Size="11px" BackColor="White">
                <Columns>
                    <asp:CommandField DeleteText="Deletar" ShowDeleteButton="false" />
                </Columns>
            </asp:GridView>				
		</div>
	</div>
 
</div>
        <div class="col-md-12">
             <div class="form-horizontal well" >
         <h4>Rellene el formulario</h4>
				 <div class="row">
 					<div class="col-md-12">
						 <div class="col-md-2">
                                <label>Fecha:</label>								
                                <asp:TextBox ID="txtid" class="form-control" runat="server" Visible="false" ></asp:TextBox>
                                <asp:TextBox ID="calendario" class="form-control" runat="server" placeholder="Fecha" ></asp:TextBox>
							</div>
 
							<div class="col-md-2">
                                <label>Codigo:</label>
								<asp:TextBox ID="codigo" class="form-control" runat="server" placeholder="Codigo" ></asp:TextBox>
							</div>
						</div>
					
				</div>	
					
					
					<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
							<div class="col-md-4">
                                <label>Nombre:</label>
                                <asp:TextBox ID="nombre" class="form-control" runat="server" placeholder="Nombre" ></asp:TextBox>
							</div>
 
							<div class="col-md-4">
                                <label>Appelido:</label>
                                <asp:TextBox ID="appelido" class="form-control" runat="server" placeholder="Appelido" ></asp:TextBox>
							</div>

                            <div class="col-md-4">
                                 <label>Cargo:</label>
                                <asp:TextBox ID="txtCargo" class="form-control" runat="server" placeholder="Cargo" ></asp:TextBox>
							</div>

						</div>
					</div>
				</div>	
					
					<div class="form-group">
					<div class="rows">
						<div class="col-md-12">
                            
							<div class="col-md-4">
                                <label>Grupo Ventas:</label>
                                <asp:DropDownList ID="ddlGrupoVendas" runat="server" class="form-control input-sm"></asp:DropDownList>
							</div>                          
							
							</div>
					</div>
				</div>	

                <div class="form-group">
					<div class="rows">
						<div class="col-md-12">
							<div class="col-md-4">
                                 <label>Email:</label>
                                <asp:TextBox ID="txtEmail" class="form-control" runat="server" placeholder="Email" ></asp:TextBox>
							</div>
 
							<div class="col-md-2">
                                 <label>Clave:</label>
                                <asp:TextBox ID="txtClave"  class="form-control" runat="server" placeholder="Clave" TextMode="Password"></asp:TextBox>
							</div>

                            
						</div>
					</div>
				</div>

                		
				<div class="form-group">
					<div class="rows">
						<div class="col-md-12"align="center">
                                <asp:Button ID="btnNovo" runat="server" class="btn btn-primary" Text="Limpiar" OnClick="btnNovo_Click" />
                                <asp:Button ID="btnSalvar" runat="server" class="btn btn-primary" Text="Cadastrar" OnClick="btnSalvar_Click" />                                
							</div>
					
					</div>
				</div>
				</div>
 
				</div>	
		 </div>
	   </div>
     </div>
	 
	 

 
 
  <!-- Why choose us Section -->
  <!-- Our clients Section -->
  <!-- Footer -->
 
 
</asp:Content>
