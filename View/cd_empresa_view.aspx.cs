﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class cd_empresa_view : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                if (_vendedor != null)
                    lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral();
            }
        }
        
        private void PopularGridGeral()
        {
            //cClientes engineClientes = new cClientes();
            //gdvDados.DataSource = engineClientes.PopularGrid(limite);
            //gdvDados.AutoGenerateColumns = true;
            //gdvDados.AutoGenerateSelectButton = false;
            //gdvDados.DataBind();
            cRelacaoVendedor engineRelacaoVendedor = new cRelacaoVendedor();
            DataSet ds = null;
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            lblNombre.Text = string.Empty;
            
                ds = engineRelacaoVendedor.PopularGrid_2("vendedorpornome", _vendedor.id.ToString());
                lblNombre.Text = _vendedor.nombre.ToString();
           
            Session["gdvDadosTable"] = ds.Tables[0];
            Session["ordenamento"] = "asc";
            gdvDados.DataSource = ds.Tables[0];
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();

            lblTotal.Text = "Total: " + ds.Tables[0].Rows.Count.ToString();
        }

                 

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }       
       }



        protected void gdvDados_Sorting(object sender, GridViewSortEventArgs e)
        {
            //DataTable dataTable = gdvDados.DataSource as DataTable;
            DataTable dataTable = (DataTable)Session["gdvDadosTable"];

            if (dataTable != null)
            {
                DataView dataView = new DataView(dataTable);
                //dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);
                dataView.Sort = e.SortExpression + " " + Session["ordenamento"];
                if (Session["ordenamento"] == "asc")
                    Session["ordenamento"] = "desc";
                else
                    Session["ordenamento"] = "asc";
                gdvDados.DataSource = dataView;
                gdvDados.DataBind();
            }
        }
        private string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "asc";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "desc";
                    break;
            }

            return newSortDirection;
        }
        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                gdvDados.AllowPaging = false;
                PopularGridGeral();

                gdvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in gdvDados.HeaderRow.Cells)
                {
                    cell.BackColor = gdvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in gdvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gdvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gdvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                gdvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //salvando a cópia local
            string filename = "informe.pdf";
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            gdvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 8f, 8f, 8f, 8f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            gdvDados.AllowPaging = true;
            gdvDados.DataBind();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
}