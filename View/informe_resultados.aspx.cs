﻿using Clients_Management.Controller;
using Clients_Management.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class informe_resultados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor_login = null;
            if (Session["vendedor"] != null)
            {
                _vendedor_login = (mVendedor)Session["vendedor"];
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

            if (!Page.IsPostBack)
            {
                PopularComboVendedores();
                PopularProdutos();
            }

            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            lblTituloRelatorio.Text = _vendedor.nombre.ToString() + " - " + DateTime.Now.Year.ToString();
            if (!Page.IsPostBack)
            {

                ddlAno.Text = DateTime.Now.Year.ToString();
                Button1_Click(null, null);


                    if (_vendedor != null)
                        lblNombre.Text = "Hola " + _vendedor_login.nombre.ToString();
            }

        }

        private void PopularProdutos()
        {
            cProduto engineProduto = new cProduto();
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());

            ddlFiltro.DataSource = engineProduto.PopularGridVendedor(_vendedor.id).Tables[0];
            ddlFiltro.DataValueField = "Id";
            ddlFiltro.DataTextField = "NOMBRE";
            ddlFiltro.DataBind();
        }

        private void PopularComboVendedores()
        {
            cVendedor engineVendedor = new cVendedor();

            DataSet ds = engineVendedor.PopularGrid(0);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendedor.DataSource = ds.Tables[0];
                    ddlVendedor.DataValueField = "Id";
                    ddlVendedor.DataTextField = "NOMBRE";
                    ddlVendedor.DataBind();
                }
                else
                {
                    ddlVendedor.DataSource = null;
                    ddlVendedor.Items.Clear();
                }

            }
            else
            {
                Alert.Show("Não há clientes");
            }
        }
        

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void ddlVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularProdutos();
            Button1_Click(null, null);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            cVendedor engineVendedor = new cVendedor();
            mVendedor _vendedor = engineVendedor.RetornarVendedor(ddlVendedor.SelectedValue.ToString());
            lblTituloRelatorio.Text = _vendedor.nombre.ToString() + " - " + DateTime.Now.Year.ToString();

            cProduto engineProduto = new cProduto();
            mProduto _produto = null;
            if(!rdTodos.Checked)
               _produto= engineProduto.RetornarProduto(ddlFiltro.SelectedValue.ToString());

            clog_arquivos engineLogArquivos = new clog_arquivos();
            //string dataInicial = ddlAno.Text + "-" + (ddlMes.SelectedIndex + 1).ToString() + "-01";
            //string dataFinal = ddlAno.Text + "-" + (ddlMes.SelectedIndex + 1).ToString() + "-31";


            DataSet ds = new DataSet();
            clsConexao banco = new clsConexao();
            string _sql = "SELECT * FROM produto";

            if (_produto != null)
                _sql += " where id=" + _produto.Id.ToString();

            //if (rdbProduto.Checked)
            //    _sql += " where id=" + _produto.Id.ToString();

            ds = banco.Selecionar(_sql);
            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add("MES");//0
            dtNovo.Columns.Add("VENTA_NETA");//1
            dtNovo.Columns.Add("META");//2
            dtNovo.Columns.Add("RENDIMIENTO");//3
            dtNovo.Columns.Add("DESEMPENO");//4           




            DataRow dr;
            double totalvenda_geral = 0;
            double totalmeta_geral = 0;
            double totalrendimento_geral = 0;
            double totaldesempenho_geral = 0;
            double[] totalvenda_array = new double[13];
            double[] totalmeta_array = new double[13];
            double[] totalrendimento_array = new double[13];
            double[] totaldesempenho_array = new double[13];

            for (int i = 1; i <= 12; i++)
            {
                string mes = i.ToString();
                string mesescrito = string.Empty;
                if (mes == "1")
                    mesescrito = "Enero";
                else if (mes == "2")
                    mesescrito = "Febrero";
                else if (mes == "3")
                    mesescrito = "Marzo";
                else if (mes == "4")
                    mesescrito = "Abril";
                else if (mes == "5")
                    mesescrito = "Mayo";
                else if (mes == "6")
                    mesescrito = "Junio";
                else if (mes == "7")
                    mesescrito = "Julio";
                else if (mes == "8")
                    mesescrito = "Agosto";
                else if (mes == "9")
                    mesescrito = "Septiembre";
                else if (mes == "10")
                    mesescrito = "Octubre";
                else if (mes == "11")
                    mesescrito = "Noviembre";
                else if (mes == "12")
                    mesescrito = "Diciembre";
                else
                    mesescrito = mes;


                double totalventa = 0;
                double totalmeta = 0;
                double totalrendimento = 0;
                double totaldesempenho = 0;
                    if (ds != null)
                    {
                        foreach (DataRow linha in ds.Tables[0].Rows)
                        {
                            totalventa += CalcularVenta(_vendedor.id.ToString(), linha["id"].ToString(), i.ToString());
                            totalmeta+=CalcularMeta(linha["id"].ToString(), _vendedor.id.ToString(), i);
                            totalrendimento += (totalventa / totalmeta) * 100;

                            //totalrendimento=CalcularRendimento(_vendedor.id.ToString(), linha["id"].ToString(), i.ToString());
                            totaldesempenho += CalcularDesempenho(_vendedor.id.ToString(), linha["id"].ToString(), i.ToString());
                        }
                    }

                    if (double.IsInfinity(totalventa) || double.IsNaN(totalventa))
                        totalventa = 0;

                    if (double.IsInfinity(totalmeta) || double.IsNaN(totalmeta))
                        totalmeta = 0;

                    double totalvendafloor = Math.Floor(totalventa);
                    double totalmetafloor = Math.Floor(totalmeta);
                
                totalvenda_array[i] = totalvendafloor;
                totalmeta_array[i] = totalmetafloor;
                    dr = dtNovo.NewRow();
                    dr[0] = mesescrito;//0
                    dr[1] = totalvendafloor.ToString();//1
                    dr[2] = totalmetafloor.ToString();//2


                    if (double.IsInfinity(totalrendimento) || double.IsNaN(totalrendimento))
                        totalrendimento = 0;

                    if (double.IsInfinity(totaldesempenho) || double.IsNaN(totaldesempenho))
                        totaldesempenho = 0;

                    double totalrendimentofloor = Math.Floor(totalrendimento);
                    double totaldesempenhofloor = Math.Floor(totaldesempenho);

                    totalrendimento_array[i] = totalrendimentofloor;
                    totaldesempenho_array[i] = totaldesempenhofloor;
                    dr[3] = totalrendimentofloor.ToString();//2;//3                    
                    dr[4] = totaldesempenhofloor.ToString();//4                     
                dtNovo.Rows.Add(dr);

                totalvenda_geral += totalventa;
                totalmeta_geral += totalmeta;
                totalrendimento_geral += totalrendimento;
                totaldesempenho_geral += totaldesempenho;
            }


            if (double.IsInfinity(totalvenda_geral) || double.IsNaN(totalvenda_geral))
                totalvenda_geral = 0;
            if (double.IsInfinity(totalmeta_geral) || double.IsNaN(totalmeta_geral))
                totalmeta_geral = 0;
            if (double.IsInfinity(totalrendimento_geral) || double.IsNaN(totalrendimento_geral))
                totalrendimento_geral = 0;
            if (double.IsInfinity(totaldesempenho_geral) || double.IsNaN(totaldesempenho_geral))
                totaldesempenho_geral = 0;

            double totalvenda_geralfloor = Math.Floor(totalvenda_geral);
            double totalmeta_geralfloor = Math.Floor(totalmeta_geral);
            double totalrendimento_geralfloor = Math.Floor(totalrendimento_geral);
            double totaldesempenho_geralfloor = Math.Floor(totaldesempenho_geral);
            dr = dtNovo.NewRow();
            dr[0] = "Total";//0
            dr[1] = totalvenda_geralfloor.ToString();//1
            dr[2] = totalmeta_geralfloor.ToString();//2
            dr[3] = totalrendimento_geralfloor.ToString();//2
            dr[4] = totaldesempenho_geralfloor.ToString();//2
            dtNovo.Rows.Add(dr);
                                              
            dgvDados.DataSource = dtNovo;
            dgvDados.DataBind();

            BindChart(totalvenda_array,totalmeta_array,totalrendimento_array,totaldesempenho_array);
            //BindChart_2(totalrendimento_array,totaldesempenho_array);
            //Grafico1.DataSource = dtNovo;
            //Grafico1.Series[0].XValueMember = "MES";
            //Grafico1.Series[0].YValueMembers = "META";
            //Grafico1.Series[1].XValueMember = "MES";
            //Grafico1.Series[1].YValueMembers = "VENTA_NETA";
            //Grafico1.DataBind();

            //Grafico2.DataSource = dtNovo;
            //Grafico2.Series[0].XValueMember = "MES";
            //Grafico2.Series[0].YValueMembers = "RENDIMIENTO";
            //Grafico2.Series[1].XValueMember = "MES";
            //Grafico2.Series[1].YValueMembers = "DESEMPENO";
            //Grafico2.DataBind();

        }

        private void BindChart_2(double[] rendimento, double[] desempenho)
        {
            DataTable dsChartData = new DataTable();
            StringBuilder strScript = new StringBuilder();

            try
            {

                strScript.Append(@"<script type=*text/javascript*> google.load( *visualization*, *1*, {packages:[*corechart*]});
            google.setOnLoadCallback(drawChart_2);
            function drawChart_2() {
            var data2 = new google.visualization.DataTable();
            data2.addColumn('string', 'Ano');
            data2.addColumn('number', 'Rendimento');
            data2.addColumn('number', 'Desempenho');
 
            data2.addRows(" + 12 + ");");


                for (int i = 0; i <= 11; i++)
                {
                    string mesescrito = string.Empty;
                    string mes = (i + 1).ToString();
                    if (mes == "1")
                        mesescrito = "Enero";
                    else if (mes == "2")
                        mesescrito = "Febrero";
                    else if (mes == "3")
                        mesescrito = "Marzo";
                    else if (mes == "4")
                        mesescrito = "Abril";
                    else if (mes == "5")
                        mesescrito = "Mayo";
                    else if (mes == "6")
                        mesescrito = "Junio";
                    else if (mes == "7")
                        mesescrito = "Julio";
                    else if (mes == "8")
                        mesescrito = "Agosto";
                    else if (mes == "9")
                        mesescrito = "Septiembre";
                    else if (mes == "10")
                        mesescrito = "Octubre";
                    else if (mes == "11")
                        mesescrito = "Noviembre";
                    else if (mes == "12")
                        mesescrito = "Diciembre";
                    else
                        mesescrito = mes;


                    strScript.Append("data2.setValue( " + i + "," + 0 + "," + "'" + mesescrito + "');");
                    strScript.Append("data2.setValue(" + i + "," + 1 + "," + rendimento[i + 1].ToString() + ") ;");
                    strScript.Append("data2.setValue(" + i + "," + 2 + "," + desempenho[i + 1].ToString() + ") ;");

                }

                strScript.Append("   var chart2 = new google.visualization.LineChart(document.getElementById('chart_div_2'));");
                strScript.Append(" chart2.draw(data2, {width: 660, height: 300, title: 'Company Performance',");
                strScript.Append("hAxis: {title: 'Ano', titleTextStyle: {color: 'green'}}");
                strScript.Append("}); }");
                strScript.Append("</script>");


                ltScripts.Text = strScript.ToString().Replace('*', '"');

            }
            catch
            {
            }
            finally
            {
                dsChartData.Dispose();
                strScript.Clear();
            }
        }  

        private void BindChart(double[] vendas,double[] metas,double[] rendimento, double[] desempenho)
        {
            DataTable dsChartData = new DataTable();
            StringBuilder strScript = new StringBuilder();

            try
            {



                //dsChartData = GetChartData();

                strScript.Append(@"<script type=*text/javascript*> google.load( *visualization*, *1*, {packages:[*corechart*]});
            google.setOnLoadCallback(drawChart);
            function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Ano');
            data.addColumn('number', 'Venta');
            data.addColumn('number', 'Meta');
            data.addColumn('number', 'Rendiemento');
            data.addColumn('number', 'Desenpeno');
 
            data.addRows(" + 12 + ");");

//                strScript.Append(@"<script type='text/javascript'>  
//                    google.load('visualization', '1', {packages: ['corechart']});</script>  
//  
//                    <script type='text/javascript'>  
//                    function drawVisualization() {         
//                    var data = google.visualization.arrayToDataTable([  
//                    ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],");
                
                for (int i = 0; i <= 11; i++)
                {
                    string mesescrito = string.Empty;
                    string mes = (i+1).ToString();
                    if (mes == "1")
                        mesescrito = "Enero";
                    else if (mes == "2")
                        mesescrito = "Febrero";
                    else if (mes == "3")
                        mesescrito = "Marzo";
                    else if (mes == "4")
                        mesescrito = "Abril";
                    else if (mes == "5")
                        mesescrito = "Mayo";
                    else if (mes == "6")
                        mesescrito = "Junio";
                    else if (mes == "7")
                        mesescrito = "Julio";
                    else if (mes == "8")
                        mesescrito = "Agosto";
                    else if (mes == "9")
                        mesescrito = "Septiembre";
                    else if (mes == "10")
                        mesescrito = "Octubre";
                    else if (mes == "11")
                        mesescrito = "Noviembre";
                    else if (mes == "12")
                        mesescrito = "Diciembre";
                    else
                        mesescrito = mes;


                    strScript.Append("data.setValue( " + i + "," + 0 + "," + "'" + mesescrito + "');");
                    strScript.Append("data.setValue(" + i + "," + 1 + "," + vendas[i+1].ToString()+ ") ;");
                    strScript.Append("data.setValue(" + i + "," + 2 + "," + metas[i+1].ToString() + ") ;");
                    strScript.Append("data.setValue(" + i + "," + 3 + "," + rendimento[i + 1].ToString() + ") ;");
                    strScript.Append("data.setValue(" + i + "," + 4 + "," + desempenho[i + 1].ToString() + ") ;");

                }

                //strScript.Append("['" + vendas[1] + "'," + vendas[2] + "," +
                //        vendas[3] + "," + vendas[4] + "," + vendas[5] + "," + vendas[6] + "," +
                //        vendas[7] + "," + vendas[8] + "," + vendas[9] + "," + vendas[10] + "," + 
                //        vendas[11] + "," + vendas[12] + "],");
                //for (int i = 1; i < 12; i++)
                //{
                    
                //}
                //foreach (DataRow row in dsChartData.Rows)
                //{
                //    strScript.Append("['" + row["Month"] + "'," + row["Bolivia"] + "," +
                //        row["Ecuador"] + "," + row["Madagascar"] + "," + row["Avarage"] + "],");
                //}
                //strScript.Remove(strScript.Length - 1, 1);
                //strScript.Append("]);");

                //strScript.Append("var options = { title : 'Ventas x Metas', vAxis: {title: 'Cups'},  hAxis: {title: 'Month'} };");
                //strScript.Append(" var chart = new google.visualization.LineChart(document.getElementById('chart_div'));  chart.draw(data, options); } google.setOnLoadCallback(drawVisualization);");
                //strScript.Append(" </script>");
                strScript.Append("   var chart = new google.visualization.LineChart(document.getElementById('chart_div'));");
                strScript.Append(" chart.draw(data, {width: 660, height: 300, title: 'Company Performance',");
                strScript.Append("hAxis: {title: 'Ano', titleTextStyle: {color: 'green'}}");
                strScript.Append("}); }");
                strScript.Append("</script>");


                ltScripts.Text = strScript.ToString().Replace('*', '"');     
                
            }
            catch
            {
            }
            finally
            {
                dsChartData.Dispose();
                strScript.Clear();
            }
        }  

        private double CalcularDesempenho(string produto_id, string vendedor_id, string mes)
        {
            //valor total de vendas           
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            DataSet ds = engineVentasDiarias.PopularGridVendedorProdutoMes(vendedor_id, produto_id, mesescrito);
            double total = 0;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                for (int i = 1; i <= 31; i++)
                {
                    total += (item[i.ToString()] != string.Empty ? Convert.ToDouble(item[i.ToString()].ToString()) : 0);
                }

            }
            //txtvendatMes.Text = total.ToString();

            //mes
            //txtmes.Text = DateTime.Now.Month.ToString();

            //meta
            ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT (sum(enero)+sum(febrero)+ sum(marzo)+sum(abril)+sum(mayo)+sum(junio)+sum(julio)+sum(agosto)+sum(septiembre)+sum(octubre)+sum(noviembre)+sum(diciembre))  as total FROM asignacionmetas a where a.vendedor=" + vendedor_id + " and a.produto=" + produto_id);
            double metacalc = 0;
            if(ds.Tables[0].Rows[0]["total"].ToString()!=string.Empty)
                metacalc = Convert.ToDouble(ds.Tables[0].Rows[0]["total"]);
            //txtmeta2.Text = metacalc.ToString();

            double venta_mes = Convert.ToDouble(total);
            double mescalc = Convert.ToDouble(DateTime.Now.Month.ToString());
            double meta = Convert.ToDouble(metacalc);
            //lbldesempenho.Text = "Desempenho" + ((((venta_mes / mescalc) * 12) / meta) * 100).ToString();

            //(((4250000/5)*21)/50000000)*100


            return (((venta_mes / mescalc) * 12) / meta) * 100;
        }

        private double CalcularMeta(string produto_id, string vendedor_id, int mes)
        {
            DataSet ds = new DataSet();
            clsConexao banco = new clsConexao();
            double meta = 0;
            string _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor";

            if (mes == 1)
                _sql += " ,sum(a.Enero) as Meta";
            if (mes == 2)
                _sql += " ,sum(a.Febrero) as Meta";
            if (mes == 3)
                _sql += " ,sum(a.Marzo) as Meta";
            if (mes == 4)
                _sql += " ,sum(a.Abril) as Meta";
            if (mes == 5)
                _sql += " ,sum(a.Mayo) as Meta";
            if (mes == 6)
                _sql += " ,sum(a.Junio) as Meta";
            if (mes == 7)
                _sql += " ,sum(a.Julio) as Meta";
            if (mes == 8)
                _sql += " ,sum(a.Agosto) as Meta";
            if (mes == 9)
                _sql += " ,sum(a.Septiembre) as Meta";
            if (mes == 10)
                _sql += " ,sum(a.Octubre) as Meta";
            if (mes == 11)
                _sql += " ,sum(a.Noviembre) as Meta";
            if (mes == 12)
                _sql += " ,sum(a.Diciembre) as Meta";


            _sql += " from asignacionmetas a"
                        + " inner join vendedor v on a.vendedor = v.id"
                        + " inner join produto p on a.produto = p.id"
                        + " inner join dias_uteis d"
                        + " where v.id=" + vendedor_id
                        + " and produto=" + produto_id
                        + " order by v.nombre";
            ds = banco.Selecionar(_sql);

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["Meta"].ToString() != string.Empty)
                        meta = Convert.ToDouble(ds.Tables[0].Rows[0]["Meta"].ToString());
                }
            }
            return meta;

        }
        private double CalcularVenta(string pCodigoVendedor, string pCodigoProduto, string mes)
        {
            string _sql = string.Empty;
            clsConexao banco = new clsConexao();
            DataSet ds = null;
            double total = 0;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;
            _sql = "SELECT"
                            + " (d1_valor + d2_valor+d3_valor+d4_valor+d5_valor+d6_valor"
                            + " +d7_valor + d8_valor+d9_valor+d10_valor+d11_valor+d12_valor)as total"
                            + " FROM ventas_dias where ano='" + ddlAno.Text + "' and mes = '" + mesescrito
                            + " ' and grupo_dia=" + pCodigoProduto
                            + " and vendedor_id=" + pCodigoVendedor;

            ds = banco.Selecionar(_sql);

            if (ds != null)
            {
                foreach (DataRow linha2 in ds.Tables[0].Rows)
                {
                    if (linha2["total"].ToString() != string.Empty)
                        total += Convert.ToDouble(linha2["total"].ToString());
                }
            }

            return total;
        }
        private double CalcularRendimento(string pCodigoVendedor, string pCodigoProduto, string mes)
        {
            //valor total de vendas
            cVentasDiarias engineVentasDiarias = new cVentasDiarias();
            DataSet ds = engineVentasDiarias.PopularGridVendedorProduto(pCodigoVendedor, pCodigoProduto);
            double total = 0;
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                for (int i = 1; i <= 31; i++)
                {
                    total += (item[i.ToString()] != string.Empty ? Convert.ToDouble(item[i.ToString()].ToString()) : 0);
                }

            }
            //txtvalorAcumuladoDia.Text = total.ToString();

            //total de dias trabalhados
            double dias_trabalhadoscalc = 0;
           

            //txtdiasUteisMes.Text = dias_uteis.ToString();
            double dias_uteis = 0;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dias_uteis++;
                        if (i <= DateTime.Now.Day)
                            dias_trabalhadoscalc++;
                    }
                }
            }
            cDiasUteis engineDiasUteis = new cDiasUteis();
            dias_uteis = Convert.ToDouble(engineDiasUteis.PopularGridPorAno(DateTime.Now.Year.ToString()).Tables[0].Rows[0][DateTime.Now.Month + 1].ToString());

            //meta    
            //string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            ds = null;
            clsConexao banco = new clsConexao();
            ds = banco.Selecionar("SELECT sum(" + mesescrito + ") as total FROM asignacionmetas a where a.vendedor=" + pCodigoVendedor + " and produto=" + pCodigoProduto);
            double metacalc = 0;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["total"].ToString() != string.Empty)
                        metacalc = Convert.ToDouble(ds.Tables[0].Rows[0]["total"]);
                }
            }

            //txtmeta.Text = metacalc.ToString();

            double valorAcumuladoDia = Convert.ToDouble(total);
            double dias_trabalhados = Convert.ToDouble(dias_trabalhadoscalc);
            double diasUteisMes = Convert.ToDouble(dias_uteis);
            double meta = Convert.ToDouble(metacalc);
            double resultado = 0;
            resultado = (((valorAcumuladoDia / dias_trabalhados) * diasUteisMes) / meta) * 100;


            return resultado;
        }

        protected void dgvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        protected void rdbTodos_CheckedChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void rdbProduto_CheckedChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);

        }

        protected void ddlproduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void rdTodos_CheckedChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void rdProduto_CheckedChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1_Click(null, null);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                dgvDados.AllowPaging = false;


                dgvDados.HeaderRow.BackColor = System.Drawing.Color.White;
                foreach (TableCell cell in dgvDados.HeaderRow.Cells)
                {
                    cell.BackColor = dgvDados.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in dgvDados.Rows)
                {
                    row.BackColor = System.Drawing.Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = dgvDados.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = dgvDados.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                dgvDados.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string filename = "panel_control_direccion.pdf";
            //salvando a cópia local
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + filename);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            dgvDados.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            var output = new FileStream(Server.MapPath("~/pdf") + "//" + filename, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, output);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


            dgvDados.AllowPaging = true;
            dgvDados.DataBind();
        }
    }
}