﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cd_panelcontrol.aspx.cs" Inherits="Clients_Management.View.cd_panelcontrol" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  

<div class="container">
         <ol class="breadcrumb">
  <li><a id="A4" runat="server" href="~/login.aspx">LOGIN</a></li>
  <li><a href="#">FUERZA DE VENTAS</a></li>
  <li><a href="#">INFORME DE RESULTADOS</a></li>
   <li class="active">PANEL DE CONTROL</li>
</ol>
</div>
<div class="container">
<div class="row">
<div class="col-lg-12">
<asp:Label ID="lblNombre" runat="server" Text="Hola NOMBRA ADMINISTRATOR "></asp:Label></p>
    <asp:DropDownList ID="ddlMes" class="form-control input-sm" runat="server" Visible="false" AutoPostBack="True" OnSelectedIndexChanged="ddlMes_SelectedIndexChanged" >
                <asp:ListItem>Enero</asp:ListItem>
                <asp:ListItem>Febrero</asp:ListItem>
                <asp:ListItem>Marzo</asp:ListItem>
                <asp:ListItem>Abril</asp:ListItem>
                <asp:ListItem>Mayo</asp:ListItem>
                <asp:ListItem>Junio</asp:ListItem>
                <asp:ListItem>Julio</asp:ListItem>
                <asp:ListItem>Agosto</asp:ListItem>
                <asp:ListItem>Septiembre</asp:ListItem>
                <asp:ListItem>Octubre</asp:ListItem>
                <asp:ListItem>Noviembre</asp:ListItem>
                <asp:ListItem>Diciembre</asp:ListItem>
            </asp:DropDownList>
</div>

<div class="container">
<div class="well well-sm">



<div class="row">
  <h4 align="left" class="col-sm-4"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>: <asp:Label ID="lblData" runat="server" Text="Label"></asp:Label></h4>
  <div align="left" class="col-sm-4"><h5>Días hábiles a la fecha: <span class="label label-danger"><asp:Label ID="lblDiasUteis" runat="server" Text="Label"></asp:Label> </span></h5></div>
  <div align="left" class="col-sm-4"><h5>Días hábiles totales: <span class="label label-danger"><asp:Label ID="lblTotalDiasUteis" runat="server" Text="Label"></asp:Label> </span></h5></div>
 
</div>




</div>


<div class="row">
<div class="col-md-12">
<div class="well">
<asp:GridView ID="dgvDados" runat="server" class="table table-striped table-bordered table-condensed" Font-Size="11px" BackColor="White" OnRowDataBound="dgvDados_RowDataBound"></asp:GridView>
</div>
</div>
</div>
</div>

</div>
     <asp:Label ID="lblvendas2" runat="server" Text="Label" Visible="false"></asp:Label><br />
 <asp:Label ID="lbldiastrabalhados2" runat="server" Text="Label" Visible="false"></asp:Label><br />
 <asp:Label ID="lbldiasuteis2" runat="server" Text="Label" Visible="false"></asp:Label><br />
 <asp:Label ID="lblmeta2" runat="server" Text="Label" Visible="false"></asp:Label><br />
 <div id="footer"></div>
<!-- Why choose us Section --><!-- Our clients Section --><!-- Footer -->
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script></div>
</asp:Content>
