﻿using Clients_Management.Controller;
using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clients_Management.View
{
    public partial class form_informe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mVendedor _vendedor = null;
            if (Session["vendedor"] != null)
            {
                _vendedor = (mVendedor)Session["vendedor"];

                if (!IsPostBack)
                {
                    for (int i = 1; i < DateTime.Now.Month; i++)
                    {
                        ddlMes.Items.RemoveAt(0);
                        ddlMes.DataBind();
                    }
                    if (_vendedor != null)
                        lblNombre.Text = "Hola " + _vendedor.nombre.ToString();
                }


            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {

                PopularGridGeral(0);
                //PopularVendedores();
                calendario.Text = DateTime.Now.ToString("dd/MM/yyyy");
                PreencherDiasDoMes();
                PopularGridGeralFiltro(ddlAno.Text, ddlMes.Text);
                PopularDiaSemana();

                ddlCliente.SelectedIndex = 0;
                ddlCliente_SelectedIndexChanged(null, null);
            }
        }
        private void PopularVendedoresPlanejado()
        {
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            cClientes engineCliente = new cClientes();
            if (engineCliente.PopularGridPorVendedor_Planejados(_vendedor.id,DateTime.Now.Year.ToString(),DateTime.Now.Month.ToString(),DateTime.Now.Day.ToString()) != null)
            {
                ddlCliente.DataSource = engineCliente.PopularGridPorVendedor_Planejados(_vendedor.id, DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString()).Tables[0];
                ddlCliente.DataValueField = "Id";
                ddlCliente.DataTextField = "Razon Social";
                ddlCliente.DataBind();
            }
            else
            {
                Alert.Show("Não há clientes");
            }

            //<asp:ListItem>0: CONTACTO ABORTADO</asp:ListItem>
            //<asp:ListItem>2: CONTACTO TELEFONICO</asp:ListItem>
            //<asp:ListItem>3: CONTACTO EFECTIVO</asp:ListItem>
            optone.Items.Clear();
            optone.Items.Add("0: CONTACTO ABORTADO");
            optone.Items.Add("2: CONTACTO TELEFONICO");
            optone.Items.Add("3: CONTACTO EFECTIVO");


            string nomecontrole = string.Empty;
            for (int i = 1; i <= 31; i++)
            {
                nomecontrole = "txtd" + i.ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                TB.Visible = false;
                nomecontrole = "lbld" + i.ToString();
                Label LB = Master.FindControl("MainContent").FindControl(nomecontrole) as Label;
                LB.Visible = false;
            }
        }

        private void PopularVendedoresNoPlanejado()
        {
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            cClientes engineCliente = new cClientes();
            if (engineCliente.PopularGridPorVendedor_No_Planejados(_vendedor.id, DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString()) != null)
            {
                ddlCliente.DataSource = engineCliente.PopularGridPorVendedor_No_Planejados(_vendedor.id, DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString()).Tables[0];
                ddlCliente.DataValueField = "Id";
                ddlCliente.DataTextField = "Razon Social";
                ddlCliente.DataBind();
            }
            else
            {
                Alert.Show("Não há clientes");
            }

            optone.Items.Clear();
            optone.Items.Add("1: NO PLAN");
            optone.Items.Add("4: FOLLOW UP");

            string nomecontrole = string.Empty;
            for (int i = 1; i <= 31; i++)
            {
                nomecontrole = "txtd" + i.ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                TB.Visible = false;
                nomecontrole = "lbld" + i.ToString();
                Label LB = Master.FindControl("MainContent").FindControl(nomecontrole) as Label;
                LB.Visible = false;
            }
            PreencherDiasDoMesControles();
            LimparControles();
        }

        public void PreencherDiasDoMesControles()
        {
            cInformediario engineInformeDiario = new cInformediario();
            mInformediario _informeDiario = new mInformediario();
            cClientes engineCliente = new cClientes();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            if (ddlCliente.SelectedValue.ToString() == string.Empty)
                return;
            mClientes _cliente = (ddlCliente.SelectedValue != null ? engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);

            IList<mInformediario> lstInformeDiario = engineInformeDiario.RetornarInformediarioDia(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), _vendedor.id.ToString(), _cliente.id.ToString());
            txtid.Text = "0";
            LimparControles();



            if (lstInformeDiario != null)
            {
                if (lstInformeDiario.Count > 0)
                {
                    _informeDiario = lstInformeDiario[0];
                    txtid.Text = lstInformeDiario[0].Id.ToString();
                    txtd1.Text = (_informeDiario.d1_valor != string.Empty ? _informeDiario.d1_valor : txtd1.Text);
                    txtd2.Text = (_informeDiario.d2_valor != string.Empty ? _informeDiario.d2_valor : txtd2.Text);
                    txtd3.Text = (_informeDiario.d3_valor != string.Empty ? _informeDiario.d3_valor : txtd3.Text);
                    txtd4.Text = (_informeDiario.d4_valor != string.Empty ? _informeDiario.d4_valor : txtd4.Text);
                    txtd5.Text = (_informeDiario.d5_valor != string.Empty ? _informeDiario.d5_valor : txtd5.Text);
                    txtd6.Text = (_informeDiario.d6_valor != string.Empty ? _informeDiario.d6_valor : txtd6.Text);
                    txtd7.Text = (_informeDiario.d7_valor != string.Empty ? _informeDiario.d7_valor : txtd7.Text);
                    txtd8.Text = (_informeDiario.d8_valor != string.Empty ? _informeDiario.d8_valor : txtd8.Text);
                    txtd9.Text = (_informeDiario.d9_valor != string.Empty ? _informeDiario.d9_valor : txtd9.Text);
                    txtd10.Text = (_informeDiario.d10_valor != string.Empty ? _informeDiario.d10_valor : txtd10.Text);
                    txtd11.Text = (_informeDiario.d11_valor != string.Empty ? _informeDiario.d11_valor : txtd11.Text);
                    txtd12.Text = (_informeDiario.d12_valor != string.Empty ? _informeDiario.d12_valor : txtd12.Text);
                    txtd13.Text = (_informeDiario.d13_valor != string.Empty ? _informeDiario.d13_valor : txtd13.Text);
                    txtd14.Text = (_informeDiario.d14_valor != string.Empty ? _informeDiario.d14_valor : txtd14.Text);
                    txtd15.Text = (_informeDiario.d15_valor != string.Empty ? _informeDiario.d15_valor : txtd15.Text);
                    txtd16.Text = (_informeDiario.d16_valor != string.Empty ? _informeDiario.d16_valor : txtd16.Text);
                    txtd17.Text = (_informeDiario.d17_valor != string.Empty ? _informeDiario.d17_valor : txtd17.Text);
                    txtd18.Text = (_informeDiario.d18_valor != string.Empty ? _informeDiario.d18_valor : txtd18.Text);
                    txtd19.Text = (_informeDiario.d19_valor != string.Empty ? _informeDiario.d19_valor : txtd19.Text);
                    txtd20.Text = (_informeDiario.d20_valor != string.Empty ? _informeDiario.d20_valor : txtd20.Text);
                    txtd21.Text = (_informeDiario.d21_valor != string.Empty ? _informeDiario.d21_valor : txtd21.Text);
                    txtd22.Text = (_informeDiario.d22_valor != string.Empty ? _informeDiario.d22_valor : txtd22.Text);
                    txtd23.Text = (_informeDiario.d23_valor != string.Empty ? _informeDiario.d23_valor : txtd23.Text);
                    txtd24.Text = (_informeDiario.d24_valor != string.Empty ? _informeDiario.d24_valor : txtd24.Text);
                    txtd25.Text = (_informeDiario.d25_valor != string.Empty ? _informeDiario.d25_valor : txtd25.Text);
                    txtd26.Text = (_informeDiario.d26_valor != string.Empty ? _informeDiario.d26_valor : txtd26.Text);
                    txtd27.Text = (_informeDiario.d27_valor != string.Empty ? _informeDiario.d27_valor : txtd27.Text);
                    txtd28.Text = (_informeDiario.d28_valor != string.Empty ? _informeDiario.d28_valor : txtd28.Text);
                    txtd29.Text = (_informeDiario.d29_valor != string.Empty ? _informeDiario.d29_valor : txtd29.Text);
                    txtd30.Text = (_informeDiario.d30_valor != string.Empty ? _informeDiario.d30_valor : txtd30.Text);
                    txtd31.Text = (_informeDiario.d31_valor != string.Empty ? _informeDiario.d31_valor : txtd31.Text);
                }
            }
        }

        public void LimparControles()
        {
            string nomecontrole = string.Empty;
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            cParametrosDiversos engineParametrosDiversos = new cParametrosDiversos();
            mParametrosDiversos _paramDiv = new mParametrosDiversos();
            int dia = 0;
            int dialimite = 0;
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                nomecontrole = "txtd" + (i + 1).ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        TB.Text = "";// ListaParametroCartaGantt[0].dia + " - ";
                        TB.Enabled = true;
                        _paramDiv = engineParametrosDiversos.RetornarParametrosDiversos("1");
                        dia = DateTime.Now.Day;
                        dialimite = dia - _paramDiv.travardias;
                        if (dia < dialimite)
                            TB.Enabled = false;
                    }
                    else
                    {
                        TB.Text = "F";
                        TB.Enabled = false;
                    }
                }
                else
                {
                    TB.Text = "F";
                    TB.Enabled = false;
                }
            }
        }
        private void PopularGridGeral(int limite)
        {
            cInformediario engineInformediario = new cInformediario();
            DataTable dt = engineInformediario.PopularGrid(limite,DateTime.Now.Year.ToString(),DateTime.Now.Month.ToString()).Tables[0];
            DataTable dtNovo = new DataTable("Dados");
            dtNovo.Columns.Add("id");
            dtNovo.Columns.Add("calendario");
            dtNovo.Columns.Add("razonsocial");
            dtNovo.Columns.Add("contacto");
            dtNovo.Columns.Add("telefone");
            dtNovo.Columns.Add("comuna");
            dtNovo.Columns.Add("ano");
            dtNovo.Columns.Add("mes");
            dtNovo.Columns.Add("1");
            dtNovo.Columns.Add("2");
            dtNovo.Columns.Add("3");
            dtNovo.Columns.Add("4");
            dtNovo.Columns.Add("5");
            dtNovo.Columns.Add("6");
            dtNovo.Columns.Add("7");
            dtNovo.Columns.Add("8");
            dtNovo.Columns.Add("9");
            dtNovo.Columns.Add("10");
            dtNovo.Columns.Add("11");
            dtNovo.Columns.Add("12");
            dtNovo.Columns.Add("13");
            dtNovo.Columns.Add("14");
            dtNovo.Columns.Add("15");
            dtNovo.Columns.Add("16");
            dtNovo.Columns.Add("17");
            dtNovo.Columns.Add("18");
            dtNovo.Columns.Add("19");
            dtNovo.Columns.Add("20");
            dtNovo.Columns.Add("21");
            dtNovo.Columns.Add("22");
            dtNovo.Columns.Add("23");
            dtNovo.Columns.Add("24");
            dtNovo.Columns.Add("25");
            dtNovo.Columns.Add("26");
            dtNovo.Columns.Add("27");
            dtNovo.Columns.Add("28");
            dtNovo.Columns.Add("29");
            dtNovo.Columns.Add("30");
            dtNovo.Columns.Add("31");
            DataRow dr= dtNovo.NewRow();
            dr["id"] = "";//0;
            dr["calendario"] = "";// DateTime.Now;
            dr["razonsocial"] = "";
            dr["contacto"] = "";
            dr["telefone"] = "";
            dr["comuna"] = "";
            dr["ano"] = "";
            dr["mes"] = "";
            cParametrosCartaGantt engineParametrosCartaGantt = new cParametrosCartaGantt();
            List<mParametrosCartaGantt> ListaParametroCartaGantt = null; ;
            for (int i = 0; i < 31; i++)
            {
                ListaParametroCartaGantt = engineParametrosCartaGantt.RetornarParametrosCartaGantt(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), (i + 1).ToString());
                if (ListaParametroCartaGantt != null)
                {
                    if (ListaParametroCartaGantt.Count > 0)
                    {
                        dr[(i+1).ToString()] = ListaParametroCartaGantt[0].dia;
                    }
                    else
                    {
                        dr[(i + 1).ToString()] = "F";
                    }
                }
                else
                {
                    dr[(i + 1).ToString()] = "F";
                }
                
            }                                                                                 
            dtNovo.Rows.Add(dr);
            foreach (DataRow linha in dt.Rows)
            {
                dr = dtNovo.NewRow();
                dr["id"] = linha["id"];
                dr["calendario"] = linha["calendario"];
                dr["razonsocial"] = linha["razonsocial"];
                dr["contacto"] = linha["contacto"];
                dr["telefone"] = linha["telefone"];
                dr["comuna"] = linha["comuna"];
                dr["ano"] = linha["ano"];
                dr["mes"] = linha["mes"];
                dr["1"] = linha["1"];
                dr["2"] = linha["2"];
                dr["3"] = linha["3"];
                dr["4"] = linha["4"];
                dr["5"] = linha["5"];
                dr["6"] = linha["6"];
                dr["7"] = linha["7"];
                dr["8"] = linha["8"];
                dr["9"] = linha["9"];
                dr["10"] = linha["10"];
                dr["11"] = linha["11"];
                dr["12"] = linha["12"];
                dr["13"] = linha["13"];
                dr["14"] = linha["14"];
                dr["15"] = linha["15"];
                dr["16"] = linha["16"];
                dr["17"] = linha["17"];
                dr["18"] = linha["18"];
                dr["19"] = linha["19"];
                dr["20"] = linha["20"];
                dr["21"] = linha["21"];
                dr["22"] = linha["22"];
                dr["23"] = linha["23"];
                dr["24"] = linha["24"];
                dr["25"] = linha["25"];
                dr["26"] = linha["26"];
                dr["27"] = linha["27"];
                dr["28"] = linha["28"];
                dr["29"] = linha["29"];
                dr["30"] = linha["30"];
                dr["31"] = linha["31"];   
                dtNovo.Rows.Add(dr);
            }

            if (dtNovo.Rows.Count > 1)
                PopularVendedoresPlanejado();
            else
                PopularVendedoresNoPlanejado();

            gdvDados.DataSource = dtNovo;
            gdvDados.AutoGenerateColumns = true;
            gdvDados.AutoGenerateSelectButton = false;
            gdvDados.DataBind();

            int contador = 0;
            foreach (GridViewRow r in gdvDados.Rows)
            {
                if (contador > 0)
                {
                    for (int i = 7; i < 39; i++)
                    {
                        //if (r.Cells[i].Text == "F" || r.Cells[i].Text == "")
                        if (r.Cells[i].Text == "F" )
                        {
                            r.Cells[i].BackColor = System.Drawing.Color.LightGray;
                            r.Cells[i].Text = "F";
                        }
                        //else if (r.Cells[i].Text == "1" || r.Cells[i].Text == "3")
                        //{
                        //    r.Cells[i].Text = "1";
                        //}
                        //else
                        //{
                        //    //r.Cells[i].Text = "";
                        //}

                    }
                    contador++;
                }
                else
                {
                    contador++;
                }
            }

        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            limpar();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cParametrosDiversos engineParametros = new cParametrosDiversos();

            string mes = DateTime.Now.Month.ToString();

            string mesescrito = string.Empty;

            if (optone.Text.Trim().ToLower()==string.Empty)
            {
                Alert.Show("Elija forma de contacto");
                return;
            }

            if (comment.Text.Trim().ToLower() == string.Empty)
            {
                Alert.Show("Elija acuerdos");
                return;
            }
            if (txtComentario.Text.Trim().ToLower() == string.Empty)
            {
                Alert.Show("Elija cotizar");
                return;
            }
            if (TextBox3.Text.Trim().ToLower() == string.Empty)
            {
                Alert.Show("Elija Proxima");
                return;
            }

            if (DateTime.Now.Day > engineParametros.RetornarParametrosDiversos()[0].travardias)
            {
                Alert.Show("Fora do prazo para planificar");
                return;
            }

            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            string nomecontrole = string.Empty;
            for (int i = 1; i <= 31; i++)
            {
                nomecontrole = "txtd" + i.ToString();
                TextBox TB = Master.FindControl("MainContent").FindControl(nomecontrole) as TextBox;
                if (TB.Text.Trim() != "1" && TB.Text.Trim() != "4" && TB.Text.Trim() != string.Empty && TB.Text.Trim() != "F")
                {
                    Alert.Show("Somente permitido entrada com 1 ou 4");
                    return;
                }
                else if (TB.Text == "1" || TB.Text == "4")
                {
                    if (engineInformediario.RetornaVisitasAgendadas(DateTime.Now.Year.ToString(), mesescrito, ((mVendedor)Session["vendedor"]).id.ToString(), i.ToString()) >= engineParametros.RetornarParametrosDiversos()[0].maxvisitasagendadas)
                    {
                        Alert.Show("Número de visitas por dia no es permitido para o dia " + i.ToString());
                        return;
                    }
                }
            }

            string valordia = optone.Text.Substring(0, 1).ToString();
            IList<mInformediario> listInforme = engineInformediario.RetornarInformediario(DateTime.Now.Year.ToString(), mesescrito, ((mVendedor)Session["vendedor"]).id.ToString(), engineCliente.RetornarClientes(ddlCliente.SelectedValue).id.ToString());
           
            if (txtid.Text !=string.Empty && txtid.Text!="0")
            {
                if (valordia != "0" && valordia != "2" && valordia != "3")
                {
                    Alert.Show("Forma de Contacto inválida");
                    return;
                }
            }

            if (listInforme != null)
            {
                if (listInforme.Count > 0)
                {
                    _Informediario = listInforme[0];
                    txtid.Text = _Informediario.Id.ToString();
                }
            }
            
            _Informediario.calendario = DateTime.Now;
            //_Informediario.cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
            _Informediario.vendedor = (mVendedor)Session["vendedor"];
            _Informediario.contacto = txtContato.Text;
            _Informediario.formacontacto = optone.Text;
            _Informediario.formacontacto2 = optone.Text;
            _Informediario.acuerdos = comment.Text;
            _Informediario.ano = DateTime.Now.Year.ToString();
            _Informediario.mes = mesescrito;
            
            //_Informediario.d1_valor = txtd1.Text;
            //_Informediario.d2_valor = txtd2.Text;
            //_Informediario.d3_valor = txtd3.Text;
            //_Informediario.d4_valor = txtd4.Text;
            //_Informediario.d5_valor = txtd5.Text;
            //    _Informediario.d6_valor = txtd6.Text;
            //    _Informediario.d7_valor = txtd7.Text;
            //    _Informediario.d8_valor = txtd8.Text;
            //    _Informediario.d9_valor = txtd9.Text;
            //    _Informediario.d10_valor = txtd10.Text;
            //    _Informediario.d11_valor = txtd11.Text;
            //    _Informediario.d12_valor = txtd12.Text;
            //    _Informediario.d13_valor = txtd13.Text;
            //    _Informediario.d14_valor = txtd14.Text;
            //    _Informediario.d15_valor = txtd15.Text;
            //    _Informediario.d16_valor = txtd16.Text;
            //    _Informediario.d17_valor = txtd17.Text;
            //    _Informediario.d18_valor = txtd18.Text;
            //    _Informediario.d19_valor = txtd19.Text;
            //    _Informediario.d20_valor = txtd2.Text;
            //    _Informediario.d21_valor = txtd21.Text;
            //    _Informediario.d22_valor = txtd22.Text;
            //    _Informediario.d23_valor = txtd23.Text;
            //    _Informediario.d24_valor = txtd24.Text;
            //    _Informediario.d25_valor = txtd25.Text;
            //    _Informediario.d26_valor = txtd26.Text;
            //    _Informediario.d27_valor = txtd27.Text;
            //    _Informediario.d28_valor = txtd28.Text;
            //    _Informediario.d29_valor = txtd29.Text;
            //    _Informediario.d30_valor = txtd30.Text;
            //    _Informediario.d31_valor = txtd31.Text;

            if (DateTime.Now.Day == 1)
                _Informediario.d1_valor = valordia;
            else if (DateTime.Now.Day == 2)
                _Informediario.d2_valor = valordia;
            else if (DateTime.Now.Day == 3)
                _Informediario.d3_valor = valordia;
            else if (DateTime.Now.Day == 4)
                _Informediario.d4_valor = valordia;
            else if (DateTime.Now.Day == 5)
                _Informediario.d5_valor = valordia;
            else if (DateTime.Now.Day == 6)
                _Informediario.d6_valor = valordia;
            else if (DateTime.Now.Day == 7)
                _Informediario.d7_valor = valordia;
            else if (DateTime.Now.Day == 8)
                _Informediario.d8_valor = valordia;
            else if (DateTime.Now.Day == 9)
                _Informediario.d9_valor = valordia;
            else if (DateTime.Now.Day == 10)
                _Informediario.d10_valor = valordia;
            else if (DateTime.Now.Day == 11)
                _Informediario.d11_valor = valordia;
            else if (DateTime.Now.Day == 12)
                _Informediario.d12_valor = valordia;
            else if (DateTime.Now.Day == 13)
                _Informediario.d13_valor = valordia;
            else if (DateTime.Now.Day == 14)
                _Informediario.d14_valor = valordia;
            else if (DateTime.Now.Day == 15)
                _Informediario.d15_valor = valordia;
            else if (DateTime.Now.Day == 16)
                _Informediario.d16_valor = valordia;
            else if (DateTime.Now.Day == 17)
                _Informediario.d17_valor = valordia;
            else if (DateTime.Now.Day == 18)
                _Informediario.d18_valor = valordia;
            else if (DateTime.Now.Day == 19)
                _Informediario.d19_valor = valordia;
            else if (DateTime.Now.Day == 20)
                _Informediario.d20_valor = valordia;
            else if (DateTime.Now.Day == 21)
                _Informediario.d21_valor = valordia;
            else if (DateTime.Now.Day == 22)
                _Informediario.d22_valor = valordia;
            else if (DateTime.Now.Day == 23)
                _Informediario.d23_valor = valordia;
            else if (DateTime.Now.Day == 24)
                _Informediario.d24_valor = valordia;
            else if (DateTime.Now.Day == 25)
                _Informediario.d25_valor = valordia;
            else if (DateTime.Now.Day == 26)
                _Informediario.d26_valor = valordia;
            else if (DateTime.Now.Day == 27)
                _Informediario.d27_valor = valordia;
            else if (DateTime.Now.Day == 28)
                _Informediario.d28_valor = valordia;
            else if (DateTime.Now.Day == 29)
                _Informediario.d29_valor = valordia;
            else if (DateTime.Now.Day == 30)
                _Informediario.d30_valor = valordia;
            else if (DateTime.Now.Day == 31)
                _Informediario.d31_valor = valordia;
        

            _Informediario.Id = (global.IsNumeric(txtid.Text) ? Convert.ToInt32(txtid.Text) : 0);

            Session["informe_id"] = string.Empty;
            if (txtid.Text == string.Empty || txtid.Text=="0")
            {
                _Informediario.cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
                _Informediario.Id = Convert.ToInt32(engineInformediario.Inserir(_Informediario));
                txtid.Text = _Informediario.Id.ToString();
                Session["informe_id"] = _Informediario.Id.ToString();
                PopularVendedoresNoPlanejado();

            }
            else
            {
                engineInformediario.Update(_Informediario);
                Session["informe_id"] = _Informediario.Id.ToString();
                limpar();
                PopularGridGeral(0);
            }

            cInformediario engineInforme = new cInformediario();
            
            mClientes _cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            string codigo = engineInforme.InserirOtro(_cliente.id.ToString(), TextBox3.Text, _vendedor.id.ToString(), Session["informe_id"].ToString());
            //Alert.Show("Inserido otro de código:" + codigo);
            //TextBox2.Text = string.Empty;
            //txtOtro.Text = string.Empty;
            Session["informesalvo"] = Session["informe_id"];

            comment.Text= string.Empty;
            txtComentario.Text = string.Empty;
            TextBox3.Text= string.Empty;
            
            Alert.Show("Sucesso");
            

        }

        private void limpar()
        {
            txtid.Text = string.Empty;
            txtContato.Text = string.Empty;
            ddlCliente.SelectedIndex = 0;
            optone.SelectedIndex = 0;
            optone.SelectedIndex = 0;            
            comment.Text = string.Empty;

        }

        protected void gdvDados_SelectedIndexChanged(object sender, EventArgs e)
        {
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = engineInformediario.RetornarInformediario(gdvDados.SelectedRow.Cells[1].Text);
            txtid.Text = _Informediario.Id.ToString();
            Session["informe_id"] = _Informediario.Id.ToString();
            calendario.Text = _Informediario.calendario.ToString("yyyy-MM-dd");
            txtContato.Text = _Informediario.contacto.ToString();
            ddlCliente.SelectedIndex = ddlCliente.Items.IndexOf(ddlCliente.Items.FindByValue(_Informediario.cliente.id.ToString()));
            //optone.Text = _Informediario.formacontacto.ToString();
            //optone.Text = _Informediario.formacontacto2.ToString();
            comment.Text = _Informediario.acuerdos.ToString();
            lblDia.Text = DateTime.Now.Day.ToString();
            //txtd1.Text = _Informediario.d1_valor;
            //txtd2.Text = _Informediario.d2_valor;
            //txtd3.Text = _Informediario.d3_valor;
            //txtd4.Text = _Informediario.d4_valor;
            //txtd5.Text = _Informediario.d5_valor;
            //txtd6.Text = _Informediario.d6_valor;
            //txtd7.Text = _Informediario.d7_valor;
            //txtd8.Text = _Informediario.d8_valor;
            //txtd9.Text = _Informediario.d9_valor;
            //txtd10.Text = _Informediario.d10_valor;
            //txtd11.Text = _Informediario.d11_valor;
            //txtd12.Text = _Informediario.d12_valor;
            //txtd13.Text = _Informediario.d13_valor;
            //txtd14.Text = _Informediario.d14_valor;
            //txtd15.Text = _Informediario.d15_valor;
            //txtd16.Text = _Informediario.d16_valor;
            //txtd17.Text = _Informediario.d17_valor;
            //txtd18.Text = _Informediario.d18_valor;
            //txtd19.Text = _Informediario.d19_valor;
            //txtd20.Text = _Informediario.d20_valor;
            //txtd21.Text = _Informediario.d21_valor;
            //txtd2.Text = _Informediario.d22_valor;
            //txtd23.Text = _Informediario.d23_valor;
            //txtd24.Text = _Informediario.d24_valor;
            //txtd25.Text = _Informediario.d25_valor;
            //txtd26.Text = _Informediario.d26_valor;
            //txtd27.Text = _Informediario.d27_valor;
            //txtd28.Text = _Informediario.d28_valor;
            //txtd29.Text = _Informediario.d29_valor;
            //txtd30.Text = _Informediario.d30_valor;
            //txtd31.Text = _Informediario.d31_valor;

        }

        protected void gdvDados_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            cInformediario engineInformediario = new cInformediario();
            mInformediario _Informediario = new mInformediario();
            _Informediario = engineInformediario.RetornarInformediario(gdvDados.Rows[e.RowIndex].Cells[1].Text);
            engineInformediario.Deletar(_Informediario);
            limpar();
            PopularGridGeral(0);
        }

        protected void gdvDados_PreRender(object sender, EventArgs e)
        {
            if (gdvDados.Rows.Count > 0)
            {
                gdvDados.UseAccessibleHeader = true;
                gdvDados.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void gdvDados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 1; j < e.Row.Cells.Count; j++)
                {
                    string encoded = e.Row.Cells[j].Text;
                    e.Row.Cells[j].Text = Context.Server.HtmlDecode(encoded);
                }

            }
        }

        protected void optone_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (optone.SelectedIndex == 1)
            //{
            //    opttwo.Items.Clear();
            //    opttwo.Items.Add("0 - No Visitado");
            //    opttwo.Items.Add("2 - Telefonico");
            //    opttwo.Items.Add("3 - Contacto Efetivo");
            //}
            //else if (optone.SelectedIndex == 2)
            //{
            //    opttwo.Items.Clear();
            //    opttwo.Items.Add("1 - No Plan");
            //}
            //else if (optone.SelectedIndex == 3)
            //{
            //    opttwo.Items.Clear();
            //    opttwo.Items.Add("4 - Segmiento");
            //}
        }

        protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            cClientes engineCliente = new cClientes();
            if (ddlCliente.SelectedValue != null)
            {
                if (ddlCliente.SelectedValue.ToString() != string.Empty)
                {
                    mClientes _cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
                    txtContato.Text = _cliente.contato;
                    PreencherDiasDoMes();
                }
            }
        }

        protected void btnSalvarVisita_Click(object sender, EventArgs e)
        {
            //cInformediario engineInforme = new cInformediario();
            //cClientes engineCliente = new cClientes();
            //mClientes _cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
            //mVendedor _vendedor = (mVendedor)Session["vendedor"];
            //string codigo = engineInforme.InserirVisita(_cliente.id.ToString(), txtFecha.Text, txtdia.Text, ddlMes.Text, _vendedor.id.ToString());
            //Alert.Show("Salvo visita de código:" + codigo);
            cInformediario engineInformeDiario = new cInformediario();
            mInformediario _informeDiario = new mInformediario();
            cClientes engineCliente = new cClientes();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            if (ddlCliente.SelectedValue.ToString() == string.Empty)
                return;
            mClientes _cliente = (ddlCliente.SelectedValue != null ? engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);

            IList<mInformediario> lstInformeDiario = engineInformeDiario.RetornarInformediario(ddlAno.Text, ddlMes.Text, _vendedor.id.ToString(), _cliente.id.ToString());
            string mes = DateTime.Now.Month.ToString();
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            mesescrito = ddlMes.Text;
            if (lstInformeDiario != null)
            {
                if (lstInformeDiario.Count > 0)
                {
                    _informeDiario = lstInformeDiario[0];
                }
                else{
                    _informeDiario = new mInformediario();
                    _informeDiario.Id = 0;
                    _informeDiario.calendario = (calendario.Text != string.Empty ? Convert.ToDateTime(calendario.Text) : Convert.ToDateTime("1900-01-01"));
                    _informeDiario.cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
                    _informeDiario.vendedor = (mVendedor)Session["vendedor"];
                    _informeDiario.contacto = txtContato.Text;
                    _informeDiario.formacontacto = optone.Text;
                    _informeDiario.formacontacto2 = optone.Text;
                    _informeDiario.acuerdos = comment.Text;
                    _informeDiario.ano = DateTime.Now.Year.ToString();
                    _informeDiario.mes = mesescrito;
                }
            }
            else
            {
                _informeDiario = new mInformediario();
                _informeDiario.Id = 0;
                _informeDiario.calendario = (calendario.Text != string.Empty ? Convert.ToDateTime(calendario.Text) : Convert.ToDateTime("1900-01-01"));
                _informeDiario.cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
                _informeDiario.vendedor = (mVendedor)Session["vendedor"];
                _informeDiario.contacto = txtContato.Text;
                _informeDiario.formacontacto = optone.Text;
                _informeDiario.formacontacto2 = optone.Text;
                _informeDiario.acuerdos = comment.Text;
                _informeDiario.ano = DateTime.Now.Year.ToString();
                _informeDiario.mes = mesescrito;
            }

                if (ddlFecha.Text == "1")
                    _informeDiario.d1_valor = "P";
                else if (ddlFecha.Text == "2")
                    _informeDiario.d2_valor = "P";
                else if (ddlFecha.Text == "3")
                    _informeDiario.d3_valor = "P";
                else if (ddlFecha.Text == "4")
                    _informeDiario.d4_valor = "P";
                else if (ddlFecha.Text == "5")
                    _informeDiario.d5_valor = "P";
                else if (ddlFecha.Text == "6")
                    _informeDiario.d6_valor = "P";
                else if (ddlFecha.Text == "7")
                    _informeDiario.d7_valor = "P";
                else if (ddlFecha.Text == "8")
                    _informeDiario.d8_valor = "P";
                else if (ddlFecha.Text == "9")
                    _informeDiario.d9_valor = "P";
                else if (ddlFecha.Text == "10")
                    _informeDiario.d10_valor = "P";
                else if (ddlFecha.Text == "11")
                    _informeDiario.d11_valor = "P";
                else if (ddlFecha.Text == "12")
                    _informeDiario.d12_valor = "P";
                else if (ddlFecha.Text == "13")
                    _informeDiario.d13_valor = "P";
                else if (ddlFecha.Text == "14")
                    _informeDiario.d14_valor = "P";
                else if (ddlFecha.Text == "15")
                    _informeDiario.d15_valor = "P";
                else if (ddlFecha.Text == "16")
                    _informeDiario.d16_valor = "P";
                else if (ddlFecha.Text == "17")
                    _informeDiario.d17_valor = "P";
                else if (ddlFecha.Text == "18")
                    _informeDiario.d18_valor = "P";
                else if (ddlFecha.Text == "19")
                    _informeDiario.d19_valor = "P";
                else if (ddlFecha.Text == "20")
                    _informeDiario.d20_valor = "P";
                else if (ddlFecha.Text == "21")
                    _informeDiario.d21_valor = "P";
                else if (ddlFecha.Text == "22")
                    _informeDiario.d22_valor = "P";
                else if (ddlFecha.Text == "23")
                    _informeDiario.d23_valor = "P";
                else if (ddlFecha.Text == "24")
                    _informeDiario.d24_valor = "P";
                else if (ddlFecha.Text == "25")
                    _informeDiario.d25_valor = "P";
                else if (ddlFecha.Text == "26")
                    _informeDiario.d26_valor = "P";
                else if (ddlFecha.Text == "27")
                    _informeDiario.d27_valor = "P";
                else if (ddlFecha.Text == "28")
                    _informeDiario.d28_valor = "P";
                else if (ddlFecha.Text == "29")
                    _informeDiario.d29_valor = "P";
                else if (ddlFecha.Text == "30")
                    _informeDiario.d30_valor = "P";
                else if (ddlFecha.Text == "31")
                    _informeDiario.d31_valor = "P";

                if (_informeDiario.Id > 0)
                {
                    engineInformeDiario.Update(_informeDiario);
                    Alert.Show("Atualizado com Sucesso");
                }
                else
                {
                    engineInformeDiario.Inserir(_informeDiario);
                    Alert.Show("Inserido com Sucesso");
                }

            
        }

        protected void btnSalvarComentario_Click(object sender, EventArgs e)
        {
            ////if (txtid.Text == string.Empty || txtid.Text == "0")
            ////{
            ////    Alert.Show("Salve o informe antes");
            ////    return;
            ////}

            //if (Session["informe_id"] == null)
            //{
            //    Alert.Show("Salve o informe antes");
            //    return;
            //}

            //if (Session["informe_id"] == string.Empty)
            //{
            //    Alert.Show("Salve o informe antes");
            //    return;
            //}

            //if (Session["informe_id"] != null)
            //{
            //    if (Session["informe_id"] != string.Empty)
            //    {
            //        if (Session["informesalvo"] == Session["informe_id"])
            //        {
            //            Alert.Show("Otro salvo antes");
            //            return;
            //        }
            //    }
            //}

            
            //cInformediario engineInforme = new cInformediario();
            //cClientes engineCliente = new cClientes();
            //mClientes _cliente = engineCliente.RetornarClientes(ddlCliente.SelectedValue);
            //mVendedor _vendedor = (mVendedor)Session["vendedor"];
            //string codigo = engineInforme.InserirOtro(_cliente.id.ToString(), txtOtro.Text, _vendedor.id.ToString(), Session["informe_id"].ToString());
            ////Alert.Show("Inserido otro de código:" + codigo);
            //TextBox2.Text = string.Empty;
            //txtOtro.Text = string.Empty;
            //Session["informesalvo"] = Session["informe_id"];
        }

        protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GerarDias();
            PreencherDiasDoMes();
        }

        protected void ddlMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GerarDias();
            PreencherDiasDoMes();
        }
        public void PreencherDiasDoMes()
        {
            cInformediario engineInformeDiario = new cInformediario();
            mInformediario _informeDiario = new mInformediario();
            cClientes engineCliente = new cClientes();
            mVendedor _vendedor = (mVendedor)Session["vendedor"];
            if (ddlCliente.SelectedValue != string.Empty)
            {
                mClientes _cliente = (ddlCliente.SelectedValue != null ? engineCliente.RetornarClientes(ddlCliente.SelectedValue.ToString()) : null);

                IList<mInformediario> lstInformeDiario = engineInformeDiario.RetornarInformediarioDia(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), _vendedor.id.ToString(), _cliente.id.ToString());
                txtid.Text = "0";



                if (lstInformeDiario != null)
                {
                    if (lstInformeDiario.Count > 0)
                    {
                        _informeDiario = lstInformeDiario[0];
                        txtid.Text = lstInformeDiario[0].Id.ToString();
                    }
                }


            }
        }

        protected void btnVistaPrevia_Click(object sender, EventArgs e)
        {
            Session["vistaPrevia"] = txtid.Text + "|" + TextBox3.Text;
            //string queryString = "vista_previa.aspx";
            Page P = HttpContext.Current.Handler as Page;
            ScriptManager.RegisterStartupScript(P, P.GetType(), "Wind", "window.open( 'vista_previa.aspx','newWin', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=800,height=600,left = 400,top = 200');", true);
            //string newWin = "window.open('" + queryString + "');";
            //ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }

        protected void ddlAnoVisita_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularGridGeralFiltro(ddlAno.Text, ddlMes.Text);
            PopularDiaSemana();
        }

        private void PopularDiaSemana()
        {
            cParametrosCartaGantt engineclavesgestioncartagantt = new cParametrosCartaGantt();
            DataSet ds = engineclavesgestioncartagantt.PopularGridAnoMes(ddlAno.Text, ddlMes.Text, ddlFecha.Text);
            lblDia.Text = string.Empty;
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblDiaVisita.Text = ds.Tables[0].Rows[0]["dia"].ToString();
            }
        }

        private void PopularGridGeralFiltro(string ano, string mes)
        {
            cParametrosCartaGantt engineclavesgestioncartagantt = new cParametrosCartaGantt();
            DataSet ds = engineclavesgestioncartagantt.PopularGridAnoMes(ano, mes);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                int dia=0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dia = Convert.ToInt32(dt.Rows[i]["fecha"].ToString());
                    if (dia < DateTime.Now.Day)
                        dt.Rows.Remove(dt.Rows[i]);
                }
                ddlFecha.DataSource = dt;
                ddlFecha.DataValueField = "fecha";
                ddlFecha.DataTextField = "fecha";
                ddlFecha.DataBind();
            }
        }

        protected void ddlDiaVisita_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopularDiaSemana();
        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnEnviarEmail_Click(object sender, EventArgs e)
        {
            //txtEmail.Text = string.Empty;
            txtComentario.Text = string.Empty;
        }
    }
}
