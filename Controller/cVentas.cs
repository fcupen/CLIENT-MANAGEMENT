﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cVentas
    {
        public mVentas RetornarVentas(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mVentas Ventas = new mVentas();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from metaventas where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAVentas.NOME
            Ventas.id = Convert.ToInt32(id);
            Ventas.vendedor = (ds.Tables[0].Rows[0]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor"].ToString()) : null);
            Ventas.produto = (ds.Tables[0].Rows[0]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["produto"].ToString()) : null);
            Ventas.Enero = ds.Tables[0].Rows[0]["Enero"].ToString();
            Ventas.Febrero = ds.Tables[0].Rows[0]["Febrero"].ToString();
            Ventas.Marzo = ds.Tables[0].Rows[0]["Marzo"].ToString();
            Ventas.Abril = ds.Tables[0].Rows[0]["Abril"].ToString();
            Ventas.Mayo = ds.Tables[0].Rows[0]["Mayo"].ToString();
            Ventas.Junio = ds.Tables[0].Rows[0]["Junio"].ToString();
            Ventas.Julio = ds.Tables[0].Rows[0]["Julio"].ToString();
            Ventas.Agosto = ds.Tables[0].Rows[0]["Agosto"].ToString();
            Ventas.Septiembre = ds.Tables[0].Rows[0]["Septiembre"].ToString();
            Ventas.Octubre = ds.Tables[0].Rows[0]["Octubre"].ToString();
            Ventas.Noviembre = ds.Tables[0].Rows[0]["Noviembre"].ToString();
            Ventas.Diciembre = ds.Tables[0].Rows[0]["Diciembre"].ToString();

            return Ventas;
        }

        public List<mVentas> RetornarVentas()
        {
            clsConexao banco = new clsConexao();
            mVentas Ventas = new mVentas();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            List<mVentas> lstVentas = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from metaventas ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstVentas = new List<mVentas>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    Ventas = new mVentas();
                    Ventas.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    Ventas.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    Ventas.produto = (ds.Tables[0].Rows[i]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["produto"].ToString()) : null);
                    Ventas.Enero = ds.Tables[0].Rows[i]["Enero"].ToString();
                    Ventas.Febrero = ds.Tables[0].Rows[i]["Febrero"].ToString();
                    Ventas.Marzo = ds.Tables[0].Rows[i]["Marzo"].ToString();
                    Ventas.Abril = ds.Tables[0].Rows[i]["Abril"].ToString();
                    Ventas.Mayo = ds.Tables[0].Rows[i]["Mayo"].ToString();
                    Ventas.Junio = ds.Tables[0].Rows[i]["Junio"].ToString();
                    Ventas.Julio = ds.Tables[0].Rows[i]["Julio"].ToString();
                    Ventas.Agosto = ds.Tables[0].Rows[i]["Agosto"].ToString();
                    Ventas.Septiembre = ds.Tables[0].Rows[i]["Septiembre"].ToString();
                    Ventas.Octubre = ds.Tables[0].Rows[i]["Octubre"].ToString();
                    Ventas.Noviembre = ds.Tables[0].Rows[i]["Noviembre"].ToString();
                    Ventas.Diciembre = ds.Tables[0].Rows[i]["Diciembre"].ToString();

                    lstVentas.Add(Ventas);
                }
            }

            return lstVentas;
        }

        public string Inserir(mVentas Ventas)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into metaventas("
                                                + "vendedor"
                                                + ",produto"
                                                + ",Enero"
                                                + ",Febrero"
                                                + ",Marzo"
                                                + ",Abril"
                                                + ",Mayo"
                                                + ",Junio"
                                                + ",Julio"
                                                + ",Agosto"
                                                + ",Septiembre"
                                                + ",Octubre"
                                                + ",Noviembre"
                                                + ",Diciembre"
                                                + ") values('"
                                                + (Ventas.vendedor != null ? Ventas.vendedor.id.ToString() : "0") + "'"
                                                + ",'" + (Ventas.produto != null ? Ventas.produto.Id.ToString() : "0") + "'"
                                                + ",'" + Ventas.Enero.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Febrero.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Marzo.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Abril.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Mayo.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Junio.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Julio.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Agosto.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Septiembre.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Octubre.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Noviembre.Replace(",", ".") + "'"
                                                + ",'" + Ventas.Diciembre.Replace(",", ".") + "'"
                                                + ")");

            return banco.Selecionar("select  * from metaventas order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mVentas Ventas)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update metaventas set "
                                                + " vendedor='" + (Ventas.vendedor != null ? Ventas.vendedor.id.ToString() : "0") + "'"
                                                + " ,produto='" + (Ventas.produto != null ? Ventas.produto.Id.ToString() : "0") + "'"
                                                + " ,Enero='" + Ventas.Enero.Replace(",",".") + "'"
                                                + " ,Febrero='" + Ventas.Febrero.Replace(",", ".") + "'"
                                                + " ,Marzo='" + Ventas.Marzo.Replace(",", ".") + "'"
                                                + " ,Abril='" + Ventas.Abril.Replace(",", ".") + "'"
                                                + " ,Mayo='" + Ventas.Mayo.Replace(",", ".") + "'"
                                                + " ,Junio='" + Ventas.Junio.Replace(",", ".") + "'"
                                                + " ,Julio='" + Ventas.Julio.Replace(",", ".") + "'"
                                                + " ,Agosto='" + Ventas.Agosto.Replace(",", ".") + "'"
                                                + " ,Septiembre='" + Ventas.Septiembre.Replace(",", ".") + "'"
                                                + " ,Octubre='" + Ventas.Octubre.Replace(",", ".") + "'"
                                                + " ,Noviembre='" + Ventas.Noviembre.Replace(",", ".") + "'"
                                                + " ,Diciembre='" + Ventas.Diciembre.Replace(",", ".") + "'"
                                                + " where id=" + Ventas.id.ToString());
        }
        public bool Deletar(mVentas Ventas)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from metaventas  "
                                                + " where id=" + Ventas.id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select mv.id"
                        +" ,v.nombre"
                        +" ,p.nombre"
                        +" ,mv.enero"
                        +" ,mv.Febrero"
                        +" ,mv.Marzo"
                        +" ,mv.Abril"
                        +" ,mv.Mayo"
                        +" ,mv.Junio"
                        +" ,mv.Julio"
                        +" ,mv.Agosto"
                        +" ,mv.Septiembre"
                        +" ,mv.Octubre"
                        +" ,mv.Noviembre"
                        +" ,mv.Diciembre"
                        +" from"
                        +" metaventas mv"
                        +" inner join produto p on mv.produto=p.id"
                        +" inner join vendedor v on mv.vendedor = v.id"
                         + " order by mv.id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select mv.id"
                        + " ,v.nombre"
                        + " ,p.nombre"
                        + " ,mv.enero"
                        + " ,mv.Febrero"
                        + " ,mv.Marzo"
                        + " ,mv.Abril"
                        + " ,mv.Mayo"
                        + " ,mv.Junio"
                        + " ,mv.Julio"
                        + " ,mv.Agosto"
                        + " ,mv.Septiembre"
                        + " ,mv.Octubre"
                        + " ,mv.Noviembre"
                        + " ,mv.Diciembre"
                        + " from"
                        + " metaventas mv"
                        + " inner join produto p on mv.produto=p.id"
                        + " inner join vendedor v on mv.vendedor = v.id"
                         + " order by mv.id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}