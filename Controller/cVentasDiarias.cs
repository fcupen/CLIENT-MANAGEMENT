﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cVentasDiarias
    {
        public mVentasDias VentasDias(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mVentasDias VentasDias = new mVentasDias();
            cVentasDiarias engineVentasDias = new cVentasDiarias();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from ventas_dias where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            VentasDias.id = Convert.ToInt32(id);
            VentasDias.ano = ds.Tables[0].Rows[0]["ano"].ToString();
            VentasDias.mes = ds.Tables[0].Rows[0]["mes"].ToString();
            VentasDias.vendedor = (ds.Tables[0].Rows[0]["vendedor_id"].ToString()!=string.Empty?engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor_id"].ToString()):null);
            VentasDias.produto = (ds.Tables[0].Rows[0]["grupo_dia"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["grupo_dia"].ToString()) : null);
            VentasDias.cliente = (ds.Tables[0].Rows[0]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[0]["cliente_id"].ToString()) : null);
            VentasDias.d1_valor = ds.Tables[0].Rows[0]["d1_valor"].ToString();
            VentasDias.d2_valor = ds.Tables[0].Rows[0]["d2_valor"].ToString();
            VentasDias.d3_valor = ds.Tables[0].Rows[0]["d3_valor"].ToString();
            VentasDias.d4_valor = ds.Tables[0].Rows[0]["d4_valor"].ToString();
            VentasDias.d5_valor = ds.Tables[0].Rows[0]["d5_valor"].ToString();
            VentasDias.d6_valor = ds.Tables[0].Rows[0]["d6_valor"].ToString();
            VentasDias.d7_valor = ds.Tables[0].Rows[0]["d7_valor"].ToString();
            VentasDias.d8_valor = ds.Tables[0].Rows[0]["d8_valor"].ToString();
            VentasDias.d9_valor = ds.Tables[0].Rows[0]["d9_valor"].ToString();
            VentasDias.d10_valor = ds.Tables[0].Rows[0]["d10_valor"].ToString();
            VentasDias.d11_valor = ds.Tables[0].Rows[0]["d11_valor"].ToString();
            VentasDias.d12_valor = ds.Tables[0].Rows[0]["d12_valor"].ToString();
            VentasDias.d13_valor = ds.Tables[0].Rows[0]["d13_valor"].ToString();
            VentasDias.d14_valor = ds.Tables[0].Rows[0]["d14_valor"].ToString();
            VentasDias.d15_valor = ds.Tables[0].Rows[0]["d15_valor"].ToString();
            VentasDias.d16_valor = ds.Tables[0].Rows[0]["d16_valor"].ToString();
            VentasDias.d17_valor = ds.Tables[0].Rows[0]["d17_valor"].ToString();
            VentasDias.d18_valor = ds.Tables[0].Rows[0]["d18_valor"].ToString();
            VentasDias.d19_valor = ds.Tables[0].Rows[0]["d19_valor"].ToString();
            VentasDias.d20_valor = ds.Tables[0].Rows[0]["d20_valor"].ToString();
            VentasDias.d21_valor = ds.Tables[0].Rows[0]["d21_valor"].ToString();
            VentasDias.d22_valor = ds.Tables[0].Rows[0]["d22_valor"].ToString();
            VentasDias.d23_valor = ds.Tables[0].Rows[0]["d23_valor"].ToString();
            VentasDias.d24_valor = ds.Tables[0].Rows[0]["d24_valor"].ToString();
            VentasDias.d25_valor = ds.Tables[0].Rows[0]["d25_valor"].ToString();
            VentasDias.d26_valor = ds.Tables[0].Rows[0]["d26_valor"].ToString();
            VentasDias.d27_valor = ds.Tables[0].Rows[0]["d27_valor"].ToString();
            VentasDias.d28_valor = ds.Tables[0].Rows[0]["d28_valor"].ToString();
            VentasDias.d29_valor = ds.Tables[0].Rows[0]["d29_valor"].ToString();
            VentasDias.d30_valor = ds.Tables[0].Rows[0]["d30_valor"].ToString();
            VentasDias.d31_valor = ds.Tables[0].Rows[0]["d31_valor"].ToString();
            return VentasDias;
        }

        public List<mVentasDias> VentasDias(int ano)
        {
            clsConexao banco = new clsConexao();
            mVentasDias VentasDias = new mVentasDias();
            cVentasDiarias engineVentasDias = new cVentasDiarias();
            List<mVentasDias> lstVentasDias = null;
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM ventas_dias where   ano='" + ano + "' ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstVentasDias = new List<mVentasDias>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    VentasDias = new mVentasDias();
                    VentasDias.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    VentasDias.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    VentasDias.mes = ds.Tables[0].Rows[i]["mes"].ToString();
                    VentasDias.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    VentasDias.produto = (ds.Tables[0].Rows[i]["grupo_dia"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["grupo_dia"].ToString()) : null);
                    VentasDias.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    VentasDias.d1_valor = ds.Tables[0].Rows[i]["d1_valor"].ToString();
                    VentasDias.d2_valor = ds.Tables[0].Rows[i]["d2_valor"].ToString();
                    VentasDias.d3_valor = ds.Tables[0].Rows[i]["d3_valor"].ToString();
                    VentasDias.d4_valor = ds.Tables[0].Rows[i]["d4_valor"].ToString();
                    VentasDias.d5_valor = ds.Tables[0].Rows[i]["d5_valor"].ToString();
                    VentasDias.d6_valor = ds.Tables[0].Rows[i]["d6_valor"].ToString();
                    VentasDias.d7_valor = ds.Tables[0].Rows[i]["d7_valor"].ToString();
                    VentasDias.d8_valor = ds.Tables[0].Rows[i]["d8_valor"].ToString();
                    VentasDias.d9_valor = ds.Tables[0].Rows[i]["d9_valor"].ToString();
                    VentasDias.d10_valor = ds.Tables[0].Rows[i]["d10_valor"].ToString();
                    VentasDias.d11_valor = ds.Tables[0].Rows[i]["d11_valor"].ToString();
                    VentasDias.d12_valor = ds.Tables[0].Rows[i]["d12_valor"].ToString();
                    VentasDias.d13_valor = ds.Tables[0].Rows[i]["d13_valor"].ToString();
                    VentasDias.d14_valor = ds.Tables[0].Rows[i]["d14_valor"].ToString();
                    VentasDias.d15_valor = ds.Tables[0].Rows[i]["d15_valor"].ToString();
                    VentasDias.d16_valor = ds.Tables[0].Rows[i]["d16_valor"].ToString();
                    VentasDias.d17_valor = ds.Tables[0].Rows[i]["d17_valor"].ToString();
                    VentasDias.d18_valor = ds.Tables[0].Rows[i]["d18_valor"].ToString();
                    VentasDias.d19_valor = ds.Tables[0].Rows[i]["d19_valor"].ToString();
                    VentasDias.d20_valor = ds.Tables[0].Rows[i]["d20_valor"].ToString();
                    VentasDias.d21_valor = ds.Tables[0].Rows[i]["d21_valor"].ToString();
                    VentasDias.d22_valor = ds.Tables[0].Rows[i]["d22_valor"].ToString();
                    VentasDias.d23_valor = ds.Tables[0].Rows[i]["d23_valor"].ToString();
                    VentasDias.d24_valor = ds.Tables[0].Rows[i]["d24_valor"].ToString();
                    VentasDias.d25_valor = ds.Tables[0].Rows[i]["d25_valor"].ToString();
                    VentasDias.d26_valor = ds.Tables[0].Rows[i]["d26_valor"].ToString();
                    VentasDias.d27_valor = ds.Tables[0].Rows[i]["d27_valor"].ToString();
                    VentasDias.d28_valor = ds.Tables[0].Rows[i]["d28_valor"].ToString();
                    VentasDias.d29_valor = ds.Tables[0].Rows[i]["d29_valor"].ToString();
                    VentasDias.d30_valor = ds.Tables[0].Rows[i]["d30_valor"].ToString();
                    VentasDias.d31_valor = ds.Tables[0].Rows[i]["d31_valor"].ToString();
                    lstVentasDias.Add(VentasDias);
                }
            }

            return lstVentasDias;
        }

        public List<mVentasDias> VentasDias(int ano,string mes, int vendedor_id,int produto,int cliente)
        {
            clsConexao banco = new clsConexao();
            mVentasDias VentasDias = new mVentasDias();
            cVentasDiarias engineVentasDias = new cVentasDiarias();
            List<mVentasDias> lstVentasDias = null;
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM ventas_dias where   ano='" + ano.ToString() + "' and mes='" + mes.ToString() + "' and vendedor_id=" + vendedor_id.ToString() + (produto > 0 ? " and grupo_dia=" + produto : "") + (cliente > 0 ? " and cliente_id=" + cliente : ""));

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstVentasDias = new List<mVentasDias>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    VentasDias = new mVentasDias();
                    VentasDias.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    VentasDias.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    VentasDias.mes = ds.Tables[0].Rows[i]["mes"].ToString();
                    VentasDias.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    VentasDias.produto = (ds.Tables[0].Rows[i]["grupo_dia"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["grupo_dia"].ToString()) : null);
                    VentasDias.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    VentasDias.d1_valor = ds.Tables[0].Rows[i]["d1_valor"].ToString();
                    VentasDias.d2_valor = ds.Tables[0].Rows[i]["d2_valor"].ToString();
                    VentasDias.d3_valor = ds.Tables[0].Rows[i]["d3_valor"].ToString();
                    VentasDias.d4_valor = ds.Tables[0].Rows[i]["d4_valor"].ToString();
                    VentasDias.d5_valor = ds.Tables[0].Rows[i]["d5_valor"].ToString();
                    VentasDias.d6_valor = ds.Tables[0].Rows[i]["d6_valor"].ToString();
                    VentasDias.d7_valor = ds.Tables[0].Rows[i]["d7_valor"].ToString();
                    VentasDias.d8_valor = ds.Tables[0].Rows[i]["d8_valor"].ToString();
                    VentasDias.d9_valor = ds.Tables[0].Rows[i]["d9_valor"].ToString();
                    VentasDias.d10_valor = ds.Tables[0].Rows[i]["d10_valor"].ToString();
                    VentasDias.d11_valor = ds.Tables[0].Rows[i]["d11_valor"].ToString();
                    VentasDias.d12_valor = ds.Tables[0].Rows[i]["d12_valor"].ToString();
                    VentasDias.d13_valor = ds.Tables[0].Rows[i]["d13_valor"].ToString();
                    VentasDias.d14_valor = ds.Tables[0].Rows[i]["d14_valor"].ToString();
                    VentasDias.d15_valor = ds.Tables[0].Rows[i]["d15_valor"].ToString();
                    VentasDias.d16_valor = ds.Tables[0].Rows[i]["d16_valor"].ToString();
                    VentasDias.d17_valor = ds.Tables[0].Rows[i]["d17_valor"].ToString();
                    VentasDias.d18_valor = ds.Tables[0].Rows[i]["d18_valor"].ToString();
                    VentasDias.d19_valor = ds.Tables[0].Rows[i]["d19_valor"].ToString();
                    VentasDias.d20_valor = ds.Tables[0].Rows[i]["d20_valor"].ToString();
                    VentasDias.d21_valor = ds.Tables[0].Rows[i]["d21_valor"].ToString();
                    VentasDias.d22_valor = ds.Tables[0].Rows[i]["d22_valor"].ToString();
                    VentasDias.d23_valor = ds.Tables[0].Rows[i]["d23_valor"].ToString();
                    VentasDias.d24_valor = ds.Tables[0].Rows[i]["d24_valor"].ToString();
                    VentasDias.d25_valor = ds.Tables[0].Rows[i]["d25_valor"].ToString();
                    VentasDias.d26_valor = ds.Tables[0].Rows[i]["d26_valor"].ToString();
                    VentasDias.d27_valor = ds.Tables[0].Rows[i]["d27_valor"].ToString();
                    VentasDias.d28_valor = ds.Tables[0].Rows[i]["d28_valor"].ToString();
                    VentasDias.d29_valor = ds.Tables[0].Rows[i]["d29_valor"].ToString();
                    VentasDias.d30_valor = ds.Tables[0].Rows[i]["d30_valor"].ToString();
                    VentasDias.d31_valor = ds.Tables[0].Rows[i]["d31_valor"].ToString();
                    lstVentasDias.Add(VentasDias);
                }
            }

            return lstVentasDias;
        }

        public List<mVentasDias> VentasDias(int ano, string mes, int vendedor_id, int produto)
        {
            clsConexao banco = new clsConexao();
            mVentasDias VentasDias = new mVentasDias();
            cVentasDiarias engineVentasDias = new cVentasDiarias();
            List<mVentasDias> lstVentasDias = null;
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM ventas_dias where   ano='" + ano.ToString() + "' and mes='" + mes.ToString() + "' and vendedor_id=" + vendedor_id.ToString() +  " and grupo_dia=" + produto );

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstVentasDias = new List<mVentasDias>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    VentasDias = new mVentasDias();
                    VentasDias.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    VentasDias.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    VentasDias.mes = ds.Tables[0].Rows[i]["mes"].ToString();
                    VentasDias.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    VentasDias.produto = (ds.Tables[0].Rows[i]["grupo_dia"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["grupo_dia"].ToString()) : null);
                    VentasDias.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    VentasDias.d1_valor = ds.Tables[0].Rows[i]["d1_valor"].ToString();
                    VentasDias.d2_valor = ds.Tables[0].Rows[i]["d2_valor"].ToString();
                    VentasDias.d3_valor = ds.Tables[0].Rows[i]["d3_valor"].ToString();
                    VentasDias.d4_valor = ds.Tables[0].Rows[i]["d4_valor"].ToString();
                    VentasDias.d5_valor = ds.Tables[0].Rows[i]["d5_valor"].ToString();
                    VentasDias.d6_valor = ds.Tables[0].Rows[i]["d6_valor"].ToString();
                    VentasDias.d7_valor = ds.Tables[0].Rows[i]["d7_valor"].ToString();
                    VentasDias.d8_valor = ds.Tables[0].Rows[i]["d8_valor"].ToString();
                    VentasDias.d9_valor = ds.Tables[0].Rows[i]["d9_valor"].ToString();
                    VentasDias.d10_valor = ds.Tables[0].Rows[i]["d10_valor"].ToString();
                    VentasDias.d11_valor = ds.Tables[0].Rows[i]["d11_valor"].ToString();
                    VentasDias.d12_valor = ds.Tables[0].Rows[i]["d12_valor"].ToString();
                    VentasDias.d13_valor = ds.Tables[0].Rows[i]["d13_valor"].ToString();
                    VentasDias.d14_valor = ds.Tables[0].Rows[i]["d14_valor"].ToString();
                    VentasDias.d15_valor = ds.Tables[0].Rows[i]["d15_valor"].ToString();
                    VentasDias.d16_valor = ds.Tables[0].Rows[i]["d16_valor"].ToString();
                    VentasDias.d17_valor = ds.Tables[0].Rows[i]["d17_valor"].ToString();
                    VentasDias.d18_valor = ds.Tables[0].Rows[i]["d18_valor"].ToString();
                    VentasDias.d19_valor = ds.Tables[0].Rows[i]["d19_valor"].ToString();
                    VentasDias.d20_valor = ds.Tables[0].Rows[i]["d20_valor"].ToString();
                    VentasDias.d21_valor = ds.Tables[0].Rows[i]["d21_valor"].ToString();
                    VentasDias.d22_valor = ds.Tables[0].Rows[i]["d22_valor"].ToString();
                    VentasDias.d23_valor = ds.Tables[0].Rows[i]["d23_valor"].ToString();
                    VentasDias.d24_valor = ds.Tables[0].Rows[i]["d24_valor"].ToString();
                    VentasDias.d25_valor = ds.Tables[0].Rows[i]["d25_valor"].ToString();
                    VentasDias.d26_valor = ds.Tables[0].Rows[i]["d26_valor"].ToString();
                    VentasDias.d27_valor = ds.Tables[0].Rows[i]["d27_valor"].ToString();
                    VentasDias.d28_valor = ds.Tables[0].Rows[i]["d28_valor"].ToString();
                    VentasDias.d29_valor = ds.Tables[0].Rows[i]["d29_valor"].ToString();
                    VentasDias.d30_valor = ds.Tables[0].Rows[i]["d30_valor"].ToString();
                    VentasDias.d31_valor = ds.Tables[0].Rows[i]["d31_valor"].ToString();
                    lstVentasDias.Add(VentasDias);
                }
            }

            return lstVentasDias;
        }

        public List<mVentasDias> VentasDias()
        {
            clsConexao banco = new clsConexao();
            mVentasDias VentasDias = new mVentasDias();
            cVentasDiarias engineVentasDias = new cVentasDiarias();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            List<mVentasDias> lstVentasDias = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from ventas_dias ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstVentasDias = new List<mVentasDias>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    VentasDias = new mVentasDias();
                    VentasDias.id =Convert.ToInt32( ds.Tables[0].Rows[i]["id"].ToString());
                    VentasDias.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    VentasDias.mes = ds.Tables[0].Rows[i]["mes"].ToString();
                    VentasDias.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    VentasDias.produto = (ds.Tables[0].Rows[i]["grupo_dia"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["grupo_dia"].ToString()) : null);
                    VentasDias.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    VentasDias.d1_valor = ds.Tables[0].Rows[i]["d1_valor"].ToString();
                    VentasDias.d2_valor = ds.Tables[0].Rows[i]["d2_valor"].ToString();
                    VentasDias.d3_valor = ds.Tables[0].Rows[i]["d3_valor"].ToString();
                    VentasDias.d4_valor = ds.Tables[0].Rows[i]["d4_valor"].ToString();
                    VentasDias.d5_valor = ds.Tables[0].Rows[i]["d5_valor"].ToString();
                    VentasDias.d6_valor = ds.Tables[0].Rows[i]["d6_valor"].ToString();
                    VentasDias.d7_valor = ds.Tables[0].Rows[i]["d7_valor"].ToString();
                    VentasDias.d8_valor = ds.Tables[0].Rows[i]["d8_valor"].ToString();
                    VentasDias.d9_valor = ds.Tables[0].Rows[i]["d9_valor"].ToString();
                    VentasDias.d10_valor = ds.Tables[0].Rows[i]["d10_valor"].ToString();
                    VentasDias.d11_valor = ds.Tables[0].Rows[i]["d11_valor"].ToString();
                    VentasDias.d12_valor = ds.Tables[0].Rows[i]["d12_valor"].ToString();
                    VentasDias.d13_valor = ds.Tables[0].Rows[i]["d13_valor"].ToString();
                    VentasDias.d14_valor = ds.Tables[0].Rows[i]["d14_valor"].ToString();
                    VentasDias.d15_valor = ds.Tables[0].Rows[i]["d15_valor"].ToString();
                    VentasDias.d16_valor = ds.Tables[0].Rows[i]["d16_valor"].ToString();
                    VentasDias.d17_valor = ds.Tables[0].Rows[i]["d17_valor"].ToString();
                    VentasDias.d18_valor = ds.Tables[0].Rows[i]["d18_valor"].ToString();
                    VentasDias.d19_valor = ds.Tables[0].Rows[i]["d19_valor"].ToString();
                    VentasDias.d20_valor = ds.Tables[0].Rows[i]["d20_valor"].ToString();
                    VentasDias.d21_valor = ds.Tables[0].Rows[i]["d21_valor"].ToString();
                    VentasDias.d22_valor = ds.Tables[0].Rows[i]["d22_valor"].ToString();
                    VentasDias.d23_valor = ds.Tables[0].Rows[i]["d23_valor"].ToString();
                    VentasDias.d24_valor = ds.Tables[0].Rows[i]["d24_valor"].ToString();
                    VentasDias.d25_valor = ds.Tables[0].Rows[i]["d25_valor"].ToString();
                    VentasDias.d26_valor = ds.Tables[0].Rows[i]["d26_valor"].ToString();
                    VentasDias.d27_valor = ds.Tables[0].Rows[i]["d27_valor"].ToString();
                    VentasDias.d28_valor = ds.Tables[0].Rows[i]["d28_valor"].ToString();
                    VentasDias.d29_valor = ds.Tables[0].Rows[i]["d29_valor"].ToString();
                    VentasDias.d30_valor = ds.Tables[0].Rows[i]["d30_valor"].ToString();
                    VentasDias.d31_valor = ds.Tables[0].Rows[i]["d31_valor"].ToString();
                    lstVentasDias.Add(VentasDias);
                }
            }

            return lstVentasDias;
        }

        public string Inserir(mVentasDias VentasDias)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into ventas_dias("
                                                + "ano"
                                                + ",mes"
                                                + ",vendedor_id"
                                                + ",cliente_id"
                                                + ",grupo_dia"
                                                + ",d1_valor"
                                                + ",d2_valor"
                                                + ",d3_valor"
                                                + ",d4_valor"
                                                + ",d5_valor"
                                                + ",d6_valor"
                                                + ",d7_valor"
                                                + ",d8_valor"
                                                + ",d9_valor"
                                                + ",d10_valor"
                                                + ",d11_valor"
                                                + ",d12_valor"
                                                + ",d13_valor"
                                                + ",d14_valor"
                                                + ",d15_valor"
                                                + ",d16_valor"
                                                + ",d17_valor"
                                                + ",d18_valor"
                                                + ",d19_valor"
                                                + ",d20_valor"
                                                + ",d21_valor"
                                                + ",d22_valor"
                                                + ",d23_valor"
                                                + ",d24_valor"
                                                + ",d25_valor"
                                                + ",d26_valor"
                                                + ",d27_valor"
                                                + ",d28_valor"
                                                + ",d29_valor"
                                                + ",d30_valor"
                                                + ",d31_valor"
                                                + ") values("
                                                + "'" + VentasDias.ano + "'"
                                                + ",'" + VentasDias.mes + "'"
                                                + ",'" + VentasDias.vendedor.id.ToString() + "'"
                                                + ",'" +(VentasDias.cliente!=null?  VentasDias.cliente.id.ToString():"0") + "'"
                                                + ",'" + (VentasDias.produto!=null? VentasDias.produto.Id.ToString():"0") + "'"
                                                + ",'" + VentasDias.d1_valor + "'"
                                                + ",'" + VentasDias.d2_valor + "'"
                                                + ",'" + VentasDias.d3_valor + "'"
                                                + ",'" + VentasDias.d4_valor + "'"
                                                + ",'" + VentasDias.d5_valor + "'"
                                                + ",'" + VentasDias.d6_valor + "'"
                                                + ",'" + VentasDias.d7_valor + "'"
                                                + ",'" + VentasDias.d8_valor + "'"
                                                + ",'" + VentasDias.d9_valor + "'"
                                                + ",'" + VentasDias.d10_valor + "'"
                                                + ",'" + VentasDias.d11_valor + "'"
                                                + ",'" + VentasDias.d12_valor + "'"
                                                + ",'" + VentasDias.d13_valor + "'"
                                                + ",'" + VentasDias.d14_valor + "'"
                                                + ",'" + VentasDias.d15_valor + "'"
                                                + ",'" + VentasDias.d16_valor + "'"
                                                + ",'" + VentasDias.d17_valor + "'"
                                                + ",'" + VentasDias.d18_valor + "'"
                                                + ",'" + VentasDias.d19_valor + "'"
                                                + ",'" + VentasDias.d20_valor + "'"
                                                + ",'" + VentasDias.d21_valor + "'"
                                                + ",'" + VentasDias.d22_valor + "'"
                                                + ",'" + VentasDias.d23_valor + "'"
                                                + ",'" + VentasDias.d24_valor + "'"
                                                + ",'" + VentasDias.d25_valor + "'"
                                                + ",'" + VentasDias.d26_valor + "'"
                                                + ",'" + VentasDias.d27_valor + "'"
                                                + ",'" + VentasDias.d28_valor + "'"
                                                + ",'" + VentasDias.d29_valor + "'"
                                                + ",'" + VentasDias.d30_valor + "'"
                                                + ",'" + VentasDias.d31_valor + "'"
                                                + ")");

            return banco.Selecionar("select  * from ventas_dias order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mVentasDias VentasDias)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update ventas_dias set "
                                                + " ano='" + VentasDias.ano + "'"
                                                + " ,mes='" + VentasDias.mes + "'"
                                                + " ,vendedor_id='" + VentasDias.vendedor.id.ToString() + "'"
                                                + " ,cliente_id='" + (VentasDias.cliente!=null? VentasDias.cliente.id.ToString():"0") + "'"
                                                + " ,grupo_dia='" + (VentasDias.produto!=null? VentasDias.produto.Id.ToString():"0") + "'"
                                                + " ,d1_valor='" + VentasDias.d1_valor + "'"
                                                + " ,d2_valor='" + VentasDias.d2_valor + "'"
                                                + " ,d3_valor='" + VentasDias.d3_valor + "'"
                                                + " ,d4_valor='" + VentasDias.d4_valor + "'"
                                                + " ,d5_valor='" + VentasDias.d5_valor + "'"
                                                + " ,d6_valor='" + VentasDias.d6_valor + "'"
                                                + " ,d7_valor='" + VentasDias.d7_valor + "'"
                                                + " ,d8_valor='" + VentasDias.d8_valor + "'"
                                                + " ,d9_valor='" + VentasDias.d9_valor + "'"
                                                + " ,d10_valor='" + VentasDias.d10_valor + "'"
                                                + " ,d11_valor='" + VentasDias.d11_valor + "'"
                                                + " ,d12_valor='" + VentasDias.d12_valor + "'"
                                                + " ,d13_valor='" + VentasDias.d13_valor + "'"
                                                + " ,d14_valor='" + VentasDias.d14_valor + "'"
                                                + " ,d15_valor='" + VentasDias.d15_valor + "'"
                                                + " ,d16_valor='" + VentasDias.d16_valor + "'"
                                                + " ,d17_valor='" + VentasDias.d17_valor + "'"
                                                + " ,d18_valor='" + VentasDias.d18_valor + "'"
                                                + " ,d19_valor='" + VentasDias.d19_valor + "'"
                                                + " ,d20_valor='" + VentasDias.d20_valor + "'"
                                                + " ,d21_valor='" + VentasDias.d21_valor + "'"
                                                + " ,d22_valor='" + VentasDias.d22_valor + "'"
                                                + " ,d23_valor='" + VentasDias.d23_valor + "'"
                                                + " ,d24_valor='" + VentasDias.d24_valor + "'"
                                                + " ,d25_valor='" + VentasDias.d25_valor + "'"
                                                + " ,d26_valor='" + VentasDias.d26_valor + "'"
                                                + " ,d27_valor='" + VentasDias.d27_valor + "'"
                                                + " ,d28_valor='" + VentasDias.d28_valor + "'"
                                                + " ,d29_valor='" + VentasDias.d29_valor + "'"
                                                + " ,d30_valor='" + VentasDias.d30_valor + "'"
                                                + " ,d31_valor='" + VentasDias.d31_valor + "'"
                                                + " where id=" + VentasDias.id.ToString());
        }
        
        public bool Deletar(mVentasDias VentasDias)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from ventas_dias  "
                                                + " where id=" + VentasDias.id.ToString());
        }

        public DataSet PopularGrid(string  pOpcao,string mes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            _sql = "select"
                   + " v.id"
                   + " ,vd.nombre as Vendedor"
                   + " ,p.nombre as Producto"
                   + " ,c.razonsocial as 'Razon Social' "
                   + " ,v.ano"
                   + " ,v.mes"
                   //+ " ,sum("
                   //+ " v.d1_valor "
                   //+ " + v.d2_valor  "
                   //+ " + v.d3_valor  "
                   //+ " + v.d4_valor  "
                   //+ " + v.d5_valor  "
                   //+ " + v.d6_valor  "
                   //+ " + v.d7_valor  "
                   //+ " + v.d8_valor  "
                   //+ " + v.d9_valor  "
                   //+ " + v.d10_valor "
                   //+ " + v.d11_valor "
                   //+ " + v.d12_valor  "
                   //+ " + v.d13_valor  "
                   //+ " + v.d14_valor  "
                   //+ " + v.d15_valor  "
                   //+ " + v.d16_valor  "
                   //+ " + v.d17_valor  "
                   //+ " + v.d18_valor  "
                   //+ " + v.d19_valor  "
                   //+ " + v.d20_valor "
                   //+ " + v.d21_valor "
                   //+ " + v.d22_valor  "
                   //+ " + v.d23_valor  "
                   //+ " + v.d24_valor  "
                   //+ " + v.d25_valor  "
                   //+ " + v.d26_valor  "
                   //+ " + v.d27_valor  "
                   //+ " + v.d28_valor  "
                   //+ " + v.d29_valor  "
                   //+ " + v.d30_valor "
                   //+ " + v.d31_valor) as Total "
                   + " ,v.d1_valor as '1'"
                   + " ,v.d2_valor as '2'"
                   + " ,v.d3_valor as '3'"
                   + " ,v.d4_valor as '4'"
                   + " ,v.d5_valor as '5'"
                   + " ,v.d6_valor as '6'"
                   + " ,v.d7_valor as '7'"
                   + " ,v.d8_valor as '8'"
                   + " ,v.d9_valor as '9'"
                   + " ,v.d10_valor as '10'"
                   + " ,v.d11_valor as '11'"
                   + " ,v.d12_valor as '12'"
                   + " ,v.d13_valor as '13'"
                   + " ,v.d14_valor as '14'"
                   + " ,v.d15_valor as '15'"
                   + " ,v.d16_valor as '16'"
                   + " ,v.d17_valor as '17'"
                   + " ,v.d18_valor as '18'"
                   + " ,v.d19_valor as '19'"
                   + " ,v.d20_valor as '20'"
                   + " ,v.d21_valor as '21'"
                   + " ,v.d22_valor as '22'"
                   + " ,v.d23_valor as '23'"
                   + " ,v.d24_valor as '24'"
                   + " ,v.d25_valor as '25'"
                   + " ,v.d26_valor as '26'"
                   + " ,v.d27_valor as '27'"
                   + " ,v.d28_valor as '28'"
                   + " ,v.d29_valor as '29'"
                   + " ,v.d30_valor as '30'"
                   + " ,v.d31_valor as '31'"
                   + " from ventas_dias v "
                   + " inner join vendedor vd on v.vendedor_id = vd.id "
                   + " left join produto p on v.grupo_dia = p.id "
                   + " left join clientes c on v.cliente_id = c.id "
                   ;

            _sql += " where v.mes='" + mesescrito + "'";
                       if (pOpcao!="0")
                           _sql += " and vd.id="+pOpcao;

                       _sql += " order by v.mes ";
            
            

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridVendedorProdutoMes(string pVendedor,string pProduto, string pMes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select"
                   + " v.id"
                   + " ,vd.nombre as Vendedor"
                   + " ,p.nombre as Producto"
                   + " ,v.ano"
                   + " ,v.mes"
                //+ " ,sum("
                //+ " v.d1_valor "
                //+ " + v.d2_valor  "
                //+ " + v.d3_valor  "
                //+ " + v.d4_valor  "
                //+ " + v.d5_valor  "
                //+ " + v.d6_valor  "
                //+ " + v.d7_valor  "
                //+ " + v.d8_valor  "
                //+ " + v.d9_valor  "
                //+ " + v.d10_valor "
                //+ " + v.d11_valor "
                //+ " + v.d12_valor  "
                //+ " + v.d13_valor  "
                //+ " + v.d14_valor  "
                //+ " + v.d15_valor  "
                //+ " + v.d16_valor  "
                //+ " + v.d17_valor  "
                //+ " + v.d18_valor  "
                //+ " + v.d19_valor  "
                //+ " + v.d20_valor "
                //+ " + v.d21_valor "
                //+ " + v.d22_valor  "
                //+ " + v.d23_valor  "
                //+ " + v.d24_valor  "
                //+ " + v.d25_valor  "
                //+ " + v.d26_valor  "
                //+ " + v.d27_valor  "
                //+ " + v.d28_valor  "
                //+ " + v.d29_valor  "
                //+ " + v.d30_valor "
                //+ " + v.d31_valor) as Total "
                   + " ,v.d1_valor as '1'"
                   + " ,v.d2_valor as '2'"
                   + " ,v.d3_valor as '3'"
                   + " ,v.d4_valor as '4'"
                   + " ,v.d5_valor as '5'"
                   + " ,v.d6_valor as '6'"
                   + " ,v.d7_valor as '7'"
                   + " ,v.d8_valor as '8'"
                   + " ,v.d9_valor as '9'"
                   + " ,v.d10_valor as '10'"
                   + " ,v.d11_valor as '11'"
                   + " ,v.d12_valor as '12'"
                   + " ,v.d13_valor as '13'"
                   + " ,v.d14_valor as '14'"
                   + " ,v.d15_valor as '15'"
                   + " ,v.d16_valor as '16'"
                   + " ,v.d17_valor as '17'"
                   + " ,v.d18_valor as '18'"
                   + " ,v.d19_valor as '19'"
                   + " ,v.d20_valor as '20'"
                   + " ,v.d21_valor as '21'"
                   + " ,v.d22_valor as '22'"
                   + " ,v.d23_valor as '23'"
                   + " ,v.d24_valor as '24'"
                   + " ,v.d25_valor as '25'"
                   + " ,v.d26_valor as '26'"
                   + " ,v.d27_valor as '27'"
                   + " ,v.d28_valor as '28'"
                   + " ,v.d29_valor as '29'"
                   + " ,v.d30_valor as '30'"
                   + " ,v.d31_valor as '31'"
                   + " from ventas_dias v "
                   + " inner join vendedor vd on v.vendedor_id = vd.id "
                   + " inner join produto p on v.grupo_dia = p.id "
                   ;

            _sql += " where v.mes='" + pMes + "'";
            _sql += " and vd.id=" + pVendedor;
            _sql += " and p.id=" + pProduto;

            _sql += " order by v.mes ";



            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }


        public DataSet PopularGridVendedorProdutoAno(string pVendedor, string pProduto)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select"
                   + " v.id"
                   + " ,vd.nombre as Vendedor"
                   + " ,p.nombre as Producto"
                   + " ,v.ano"
                   + " ,v.mes"
                //+ " ,sum("
                //+ " v.d1_valor "
                //+ " + v.d2_valor  "
                //+ " + v.d3_valor  "
                //+ " + v.d4_valor  "
                //+ " + v.d5_valor  "
                //+ " + v.d6_valor  "
                //+ " + v.d7_valor  "
                //+ " + v.d8_valor  "
                //+ " + v.d9_valor  "
                //+ " + v.d10_valor "
                //+ " + v.d11_valor "
                //+ " + v.d12_valor  "
                //+ " + v.d13_valor  "
                //+ " + v.d14_valor  "
                //+ " + v.d15_valor  "
                //+ " + v.d16_valor  "
                //+ " + v.d17_valor  "
                //+ " + v.d18_valor  "
                //+ " + v.d19_valor  "
                //+ " + v.d20_valor "
                //+ " + v.d21_valor "
                //+ " + v.d22_valor  "
                //+ " + v.d23_valor  "
                //+ " + v.d24_valor  "
                //+ " + v.d25_valor  "
                //+ " + v.d26_valor  "
                //+ " + v.d27_valor  "
                //+ " + v.d28_valor  "
                //+ " + v.d29_valor  "
                //+ " + v.d30_valor "
                //+ " + v.d31_valor) as Total "
                   + " ,sum(v.d1_valor) as '1'"
                   + " ,sum(v.d2_valor) as '2'"
                   + " ,sum(v.d3_valor) as '3'"
                   + " ,sum(v.d4_valor) as '4'"
                   + " ,sum(v.d5_valor) as '5'"
                   + " ,sum(v.d6_valor) as '6'"
                   + " ,sum(v.d7_valor) as '7'"
                   + " ,sum(v.d8_valor) as '8'"
                   + " ,sum(v.d9_valor) as '9'"
                   + " ,sum(v.d10_valor) as '10'"
                   + " ,sum(v.d11_valor) as '11'"
                   + " ,sum(v.d12_valor) as '12'"
                   + " ,sum(v.d13_valor) as '13'"
                   + " ,sum(v.d14_valor) as '14'"
                   + " ,sum(v.d15_valor) as '15'"
                   + " ,sum(v.d16_valor) as '16'"
                   + " ,sum(v.d17_valor) as '17'"
                   + " ,sum(v.d18_valor) as '18'"
                   + " ,sum(v.d19_valor) as '19'"
                   + " ,sum(v.d20_valor) as '20'"
                   + " ,sum(v.d21_valor) as '21'"
                   + " ,sum(v.d22_valor) as '22'"
                   + " ,sum(v.d23_valor) as '23'"
                   + " ,sum(v.d24_valor) as '24'"
                   + " ,sum(v.d25_valor) as '25'"
                   + " ,sum(v.d26_valor) as '26'"
                   + " ,sum(v.d27_valor) as '27'"
                   + " ,sum(v.d28_valor) as '28'"
                   + " ,sum(v.d29_valor) as '29'"
                   + " ,sum(v.d30_valor) as '30'"
                   + " ,sum(v.d31_valor) as '31'"
                   + " from ventas_dias v "
                   + " inner join vendedor vd on v.vendedor_id = vd.id "
                   + " inner join produto p on v.grupo_dia = p.id "
                   ;

            _sql += " where vd.id=" + pVendedor;
            _sql += " and p.id=" + pProduto;

            _sql += " order by v.mes ";



            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        public DataSet PopularGridVendedorProduto(string pVendedor, string pProduto)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select"
                   + " v.id"
                   + " ,vd.nombre as Vendedor"
                   + " ,p.nombre as Producto"
                   + " ,v.ano"
                   + " ,v.mes"                
                   + " ,v.d1_valor as '1'"
                   + " ,v.d2_valor as '2'"
                   + " ,v.d3_valor as '3'"
                   + " ,v.d4_valor as '4'"
                   + " ,v.d5_valor as '5'"
                   + " ,v.d6_valor as '6'"
                   + " ,v.d7_valor as '7'"
                   + " ,v.d8_valor as '8'"
                   + " ,v.d9_valor as '9'"
                   + " ,v.d10_valor as '10'"
                   + " ,v.d11_valor as '11'"
                   + " ,v.d12_valor as '12'"
                   + " ,v.d13_valor as '13'"
                   + " ,v.d14_valor as '14'"
                   + " ,v.d15_valor as '15'"
                   + " ,v.d16_valor as '16'"
                   + " ,v.d17_valor as '17'"
                   + " ,v.d18_valor as '18'"
                   + " ,v.d19_valor as '19'"
                   + " ,v.d20_valor as '20'"
                   + " ,v.d21_valor as '21'"
                   + " ,v.d22_valor as '22'"
                   + " ,v.d23_valor as '23'"
                   + " ,v.d24_valor as '24'"
                   + " ,v.d25_valor as '25'"
                   + " ,v.d26_valor as '26'"
                   + " ,v.d27_valor as '27'"
                   + " ,v.d28_valor as '28'"
                   + " ,v.d29_valor as '29'"
                   + " ,v.d30_valor as '30'"
                   + " ,v.d31_valor as '31'"
                   + " from ventas_dias v "
                   + " inner join vendedor vd on v.vendedor_id = vd.id "
                   + " inner join produto p on v.grupo_dia = p.id "
                   ;

            _sql += " where vd.id='" + pVendedor + "'";
            _sql += " and p.id=" + pProduto;

            _sql += " order by v.mes ";



            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridVendedorProduto(string pVendedor, string pProduto,string ano,string mes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            _sql = "select"
                   + " v.id"
                   + " ,vd.nombre as Vendedor"
                   + " ,p.nombre as Producto"
                   + " ,v.ano"
                   + " ,v.mes"
                   + " ,v.d1_valor as '1'"
                   + " ,v.d2_valor as '2'"
                   + " ,v.d3_valor as '3'"
                   + " ,v.d4_valor as '4'"
                   + " ,v.d5_valor as '5'"
                   + " ,v.d6_valor as '6'"
                   + " ,v.d7_valor as '7'"
                   + " ,v.d8_valor as '8'"
                   + " ,v.d9_valor as '9'"
                   + " ,v.d10_valor as '10'"
                   + " ,v.d11_valor as '11'"
                   + " ,v.d12_valor as '12'"
                   + " ,v.d13_valor as '13'"
                   + " ,v.d14_valor as '14'"
                   + " ,v.d15_valor as '15'"
                   + " ,v.d16_valor as '16'"
                   + " ,v.d17_valor as '17'"
                   + " ,v.d18_valor as '18'"
                   + " ,v.d19_valor as '19'"
                   + " ,v.d20_valor as '20'"
                   + " ,v.d21_valor as '21'"
                   + " ,v.d22_valor as '22'"
                   + " ,v.d23_valor as '23'"
                   + " ,v.d24_valor as '24'"
                   + " ,v.d25_valor as '25'"
                   + " ,v.d26_valor as '26'"
                   + " ,v.d27_valor as '27'"
                   + " ,v.d28_valor as '28'"
                   + " ,v.d29_valor as '29'"
                   + " ,v.d30_valor as '30'"
                   + " ,v.d31_valor as '31'"
                   + " from ventas_dias v "
                   + " inner join vendedor vd on v.vendedor_id = vd.id "
                   + " inner join produto p on v.grupo_dia = p.id "
                   + " where ano='" + ano + "'"
                   + " and mes='" + mesescrito+ "'"
                   ;

            _sql += " and vd.id='" + pVendedor + "'";
            _sql += " and p.id=" + pProduto;

            _sql += " order by v.mes ";



            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        public DataSet PopularGrid(string pOpcao)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select"
                   + " v.id"
                   + " ,vd.nombre as Vendedor"
                   + " ,p.nombre as Producto"
                   + " ,v.ano"
                   + " ,v.mes"
                //+ " ,sum("
                //+ " v.d1_valor "
                //+ " + v.d2_valor  "
                //+ " + v.d3_valor  "
                //+ " + v.d4_valor  "
                //+ " + v.d5_valor  "
                //+ " + v.d6_valor  "
                //+ " + v.d7_valor  "
                //+ " + v.d8_valor  "
                //+ " + v.d9_valor  "
                //+ " + v.d10_valor "
                //+ " + v.d11_valor "
                //+ " + v.d12_valor  "
                //+ " + v.d13_valor  "
                //+ " + v.d14_valor  "
                //+ " + v.d15_valor  "
                //+ " + v.d16_valor  "
                //+ " + v.d17_valor  "
                //+ " + v.d18_valor  "
                //+ " + v.d19_valor  "
                //+ " + v.d20_valor "
                //+ " + v.d21_valor "
                //+ " + v.d22_valor  "
                //+ " + v.d23_valor  "
                //+ " + v.d24_valor  "
                //+ " + v.d25_valor  "
                //+ " + v.d26_valor  "
                //+ " + v.d27_valor  "
                //+ " + v.d28_valor  "
                //+ " + v.d29_valor  "
                //+ " + v.d30_valor "
                //+ " + v.d31_valor) as Total "
                   + " ,v.d1_valor as '1'"
                   + " ,v.d2_valor as '2'"
                   + " ,v.d3_valor as '3'"
                   + " ,v.d4_valor as '4'"
                   + " ,v.d5_valor as '5'"
                   + " ,v.d6_valor as '6'"
                   + " ,v.d7_valor as '7'"
                   + " ,v.d8_valor as '8'"
                   + " ,v.d9_valor as '9'"
                   + " ,v.d10_valor as '10'"
                   + " ,v.d11_valor as '11'"
                   + " ,v.d12_valor as '12'"
                   + " ,v.d13_valor as '13'"
                   + " ,v.d14_valor as '14'"
                   + " ,v.d15_valor as '15'"
                   + " ,v.d16_valor as '16'"
                   + " ,v.d17_valor as '17'"
                   + " ,v.d18_valor as '18'"
                   + " ,v.d19_valor as '19'"
                   + " ,v.d20_valor as '20'"
                   + " ,v.d21_valor as '21'"
                   + " ,v.d22_valor as '22'"
                   + " ,v.d23_valor as '23'"
                   + " ,v.d24_valor as '24'"
                   + " ,v.d25_valor as '25'"
                   + " ,v.d26_valor as '26'"
                   + " ,v.d27_valor as '27'"
                   + " ,v.d28_valor as '28'"
                   + " ,v.d29_valor as '29'"
                   + " ,v.d30_valor as '30'"
                   + " ,v.d31_valor as '31'"
                   + " from ventas_dias v "
                   + " inner join vendedor vd on v.vendedor_id = vd.id "
                   + " inner join produto p on v.grupo_dia = p.id "
                   ;

            
            if (pOpcao != "0")
                _sql += " where vd.id=" + pOpcao;

            _sql += " order by v.mes ";



            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        public DataSet PopularGrid(string pOpcao,int idProduto)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select"
                   + " v.id"
                   + " ,vd.nombre as Vendedor"
                   + " ,p.nombre as Producto"
                   + " ,v.ano"
                   + " ,v.mes"
                   //+ " ,sum("
                   //+ " v.d1_valor "
                   //+ " + v.d2_valor  "
                   //+ " + v.d3_valor  "
                   //+ " + v.d4_valor  "
                   //+ " + v.d5_valor  "
                   //+ " + v.d6_valor  "
                   //+ " + v.d7_valor  "
                   //+ " + v.d8_valor  "
                   //+ " + v.d9_valor  "
                   //+ " + v.d10_valor "
                   //+ " + v.d11_valor "
                   //+ " + v.d12_valor  "
                   //+ " + v.d13_valor  "
                   //+ " + v.d14_valor  "
                   //+ " + v.d15_valor  "
                   //+ " + v.d16_valor  "
                   //+ " + v.d17_valor  "
                   //+ " + v.d18_valor  "
                   //+ " + v.d19_valor  "
                   //+ " + v.d20_valor "
                   //+ " + v.d21_valor "
                   //+ " + v.d22_valor  "
                   //+ " + v.d23_valor  "
                   //+ " + v.d24_valor  "
                   //+ " + v.d25_valor  "
                   //+ " + v.d26_valor  "
                   //+ " + v.d27_valor  "
                   //+ " + v.d28_valor  "
                   //+ " + v.d29_valor  "
                   //+ " + v.d30_valor "
                   //+ " + v.d31_valor) as Total "
                    + " ,v.d1_valor as '1'"
                   + " ,v.d2_valor as '2'"
                   + " ,v.d3_valor as '3'"
                   + " ,v.d4_valor as '4'"
                   + " ,v.d5_valor as '5'"
                   + " ,v.d6_valor as '6'"
                   + " ,v.d7_valor as '7'"
                   + " ,v.d8_valor as '8'"
                   + " ,v.d9_valor as '9'"
                   + " ,v.d10_valor as '10'"
                   + " ,v.d11_valor as '11'"
                   + " ,v.d12_valor as '12'"
                   + " ,v.d13_valor as '13'"
                   + " ,v.d14_valor as '14'"
                   + " ,v.d15_valor as '15'"
                   + " ,v.d16_valor as '16'"
                   + " ,v.d17_valor as '17'"
                   + " ,v.d18_valor as '18'"
                   + " ,v.d19_valor as '19'"
                   + " ,v.d20_valor as '20'"
                   + " ,v.d21_valor as '21'"
                   + " ,v.d22_valor as '22'"
                   + " ,v.d23_valor as '23'"
                   + " ,v.d24_valor as '24'"
                   + " ,v.d25_valor as '25'"
                   + " ,v.d26_valor as '26'"
                   + " ,v.d27_valor as '27'"
                   + " ,v.d28_valor as '28'"
                   + " ,v.d29_valor as '29'"
                   + " ,v.d30_valor as '30'"
                   + " ,v.d31_valor as '31'"
                   + " from ventas_dias v "
                   + " inner join vendedor vd on v.vendedor_id = vd.id "
                   + " inner join produto p on v.grupo_dia = p.id "
                   ;

            _sql += " where vd.id=" + pOpcao + " and v.grupo_dia=" + idProduto;

            _sql += " order by v.mes ";



            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGrid(string pOpcao, int idProduto,string mes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select"
                   + " v.id"
                   + " ,vd.nombre as Vendedor"
                   + " ,p.nombre as Producto"
                   + " ,v.ano"
                   + " ,v.mes"
                //+ " ,sum("
                //+ " v.d1_valor "
                //+ " + v.d2_valor  "
                //+ " + v.d3_valor  "
                //+ " + v.d4_valor  "
                //+ " + v.d5_valor  "
                //+ " + v.d6_valor  "
                //+ " + v.d7_valor  "
                //+ " + v.d8_valor  "
                //+ " + v.d9_valor  "
                //+ " + v.d10_valor "
                //+ " + v.d11_valor "
                //+ " + v.d12_valor  "
                //+ " + v.d13_valor  "
                //+ " + v.d14_valor  "
                //+ " + v.d15_valor  "
                //+ " + v.d16_valor  "
                //+ " + v.d17_valor  "
                //+ " + v.d18_valor  "
                //+ " + v.d19_valor  "
                //+ " + v.d20_valor "
                //+ " + v.d21_valor "
                //+ " + v.d22_valor  "
                //+ " + v.d23_valor  "
                //+ " + v.d24_valor  "
                //+ " + v.d25_valor  "
                //+ " + v.d26_valor  "
                //+ " + v.d27_valor  "
                //+ " + v.d28_valor  "
                //+ " + v.d29_valor  "
                //+ " + v.d30_valor "
                //+ " + v.d31_valor) as Total "
                    + " ,v.d1_valor as '1'"
                   + " ,v.d2_valor as '2'"
                   + " ,v.d3_valor as '3'"
                   + " ,v.d4_valor as '4'"
                   + " ,v.d5_valor as '5'"
                   + " ,v.d6_valor as '6'"
                   + " ,v.d7_valor as '7'"
                   + " ,v.d8_valor as '8'"
                   + " ,v.d9_valor as '9'"
                   + " ,v.d10_valor as '10'"
                   + " ,v.d11_valor as '11'"
                   + " ,v.d12_valor as '12'"
                   + " ,v.d13_valor as '13'"
                   + " ,v.d14_valor as '14'"
                   + " ,v.d15_valor as '15'"
                   + " ,v.d16_valor as '16'"
                   + " ,v.d17_valor as '17'"
                   + " ,v.d18_valor as '18'"
                   + " ,v.d19_valor as '19'"
                   + " ,v.d20_valor as '20'"
                   + " ,v.d21_valor as '21'"
                   + " ,v.d22_valor as '22'"
                   + " ,v.d23_valor as '23'"
                   + " ,v.d24_valor as '24'"
                   + " ,v.d25_valor as '25'"
                   + " ,v.d26_valor as '26'"
                   + " ,v.d27_valor as '27'"
                   + " ,v.d28_valor as '28'"
                   + " ,v.d29_valor as '29'"
                   + " ,v.d30_valor as '30'"
                   + " ,v.d31_valor as '31'"
                   + " from ventas_dias v "
                   + " inner join vendedor vd on v.vendedor_id = vd.id "
                   + " inner join produto p on v.grupo_dia = p.id "
                   ;

            _sql += " where vd.id=" + pOpcao + " and v.grupo_dia=" + idProduto;
            _sql += " and v.mes='" + mes+ "'";

            _sql += " order by v.mes ";



            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }      
    }
}