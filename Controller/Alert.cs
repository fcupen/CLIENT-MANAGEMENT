﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Clients_Management.Controller
{
    public static class Alert
    {
        /// <summary>
        /// Shows a client-side JavaScript alert in the browser.
        /// </summary>
        /// <param name="message">The message to appear in the alert.</param>
        public static void Show(string message)
        {
            Page P = HttpContext.Current.Handler as Page;
            ScriptManager.RegisterStartupScript(P, P.GetType(), "Msg", "alert('" + message + "');", true);
        }
    }
}