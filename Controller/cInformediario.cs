﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cInformediario
    {
        public mInformediario RetornarInformediario(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mInformediario Informediario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cVendedor engineVendedor = new cVendedor();
            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from informediario where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAInformediario.NOME
            Informediario.Id = Convert.ToInt32(id);            
            Informediario.calendario = (ds.Tables[0].Rows[0]["calendario"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[0]["calendario"].ToString()) : Convert.ToDateTime("1900-01-01"));
            Informediario.cliente = (ds.Tables[0].Rows[0]["cliente_id"].ToString()!=string.Empty?engineCliente.RetornarClientes(ds.Tables[0].Rows[0]["cliente_id"].ToString()):null);
            Informediario.vendedor = (ds.Tables[0].Rows[0]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor_id"].ToString()) : null);
            Informediario.contacto = ds.Tables[0].Rows[0]["contacto"].ToString();
            Informediario.formacontacto = ds.Tables[0].Rows[0]["formacontacto"].ToString();
            Informediario.formacontacto2 = ds.Tables[0].Rows[0]["formacontacto2"].ToString();
            Informediario.acuerdos = ds.Tables[0].Rows[0]["acuerdos"].ToString();
            Informediario.d1_valor = ds.Tables[0].Rows[0]["d1_valor"].ToString();
            Informediario.d2_valor = ds.Tables[0].Rows[0]["d2_valor"].ToString();
            Informediario.d3_valor = ds.Tables[0].Rows[0]["d3_valor"].ToString();
            Informediario.d4_valor = ds.Tables[0].Rows[0]["d4_valor"].ToString();
            Informediario.d5_valor = ds.Tables[0].Rows[0]["d5_valor"].ToString();
            Informediario.d6_valor = ds.Tables[0].Rows[0]["d6_valor"].ToString();
            Informediario.d7_valor = ds.Tables[0].Rows[0]["d7_valor"].ToString();
            Informediario.d8_valor = ds.Tables[0].Rows[0]["d8_valor"].ToString();
            Informediario.d9_valor = ds.Tables[0].Rows[0]["d9_valor"].ToString();
            Informediario.d10_valor = ds.Tables[0].Rows[0]["d10_valor"].ToString();
            Informediario.d11_valor = ds.Tables[0].Rows[0]["d11_valor"].ToString();
            Informediario.d12_valor = ds.Tables[0].Rows[0]["d12_valor"].ToString();
            Informediario.d13_valor = ds.Tables[0].Rows[0]["d13_valor"].ToString();
            Informediario.d14_valor = ds.Tables[0].Rows[0]["d14_valor"].ToString();
            Informediario.d15_valor = ds.Tables[0].Rows[0]["d15_valor"].ToString();
            Informediario.d16_valor = ds.Tables[0].Rows[0]["d16_valor"].ToString();
            Informediario.d17_valor = ds.Tables[0].Rows[0]["d17_valor"].ToString();
            Informediario.d18_valor = ds.Tables[0].Rows[0]["d18_valor"].ToString();
            Informediario.d19_valor = ds.Tables[0].Rows[0]["d19_valor"].ToString();
            Informediario.d20_valor = ds.Tables[0].Rows[0]["d20_valor"].ToString();
            Informediario.d21_valor = ds.Tables[0].Rows[0]["d21_valor"].ToString();
            Informediario.d22_valor = ds.Tables[0].Rows[0]["d22_valor"].ToString();
            Informediario.d23_valor = ds.Tables[0].Rows[0]["d23_valor"].ToString();
            Informediario.d24_valor = ds.Tables[0].Rows[0]["d24_valor"].ToString();
            Informediario.d25_valor = ds.Tables[0].Rows[0]["d25_valor"].ToString();
            Informediario.d26_valor = ds.Tables[0].Rows[0]["d26_valor"].ToString();
            Informediario.d27_valor = ds.Tables[0].Rows[0]["d27_valor"].ToString();
            Informediario.d28_valor = ds.Tables[0].Rows[0]["d28_valor"].ToString();
            Informediario.d29_valor = ds.Tables[0].Rows[0]["d29_valor"].ToString();
            Informediario.d30_valor = ds.Tables[0].Rows[0]["d30_valor"].ToString();
            Informediario.d31_valor = ds.Tables[0].Rows[0]["d31_valor"].ToString();
            Informediario.ano = ds.Tables[0].Rows[0]["ano"].ToString();
            Informediario.mes = ds.Tables[0].Rows[0]["mes"].ToString();


            return Informediario;
        }

        public int RetornaVisitasAgendadas(string ano, string mes, string vendedor,string dia)
        {
            try
            {
                string sql = "SELECT * FROM informediario where ano='" + ano + "' and mes='" + mes + "' and vendedor_id=" + vendedor + " and d" + dia + "_valor<>'';";
                clsConexao banco = new clsConexao();
                DataSet ds = null;
                ds = banco.Selecionar(sql);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds.Tables[0].Rows.Count;
                    }
                }
                return 0;
            }
            catch 
            {
                return 0;
            }
        }

        public List<mInformediario> RetornarInformediario()
        {
            clsConexao banco = new clsConexao();
            mInformediario Informediario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cVendedor engineVendedor = new cVendedor();
            List<mInformediario> lstInformediario = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from informediario ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstInformediario = new List<mInformediario>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    Informediario = new mInformediario();
                    Informediario.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    Informediario.calendario = (ds.Tables[0].Rows[i]["calendario"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["calendario"].ToString()) : Convert.ToDateTime("1900-01-01"));
                    Informediario.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString()!=string.Empty?engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()):null);
                    Informediario.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    Informediario.contacto = ds.Tables[0].Rows[i]["contacto"].ToString();
                    Informediario.formacontacto = ds.Tables[0].Rows[i]["formacontacto"].ToString();
                    Informediario.formacontacto2 = ds.Tables[0].Rows[i]["formacontacto2"].ToString();
                    Informediario.acuerdos = ds.Tables[0].Rows[i]["acuerdos"].ToString();
                    Informediario.d1_valor = ds.Tables[0].Rows[i]["d1_valor"].ToString();
                    Informediario.d2_valor = ds.Tables[0].Rows[i]["d2_valor"].ToString();
                    Informediario.d3_valor = ds.Tables[0].Rows[i]["d3_valor"].ToString();
                    Informediario.d4_valor = ds.Tables[0].Rows[i]["d4_valor"].ToString();
                    Informediario.d5_valor = ds.Tables[0].Rows[i]["d5_valor"].ToString();
                    Informediario.d6_valor = ds.Tables[0].Rows[i]["d6_valor"].ToString();
                    Informediario.d7_valor = ds.Tables[0].Rows[i]["d7_valor"].ToString();
                    Informediario.d8_valor = ds.Tables[0].Rows[i]["d8_valor"].ToString();
                    Informediario.d9_valor = ds.Tables[0].Rows[i]["d9_valor"].ToString();
                    Informediario.d10_valor = ds.Tables[0].Rows[i]["d10_valor"].ToString();
                    Informediario.d11_valor = ds.Tables[0].Rows[i]["d11_valor"].ToString();
                    Informediario.d12_valor = ds.Tables[0].Rows[i]["d12_valor"].ToString();
                    Informediario.d13_valor = ds.Tables[0].Rows[i]["d13_valor"].ToString();
                    Informediario.d14_valor = ds.Tables[0].Rows[i]["d14_valor"].ToString();
                    Informediario.d15_valor = ds.Tables[0].Rows[i]["d15_valor"].ToString();
                    Informediario.d16_valor = ds.Tables[0].Rows[i]["d16_valor"].ToString();
                    Informediario.d17_valor = ds.Tables[0].Rows[i]["d17_valor"].ToString();
                    Informediario.d18_valor = ds.Tables[0].Rows[i]["d18_valor"].ToString();
                    Informediario.d19_valor = ds.Tables[0].Rows[i]["d19_valor"].ToString();
                    Informediario.d20_valor = ds.Tables[0].Rows[i]["d20_valor"].ToString();
                    Informediario.d21_valor = ds.Tables[0].Rows[i]["d21_valor"].ToString();
                    Informediario.d22_valor = ds.Tables[0].Rows[i]["d22_valor"].ToString();
                    Informediario.d23_valor = ds.Tables[0].Rows[i]["d23_valor"].ToString();
                    Informediario.d24_valor = ds.Tables[0].Rows[i]["d24_valor"].ToString();
                    Informediario.d25_valor = ds.Tables[0].Rows[i]["d25_valor"].ToString();
                    Informediario.d26_valor = ds.Tables[0].Rows[i]["d26_valor"].ToString();
                    Informediario.d27_valor = ds.Tables[0].Rows[i]["d27_valor"].ToString();
                    Informediario.d28_valor = ds.Tables[0].Rows[i]["d28_valor"].ToString();
                    Informediario.d29_valor = ds.Tables[0].Rows[i]["d29_valor"].ToString();
                    Informediario.d30_valor = ds.Tables[0].Rows[i]["d30_valor"].ToString();
                    Informediario.d31_valor = ds.Tables[0].Rows[i]["d31_valor"].ToString();
                    Informediario.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    Informediario.mes = ds.Tables[0].Rows[i]["mes"].ToString();
                    lstInformediario.Add(Informediario);
                }
            }

            return lstInformediario;
        }

        public List<mInformediario> RetornarInformediario(string ano, string mes,string vendedor, string cliente)
        {
            clsConexao banco = new clsConexao();
            mInformediario Informediario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cVendedor engineVendedor = new cVendedor();
            List<mInformediario> lstInformediario = null;
            DataSet ds = null;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            ds = banco.Selecionar("select * from informediario where ano='" + ano + "' and mes='" + mesescrito + "' and cliente_id=" + cliente + " and vendedor_id=" + vendedor + "  ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstInformediario = new List<mInformediario>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    Informediario = new mInformediario();
                    Informediario.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    Informediario.calendario = (ds.Tables[0].Rows[i]["calendario"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["calendario"].ToString()) : Convert.ToDateTime("1900-01-01"));
                    Informediario.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    Informediario.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    Informediario.contacto = ds.Tables[0].Rows[i]["contacto"].ToString();
                    Informediario.formacontacto = ds.Tables[0].Rows[i]["formacontacto"].ToString();
                    Informediario.formacontacto2 = ds.Tables[0].Rows[i]["formacontacto2"].ToString();
                    Informediario.acuerdos = ds.Tables[0].Rows[i]["acuerdos"].ToString();
                    Informediario.d1_valor = ds.Tables[0].Rows[i]["d1_valor"].ToString();
                    Informediario.d2_valor = ds.Tables[0].Rows[i]["d2_valor"].ToString();
                    Informediario.d3_valor = ds.Tables[0].Rows[i]["d3_valor"].ToString();
                    Informediario.d4_valor = ds.Tables[0].Rows[i]["d4_valor"].ToString();
                    Informediario.d5_valor = ds.Tables[0].Rows[i]["d5_valor"].ToString();
                    Informediario.d6_valor = ds.Tables[0].Rows[i]["d6_valor"].ToString();
                    Informediario.d7_valor = ds.Tables[0].Rows[i]["d7_valor"].ToString();
                    Informediario.d8_valor = ds.Tables[0].Rows[i]["d8_valor"].ToString();
                    Informediario.d9_valor = ds.Tables[0].Rows[i]["d9_valor"].ToString();
                    Informediario.d10_valor = ds.Tables[0].Rows[i]["d10_valor"].ToString();
                    Informediario.d11_valor = ds.Tables[0].Rows[i]["d11_valor"].ToString();
                    Informediario.d12_valor = ds.Tables[0].Rows[i]["d12_valor"].ToString();
                    Informediario.d13_valor = ds.Tables[0].Rows[i]["d13_valor"].ToString();
                    Informediario.d14_valor = ds.Tables[0].Rows[i]["d14_valor"].ToString();
                    Informediario.d15_valor = ds.Tables[0].Rows[i]["d15_valor"].ToString();
                    Informediario.d16_valor = ds.Tables[0].Rows[i]["d16_valor"].ToString();
                    Informediario.d17_valor = ds.Tables[0].Rows[i]["d17_valor"].ToString();
                    Informediario.d18_valor = ds.Tables[0].Rows[i]["d18_valor"].ToString();
                    Informediario.d19_valor = ds.Tables[0].Rows[i]["d19_valor"].ToString();
                    Informediario.d20_valor = ds.Tables[0].Rows[i]["d20_valor"].ToString();
                    Informediario.d21_valor = ds.Tables[0].Rows[i]["d21_valor"].ToString();
                    Informediario.d22_valor = ds.Tables[0].Rows[i]["d22_valor"].ToString();
                    Informediario.d23_valor = ds.Tables[0].Rows[i]["d23_valor"].ToString();
                    Informediario.d24_valor = ds.Tables[0].Rows[i]["d24_valor"].ToString();
                    Informediario.d25_valor = ds.Tables[0].Rows[i]["d25_valor"].ToString();
                    Informediario.d26_valor = ds.Tables[0].Rows[i]["d26_valor"].ToString();
                    Informediario.d27_valor = ds.Tables[0].Rows[i]["d27_valor"].ToString();
                    Informediario.d28_valor = ds.Tables[0].Rows[i]["d28_valor"].ToString();
                    Informediario.d29_valor = ds.Tables[0].Rows[i]["d29_valor"].ToString();
                    Informediario.d30_valor = ds.Tables[0].Rows[i]["d30_valor"].ToString();
                    Informediario.d31_valor = ds.Tables[0].Rows[i]["d31_valor"].ToString();
                    Informediario.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    Informediario.mes = ds.Tables[0].Rows[i]["mes"].ToString();
                    lstInformediario.Add(Informediario);
                }
            }

            return lstInformediario;
        }

        public List<mInformediario> RetornarInformediario(string ano, string mes, string vendedor)
        {
            clsConexao banco = new clsConexao();
            mInformediario Informediario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cVendedor engineVendedor = new cVendedor();
            List<mInformediario> lstInformediario = null;
            DataSet ds = null;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            ds = banco.Selecionar("select * from informediario where ano='" + ano + "' and mes='" + mesescrito + "'  and vendedor_id=" + vendedor + "  ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstInformediario = new List<mInformediario>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    Informediario = new mInformediario();
                    Informediario.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    Informediario.calendario = (ds.Tables[0].Rows[i]["calendario"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["calendario"].ToString()) : Convert.ToDateTime("1900-01-01"));
                    Informediario.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    Informediario.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    Informediario.contacto = ds.Tables[0].Rows[i]["contacto"].ToString();
                    Informediario.formacontacto = ds.Tables[0].Rows[i]["formacontacto"].ToString();
                    Informediario.formacontacto2 = ds.Tables[0].Rows[i]["formacontacto2"].ToString();
                    Informediario.acuerdos = ds.Tables[0].Rows[i]["acuerdos"].ToString();
                    Informediario.d1_valor = ds.Tables[0].Rows[i]["d1_valor"].ToString();
                    Informediario.d2_valor = ds.Tables[0].Rows[i]["d2_valor"].ToString();
                    Informediario.d3_valor = ds.Tables[0].Rows[i]["d3_valor"].ToString();
                    Informediario.d4_valor = ds.Tables[0].Rows[i]["d4_valor"].ToString();
                    Informediario.d5_valor = ds.Tables[0].Rows[i]["d5_valor"].ToString();
                    Informediario.d6_valor = ds.Tables[0].Rows[i]["d6_valor"].ToString();
                    Informediario.d7_valor = ds.Tables[0].Rows[i]["d7_valor"].ToString();
                    Informediario.d8_valor = ds.Tables[0].Rows[i]["d8_valor"].ToString();
                    Informediario.d9_valor = ds.Tables[0].Rows[i]["d9_valor"].ToString();
                    Informediario.d10_valor = ds.Tables[0].Rows[i]["d10_valor"].ToString();
                    Informediario.d11_valor = ds.Tables[0].Rows[i]["d11_valor"].ToString();
                    Informediario.d12_valor = ds.Tables[0].Rows[i]["d12_valor"].ToString();
                    Informediario.d13_valor = ds.Tables[0].Rows[i]["d13_valor"].ToString();
                    Informediario.d14_valor = ds.Tables[0].Rows[i]["d14_valor"].ToString();
                    Informediario.d15_valor = ds.Tables[0].Rows[i]["d15_valor"].ToString();
                    Informediario.d16_valor = ds.Tables[0].Rows[i]["d16_valor"].ToString();
                    Informediario.d17_valor = ds.Tables[0].Rows[i]["d17_valor"].ToString();
                    Informediario.d18_valor = ds.Tables[0].Rows[i]["d18_valor"].ToString();
                    Informediario.d19_valor = ds.Tables[0].Rows[i]["d19_valor"].ToString();
                    Informediario.d20_valor = ds.Tables[0].Rows[i]["d20_valor"].ToString();
                    Informediario.d21_valor = ds.Tables[0].Rows[i]["d21_valor"].ToString();
                    Informediario.d22_valor = ds.Tables[0].Rows[i]["d22_valor"].ToString();
                    Informediario.d23_valor = ds.Tables[0].Rows[i]["d23_valor"].ToString();
                    Informediario.d24_valor = ds.Tables[0].Rows[i]["d24_valor"].ToString();
                    Informediario.d25_valor = ds.Tables[0].Rows[i]["d25_valor"].ToString();
                    Informediario.d26_valor = ds.Tables[0].Rows[i]["d26_valor"].ToString();
                    Informediario.d27_valor = ds.Tables[0].Rows[i]["d27_valor"].ToString();
                    Informediario.d28_valor = ds.Tables[0].Rows[i]["d28_valor"].ToString();
                    Informediario.d29_valor = ds.Tables[0].Rows[i]["d29_valor"].ToString();
                    Informediario.d30_valor = ds.Tables[0].Rows[i]["d30_valor"].ToString();
                    Informediario.d31_valor = ds.Tables[0].Rows[i]["d31_valor"].ToString();
                    Informediario.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    Informediario.mes = ds.Tables[0].Rows[i]["mes"].ToString();
                    lstInformediario.Add(Informediario);
                }
            }

            return lstInformediario;
        }

        public List<mInformediario> RetornarInformediarioDia(string ano, string mes, string vendedor, string cliente)
        {
            clsConexao banco = new clsConexao();
            mInformediario Informediario = new mInformediario();
            cClientes engineCliente = new cClientes();
            cVendedor engineVendedor = new cVendedor();
            List<mInformediario> lstInformediario = null;
            DataSet ds = null;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            ds = banco.Selecionar("select * from informediario where ano='" + ano + "' and mes='" + mesescrito + "' and cliente_id=" + cliente + " and vendedor_id=" + vendedor + " and d"+DateTime.Now.Day.ToString()+"_valor<>'' ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstInformediario = new List<mInformediario>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    Informediario = new mInformediario();
                    Informediario.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    Informediario.calendario = (ds.Tables[0].Rows[i]["calendario"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["calendario"].ToString()) : Convert.ToDateTime("1900-01-01"));
                    Informediario.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    Informediario.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    Informediario.contacto = ds.Tables[0].Rows[i]["contacto"].ToString();
                    Informediario.formacontacto = ds.Tables[0].Rows[i]["formacontacto"].ToString();
                    Informediario.formacontacto2 = ds.Tables[0].Rows[i]["formacontacto2"].ToString();
                    Informediario.acuerdos = ds.Tables[0].Rows[i]["acuerdos"].ToString();
                    Informediario.d1_valor = ds.Tables[0].Rows[i]["d1_valor"].ToString();
                    Informediario.d2_valor = ds.Tables[0].Rows[i]["d2_valor"].ToString();
                    Informediario.d3_valor = ds.Tables[0].Rows[i]["d3_valor"].ToString();
                    Informediario.d4_valor = ds.Tables[0].Rows[i]["d4_valor"].ToString();
                    Informediario.d5_valor = ds.Tables[0].Rows[i]["d5_valor"].ToString();
                    Informediario.d6_valor = ds.Tables[0].Rows[i]["d6_valor"].ToString();
                    Informediario.d7_valor = ds.Tables[0].Rows[i]["d7_valor"].ToString();
                    Informediario.d8_valor = ds.Tables[0].Rows[i]["d8_valor"].ToString();
                    Informediario.d9_valor = ds.Tables[0].Rows[i]["d9_valor"].ToString();
                    Informediario.d10_valor = ds.Tables[0].Rows[i]["d10_valor"].ToString();
                    Informediario.d11_valor = ds.Tables[0].Rows[i]["d11_valor"].ToString();
                    Informediario.d12_valor = ds.Tables[0].Rows[i]["d12_valor"].ToString();
                    Informediario.d13_valor = ds.Tables[0].Rows[i]["d13_valor"].ToString();
                    Informediario.d14_valor = ds.Tables[0].Rows[i]["d14_valor"].ToString();
                    Informediario.d15_valor = ds.Tables[0].Rows[i]["d15_valor"].ToString();
                    Informediario.d16_valor = ds.Tables[0].Rows[i]["d16_valor"].ToString();
                    Informediario.d17_valor = ds.Tables[0].Rows[i]["d17_valor"].ToString();
                    Informediario.d18_valor = ds.Tables[0].Rows[i]["d18_valor"].ToString();
                    Informediario.d19_valor = ds.Tables[0].Rows[i]["d19_valor"].ToString();
                    Informediario.d20_valor = ds.Tables[0].Rows[i]["d20_valor"].ToString();
                    Informediario.d21_valor = ds.Tables[0].Rows[i]["d21_valor"].ToString();
                    Informediario.d22_valor = ds.Tables[0].Rows[i]["d22_valor"].ToString();
                    Informediario.d23_valor = ds.Tables[0].Rows[i]["d23_valor"].ToString();
                    Informediario.d24_valor = ds.Tables[0].Rows[i]["d24_valor"].ToString();
                    Informediario.d25_valor = ds.Tables[0].Rows[i]["d25_valor"].ToString();
                    Informediario.d26_valor = ds.Tables[0].Rows[i]["d26_valor"].ToString();
                    Informediario.d27_valor = ds.Tables[0].Rows[i]["d27_valor"].ToString();
                    Informediario.d28_valor = ds.Tables[0].Rows[i]["d28_valor"].ToString();
                    Informediario.d29_valor = ds.Tables[0].Rows[i]["d29_valor"].ToString();
                    Informediario.d30_valor = ds.Tables[0].Rows[i]["d30_valor"].ToString();
                    Informediario.d31_valor = ds.Tables[0].Rows[i]["d31_valor"].ToString();
                    Informediario.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    Informediario.mes = ds.Tables[0].Rows[i]["mes"].ToString();
                    lstInformediario.Add(Informediario);
                }
            }

            return lstInformediario;
        }

        public string Inserir(mInformediario Informediario)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into informediario("
                                                + "calendario"
                                                + ",cliente_id"
                                                + ",vendedor_id"
                                                + ",contacto"
                                                + ",formacontacto"
                                                + ",formacontacto2"
                                                + ",acuerdos"
                                                + ",ano"
                                                + ",mes"
                                                + ",d1_valor"
                                                + ",d2_valor"
                                                + ",d3_valor"
                                                + ",d4_valor"
                                                + ",d5_valor"
                                                + ",d6_valor"
                                                + ",d7_valor"
                                                + ",d8_valor"
                                                + ",d9_valor"
                                                + ",d10_valor"
                                                + ",d11_valor"
                                                + ",d12_valor"
                                                + ",d13_valor"
                                                + ",d14_valor"
                                                + ",d15_valor"
                                                + ",d16_valor"
                                                + ",d17_valor"
                                                + ",d18_valor"
                                                + ",d19_valor"
                                                + ",d20_valor"
                                                + ",d21_valor"
                                                + ",d22_valor"
                                                + ",d23_valor"
                                                + ",d24_valor"
                                                + ",d25_valor"
                                                + ",d26_valor"
                                                + ",d27_valor"
                                                + ",d28_valor"
                                                + ",d29_valor"
                                                + ",d30_valor"
                                                + ",d31_valor"
                                                + ") values('"
                                                + Informediario.calendario.ToString("yyyy-MM-dd") + "'"
                                                + ",'" + (Informediario.cliente!=null? Informediario.cliente.id.ToString():"0") + "'"
                                                + ",'" + (Informediario.vendedor != null ? Informediario.vendedor.id.ToString() : "0") + "'"
                                                + ",'" + Informediario.contacto + "'"
                                                + ",'" + Informediario.formacontacto + "'"
                                                + ",'" + Informediario.formacontacto2 + "'"
                                                + ",'" + Informediario.acuerdos + "'"
                                                + ",'" + Informediario.ano + "'"
                                                + ",'" + Informediario.mes + "'"
                                                + ",'" + Informediario.d1_valor + "'"
                                                + ",'" + Informediario.d2_valor + "'"
                                                + ",'" + Informediario.d3_valor + "'"
                                                + ",'" + Informediario.d4_valor + "'"
                                                + ",'" + Informediario.d5_valor + "'"
                                                + ",'" + Informediario.d6_valor + "'"
                                                + ",'" + Informediario.d7_valor + "'"
                                                + ",'" + Informediario.d8_valor + "'"
                                                + ",'" + Informediario.d9_valor + "'"
                                                + ",'" + Informediario.d10_valor + "'"
                                                + ",'" + Informediario.d11_valor + "'"
                                                + ",'" + Informediario.d12_valor + "'"
                                                + ",'" + Informediario.d13_valor + "'"
                                                + ",'" + Informediario.d14_valor + "'"
                                                + ",'" + Informediario.d15_valor + "'"
                                                + ",'" + Informediario.d16_valor + "'"
                                                + ",'" + Informediario.d17_valor + "'"
                                                + ",'" + Informediario.d18_valor + "'"
                                                + ",'" + Informediario.d19_valor + "'"
                                                + ",'" + Informediario.d20_valor + "'"
                                                + ",'" + Informediario.d21_valor + "'"
                                                + ",'" + Informediario.d22_valor + "'"
                                                + ",'" + Informediario.d23_valor + "'"
                                                + ",'" + Informediario.d24_valor + "'"
                                                + ",'" + Informediario.d25_valor + "'"
                                                + ",'" + Informediario.d26_valor + "'"
                                                + ",'" + Informediario.d27_valor + "'"
                                                + ",'" + Informediario.d28_valor + "'"
                                                + ",'" + Informediario.d29_valor + "'"
                                                + ",'" + Informediario.d30_valor + "'"
                                                + ",'" + Informediario.d31_valor + "'"
                                                + ")");

            return banco.Selecionar("select  * from informediario order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();
        }

        public string InserirVisita(string cliente, string fecha, string dia, string mes,string vendedor)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into visita_informediario("
                                                + "cliente_id"
                                                + ",dia"
                                                + ",mes"
                                                + ",vendedor_id"
                                                + ",fecha"
                                                + ") values('"
                                                + cliente + "'"
                                                + ",'" + dia + "'"
                                                + ",'" + mes + "'"
                                                + ",'" + vendedor + "'"
                                                + ",'" + fecha + "'"
                                                + ")");

            return banco.Selecionar("select  * from visita_informediario order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();
        }

        public string InserirOtro(string cliente, string otro, string vendedor,string informe_id)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into otro_informediario("
                                                + "cliente_id"
                                                + ",otro"
                                                + ",vendedor_id"
                                                + ",informe_id"
                                                + ",data"  
                                                + ") values('"
                                                + cliente + "'"
                                                + ",'" + otro + "'"
                                                + ",'" + vendedor + "'"
                                                + ",'" + informe_id + "'"
                                                + ",'" + DateTime.Now.ToString("yyyy-MM-dd") + "'"
                                                + ")");

            return banco.Selecionar("select  * from otro_informediario order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mInformediario Informediario)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update informediario set "
                                                + " calendario='" + Informediario.calendario.ToString("yyyy-MM-dd") + "'"
                                                + " ,cliente_id='" + (Informediario.cliente.id.ToString()!=null?Informediario.cliente.id.ToString():"0") + "'"
                                                + " ,vendedor_id='" + (Informediario.vendedor.id.ToString() != null ? Informediario.vendedor.id.ToString() : "0") + "'"
                                                + " ,contacto='" + Informediario.contacto + "'"
                                                + " ,formacontacto='" + Informediario.formacontacto + "'"
                                                + " ,formacontacto2='" + Informediario.formacontacto2 + "'"
                                                + " ,acuerdos='" + Informediario.acuerdos + "'"
                                                + " ,ano='" + Informediario.ano + "'"
                                                + " ,mes='" + Informediario.mes + "'"
                                                + (Informediario.d1_valor!=null? " ,d1_valor='" + Informediario.d1_valor + "'":"")
                                                + (Informediario.d2_valor != null ? " ,d2_valor='" + Informediario.d2_valor + "'" : "")
                                                + (Informediario.d3_valor != null ? " ,d3_valor='" + Informediario.d3_valor + "'" : "")
                                                + (Informediario.d4_valor != null ? " ,d4_valor='" + Informediario.d4_valor + "'" : "")
                                                + (Informediario.d5_valor != null ? " ,d5_valor='" + Informediario.d5_valor + "'" : "")
                                                + (Informediario.d6_valor != null ? " ,d6_valor='" + Informediario.d6_valor + "'" : "")
                                                + (Informediario.d7_valor != null ? " ,d7_valor='" + Informediario.d7_valor + "'" : "")
                                                + (Informediario.d8_valor != null ? " ,d8_valor='" + Informediario.d8_valor + "'" : "")
                                                + (Informediario.d9_valor != null ? " ,d9_valor='" + Informediario.d9_valor + "'" : "")
                                                + (Informediario.d10_valor != null ? " ,d10_valor='" + Informediario.d10_valor + "'" : "")
                                                + (Informediario.d11_valor != null ? " ,d11_valor='" + Informediario.d11_valor + "'" : "")
                                                + (Informediario.d12_valor != null ? " ,d12_valor='" + Informediario.d12_valor + "'" : "")
                                                + (Informediario.d13_valor != null ? " ,d13_valor='" + Informediario.d13_valor + "'" : "")
                                                + (Informediario.d14_valor != null ? " ,d14_valor='" + Informediario.d14_valor + "'" : "")
                                                + (Informediario.d15_valor != null ? " ,d15_valor='" + Informediario.d15_valor + "'" : "")
                                                + (Informediario.d16_valor != null ? " ,d16_valor='" + Informediario.d16_valor + "'" : "")
                                                + (Informediario.d17_valor != null ? " ,d17_valor='" + Informediario.d17_valor + "'" : "")
                                                + (Informediario.d18_valor != null ? " ,d18_valor='" + Informediario.d18_valor + "'" : "")
                                                + (Informediario.d19_valor != null ? " ,d19_valor='" + Informediario.d19_valor + "'" : "")
                                                + (Informediario.d20_valor != null ? " ,d20_valor='" + Informediario.d20_valor + "'" : "")
                                                + (Informediario.d21_valor != null ? " ,d21_valor='" + Informediario.d21_valor + "'" : "")
                                                + (Informediario.d22_valor != null ? " ,d22_valor='" + Informediario.d22_valor + "'" : "")
                                                + (Informediario.d23_valor != null ? " ,d23_valor='" + Informediario.d23_valor + "'" : "")
                                                + (Informediario.d24_valor != null ? " ,d24_valor='" + Informediario.d24_valor + "'" : "")
                                                + (Informediario.d25_valor != null ? " ,d25_valor='" + Informediario.d25_valor + "'" : "")
                                                + (Informediario.d26_valor != null ? " ,d26_valor='" + Informediario.d26_valor + "'" : "")
                                                + (Informediario.d27_valor != null ? " ,d27_valor='" + Informediario.d27_valor + "'" : "")
                                                + (Informediario.d28_valor != null ? " ,d28_valor='" + Informediario.d28_valor + "'" : "")
                                                + (Informediario.d29_valor != null ? " ,d29_valor='" + Informediario.d29_valor + "'" : "")
                                                + (Informediario.d30_valor != null ? " ,d30_valor='" + Informediario.d30_valor + "'" : "")
                                                + (Informediario.d31_valor != null ? " ,d31_valor='" + Informediario.d31_valor + "'" : "")
                                                + " where id=" + Informediario.Id.ToString());
        }

        public bool Deletar(mInformediario Informediario)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from informediario  "
                                                + " where id=" + Informediario.Id.ToString());
        }

        public DataSet PopularGrid(int limite, string ano, string mes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            string valordia = string.Empty;
                valordia = " and (d"+DateTime.Now.Day.ToString()+"_valor<>'' and d"+DateTime.Now.Day.ToString()+"_valor<>'F' "
                            +" and d" + DateTime.Now.Day.ToString()+"_valor='P'"
                            +")";
        

            if (limite > 0)
            {
                _sql = "select "
                         + " i.id"
                         + "  ,i.calendario"
                         + "  ,c.razonsocial"
                         + "  ,i.contacto"
                         + "  ,c.telefone"
                         + "  ,c.comuna"
                         + "  ,i.ano"
                         + "  ,i.mes"
                         + " ,i.d1_valor as '1'"
                         + " ,i.d2_valor as '2'"
                         + " ,i.d3_valor as '3'"
                         + " ,i.d4_valor as '4'"
                         + " ,i.d5_valor as '5'"
                         + " ,i.d6_valor as '6'"
                         + " ,i.d7_valor as '7'"
                         + " ,i.d8_valor as '8'"
                         + " ,i.d9_valor as '9'"
                         + " ,i.d10_valor as '10'"
                         + " ,i.d11_valor as '11'"
                         + " ,i.d12_valor as '12'"
                         + " ,i.d13_valor as '13'"
                         + " ,i.d14_valor as '14'"
                         + " ,i.d15_valor as '15'"
                         + " ,i.d16_valor as '16'"
                         + " ,i.d17_valor as '17'"
                         + " ,i.d18_valor as '18'"
                         + " ,i.d19_valor as '19'"
                         + " ,i.d20_valor as '20'"
                         + " ,i.d21_valor as '21'"
                         + " ,i.d22_valor as '22'"
                         + " ,i.d23_valor as '23'"
                         + " ,i.d24_valor as '24'"
                         + " ,i.d25_valor as '25'"
                         + " ,i.d26_valor as '26'"
                         + " ,i.d27_valor as '27'"
                         + " ,i.d28_valor as '28'"
                         + " ,i.d29_valor as '29'"
                         + " ,i.d30_valor as '30'"
                         + " ,i.d31_valor as '31'"
                         + " from informediario i "
                         + " inner join clientes c on i.cliente_id = c.id  "
                         + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "' "
                         + valordia
                         + " order by i.id desc limit " + limite.ToString();
            }
            else
            {
                
                         _sql = "select "
                         + " i.id"
                         + "  ,i.calendario"
                         + "  ,c.razonsocial"
                         + "  ,i.contacto"
                         + "  ,c.telefone"
                         + "  ,c.comuna"
                         + "  ,i.ano"
                         + "  ,i.mes"
                         + " ,i.d1_valor as '1'"
                         + " ,i.d2_valor as '2'"
                         + " ,i.d3_valor as '3'"
                         + " ,i.d4_valor as '4'"
                         + " ,i.d5_valor as '5'"
                         + " ,i.d6_valor as '6'"
                         + " ,i.d7_valor as '7'"
                         + " ,i.d8_valor as '8'"
                         + " ,i.d9_valor as '9'"
                         + " ,i.d10_valor as '10'"
                         + " ,i.d11_valor as '11'"
                         + " ,i.d12_valor as '12'"
                         + " ,i.d13_valor as '13'"
                         + " ,i.d14_valor as '14'"
                         + " ,i.d15_valor as '15'"
                         + " ,i.d16_valor as '16'"
                         + " ,i.d17_valor as '17'"
                         + " ,i.d18_valor as '18'"
                         + " ,i.d19_valor as '19'"
                         + " ,i.d20_valor as '20'"
                         + " ,i.d21_valor as '21'"
                         + " ,i.d22_valor as '22'"
                         + " ,i.d23_valor as '23'"
                         + " ,i.d24_valor as '24'"
                         + " ,i.d25_valor as '25'"
                         + " ,i.d26_valor as '26'"
                         + " ,i.d27_valor as '27'"
                         + " ,i.d28_valor as '28'"
                         + " ,i.d29_valor as '29'"
                         + " ,i.d30_valor as '30'"
                         + " ,i.d31_valor as '31'"
                         + " from informediario i "
                         + " inner join clientes c on i.cliente_id = c.id  "
                         + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "' "
                         + valordia
                         + " order by i.id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridVistaPrevia(string ano, string mes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            string valordia = string.Empty;
            valordia = " and (d" + DateTime.Now.Day.ToString() + "_valor<>'' and d" + DateTime.Now.Day.ToString() + "_valor<>'F')";


            _sql = "select "
                     + " c.razonsocial as Instituicao"
                     + " ,i.contacto as Contacto"
                     + " ,i.acuerdos as Acuerdos"
                     + " ,o.otro as 'Proxima Actividade'"
                     + " from  "
                     + " informediario i"
                     + "   inner join clientes c on i.cliente_id =c.id"
                     + "   left join otro_informediario o on o.informe_id = i.id  "
                     + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "' "
                     + " and o.data='"+DateTime.Now.ToString("yyyy-MM-dd") + "' "
                     + valordia;

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet RetornarVisitasPorStatus(string ano, string mes,string dia,string status)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            //string valordia = string.Empty;
            //valordia = " and (d" + DateTime.Now.Day.ToString() + "_valor<>'' and d" + DateTime.Now.Day.ToString() + "_valor<>'F')";


            _sql = "select "
                     + " c.razonsocial as Instituicao"
                     + " ,i.contacto as Contacto"
                     + " ,i.acuerdos as Acuerdos"
                     + " ,o.otro as 'Proxima Actividade'"
                     + " ,d"+dia + "_valor as 'dia'"
                     + " from  "
                     + " informediario i"
                     + "   inner join clientes c on i.cliente_id =c.id"
                     + "   left join otro_informediario o on o.informe_id = i.id  "
                     + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "' "
                     + " and i.d" + dia + "_valor='" + status + "' ";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet RetornarVisitasPorStatusSemOtro(string ano, string mes, string dia, string status)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            //string valordia = string.Empty;
            //valordia = " and (d" + DateTime.Now.Day.ToString() + "_valor<>'' and d" + DateTime.Now.Day.ToString() + "_valor<>'F')";


            _sql = "select "
                     + " c.razonsocial as Instituicao"
                     + " ,i.contacto as Contacto"
                     + " ,i.acuerdos as Acuerdos"
                     + " ,d" + dia + "_valor as 'dia'"
                     + " from  "
                     + " informediario i"
                     + "   inner join clientes c on i.cliente_id =c.id"
                     + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "' "
                     + " and i.d" + dia + "_valor='" + status + "' ";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet RetornarVisitasPorStatusSemOtroPorVendedor(string ano, string mes, string dia, string status,string vendedor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            //string valordia = string.Empty;
            //valordia = " and (d" + DateTime.Now.Day.ToString() + "_valor<>'' and d" + DateTime.Now.Day.ToString() + "_valor<>'F')";


            _sql = "select "
                     + " c.razonsocial as Instituicao"
                     + " ,i.contacto as Contacto"
                     + " ,i.acuerdos as Acuerdos"
                     + " ,d" + dia + "_valor as 'dia'"
                     + " from  "
                     + " informediario i"
                     + "   inner join clientes c on i.cliente_id =c.id"
                     + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "' "
                     + " and i.d" + dia + "_valor='" + status + "' "
                     +"  and i.vendedor_id=" + vendedor;

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet RetornarVisitasPorStatusSemOtroPorVendedorCliente(string ano, string mes, string dia, string status, string vendedor,string cliente)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            //string valordia = string.Empty;
            //valordia = " and (d" + DateTime.Now.Day.ToString() + "_valor<>'' and d" + DateTime.Now.Day.ToString() + "_valor<>'F')";


            _sql = "select "
                     + " c.razonsocial as Instituicao"
                     + " ,i.contacto as Contacto"
                     + " ,i.acuerdos as Acuerdos"
                     + " ,d" + dia + "_valor as 'dia'"
                     + " from  "
                     + " informediario i"
                     + "   inner join clientes c on i.cliente_id =c.id"
                     + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "' "
                     + " and i.d" + dia + "_valor='" + status + "' "
                     + "  and i.vendedor_id=" + vendedor + " and i.cliente_id=" + cliente;

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        
        public DataSet PopularGridAgenda(int limite, string ano,string mes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;
            if (limite > 0)
            {
                _sql = "select "
                         + " i.id"
                         + "  ,i.calendario"
                         + "  ,c.razonsocial"
                         + "  ,i.contacto"
                         + "  ,c.telefone"
                         + "  ,c.comuna"
                         + "  ,i.ano"
                         + "  ,i.mes"
                         + " ,i.d1_valor as '1'"
                         + " ,i.d2_valor as '2'"
                         + " ,i.d3_valor as '3'"
                         + " ,i.d4_valor as '4'"
                         + " ,i.d5_valor as '5'"
                         + " ,i.d6_valor as '6'"
                         + " ,i.d7_valor as '7'"
                         + " ,i.d8_valor as '8'"
                         + " ,i.d9_valor as '9'"
                         + " ,i.d10_valor as '10'"
                         + " ,i.d11_valor as '11'"
                         + " ,i.d12_valor as '12'"
                         + " ,i.d13_valor as '13'"
                         + " ,i.d14_valor as '14'"
                         + " ,i.d15_valor as '15'"
                         + " ,i.d16_valor as '16'"
                         + " ,i.d17_valor as '17'"
                         + " ,i.d18_valor as '18'"
                         + " ,i.d19_valor as '19'"
                         + " ,i.d20_valor as '20'"
                         + " ,i.d21_valor as '21'"
                         + " ,i.d22_valor as '22'"
                         + " ,i.d23_valor as '23'"
                         + " ,i.d24_valor as '24'"
                         + " ,i.d25_valor as '25'"
                         + " ,i.d26_valor as '26'"
                         + " ,i.d27_valor as '27'"
                         + " ,i.d28_valor as '28'"
                         + " ,i.d29_valor as '29'"
                         + " ,i.d30_valor as '30'"
                         + " ,i.d31_valor as '31'"
                         + " from informediario i "
                         + " inner join clientes c on i.cliente_id = c.id  "
                         + " where i.ano='"+ano+"' and i.mes='"+mesescrito+"'"
                         + " order by i.id desc limit " + limite.ToString();
            }
            else
            {

                _sql = "select "
                + " i.id"
                + "  ,i.calendario"
                + "  ,c.razonsocial"
                + "  ,i.contacto"
                + "  ,c.telefone"
                + "  ,c.comuna"
                + "  ,i.ano"
                + "  ,i.mes"
                + " ,i.d1_valor as '1'"
                + " ,i.d2_valor as '2'"
                + " ,i.d3_valor as '3'"
                + " ,i.d4_valor as '4'"
                + " ,i.d5_valor as '5'"
                + " ,i.d6_valor as '6'"
                + " ,i.d7_valor as '7'"
                + " ,i.d8_valor as '8'"
                + " ,i.d9_valor as '9'"
                + " ,i.d10_valor as '10'"
                + " ,i.d11_valor as '11'"
                + " ,i.d12_valor as '12'"
                + " ,i.d13_valor as '13'"
                + " ,i.d14_valor as '14'"
                + " ,i.d15_valor as '15'"
                + " ,i.d16_valor as '16'"
                + " ,i.d17_valor as '17'"
                + " ,i.d18_valor as '18'"
                + " ,i.d19_valor as '19'"
                + " ,i.d20_valor as '20'"
                + " ,i.d21_valor as '21'"
                + " ,i.d22_valor as '22'"
                + " ,i.d23_valor as '23'"
                + " ,i.d24_valor as '24'"
                + " ,i.d25_valor as '25'"
                + " ,i.d26_valor as '26'"
                + " ,i.d27_valor as '27'"
                + " ,i.d28_valor as '28'"
                + " ,i.d29_valor as '29'"
                + " ,i.d30_valor as '30'"
                + " ,i.d31_valor as '31'"
                + " from informediario i "
                + " inner join clientes c on i.cliente_id = c.id  "
                + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "'"
                + " order by i.id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridAgenda(int limite, string ano, string mes,string vendedor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;
            if (limite > 0)
            {
                _sql = "select "
                         + " i.id"
                         + "  ,i.calendario"
                         + "  ,c.razonsocial"
                         + "  ,i.contacto"
                         + "  ,c.telefone"
                         + "  ,c.comuna"
                         + "  ,i.ano"
                         + "  ,i.mes"
                         + " ,i.d1_valor as '1'"
                         + " ,i.d2_valor as '2'"
                         + " ,i.d3_valor as '3'"
                         + " ,i.d4_valor as '4'"
                         + " ,i.d5_valor as '5'"
                         + " ,i.d6_valor as '6'"
                         + " ,i.d7_valor as '7'"
                         + " ,i.d8_valor as '8'"
                         + " ,i.d9_valor as '9'"
                         + " ,i.d10_valor as '10'"
                         + " ,i.d11_valor as '11'"
                         + " ,i.d12_valor as '12'"
                         + " ,i.d13_valor as '13'"
                         + " ,i.d14_valor as '14'"
                         + " ,i.d15_valor as '15'"
                         + " ,i.d16_valor as '16'"
                         + " ,i.d17_valor as '17'"
                         + " ,i.d18_valor as '18'"
                         + " ,i.d19_valor as '19'"
                         + " ,i.d20_valor as '20'"
                         + " ,i.d21_valor as '21'"
                         + " ,i.d22_valor as '22'"
                         + " ,i.d23_valor as '23'"
                         + " ,i.d24_valor as '24'"
                         + " ,i.d25_valor as '25'"
                         + " ,i.d26_valor as '26'"
                         + " ,i.d27_valor as '27'"
                         + " ,i.d28_valor as '28'"
                         + " ,i.d29_valor as '29'"
                         + " ,i.d30_valor as '30'"
                         + " ,i.d31_valor as '31'"
                         + " from informediario i "
                         + " inner join clientes c on i.cliente_id = c.id  "
                         + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "'"
                         + " and i.vendedor_id='" + vendedor + "'"
                         + " order by i.id desc limit " + limite.ToString();
            }
            else
            {

                _sql = "select "
                + " i.id"
                + "  ,i.calendario"
                + "  ,c.razonsocial"
                + "  ,i.contacto"
                + "  ,c.telefone"
                + "  ,c.comuna"
                + "  ,i.ano"
                + "  ,i.mes"
                + " ,i.d1_valor as '1'"
                + " ,i.d2_valor as '2'"
                + " ,i.d3_valor as '3'"
                + " ,i.d4_valor as '4'"
                + " ,i.d5_valor as '5'"
                + " ,i.d6_valor as '6'"
                + " ,i.d7_valor as '7'"
                + " ,i.d8_valor as '8'"
                + " ,i.d9_valor as '9'"
                + " ,i.d10_valor as '10'"
                + " ,i.d11_valor as '11'"
                + " ,i.d12_valor as '12'"
                + " ,i.d13_valor as '13'"
                + " ,i.d14_valor as '14'"
                + " ,i.d15_valor as '15'"
                + " ,i.d16_valor as '16'"
                + " ,i.d17_valor as '17'"
                + " ,i.d18_valor as '18'"
                + " ,i.d19_valor as '19'"
                + " ,i.d20_valor as '20'"
                + " ,i.d21_valor as '21'"
                + " ,i.d22_valor as '22'"
                + " ,i.d23_valor as '23'"
                + " ,i.d24_valor as '24'"
                + " ,i.d25_valor as '25'"
                + " ,i.d26_valor as '26'"
                + " ,i.d27_valor as '27'"
                + " ,i.d28_valor as '28'"
                + " ,i.d29_valor as '29'"
                + " ,i.d30_valor as '30'"
                + " ,i.d31_valor as '31'"
                + " from informediario i "
                + " inner join clientes c on i.cliente_id = c.id  "
                + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "'"
                + " and i.vendedor_id='" + vendedor + "'"
                + " order by i.id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridCartaGantt(int limite, string ano, string mes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;
            if (limite > 0)
            {
                _sql = "select "
                         + " i.id"
                         + "  ,i.calendario"
                         + "  ,v.nombre as vendedor"
                         + "  ,c.razonsocial"
                         + "  ,i.contacto"
                         + "  ,c.telefone"
                         + "  ,c.comuna"
                         + "  ,i.ano"
                         + "  ,i.mes"
                         + " ,i.d1_valor as '1'"
                         + " ,i.d2_valor as '2'"
                         + " ,i.d3_valor as '3'"
                         + " ,i.d4_valor as '4'"
                         + " ,i.d5_valor as '5'"
                         + " ,i.d6_valor as '6'"
                         + " ,i.d7_valor as '7'"
                         + " ,i.d8_valor as '8'"
                         + " ,i.d9_valor as '9'"
                         + " ,i.d10_valor as '10'"
                         + " ,i.d11_valor as '11'"
                         + " ,i.d12_valor as '12'"
                         + " ,i.d13_valor as '13'"
                         + " ,i.d14_valor as '14'"
                         + " ,i.d15_valor as '15'"
                         + " ,i.d16_valor as '16'"
                         + " ,i.d17_valor as '17'"
                         + " ,i.d18_valor as '18'"
                         + " ,i.d19_valor as '19'"
                         + " ,i.d20_valor as '20'"
                         + " ,i.d21_valor as '21'"
                         + " ,i.d22_valor as '22'"
                         + " ,i.d23_valor as '23'"
                         + " ,i.d24_valor as '24'"
                         + " ,i.d25_valor as '25'"
                         + " ,i.d26_valor as '26'"
                         + " ,i.d27_valor as '27'"
                         + " ,i.d28_valor as '28'"
                         + " ,i.d29_valor as '29'"
                         + " ,i.d30_valor as '30'"
                         + " ,i.d31_valor as '31'"
                         + " from informediario i "
                         + " inner join clientes c on i.cliente_id = c.id  "
                         + " inner join vendedor v on i.vendedor_id = v.id  "
                         + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "'"
                         + " order by i.id desc limit " + limite.ToString();
            }
            else
            {

                _sql = "select "
                + " i.id"
                + "  ,i.calendario"
                + "  ,v.nombre as vendedor"
                + "  ,c.razonsocial"
                + "  ,i.contacto"
                + "  ,c.telefone"
                + "  ,c.comuna"
                + "  ,i.ano"
                + "  ,i.mes"
                + " ,i.d1_valor as '1'"
                + " ,i.d2_valor as '2'"
                + " ,i.d3_valor as '3'"
                + " ,i.d4_valor as '4'"
                + " ,i.d5_valor as '5'"
                + " ,i.d6_valor as '6'"
                + " ,i.d7_valor as '7'"
                + " ,i.d8_valor as '8'"
                + " ,i.d9_valor as '9'"
                + " ,i.d10_valor as '10'"
                + " ,i.d11_valor as '11'"
                + " ,i.d12_valor as '12'"
                + " ,i.d13_valor as '13'"
                + " ,i.d14_valor as '14'"
                + " ,i.d15_valor as '15'"
                + " ,i.d16_valor as '16'"
                + " ,i.d17_valor as '17'"
                + " ,i.d18_valor as '18'"
                + " ,i.d19_valor as '19'"
                + " ,i.d20_valor as '20'"
                + " ,i.d21_valor as '21'"
                + " ,i.d22_valor as '22'"
                + " ,i.d23_valor as '23'"
                + " ,i.d24_valor as '24'"
                + " ,i.d25_valor as '25'"
                + " ,i.d26_valor as '26'"
                + " ,i.d27_valor as '27'"
                + " ,i.d28_valor as '28'"
                + " ,i.d29_valor as '29'"
                + " ,i.d30_valor as '30'"
                + " ,i.d31_valor as '31'"
                + " from informediario i "
                + " inner join clientes c on i.cliente_id = c.id  "
                + " inner join vendedor v on i.vendedor_id = v.id  "
                + " where i.ano='" + ano + "' and i.mes='" + mesescrito + "'"
                + " order by i.id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}