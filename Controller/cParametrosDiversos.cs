﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cParametrosDiversos
    {
        public mParametrosDiversos RetornarParametrosDiversos(string id)
        {
            clsConexao banco = new clsConexao();

            mParametrosDiversos ParametrosDiversos = new mParametrosDiversos();

            DataSet ds = null;

            if (id == "0")
                return null;

            ds = banco.Selecionar("select * from parametros ");


            ParametrosDiversos = null;
            if (ds.Tables[0].Rows.Count > 0)
            {
                ParametrosDiversos = new mParametrosDiversos();
                ParametrosDiversos.travardias = Convert.ToInt32( ds.Tables[0].Rows[0]["travardias"].ToString()!=string.Empty?ds.Tables[0].Rows[0]["travardias"].ToString():"0");
                ParametrosDiversos.maxvisitasdias = Convert.ToInt32(ds.Tables[0].Rows[0]["maxvisitasdias"].ToString()!=string.Empty? ds.Tables[0].Rows[0]["maxvisitasdias"].ToString():"0");
                ParametrosDiversos.maxvisitasagendadas = Convert.ToInt32(ds.Tables[0].Rows[0]["maxvisitasagendadas"].ToString() != string.Empty ? ds.Tables[0].Rows[0]["maxvisitasagendadas"].ToString() : "0");
                ParametrosDiversos.tkc= Convert.ToInt32(ds.Tables[0].Rows[0]["tkc"].ToString() != string.Empty ? ds.Tables[0].Rows[0]["tkc"].ToString() : "0");
                ParametrosDiversos.metaprometodiaria = Convert.ToInt32(ds.Tables[0].Rows[0]["metaprometodiaria"].ToString() != string.Empty ? ds.Tables[0].Rows[0]["metaprometodiaria"].ToString() : "0");       
                
            }
            return ParametrosDiversos;
        }

        public List<mParametrosDiversos> RetornarParametrosDiversos()
        {
            clsConexao banco = new clsConexao();
            mParametrosDiversos ParametrosDiversos = new mParametrosDiversos();
            List<mParametrosDiversos> lstParametrosDiversos = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from parametros ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstParametrosDiversos = new List<mParametrosDiversos>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    ParametrosDiversos = new mParametrosDiversos();
                    ParametrosDiversos.travardias = Convert.ToInt32(ds.Tables[0].Rows[i]["travardias"].ToString());
                    ParametrosDiversos.maxvisitasdias = Convert.ToInt32(ds.Tables[0].Rows[i]["maxvisitasdias"].ToString());
                    ParametrosDiversos.maxvisitasagendadas = Convert.ToInt32(ds.Tables[0].Rows[i]["maxvisitasagendadas"].ToString());
                    ParametrosDiversos.tkc= Convert.ToInt32(ds.Tables[0].Rows[i]["tkc"].ToString());
                    ParametrosDiversos.metaprometodiaria = Convert.ToInt32(ds.Tables[0].Rows[i]["metaprometodiaria"].ToString());

                    lstParametrosDiversos.Add(ParametrosDiversos);
                }
            }

            return lstParametrosDiversos;
        }

        public string Inserir(mParametrosDiversos ParametrosDiversos)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into parametros("
                                                + "travardias"
                                                + ",maxvisitasdias"
                                                + ",maxvisitasagendadas"
                                                + ",tkc"
                                                + ",metaprometodiaria"
                                                + ") values('"
                                                + ParametrosDiversos.travardias+ "'"
                                                + ",'" + ParametrosDiversos.maxvisitasdias + "'"
                                                + ",'" + ParametrosDiversos.maxvisitasagendadas + "'"
                                                + ",'" + ParametrosDiversos.tkc+ "'"
                                                + ",'" + ParametrosDiversos.metaprometodiaria + "'"
                                                + ")");

            return banco.Selecionar("select  * from parametros order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mParametrosDiversos ParametrosDiversos)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update parametros set "
                                                + " travardias='" + ParametrosDiversos.travardias + "'"
                                                + " ,maxvisitasdias='" + ParametrosDiversos.maxvisitasdias + "'"
                                                + " ,tkc='" + ParametrosDiversos.tkc+ "'"
                                                + " ,metaprometodiaria='" + ParametrosDiversos.metaprometodiaria + "'"                                                
                                                + " ,maxvisitasagendadas='" + ParametrosDiversos.maxvisitasagendadas + "'");
        }
        public bool Deletar(mParametrosDiversos ParametrosDiversos)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from parametros  "
                );
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select "
                         + " travardias as 'Maximo de Dias para planificar'"
                         + " ,maxvisitasdias as 'Maximo de Visitas Diarias' "
                         + " ,maxvisitasagendadas as 'Maximo de Visitas Agendadas' "
                         + " ,tkc as 'TKC' "
                         + " ,metaprometodiaria as 'META PROMEDIO DIARIA VISITAS' "
                         + " from parametros "
                         + "  limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                         + " travardias as 'Maximo de Dias para planificar'"
                         + " ,maxvisitasdias as 'Maximo de Visitas Diarias' "
                         + " ,maxvisitasagendadas as 'Maximo de Visitas Agendadas' "
                         + " ,tkc as 'TKC' "
                         + " ,metaprometodiaria as 'META PROMEDIO DIARIA VISITAS' "
                         + " from parametros "
                         + " ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}