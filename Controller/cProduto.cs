﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cProduto
    {
        public mProduto RetornarProduto(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mProduto Produto = new mProduto();
            cVendedor engineVendedor = new cVendedor();
            

            DataSet ds = null;

            if (id == "0" || id=="")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from produto where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAProduto.NOME
            
            Produto.Id = Convert.ToInt32(id);
            Produto.nombre = ds.Tables[0].Rows[0]["nombre"].ToString();
            Produto.codigo = ds.Tables[0].Rows[0]["codigo"].ToString();
            Produto.horizonte = ds.Tables[0].Rows[0]["horizonte"].ToString();
            return Produto;
        }

        public List<mProduto> RetornarProduto()
        {
            clsConexao banco = new clsConexao();
            mProduto Produto = new mProduto();
            List<mProduto> lstProduto = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from produto ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstProduto = new List<mProduto>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    Produto = new mProduto();
                    Produto.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    Produto.nombre = ds.Tables[0].Rows[i]["nombre"].ToString();
                    Produto.codigo = ds.Tables[0].Rows[i]["codigo"].ToString();
                    Produto.horizonte = ds.Tables[0].Rows[i]["horizonte"].ToString();
                    lstProduto.Add(Produto);
                }
            }

            return lstProduto;
        }

        public string Inserir(mProduto Produto)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into produto("
                                                + "nombre"
                                                + ",codigo"
                                                + ",horizonte"
                                                + ") values('"
                                                + Produto.nombre + "'"
                                                + ",'" + Produto.codigo + "'"
                                                + ",'" + Produto.horizonte+ "'"
                                                + ")");

            return banco.Selecionar("select  * from produto order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mProduto Produto)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update produto set "
                                                + " nombre='" + Produto.nombre + "'"
                                                + " ,codigo='" + Produto.codigo + "'"
                                                + " ,horizonte='" + Produto.horizonte+ "'"
                                                + " where id=" + Produto.Id.ToString());
        }
        public bool Deletar(mProduto Produto)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from produto  "
                                                + " where id=" + Produto.Id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select "
                         + " id"
                         + "  ,nombre"
                         + "  ,codigo"
                         + "  ,horizonte"
                         + " from produto "
                         + " order by id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                         + " id"
                         + "  ,nombre"
                         + "  ,codigo"
                         + "  ,horizonte"
                         + " from produto "
                         + " order by id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridVendedor(int idVendedor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


                _sql = "select "
                         + " p.id"
                         + "  ,p.nombre"
                         + "  ,p.codigo"
                         + "  ,p.horizonte"
                         + " from produto p"
                         + " inner join asignacionmetas m on p.id = m.produto "
                         + " where m.vendedor="+idVendedor
                         + " order by p.nombre asc ";
            

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}