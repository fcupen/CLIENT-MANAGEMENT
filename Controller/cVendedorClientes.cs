﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cVendedorClientes
    {
        public mVendedorClientes RetornarGrupoVendedores(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mVendedorClientes vendedoreClientes = new mVendedorClientes();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from vendedor_clientes where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAGrupoVendedores.NOME
            vendedoreClientes.Id = Convert.ToInt32(id);
            vendedoreClientes.vendedor = engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor_id"].ToString());
            vendedoreClientes.produto = engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["produto_id"].ToString());
            vendedoreClientes.clientes = Convert.ToInt32(ds.Tables[0].Rows[0]["clientes"].ToString());

            return vendedoreClientes;
        }

        public mVendedorClientes RetornarGrupoVendedorPorVendedor(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mVendedorClientes vendedoreClientes = new mVendedorClientes();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from vendedor_clientes where vendedor_id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAGrupoVendedores.NOME
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    vendedoreClientes.Id = Convert.ToInt32(ds.Tables[0].Rows[0]["id"].ToString());
                    vendedoreClientes.vendedor = engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor_id"].ToString());
                    vendedoreClientes.produto = engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["produto_id"].ToString());
                    vendedoreClientes.clientes = Convert.ToInt32(ds.Tables[0].Rows[0]["clientes"].ToString());

                    return vendedoreClientes;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        public List<mVendedorClientes> RetornarGrupoVendedores()
        {
            clsConexao banco = new clsConexao();
            mVendedorClientes vendedoreClientes = new mVendedorClientes();
            List<mVendedorClientes> lstGrupoVendedores = null;
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            DataSet ds = null;

            ds = banco.Selecionar("select * from vendedor_clientes ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstGrupoVendedores = new List<mVendedorClientes>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    vendedoreClientes = new mVendedorClientes();
                    vendedoreClientes.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    vendedoreClientes.vendedor = engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString());
                    vendedoreClientes.produto = engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["produto_id"].ToString());
                    vendedoreClientes.clientes = Convert.ToInt32(ds.Tables[0].Rows[i]["clientes"].ToString());
                    lstGrupoVendedores.Add(vendedoreClientes);
                }
            }

            return lstGrupoVendedores;
        }

        public string Inserir(mVendedorClientes vendedoreClientes)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into vendedor_clientes("
                                                + "vendedor_id"
                                                + ",produto_id"
                                                + ",clientes"
                                                + ") values('"
                                                + vendedoreClientes.vendedor.id.ToString() + "'"
                                                + ",'" + (vendedoreClientes.produto!=null ?vendedoreClientes.produto.Id.ToString():"0") + "'"
                                                + ",'" + vendedoreClientes.clientes.ToString() + "'"
                                                + ")");

            return banco.Selecionar("select  * from vendedor_clientes order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mVendedorClientes vendedoreClientes)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update vendedor_clientes set "
                                                + " vendedor_id='" + vendedoreClientes.vendedor.id.ToString() + "'"
                                                + " ,clientes='" + vendedoreClientes.clientes.ToString() + "'"
                                                + " ,produto_id='" + (vendedoreClientes.produto!=null? vendedoreClientes.produto.Id.ToString():"0") + "'"
                                                + " where id=" + vendedoreClientes.Id.ToString());
        }
        public bool Deletar(mVendedorClientes vendedoreClientes)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from vendedor_clientes  "
                                                + " where id=" + vendedoreClientes.Id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select "
                         + " vc.id"
                         + "  ,v.nombre as Vendedor"
                         + "  ,vc.clientes as 'Tamano de Cartera de Clientes'"
                         + " from vendedor_clientes vc "
                         + " inner join vendedor v on vc.vendedor_id=v.id "
                         + " order by vc.id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                         + " vc.id"
                         + "  ,v.nombre"
                         + "  ,vc.clientes as 'Tamano de Cartera de Clientes'"
                         + " from vendedor_clientes vc "
                         + " inner join vendedor v on vc.vendedor_id=v.id "
                         + " order by vc.id desc ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}