﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cGrupoVendedores
    {
        public mGrupoVendedores RetornarGrupoVendedores(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mGrupoVendedores GrupoVendedores = new mGrupoVendedores();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from grupovendedores where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAGrupoVendedores.NOME
            GrupoVendedores = null;
            if (ds.Tables[0].Rows.Count>0)
            {
                GrupoVendedores = new mGrupoVendedores();
                GrupoVendedores.Id = Convert.ToInt32(id);
                GrupoVendedores.nombre = ds.Tables[0].Rows[0]["nomegrupovendedores"].ToString();
                GrupoVendedores.codigo = ds.Tables[0].Rows[0]["grupo"].ToString();
            }
            return GrupoVendedores;
        }

        public List<mGrupoVendedores> RetornarGrupoVendedores()
        {
            clsConexao banco = new clsConexao();
            mGrupoVendedores GrupoVendedores = new mGrupoVendedores();
            List<mGrupoVendedores> lstGrupoVendedores = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from grupovendedores ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstGrupoVendedores = new List<mGrupoVendedores>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    GrupoVendedores = new mGrupoVendedores();
                    GrupoVendedores.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    GrupoVendedores.nombre = ds.Tables[0].Rows[i]["nomegrupovendedores"].ToString();
                    GrupoVendedores.codigo = ds.Tables[0].Rows[i]["grupo"].ToString();
                    lstGrupoVendedores.Add(GrupoVendedores);
                }
            }

            return lstGrupoVendedores;
        }

        public string Inserir(mGrupoVendedores GrupoVendedores)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into grupovendedores("
                                                + "nomegrupovendedores"
                                                + ",grupo"
                                                + ") values('"
                                                + GrupoVendedores.nombre + "'"
                                                + ",'" + GrupoVendedores.codigo + "'"
                                                + ")");

            return banco.Selecionar("select  * from grupovendedores order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mGrupoVendedores GrupoVendedores)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update grupovendedores set "
                                                + " nomegrupovendedores='" + GrupoVendedores.nombre + "'"
                                                + " ,grupo='" + GrupoVendedores.codigo + "'"
                                                + " where id=" + GrupoVendedores.Id.ToString());
        }
        public bool Deletar(mGrupoVendedores GrupoVendedores)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from grupovendedores  "
                                                + " where id=" + GrupoVendedores.Id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select "
                         + " id"
                         + "  ,nomegrupovendedores"
                         + "  ,grupo"
                         + " from grupovendedores "
                         + " order by id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                         + " id"
                         + "  ,nomegrupovendedores"
                         + "  ,grupo"
                         + " from grupovendedores "
                         + " order by id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}
