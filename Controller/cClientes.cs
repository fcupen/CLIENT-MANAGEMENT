﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cClientes
    {
        public mClientes RetornarClientes(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mClientes Clientes = new mClientes();
            //cVendedor engineVendedor = new cVendedor();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from clientes where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAClientes.NOME
            Clientes.id = Convert.ToInt32(id);
            //Clientes.vendedor = (ds.Tables[0].Rows[0]["vendedor"].ToString()!=string.Empty?engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor"].ToString()):null);
            Clientes.calendario =(ds.Tables[0].Rows[0]["calendario"].ToString()!=string.Empty? Convert.ToDateTime(ds.Tables[0].Rows[0]["calendario"].ToString()):Convert.ToDateTime( "1900-01-01"));
            Clientes.rut = ds.Tables[0].Rows[0]["rut"].ToString();
            Clientes.razonsocial = ds.Tables[0].Rows[0]["razonsocial"].ToString();
            Clientes.direccion = ds.Tables[0].Rows[0]["direccion"].ToString();
            Clientes.comuna = ds.Tables[0].Rows[0]["comuna"].ToString();
            Clientes.ciudade = ds.Tables[0].Rows[0]["ciudade"].ToString();
            Clientes.region = ds.Tables[0].Rows[0]["region"].ToString();
            Clientes.sucursal = ds.Tables[0].Rows[0]["sucursal"].ToString();
            Clientes.contato = ds.Tables[0].Rows[0]["contato"].ToString();
            Clientes.telefone = ds.Tables[0].Rows[0]["telefone"].ToString();
            Clientes.email = ds.Tables[0].Rows[0]["email"].ToString();

            return Clientes;
        }

        public List<mClientes> RetornarClientes()
        {
            clsConexao banco = new clsConexao();
            mClientes Clientes = new mClientes();
            //cVendedor engineVendedor = new cVendedor();
            List<mClientes> lstClientes = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from clientes ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstClientes = new List<mClientes>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    Clientes = new mClientes();
                    Clientes.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    //Clientes.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    Clientes.calendario = (ds.Tables[0].Rows[i]["calendario"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["calendario"].ToString()) : Convert.ToDateTime("1900-01-01"));
                    Clientes.rut = ds.Tables[0].Rows[i]["rut"].ToString();
                    Clientes.razonsocial = ds.Tables[0].Rows[i]["razonsocial"].ToString();
                    Clientes.direccion = ds.Tables[0].Rows[i]["direccion"].ToString();
                    Clientes.comuna = ds.Tables[0].Rows[i]["comuna"].ToString();
                    Clientes.ciudade = ds.Tables[0].Rows[i]["ciudade"].ToString();
                    Clientes.region = ds.Tables[0].Rows[i]["region"].ToString();
                    Clientes.sucursal = ds.Tables[0].Rows[i]["sucursal"].ToString();
                    Clientes.contato = ds.Tables[0].Rows[i]["contato"].ToString();
                    Clientes.telefone = ds.Tables[0].Rows[i]["telefone"].ToString();
                    Clientes.email = ds.Tables[0].Rows[i]["email"].ToString();
                    lstClientes.Add(Clientes);
                }
            }

            return lstClientes;
        }

        public string Inserir(mClientes Clientes)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into clientes("
                                                //+ "vendedor"
                                                + "calendario"                                
                                                + ",rut"
                                                + ",razonsocial"
                                                + ",direccion"
                                                + ",comuna"
                                                + ",ciudade"
                                                + ",region"
                                                + ",sucursal"
                                                + ",contato"
                                                + ",telefone"
                                                + ",email"
                                                + ") values('"
                                                //+ (Clientes.vendedor!=null? Clientes.vendedor.id.ToString():"0") + "'"
                                                + Clientes.calendario.ToString("yyyy-MM-dd") + "'"
                                                + ",'" + Clientes.rut + "'"
                                                + ",'" + Clientes.razonsocial + "'"
                                                + ",'" + Clientes.direccion + "'"
                                                + ",'" + Clientes.comuna + "'"
                                                + ",'" + Clientes.ciudade + "'"
                                                + ",'" + Clientes.region + "'"
                                                + ",'" + Clientes.sucursal + "'"
                                                + ",'" + Clientes.contato + "'"
                                                + ",'" + Clientes.telefone + "'"
                                                + ",'" + Clientes.email + "'"
                                                + ")");

            return banco.Selecionar("select  * from clientes order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mClientes Clientes)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update clientes set "
                                                //+ " vendedor='" + (Clientes.vendedor!=null?Clientes.vendedor.id.ToString():"0") + "'"
                                                + " calendario='" + Clientes.calendario.ToString("yyyy-MM-dd") + "'"
                                                + " ,rut='" + Clientes.rut + "'"
                                                + " ,razonsocial='" + Clientes.razonsocial + "'"
                                                + " ,direccion='" + Clientes.direccion + "'"
                                                + " ,comuna='" + Clientes.comuna + "'"
                                                + " ,ciudade='" + Clientes.ciudade + "'"
                                                + " ,region='" + Clientes.region + "'"
                                                + " ,sucursal='" + Clientes.sucursal + "'"
                                                + " ,contato='" + Clientes.contato + "'"
                                                + " ,telefone='" + Clientes.telefone + "'"
                                                + " ,email='" + Clientes.email + "'"
                                                + " where id=" + Clientes.id.ToString());
        }
        public bool Deletar(mClientes Clientes)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from clientes  "
                                                + " where id=" + Clientes.id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select "
                         + " c.id as Id"
                         + "  ,if((v.nombre is null),'SIN ASIGNAR',v.nombre) as Vendedor"
                         //+ "  ,v.nombre as 'Nombre Vendedor'"
                         + "  ,c.calendario as Calendario"
                         + "  ,c.rut as Rut"
                         + "  ,c.razonsocial as 'Razon Social'"
                         + "  ,c.direccion as Direccion"
                         + "  ,c.comuna as Comuna"
                         + "  ,c.ciudade as Ciudad"
                         + "  ,c.region as Region"
                         + "  ,c.sucursal as Sucursal"
                         + "  ,c.contato as Contacto"
                         + "  ,c.telefone as Telefono"
                         + "  ,c.email as Mail"
                         + " from clientes c"
                         + " left join relacaovendedor r on c.id = r.cliente "
                         +" left join vendedor v on v.id = r.vendedor "
                         + " order by c.id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                         + " c.id as Id"
                         + "  ,if((v.nombre is null),'SIN ASIGNAR',v.nombre) as Vendedor"
                         + "  ,c.calendario as Calendario"
                         + "  ,c.rut as Rut"
                         + "  ,c.razonsocial as 'Razon Social'"
                         + "  ,c.direccion as Direccion"
                         + "  ,c.comuna as Comuna"
                         + "  ,c.ciudade as Ciudad"
                         + "  ,c.region as Region"
                         + "  ,c.sucursal as Sucursal"
                         + "  ,c.contato as Contacto"
                         + "  ,c.telefone as Telefono"
                         + "  ,c.email as Mail"
                         + " from clientes c"
                         + " left join relacaovendedor r on c.id = r.cliente "
                         + " left join vendedor v on v.id = r.vendedor "
                         + " order by c.id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        //select v.id,v.nombre from vendedor v where v.id not in (select distinct(vendedor_id) from informediario);
        public DataSet PopularGridPorVendedorInforme(int id)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            _sql = "select "
                        + " c.id as Id"
                        + "  ,if((v.nombre is null),'SIN ASIGNAR',v.nombre) as Vendedor"
                        + "  ,c.calendario as Calendario"
                        + "  ,c.rut as Rut"
                        + "  ,c.razonsocial as 'Razon Social'"
                        + "  ,c.direccion as Direccion"
                        + "  ,c.comuna as Comuna"
                        + "  ,c.ciudade as Ciudade"
                        + "  ,c.region as Region"
                        + "  ,c.sucursal as Sucursal"
                        + "  ,c.contato as Contacto"
                        + "  ,c.telefone as Telefono"
                        + "  ,c.email as Mail"
                        + " from clientes c"
                        + " inner join relacaovendedor r on c.id = r.cliente "
                        + " inner join vendedor v on v.id = r.vendedor "
                        + " where r.vendedor=" + id
                        + " and c.id not in (select distinct(cliente_id) from informediario) "
                        + " order by c.razonsocial asc ";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridPorVendedor(int id)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            
                _sql = "select "
                         + " c.id as Id"
                         + "  ,if((v.nombre is null),'SIN ASIGNAR',v.nombre) as Vendedor"
                    //+ "  ,v.nombre as 'Nombre Vendedor'"
                         + "  ,c.calendario as Calendario"
                         + "  ,c.rut as Rut"
                         + "  ,c.razonsocial as 'Razon Social'"
                         + "  ,c.direccion as Direccion"
                         + "  ,c.comuna as Comuna"
                         + "  ,c.ciudade as Ciudade"
                         + "  ,c.region as Region"
                         + "  ,c.sucursal as Sucursal"
                         + "  ,c.contato as Contacto"
                         + "  ,c.telefone as Telefono"
                         + "  ,c.email as Mail"
                         + " from clientes c"
                         + " inner join relacaovendedor r on c.id = r.cliente "
                         + " inner join vendedor v on v.id = r.vendedor "
                         + " where r.vendedor=" + id
                         + " order by c.razonsocial asc ";
            

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridPorVendedorSemAgendaCompleta(int id)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mes = DateTime.Now.Month.ToString();

            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            _sql = "select "
                     + " c.id as Id"
                     + "  ,if((v.nombre is null),'SIN ASIGNAR',v.nombre) as Vendedor"
                //+ "  ,v.nombre as 'Nombre Vendedor'"
                     + "  ,c.calendario as Calendario"
                     + "  ,c.rut as Rut"
                     + "  ,c.razonsocial as 'Razon Social'"
                     + "  ,c.direccion as Direccion"
                     + "  ,c.comuna as Comuna"
                     + "  ,c.ciudade as Ciudade"
                     + "  ,c.region as Region"
                     + "  ,c.sucursal as Sucursal"
                     + "  ,c.contato as Contacto"
                     + "  ,c.telefone as Telefono"
                     + "  ,c.email as Mail"
                     + " from clientes c"
                     + " inner join relacaovendedor r on c.id = r.cliente "
                     + " inner join vendedor v on v.id = r.vendedor "
                     + " where r.vendedor=" + id
                     + " and c.id not in ("
                     + " select "
                     + " i.cliente_id"
                     + " from informediario i"
                     + " inner join clientes c on i.cliente_id = c.id"
                     + " where i.ano='"+DateTime.Now.Year.ToString()+"' and i.mes='"+mesescrito+"'"
                     + " and i.vendedor_id="+id  ;
            for (int i = 1; i <= 31; i++)
            {
                _sql += " and i.d"+i.ToString()+"_valor<>''";
            }
                 _sql += ")"                                
                     + " order by c.razonsocial asc ";


            dsConsulta = banco.Selecionar(_sql);




            return dsConsulta;
        }


        public DataSet PopularGridPorVendedor_Planejados(int id, string ano, string mes, string dia)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            _sql = "select "
                     + " c.id as Id"
                     + "  ,if((v.nombre is null),'SIN ASIGNAR',v.nombre) as Vendedor"
                //+ "  ,v.nombre as 'Nombre Vendedor'"
                     + "  ,c.calendario as Calendario"
                     + "  ,c.rut as Rut"
                     + "  ,c.razonsocial as 'Razon Social'"
                     + "  ,c.direccion as Direccion"
                     + "  ,c.comuna as Comuna"
                     + "  ,c.ciudade as Ciudade"
                     + "  ,c.region as Region"
                     + "  ,c.sucursal as Sucursal"
                     + "  ,c.contato as Contacto"
                     + "  ,c.telefone as Telefono"
                     + "  ,c.email as Mail"
                     + " from clientes c"
                     + " inner join relacaovendedor r on c.id = r.cliente "
                     + " inner join vendedor v on v.id = r.vendedor "
                     + " where r.vendedor=" + id
                     + " and c.id in("
                     + " SELECT distinct(cliente_id) as id FROM informediario "
                     + " where ano='" + ano + "' and mes ='" + mesescrito + "' and vendedor_id=" + id + " and d" + dia + "_valor='P')"
                     + " order by c.razonsocial asc ";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridPorVendedor_No_Planejados(int id,string ano, string mes, string dia)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;
            string mesescrito = string.Empty;
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;


            _sql = "select "
                     + " c.id as Id"
                     + "  ,if((v.nombre is null),'SIN ASIGNAR',v.nombre) as Vendedor"
                //+ "  ,v.nombre as 'Nombre Vendedor'"
                     + "  ,c.calendario as Calendario"
                     + "  ,c.rut as Rut"
                     + "  ,c.razonsocial as 'Razon Social'"
                     + "  ,c.direccion as Direccion"
                     + "  ,c.comuna as Comuna"
                     + "  ,c.ciudade as Ciudade"
                     + "  ,c.region as Region"
                     + "  ,c.sucursal as Sucursal"
                     + "  ,c.contato as Contacto"
                     + "  ,c.telefone as Telefono"
                     + "  ,c.email as Mail"
                     + " from clientes c"
                     + " inner join relacaovendedor r on c.id = r.cliente "
                     + " inner join vendedor v on v.id = r.vendedor "
                     + " where r.vendedor=" + id
                     + " and c.id not in("
                     + " SELECT distinct(cliente_id) as id FROM informediario "
                     + " where ano='"+ano+"' and mes ='"+mesescrito+"' and vendedor_id="+id+" and d"+dia+"_valor<>'')"
                     + " order by c.razonsocial asc ";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridAsignados(string asignados)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (asignados=="sim")
            {
                _sql = "select "
                         + " distinct(c.id) as Id"
                         + "  ,c.calendario as Calendario"
                         + "  ,c.rut as Rut"
                         + "  ,c.razonsocial as 'Razon Social'"
                         + "  ,c.direccion as Direccion"
                         + "  ,c.comuna as Comuna"
                         + "  ,c.ciudade as Ciudade"
                         + "  ,c.region as Region"
                         + "  ,c.sucursal as Sucursal"
                         + "  ,c.contato as Contacto"
                         + "  ,c.telefone as Telefono"
                         + "  ,c.email as Mail"
                         + " from clientes c"
                         + " inner join relacaovendedor r on c.id = r.cliente "
                         + " order by c.razonsocial asc";
            }
            else if (asignados == "nao")
            {
                _sql = "select "
                         + " distinct(c.id) as Id"
                         + "  ,c.calendario as Calendario"
                         + "  ,c.rut as Rut"
                         + "  ,c.razonsocial as 'Razon Social'"
                         + "  ,c.direccion as Direccion"
                         + "  ,c.comuna as Comuna"
                         + "  ,c.ciudade as Ciudade"
                         + "  ,c.region as Region"
                         + "  ,c.sucursal as Sucursal"
                         + "  ,c.contato as Contacto"
                         + "  ,c.telefone as Telefono"
                         + "  ,c.email as Mail"
                         + " from clientes c"
                         + " left join relacaovendedor r on c.id = r.cliente "
                         + " where r.id is null "
                         + " order by c.razonsocial asc";
            }
            else
            {
                _sql = "select "
                         + " c.id as Id"
                         + "  ,c.calendario as Calendario"
                         + "  ,c.rut as Rut"
                         + "  ,c.razonsocial as 'Razon Social'"
                         + "  ,c.direccion as Direccion"
                         + "  ,c.comuna as Comuna"
                         + "  ,c.ciudade as Ciudade"
                         + "  ,c.region as Region"
                         + "  ,c.sucursal as Sucursal"
                         + "  ,c.contato as Contacto"
                         + "  ,c.telefone as Telefono"
                         + "  ,c.email as Mail"
                         + " from clientes c"
                         + " order by c.razonsocial asc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}