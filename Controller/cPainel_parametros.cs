﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cPainel_parametros
    {
        public mPainel_parametros RetornarPainel_parametros(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mPainel_parametros Painel_parametros = new mPainel_parametros();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from painel_parametros where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAPainel_parametros.NOME
            Painel_parametros.Id = Convert.ToInt32(id);
            Painel_parametros.horizontes_control = ds.Tables[0].Rows[0]["horizontes_control"].ToString();
            Painel_parametros.parametrosbdn = ds.Tables[0].Rows[0]["parametrosbdn"].ToString();

            return Painel_parametros;
        }

        public List<mPainel_parametros> RetornarPainel_parametros()
        {
            clsConexao banco = new clsConexao();
            mPainel_parametros Painel_parametros = new mPainel_parametros();
            List<mPainel_parametros> lstPainel_parametros = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from painel_parametros ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstPainel_parametros = new List<mPainel_parametros>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    Painel_parametros = new mPainel_parametros();
                    Painel_parametros.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    Painel_parametros.horizontes_control = ds.Tables[0].Rows[i]["horizontes_control"].ToString();
                    Painel_parametros.parametrosbdn = ds.Tables[0].Rows[i]["parametrosbdn"].ToString();
                    lstPainel_parametros.Add(Painel_parametros);
                }
            }

            return lstPainel_parametros;
        }

        public string Inserir(mPainel_parametros Painel_parametros)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into painel_parametros("
                                                + "horizontes_control"
                                                + ",parametrosbdn"
                                                + ") values('"
                                                + Painel_parametros.horizontes_control + "'"
                                                + ",'" + Painel_parametros.parametrosbdn + "'"
                                                + ")");

            return banco.Selecionar("select  * from painel_parametros order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mPainel_parametros Painel_parametros)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update painel_parametros set "
                                                + " horizontes_control='" + Painel_parametros.horizontes_control + "'"
                                                + " ,parametrosbdn='" + Painel_parametros.parametrosbdn + "'"
                                                + " where id=" + Painel_parametros.Id.ToString());
        }
        public bool Deletar(mPainel_parametros Painel_parametros)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from painel_parametros  "
                                                + " where id=" + Painel_parametros.Id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select "
                         + " id"
                         + "  ,horizontes_control"
                         + "  ,parametrosbdn"
                         + " from painel_parametros "
                         + " order by id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                         + " id"
                         + "  ,horizontes_control"
                         + "  ,parametrosbdn"
                         + " from painel_parametros "
                         + " order by id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}
