﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cBDN
    {
        public mBDN RetornaBDN(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mBDN BDN = new mBDN();
            cClientes engineCliente = new cClientes();
            cProduto engineProduto = new cProduto();
            cVendedor engineVendedor = new cVendedor();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from bdn where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            BDN.Id = Convert.ToInt32(id);
            BDN.fecha = (ds.Tables[0].Rows[0]["fecha"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[0]["fecha"].ToString()) : Convert.ToDateTime("1900-01-01"));
            BDN.cotizacion = ds.Tables[0].Rows[0]["cotizacion"].ToString();
            BDN.instituicion = (ds.Tables[0].Rows[0]["instituicion"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[0]["instituicion"].ToString()) : null);
            BDN.produto = (ds.Tables[0].Rows[0]["grupo_produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["grupo_produto"].ToString()) : null);
            BDN.vendedor = (ds.Tables[0].Rows[0]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor"].ToString()) : null);
            BDN.contacto = ds.Tables[0].Rows[0]["contacto"].ToString();
            BDN.descripcion_productos = ds.Tables[0].Rows[0]["descripcion_productos"].ToString();
            BDN.totalneto = (ds.Tables[0].Rows[0]["totalneto"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["totalneto"].ToString()) : 0);
            BDN.status = ds.Tables[0].Rows[0]["status"].ToString();
            return BDN;
        }

        public List<mBDN> RetornaBDN()
        {
            clsConexao banco = new clsConexao();
            mBDN BDN = new mBDN();
            cClientes engineCliente = new cClientes();
            cProduto engineProduto = new cProduto();
            cVendedor engineVendedor = new cVendedor();
            List<mBDN> lstMBDN = null;
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM bdn ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstMBDN = new List<mBDN>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    BDN = new mBDN();
                    BDN.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());                   
                    BDN.fecha = (ds.Tables[0].Rows[i]["fecha"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["fecha"].ToString()) : Convert.ToDateTime("1900-01-01"));
                    BDN.cotizacion = ds.Tables[0].Rows[i]["cotizacion"].ToString();
                    BDN.instituicion = (ds.Tables[0].Rows[i]["instituicion"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["instituicion"].ToString()) : null);
                    BDN.produto = (ds.Tables[0].Rows[i]["grupo_produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["grupo_produto"].ToString()) : null);
                    BDN.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    BDN.contacto = ds.Tables[0].Rows[i]["contacto"].ToString();
                    BDN.descripcion_productos = ds.Tables[0].Rows[i]["descripcion_productos"].ToString();
                    BDN.totalneto = (ds.Tables[0].Rows[i]["totalneto"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[i]["totalneto"].ToString()) : 0);
                    BDN.status = ds.Tables[0].Rows[i]["status"].ToString();
                    lstMBDN.Add(BDN);
                }
            }

            return lstMBDN;
        }        

        public string Inserir(mBDN bdn)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into bdn("
                                                + "fecha"
                                                + ",cotizacion"
                                                + ",instituicion"
                                                + ",contacto"
                                                + ",descripcion_productos"
                                                + ",totalneto"
                                                + ",status"
                                                + ",grupo_produto"
                                                + ",vendedor"      
                                                + ") values('"
                                                + bdn.fecha.ToString("yyyy-MM-dd") + "'"
                                                + ",'" + bdn.cotizacion + "'"
                                                + ",'" + (bdn.instituicion != null ? bdn.instituicion.id.ToString() : "0") + "'"
                                                + ",'" + bdn.contacto + "'"
                                                + ",'" + bdn.descripcion_productos + "'"
                                                + ",'" + bdn.totalneto.ToString().Replace(",", ".") + "'"
                                                + ",'" + bdn.status + "'"
                                                + ",'" + (bdn.produto != null ? bdn.produto.Id.ToString() : "0") + "'"
                                                + ",'" + (bdn.vendedor != null ? bdn.vendedor.id.ToString() : "0") + "'"
                                                + ")");

            return banco.Selecionar("select  * from bdn order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mBDN bdn)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update bdn set "
                                                + " fecha='" + bdn.fecha.ToString("yyyy-MM-dd") + "'"
                                                + " ,instituicion='" + (bdn.instituicion != null ? bdn.instituicion.id.ToString() : "0") + "'"
                                                + " ,cotizacion='" + bdn.cotizacion + "'"
                                                + " ,contacto='" + bdn.contacto + "'"
                                                + " ,descripcion_productos='" + bdn.descripcion_productos + "'"
                                                + " ,status='" + bdn.status + "'"
                                                + " ,totalneto='" + bdn.totalneto.ToString().Replace(",", ".") + "'"
                                                + " ,grupo_produto='" + (bdn.produto != null ? bdn.produto.Id.ToString() : "0") + "'"
                                                + " ,vendedor='" + (bdn.vendedor != null ? bdn.vendedor.id.ToString() : "0") + "'"
                                                + " where id=" + bdn.Id.ToString());
        }
        public bool Deletar(mBDN bdn)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from bdn  "
                                                + " where id=" + bdn.Id.ToString());
        }

        public DataSet PopularGrid(int limite,string idVendedor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select"
                        +" b.id"
                        +" ,b.fecha"
                        +" ,c.razonsocial"
                        + " ,p.nombre as Produto"
                        +" ,b.cotizacion"
                        +" ,b.contacto"
                        +" ,b.descripcion_productos"
                        +" ,b.totalneto"
                        +" ,b.status"
                        +" from bdn b"
                        +" inner join clientes c"
                        + " on b.instituicion = c.id"
                        + " inner join produto p on b.grupo_produto = p.id"
                        + " where b.vendedor="+idVendedor
                       + " order by c.razonsocial limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                      + " b.id"
                        + " ,b.fecha"
                        + " ,c.razonsocial"
                        + " ,p.nombre as Produto"
                        + " ,b.cotizacion"
                        + " ,b.contacto"
                        + " ,b.descripcion_productos"
                        + " ,b.totalneto"
                        + " ,b.status"
                        + " from bdn b"
                        + " inner join clientes c"
                        + " on b.instituicion = c.id"
                        + " inner join produto p on b.grupo_produto = p.id"
                        + " where b.vendedor=" + idVendedor
                       + " order by c.razonsocial  ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }


        public DataSet PopularGrid_2(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from bdn b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                + " b.id as 'id' "
                + ", b.fecha as 'FECHA' "                     
                     + " ,b.cotizacion as 'COTIZACIÓN' "
                     + " ,c.razonsocial 'INSTITUCIÓN' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses)>p.horizonte,'S','N') as Expirado "
                     + " from bdn b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"
                     + " order by b.totalneto asc;";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGrid_2PorVendedor(int idVendedor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from bdn b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                + " b.id as 'id' "     
                + ", b.fecha as 'FECHA' "                     
                     + " ,b.cotizacion as 'COTIZACION' "
                     + " ,c.razonsocial 'INSTITUCION' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,p.nombre as Produto"
                     + " ,b.descripcion_productos as 'DESCRIPCION PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses) > p.horizonte,'S','N') as Expirado "
                     + " from bdn b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"
                     + (idVendedor>0? " where b.vendedor=" + idVendedor: "")
                     + " order by b.totalneto asc;";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGrid_2PorVendedorDominante(int idVendedor,double media)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from bdn b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                + " b.id as 'ID' "
                + ", b.fecha as 'FECHA' "
                     + " ,b.cotizacion as 'COTIZACION' "
                     + " ,c.razonsocial 'INSTITUCION' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,p.nombre as PRODUCTO"
                     + " ,b.descripcion_productos as 'DESCRIPCION PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses)>p.horizonte,'S','N') as EXPIRADO "
                     + " from bdn b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"                     
                     + " where (select TIMESTAMPDIFF(MONTH, b.fecha, now()))<p.horizonte"
                     + (idVendedor>0? " and b.vendedor=" + idVendedor: "")
                     +" and b.totalneto>" + media.ToString("N2").Replace(",", "")
                     + " order by b.totalneto asc;";
            
            dsConsulta = banco.Selecionar(_sql);
            return dsConsulta;
        }

        public DataSet PopularGrid_2PorVendedorDominanteDireccion(int idVendedor, double media)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from bdn b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                + " b.id as 'ID' "
                +" ,v.nombre as VENDEDOR"
                + ", b.fecha as 'FECHA' "
                     + " ,b.cotizacion as 'COTIZACION' "
                     + " ,c.razonsocial 'INSTITUCION' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,p.nombre as Produto"
                     + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses)>p.horizonte,'S','N') as EXPIRADO "
                     + " from bdn b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"
                     + " inner join vendedor v on b.vendedor=v.id"
                     + " where (select TIMESTAMPDIFF(MONTH, b.fecha, now())) < p.horizonte"
                     + (idVendedor > 0 ? " and b.vendedor=" + idVendedor : "")
                     + " and b.totalneto > " + media.ToString("N2").Replace(",","")
                     + " order by b.totalneto asc;";

            dsConsulta = banco.Selecionar(_sql);
            return dsConsulta;
        }

        public string PopularGrid_2PorVendedorDominanteDireccionString(int idVendedor, double media)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from bdn b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                + " b.id as 'id' "
                + " ,v.nombre as Vendedor"
                + ", b.fecha as 'FECHA' "
                     + " ,b.cotizacion as 'COTIZACION' "
                     + " ,c.razonsocial 'INSTITUCION' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,p.nombre as Produto"
                     + " ,b.descripcion_productos as 'DESCRIPCION PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses)>p.horizonte,'S','N') as Expirado "
                     + " from bdn b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"
                     + " inner join vendedor v on b.vendedor=v.id"
                     + " where (select TIMESTAMPDIFF(MONTH, b.fecha, now()))<p.horizonte"
                     + (idVendedor > 0 ? " and b.vendedor=" + idVendedor : "")
                     + " and b.totalneto>" + media.ToString("N2").Replace(".", "").Replace(",", ".")
                     + " order by b.totalneto asc;";

            //dsConsulta = banco.Selecionar(_sql);
            return _sql;
        }
        public DataSet PopularGrid_2PorVendedorProdutoCalculo(int idVendedor,int idProduto)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from bdn b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                + " b.id as 'id' "
                + ", b.fecha as 'FECHA' "
                     + " ,b.cotizacion as 'COTIZACION' "
                     + " ,c.razonsocial 'INSTITUCION' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,p.nombre as Produto"
                     + " ,b.descripcion_productos as 'DESCRIPCION PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses)>p.horizonte,'S','N') as Expirado "
                     + " from bdn b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"
                     + " where b.vendedor=" + idVendedor
                     + " and p.id=" + idProduto
                     + " order by b.totalneto asc;";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGrid_2PorVendedorProduto(int idVendedor,int idProducto)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from bdn b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                + " b.id as 'id' "
                + ", b.fecha as 'FECHA' "
                     + " ,b.cotizacion as 'COTIZACION' "
                     + " ,c.razonsocial 'INSTITUCION' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,p.nombre as Produto"
                     + " ,b.descripcion_productos as 'DESCRIPCION PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses)>p.horizonte,'S','N') as Expirado "
                     + " from bdn b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"
                     + " where 1=1 "
                     + (idProducto>0? "and b.grupo_produto=" + idProducto:"")
                     + (idVendedor>0? " and b.vendedor=" + idVendedor:"")
                     + " order by b.totalneto asc;";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGrid_2PorVendedorProdutoDominanteDireccion(int idVendedor, int idProducto,double media)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from bdn b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                + " b.id as 'id' "
                + " ,v.nombre as Vendedor "
                + ", b.fecha as 'FECHA' "
                     + " ,b.cotizacion as 'COTIZACION' "
                     + " ,c.razonsocial 'INSTITUCION' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,p.nombre as Produto"
                     + " ,b.descripcion_productos as 'DESCRIPCION PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses)>p.horizonte,'S','N') as Expirado "
                     + " from bdn b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"
                     + " inner join vendedor v on b.vendedor=v.id"
                     + " where b.totalneto>" + media.ToString("N2").Replace(",", "")
                     + (idProducto>0? " and b.grupo_produto=" + idProducto:"")
                     + (idVendedor>0? " and b.vendedor=" + idVendedor:"")
                     + " and (select TIMESTAMPDIFF(MONTH, b.fecha, now())) < p.horizonte"                     
                     + " order by b.totalneto asc;";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        public DataSet PopularGrid_2PorVendedorProdutoDominante(int idVendedor, int idProducto, double media)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from bdn b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                + " b.id as 'id' "
                + ", b.fecha as 'FECHA' "
                     + " ,b.cotizacion as 'COTIZACION' "
                     + " ,c.razonsocial 'INSTITUCION' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,p.nombre as Produto"
                     + " ,b.descripcion_productos as 'DESCRIPCION PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses)>p.horizonte,'S','N') as Expirado "
                     + " from bdn b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"
                     + " where b.grupo_produto=" + idProducto
                     + (idVendedor > 0 ? " and b.vendedor=" + idVendedor : "")
                     + " and (select TIMESTAMPDIFF(MONTH, b.fecha, now()))<p.horizonte"
                     + " and b.totalneto>" + media.ToString("N2").Replace(",", "")
                     + " order by b.totalneto asc;";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

    }
}