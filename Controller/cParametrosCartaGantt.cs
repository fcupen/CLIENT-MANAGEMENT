﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cParametrosCartaGantt
    {
        public mParametrosCartaGantt RetornarParametrosCartaGantt(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mParametrosCartaGantt ParametrosCartaGantt = new mParametrosCartaGantt();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from parametrosCartaGantt where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAParametrosCartaGantt.NOME
            ParametrosCartaGantt.Id = Convert.ToInt32(id);
            ParametrosCartaGantt.ano = ds.Tables[0].Rows[0]["ano"].ToString();
            ParametrosCartaGantt.mes = ds.Tables[0].Rows[0]["mes"].ToString();
            ParametrosCartaGantt.dia = ds.Tables[0].Rows[0]["dia"].ToString();
            ParametrosCartaGantt.fecha = ds.Tables[0].Rows[0]["fecha"].ToString();            

            return ParametrosCartaGantt;
        }

        public List<mParametrosCartaGantt> RetornarParametrosCartaGantt()
        {
            clsConexao banco = new clsConexao();
            mParametrosCartaGantt ParametrosCartaGantt = new mParametrosCartaGantt();
            List<mParametrosCartaGantt> lstParametrosCartaGantt = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from parametrosCartaGantt ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstParametrosCartaGantt = new List<mParametrosCartaGantt>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    ParametrosCartaGantt = new mParametrosCartaGantt();
                    ParametrosCartaGantt.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    ParametrosCartaGantt.dia = ds.Tables[0].Rows[i]["dia"].ToString();
                    ParametrosCartaGantt.fecha = ds.Tables[0].Rows[i]["fecha"].ToString();
                    ParametrosCartaGantt.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    ParametrosCartaGantt.mes = ds.Tables[0].Rows[i]["mes"].ToString();

                    lstParametrosCartaGantt.Add(ParametrosCartaGantt);
                }
            }

            return lstParametrosCartaGantt;
        }

        public List<mParametrosCartaGantt> RetornarParametrosCartaGantt(string ano,string mes,string fecha)
        {
            clsConexao banco = new clsConexao();
            mParametrosCartaGantt ParametrosCartaGantt = new mParametrosCartaGantt();
            List<mParametrosCartaGantt> lstParametrosCartaGantt = null;
            DataSet ds = null;

            string mesescrito=string.Empty;
            //<asp:ListItem>Enero</asp:ListItem>
            //<asp:ListItem>Febrero</asp:ListItem>
            //<asp:ListItem>Marzo</asp:ListItem>
            //<asp:ListItem>Abril</asp:ListItem>
            //<asp:ListItem>Mayo</asp:ListItem>
            //<asp:ListItem>Junio</asp:ListItem>
            //<asp:ListItem>Julio</asp:ListItem>
            //<asp:ListItem>Agosto</asp:ListItem>
            //<asp:ListItem>Septiembre</asp:ListItem>
            //<asp:ListItem>Octubre</asp:ListItem>
            //<asp:ListItem>Noviembre</asp:ListItem>
            //<asp:ListItem>Diciembre</asp:ListItem>
            if (mes == "1")
                mesescrito = "Enero";
            else if (mes == "2")
                mesescrito = "Febrero";
            else if (mes == "3")
                mesescrito = "Marzo";
            else if (mes == "4")
                mesescrito = "Abril";
            else if (mes == "5")
                mesescrito = "Mayo";
            else if (mes == "6")
                mesescrito = "Junio";
            else if (mes == "7")
                mesescrito = "Julio";
            else if (mes == "8")
                mesescrito = "Agosto";
            else if (mes == "9")
                mesescrito = "Septiembre";
            else if (mes == "10")
                mesescrito = "Octubre";
            else if (mes == "11")
                mesescrito = "Noviembre";
            else if (mes == "12")
                mesescrito = "Diciembre";
            else
                mesescrito = mes;

            ds = banco.Selecionar("select * from parametrosCartaGantt where ano='" + ano + "' and mes='" + mesescrito + "' and fecha='" + fecha + "'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstParametrosCartaGantt = new List<mParametrosCartaGantt>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    ParametrosCartaGantt = new mParametrosCartaGantt();
                    ParametrosCartaGantt.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    ParametrosCartaGantt.dia = ds.Tables[0].Rows[i]["dia"].ToString();
                    ParametrosCartaGantt.fecha = ds.Tables[0].Rows[i]["fecha"].ToString();
                    ParametrosCartaGantt.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    ParametrosCartaGantt.mes = ds.Tables[0].Rows[i]["mes"].ToString();

                    lstParametrosCartaGantt.Add(ParametrosCartaGantt);
                }
            }

            return lstParametrosCartaGantt;
        }
        public string Inserir(mParametrosCartaGantt ParametrosCartaGantt)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into parametrosCartaGantt("
                                                + "ano"
                                                + ",mes"
                                                + ",dia"
                                                + ",fecha"                                               
                                                + ") values('"
                                                + ParametrosCartaGantt.ano + "'"
                                                + ",'" + ParametrosCartaGantt.mes+ "'"
                                                + ",'" + ParametrosCartaGantt.dia+ "'"
                                                + ",'" + ParametrosCartaGantt.fecha+ "'"
                                                + ")");

            return banco.Selecionar("select  * from parametrosCartaGantt order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mParametrosCartaGantt ParametrosCartaGantt)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update parametrosCartaGantt set "
                                                + " ano='" + ParametrosCartaGantt.ano+ "'"
                                                + " ,mes='" + ParametrosCartaGantt.mes+ "'"
                                                + " ,dia='" + ParametrosCartaGantt.dia+ "'"
                                                + " ,fecha='" + ParametrosCartaGantt.fecha+ "'"
                                                + " where id=" + ParametrosCartaGantt.Id.ToString());
        }
        public bool Deletar(mParametrosCartaGantt ParametrosCartaGantt)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from parametrosCartaGantt  "
                                                + " where id=" + ParametrosCartaGantt.Id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select mv.id"
                    + " ,mv.ano"    
                    + " ,mv.mes"
                        + " ,mv.dia"
                        + " ,mv.fecha"
                        + " from"
                        + " parametrosCartaGantt mv"
                         + " order by mv.id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select mv.id"
     + " ,mv.ano"
     + " ,mv.mes"
         + " ,mv.dia"
         + " ,mv.fecha"
         + " from"
         + " parametrosCartaGantt mv"
                         + " order by mv.id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridAnoMes(string ano, string mes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select mv.id"
                + " ,mv.ano"
                + " ,mv.mes"
                + " ,mv.dia"
                + " ,mv.fecha"
                + " from"
                + " parametrosCartaGantt mv"
                + " where mv.ano='"+ano + "' and mes='" + mes + "'"
                + " order by mv.id desc";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridAnoMesOrdenado(string ano, string mes)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select mv.id"
                + " ,mv.ano"
                + " ,mv.mes"
                + " ,mv.dia"
                + " ,mv.fecha"
                + " from"
                + " parametrosCartaGantt mv"
                + " where mv.ano='" + ano + "' and mes='" + mes + "'"
                + " order by CAST(SUBSTR(fecha FROM 1) AS UNSIGNED) asc";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        public DataSet PopularGridAnoMes(string ano, string mes,string fecha)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select mv.id"
                + " ,mv.ano"
                + " ,mv.mes"
                + " ,mv.dia"
                + " ,mv.fecha"
                + " from"
                + " parametrosCartaGantt mv"
                + " where mv.ano='" + ano + "' and mes='" + mes + "' and fecha='"+fecha+"'"
                + " order by mv.id desc";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}