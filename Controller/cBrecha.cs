﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cBrecha
    {
        public mBrecha RetornarBrecha(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mBrecha brecha = new mBrecha();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from brecha where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            brecha.Id = Convert.ToInt32(id);
            brecha.vendedor = (ds.Tables[0].Rows[0]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor"].ToString()) : null);
            brecha.produto = (ds.Tables[0].Rows[0]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["produto"].ToString()) : null);
            brecha.cliente = (ds.Tables[0].Rows[0]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[0]["cliente_id"].ToString()) : null);
            brecha.valor = (ds.Tables[0].Rows[0]["valor"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["valor"].ToString()) : 0);
            return brecha;
        }

        public List<mBrecha> RetornarBrecha(int vendedor, int produto,int cliente)
        {
            clsConexao banco = new clsConexao();
            mBrecha brecha = new mBrecha();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            List<mBrecha> lstbrecha = null;
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM brecha where vendedor=" + vendedor + (produto>0? " and produto=" + produto:"") + (cliente>0?" and cliente_id=" + cliente:""));

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstbrecha = new List<mBrecha>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    brecha = new mBrecha();
                    brecha.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    brecha.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    brecha.produto = (ds.Tables[0].Rows[i]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["produto"].ToString()) : null);
                    brecha.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    brecha.valor = (ds.Tables[i].Rows[i]["valor"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["valor"].ToString()) : 0);
                    lstbrecha.Add(brecha);
                }
            }

            return lstbrecha;
        }

        public List<mBrecha> RetornarBrecha(int vendedor, int produto)
        {
            clsConexao banco = new clsConexao();
            mBrecha brecha = new mBrecha();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            List<mBrecha> lstbrecha = null;
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM brecha where vendedor=" + vendedor + " and produto=" + produto + "  ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstbrecha = new List<mBrecha>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    brecha = new mBrecha();
                    brecha.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    brecha.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    brecha.produto = (ds.Tables[0].Rows[i]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["produto"].ToString()) : null);
                    brecha.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    brecha.valor = (ds.Tables[0].Rows[i]["valor"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[i]["valor"].ToString()) : 0);                    
                    lstbrecha.Add(brecha);
                }
            }

            return lstbrecha;
        }

        public List<mBrecha> RetornarBrechaPorData(int vendedor,string dataInicial, string dataFinal,int cliente_id)
        {
            clsConexao banco = new clsConexao();
            mBrecha brecha = new mBrecha();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            List<mBrecha> lstbrecha = null;
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM brecha where vendedor=" + vendedor + " and cliente_id=" + cliente_id );

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstbrecha = new List<mBrecha>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    brecha = new mBrecha();
                    brecha.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    brecha.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    brecha.produto = (ds.Tables[0].Rows[i]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["produto"].ToString()) : null);
                    brecha.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    brecha.valor = (ds.Tables[0].Rows[i]["valor"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[i]["valor"].ToString()) : 0);
                    lstbrecha.Add(brecha);
                }
            }

            return lstbrecha;
        }

        public List<mBrecha> RetornarBrecha()
        {
            clsConexao banco = new clsConexao();
            mBrecha brecha = new mBrecha();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            List<mBrecha> lstbrecha = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from asignacionbrecha ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstbrecha = new List<mBrecha>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    brecha = new mBrecha();
                    brecha.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    brecha.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    brecha.produto = (ds.Tables[0].Rows[i]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["produto"].ToString()) : null);
                    brecha.cliente = (ds.Tables[0].Rows[i]["cliente_id"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente_id"].ToString()) : null);
                    brecha.valor = (ds.Tables[i].Rows[i]["valor"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["valor"].ToString()) : 0);
                    lstbrecha.Add(brecha);
                }
            }

            return lstbrecha;
        }

        public string Inserir(mBrecha brecha)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into brecha("
                                                + "vendedor"
                                                + ",produto"
                                                + ",cliente_id"
                                                + ",valor"
                                                + ") values('"
                                                + (brecha.vendedor != null ? brecha.vendedor.id.ToString() : "0") + "'"
                                                + ",'" + (brecha.produto != null ? brecha.produto.Id.ToString() : "0") + "'"
                                                + ",'" + (brecha.cliente != null ? brecha.cliente.id.ToString() : "0") + "'"
                                                + ",'" + brecha.valor.ToString().Replace(",", ".") + "'"
                                                + ")");

            return banco.Selecionar("select  * from brecha order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mBrecha brecha)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update brecha set "
                                                + " vendedor='" + (brecha.vendedor != null ? brecha.vendedor.id.ToString() : "0") + "'"
                                                + " ,produto='" + (brecha.produto != null ? brecha.produto.Id.ToString() : "0") + "'"
                                                + " ,cliente_id='" + (brecha.cliente != null ? brecha.cliente.id.ToString() : "0") + "'"
                                                + " ,valor='" + brecha.valor.ToString().Replace(",", ".") + "'"
                                                + " where id=" + brecha.Id.ToString());
        }
        public bool Deletar(mBrecha brecha)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from brecha  "
                                                + " where id=" + brecha.Id.ToString());
        }

        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select"
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,p.nombre as Producto"
                       + " ,c.razonsocial as 'Razon Social'"
                       + " ,a.valor"                       
                       + " from brecha a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " left join produto p on a.produto = p.id"
                       + " left join clientes c on a.cliente_id = c.id"
                       + " order by v.nombre limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,p.nombre as Producto"
                       + " ,c.razonsocial as 'Razon Social'"
                       + " ,a.valor"
                       + " from brecha a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " left join produto p on a.produto = p.id"
                       + " left join clientes c on a.cliente_id = c.id"
                       + " order by v.nombre  ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }


        
    }
}