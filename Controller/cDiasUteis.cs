﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cDiasUteis
    {
        public mDiasUteis DiasUteis(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mDiasUteis diasUteis = new mDiasUteis();
            cDiasUteis enginediasUteis = new cDiasUteis();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from dias_uteis where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            diasUteis.id = Convert.ToInt32(id);
            diasUteis.ano = ds.Tables[0].Rows[0]["ano"].ToString();
            diasUteis.Enero_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Enero_diashabilesdetrabajo"].ToString();
            diasUteis.Febrero_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Febrero_diashabilesdetrabajo"].ToString();
            diasUteis.Marzo_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Marzo_diashabilesdetrabajo"].ToString();
            diasUteis.Abril_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Abril_diashabilesdetrabajo"].ToString();
            diasUteis.Mayo_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Mayo_diashabilesdetrabajo"].ToString();
            diasUteis.Junio_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Junio_diashabilesdetrabajo"].ToString();
            diasUteis.Julio_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Julio_diashabilesdetrabajo"].ToString();
            diasUteis.Agosto_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Agosto_diashabilesdetrabajo"].ToString();
            diasUteis.Septiembre_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Septiembre_diashabilesdetrabajo"].ToString();
            diasUteis.Octubre_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Octubre_diashabilesdetrabajo"].ToString();
            diasUteis.Noviembre_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Noviembre_diashabilesdetrabajo"].ToString();
            diasUteis.Diciembre_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Diciembre_diashabilesdetrabajo"].ToString();
            return diasUteis;
        }

        public List<mDiasUteis> AsignaciondiasUteis( int ano)
        {
            clsConexao banco = new clsConexao();
            mDiasUteis diasUteis = new mDiasUteis();
            cDiasUteis enginediasUteis = new cDiasUteis();
            List<mDiasUteis> lstdiasUteis = null;
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM dias_uteis where   ano='" + ano + "' ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstdiasUteis = new List<mDiasUteis>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    diasUteis = new mDiasUteis();
                    diasUteis.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    diasUteis.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    diasUteis.Enero_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Enero_diashabilesdetrabajo"].ToString();
                    diasUteis.Febrero_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Febrero_diashabilesdetrabajo"].ToString();
                    diasUteis.Marzo_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Marzo_diashabilesdetrabajo"].ToString();
                    diasUteis.Abril_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Abril_diashabilesdetrabajo"].ToString();
                    diasUteis.Mayo_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Mayo_diashabilesdetrabajo"].ToString();
                    diasUteis.Junio_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Junio_diashabilesdetrabajo"].ToString();
                    diasUteis.Julio_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Julio_diashabilesdetrabajo"].ToString();
                    diasUteis.Agosto_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Agosto_diashabilesdetrabajo"].ToString();
                    diasUteis.Septiembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Septiembre_diashabilesdetrabajo"].ToString();
                    diasUteis.Octubre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Octubre_diashabilesdetrabajo"].ToString();
                    diasUteis.Noviembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Noviembre_diashabilesdetrabajo"].ToString();
                    diasUteis.Diciembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Diciembre_diashabilesdetrabajo"].ToString();
                    lstdiasUteis.Add(diasUteis);
                }
            }

            return lstdiasUteis;
        }


        public List<mDiasUteis> AsignaciondiasUteis()
        {
            clsConexao banco = new clsConexao();
            mDiasUteis diasUteis = new mDiasUteis();
            cDiasUteis enginediasUteis = new cDiasUteis();
            List<mDiasUteis> lstdiasUteis = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from dias_uteis ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstdiasUteis = new List<mDiasUteis>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    diasUteis = new mDiasUteis();
                    diasUteis.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    diasUteis.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    diasUteis.Enero_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Enero_diashabilesdetrabajo"].ToString();
                    diasUteis.Febrero_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Febrero_diashabilesdetrabajo"].ToString();
                    diasUteis.Marzo_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Marzo_diashabilesdetrabajo"].ToString();
                    diasUteis.Abril_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Abril_diashabilesdetrabajo"].ToString();
                    diasUteis.Mayo_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Mayo_diashabilesdetrabajo"].ToString();
                    diasUteis.Junio_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Junio_diashabilesdetrabajo"].ToString();
                    diasUteis.Julio_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Julio_diashabilesdetrabajo"].ToString();
                    diasUteis.Agosto_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Agosto_diashabilesdetrabajo"].ToString();
                    diasUteis.Septiembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Septiembre_diashabilesdetrabajo"].ToString();
                    diasUteis.Octubre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Octubre_diashabilesdetrabajo"].ToString();
                    diasUteis.Noviembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Noviembre_diashabilesdetrabajo"].ToString();
                    diasUteis.Diciembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Diciembre_diashabilesdetrabajo"].ToString();
                    lstdiasUteis.Add(diasUteis);
                }
            }

            return lstdiasUteis;
        }

        public string Inserir(mDiasUteis diasUteis)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into dias_uteis("
                                                + "ano"
                                                + ",Enero_diashabilesdetrabajo"
                                                + ",Febrero_diashabilesdetrabajo"
                                                + ",Marzo_diashabilesdetrabajo"
                                                + ",Abril_diashabilesdetrabajo"
                                                + ",Mayo_diashabilesdetrabajo"
                                                + ",Junio_diashabilesdetrabajo"
                                                + ",Julio_diashabilesdetrabajo"
                                                + ",Agosto_diashabilesdetrabajo"
                                                + ",Septiembre_diashabilesdetrabajo"
                                                + ",Octubre_diashabilesdetrabajo"
                                                + ",Noviembre_diashabilesdetrabajo"
                                                + ",Diciembre_diashabilesdetrabajo"
                                                + ") values("
                                                + "'" + diasUteis.ano + "'"
                                                + ",'" + diasUteis.Enero_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Febrero_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Marzo_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Abril_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Mayo_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Junio_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Julio_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Agosto_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Septiembre_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Octubre_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Noviembre_diashabilesdetrabajo + "'"
                                                + ",'" + diasUteis.Diciembre_diashabilesdetrabajo + "'"
                                                + ")");
            
            return banco.Selecionar("select  * from dias_uteis order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mDiasUteis diasUteis)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update dias_uteis set "
                                                + " ano='" + diasUteis.ano + "'"
                                                + " ,Enero_diashabilesdetrabajo='" + diasUteis.Enero_diashabilesdetrabajo + "'"
                                                + " ,Febrero_diashabilesdetrabajo='" + diasUteis.Febrero_diashabilesdetrabajo + "'"
                                                + " ,Marzo_diashabilesdetrabajo='" + diasUteis.Marzo_diashabilesdetrabajo + "'"
                                                + " ,Abril_diashabilesdetrabajo='" + diasUteis.Abril_diashabilesdetrabajo + "'"
                                                + " ,Mayo_diashabilesdetrabajo='" + diasUteis.Mayo_diashabilesdetrabajo + "'"
                                                + " ,Junio_diashabilesdetrabajo='" + diasUteis.Junio_diashabilesdetrabajo + "'"
                                                + " ,Julio_diashabilesdetrabajo='" + diasUteis.Julio_diashabilesdetrabajo + "'"
                                                + " ,Agosto_diashabilesdetrabajo='" + diasUteis.Agosto_diashabilesdetrabajo + "'"
                                                + " ,Septiembre_diashabilesdetrabajo='" + diasUteis.Septiembre_diashabilesdetrabajo + "'"
                                                + " ,Octubre_diashabilesdetrabajo='" + diasUteis.Octubre_diashabilesdetrabajo + "'"
                                                + " ,Noviembre_diashabilesdetrabajo='" + diasUteis.Noviembre_diashabilesdetrabajo + "'"
                                                + " ,Diciembre_diashabilesdetrabajo='" + diasUteis.Diciembre_diashabilesdetrabajo + "'"
                                                + " where id=" + diasUteis.id.ToString());
        }
        public bool Deletar(mDiasUteis diasUteis)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from dias_uteis  "
                                                + " where id=" + diasUteis.id.ToString());
        }

        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select"
                       + " a.id"
                       + " ,a.Enero"
                       + " ,a.Febrero"
                       + " ,a.Marzo"
                       + " ,a.Abril"
                       + " ,a.Mayo"
                       + " ,a.Junio"
                       + " ,a.Julio"
                       + " ,a.Agosto"
                       + " ,a.Septiembre"
                       + " ,a.Octubre"
                       + " ,a.Noviembre"
                       + " ,a.Diciembre"
                       + " from asignaciondiasUteis a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " inner join produto p on a.produto = p.id"
                       + " order by v.nombre limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,a.Enero"
                       + " ,a.Febrero"
                       + " ,a.Marzo"
                       + " ,a.Abril"
                       + " ,a.Mayo"
                       + " ,a.Junio"
                       + " ,a.Julio"
                       + " ,a.Agosto"
                       + " ,a.Septiembre"
                       + " ,a.Octubre"
                       + " ,a.Noviembre"
                       + " ,a.Diciembre"
                       + " from asignaciondiasUteis a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " inner join produto p on a.produto = p.id"
                       + " order by v.nombre  ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }


        public DataSet PopularGrid()
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            _sql = "select "
                   + " a.id"
                   + " ,v.nombre as Vendedor"
                   + " ,a.Enero"
                   + " ,a.Febrero"
                   + " ,a.Marzo"
                   + " ,a.Abril"
                   + " ,a.Mayo"
                   + " ,a.Junio"
                   + " ,a.Julio"
                   + " ,a.Agosto"
                   + " ,a.Septiembre"
                   + " ,a.Octubre"
                   + " ,a.Noviembre"
                   + " ,a.Diciembre"
                   + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                   + " from asignaciondiasUteis a "
                   + " inner join vendedor v on a.vendedor = v.id "
                   + " inner join produto p on a.produto = p.id"
                   + " order by v.nombre  ";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridProduto(int produto)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            _sql = "select "
                   + " a.id"
                   + " ,v.nombre as Vendedor"
                   + " ,a.Enero"
                   + " ,a.Febrero"
                   + " ,a.Marzo"
                   + " ,a.Abril"
                   + " ,a.Mayo"
                   + " ,a.Junio"
                   + " ,a.Julio"
                   + " ,a.Agosto"
                   + " ,a.Septiembre"
                   + " ,a.Octubre"
                   + " ,a.Noviembre"
                   + " ,a.Diciembre"
                   + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                   + " from asignaciondiasUteis a "
                   + " inner join vendedor v on a.vendedor = v.id "
                   + " inner join produto p on a.produto = p.id"
                   + " where p.id=" + produto
                   + " order by v.nombre  ";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridFiltros(string pOpcao, string pValor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (pOpcao == "vendedor")
            {
                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,a.Enero"
                       + " ,a.Febrero"
                       + " ,a.Marzo"
                       + " ,a.Abril"
                       + " ,a.Mayo"
                       + " ,a.Junio"
                       + " ,a.Julio"
                       + " ,a.Agosto"
                       + " ,a.Septiembre"
                       + " ,a.Octubre"
                       + " ,a.Noviembre"
                       + " ,a.Diciembre"
                       + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                       + " from asignaciondiasUteis a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " inner join produto p on a.produto = p.id"
                       + " where p.id=" + pValor
                       + " order by v.nombre  ";
            }
            else if (pOpcao == "todos")
            {
                _sql = "select "
                      + " a.id"
                      + " ,v.nombre as Vendedor"
                      + " ,a.Enero"
                      + " ,a.Febrero"
                      + " ,a.Marzo"
                      + " ,a.Abril"
                      + " ,a.Mayo"
                      + " ,a.Junio"
                      + " ,a.Julio"
                      + " ,a.Agosto"
                      + " ,a.Septiembre"
                      + " ,a.Octubre"
                      + " ,a.Noviembre"
                      + " ,a.Diciembre"
                      + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                      + " from asignaciondiasUteis a "
                      + " inner join vendedor v on a.vendedor = v.id "
                      + " inner join produto p on a.produto = p.id"
                      + " order by v.nombre  ";
            }
            else if (pOpcao == "semmeta")
            {
                _sql = "select "
                      + " v.id"
                      + " ,v.nombre as Vendedor"
                      + " from asignaciondiasUteis a "
                      + " right join vendedor v on a.vendedor = v.id "
                      + " where a.id is null"
                      + " order by v.nombre  ";
            }
            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        public DataSet PopularGridPorVendedor(string codigoVendedor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            _sql = "select m.id"
                    + " ,m.ano"
                    + " ,m.mes"
                    + " ,m.diashabilesdetrabajodelmes"
                    + " ,v.nombre"
                    + " ,m.meta"
                    + " FROM asignaciondiasUteis m"
                    + " inner join vendedor v on m.vendedor=v.id ";

            if (codigoVendedor != "0")
                _sql += " where v.id=" + codigoVendedor;

            _sql += " order by id desc";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridPorAno(string ano)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            _sql = "select *"
                    + " FROM dias_uteis "
                    + " where ano='"+ano + "'";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}