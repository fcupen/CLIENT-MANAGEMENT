﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cRelacaoVendedor
    {
        public mRelacaoVendedor RetornarRelacaoVendedor(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mRelacaoVendedor RelacaoVendedor = new mRelacaoVendedor();
            cVendedor engineVendedor = new cVendedor();
            cClientes engineCliente = new cClientes();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from relacaovendedor where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            RelacaoVendedor.Id = Convert.ToInt32(id);
            RelacaoVendedor.vendedor = (ds.Tables[0].Rows[0]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor"].ToString()) : null);
            RelacaoVendedor.cliente = (ds.Tables[0].Rows[0]["cliente"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[0]["cliente"].ToString()) : null);

            return RelacaoVendedor;
        }

        public List<mRelacaoVendedor> RetornarRelacaoVendedor()
        {
            clsConexao banco = new clsConexao();
            mRelacaoVendedor RelacaoVendedor = new mRelacaoVendedor();
            cVendedor engineVendedor = new cVendedor();
            cClientes engineCliente = new cClientes();
            List<mRelacaoVendedor> lstRelacaoVendedor = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from relacaovendedor ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstRelacaoVendedor = new List<mRelacaoVendedor>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    RelacaoVendedor = new mRelacaoVendedor();
                    RelacaoVendedor.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    RelacaoVendedor.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    RelacaoVendedor.cliente = (ds.Tables[0].Rows[i]["cliente"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente"].ToString()) : null);
                    lstRelacaoVendedor.Add(RelacaoVendedor);
                }
            }

            return lstRelacaoVendedor;
        }

        public mRelacaoVendedor RetornarRelacaoVendedorPorVendedorCliente(string vendedor,string cliente)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mRelacaoVendedor RelacaoVendedor = new mRelacaoVendedor();
            cVendedor engineVendedor = new cVendedor();
            cClientes engineCliente = new cClientes();

            DataSet ds = null;

            if (vendedor == "0" || cliente=="0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from relacaovendedor where vendedor=" + vendedor + " and cliente=" + cliente );

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            if (ds.Tables[0].Rows.Count > 0)
            {
                RelacaoVendedor.Id = Convert.ToInt32(ds.Tables[0].Rows[0]["id"].ToString());
                RelacaoVendedor.vendedor = (ds.Tables[0].Rows[0]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor"].ToString()) : null);
                RelacaoVendedor.cliente = (ds.Tables[0].Rows[0]["cliente"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[0]["cliente"].ToString()) : null);
                return RelacaoVendedor;
            }
            else
            {
                return null;
            }            
        }

        public List<mRelacaoVendedor> RetornarRelacaoVendedorPorVendedor(string vendedor_id)
        {
            clsConexao banco = new clsConexao();
            mRelacaoVendedor RelacaoVendedor = new mRelacaoVendedor();
            cVendedor engineVendedor = new cVendedor();
            cClientes engineCliente = new cClientes();
            List<mRelacaoVendedor> lstRelacaoVendedor = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from relacaovendedor where vendedor="+vendedor_id);

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstRelacaoVendedor = new List<mRelacaoVendedor>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    RelacaoVendedor = new mRelacaoVendedor();
                    RelacaoVendedor.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    RelacaoVendedor.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    RelacaoVendedor.cliente = (ds.Tables[0].Rows[i]["cliente"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente"].ToString()) : null);
                    lstRelacaoVendedor.Add(RelacaoVendedor);
                }
            }

            return lstRelacaoVendedor;
        }

        public string Inserir(mRelacaoVendedor RelacaoVendedor)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into relacaovendedor("
                                                + "vendedor"
                                                + ",cliente"
                                                + ") values('"
                                                + (RelacaoVendedor.vendedor != null ? RelacaoVendedor.vendedor.id.ToString() : "0") + "'"
                                                + ",'" + (RelacaoVendedor.cliente != null ? RelacaoVendedor.cliente.id.ToString() : "0") + "'"
                                                + ")");

            return banco.Selecionar("select  * from relacaovendedor order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mRelacaoVendedor RelacaoVendedor)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update relacaovendedor set "
                                                + " vendedor='" + (RelacaoVendedor.vendedor != null ? RelacaoVendedor.vendedor.id.ToString() : "0") + "'"
                                                + " ,cliente='" + (RelacaoVendedor.cliente != null ? RelacaoVendedor.cliente.id.ToString() : "0") + "'"
                                                + " where id=" + RelacaoVendedor.Id.ToString());
        }
        public bool Deletar(mRelacaoVendedor RelacaoVendedor)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from relacaovendedor  "
                                                + " where id=" + RelacaoVendedor.Id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select "
                        + " r.id as Id"
                        + "    ,v.nombre as Nombre"
                        + " ,c.razonsocial as 'Razon Social'"
                        + " from relacaovendedor r"
                        + " inner join clientes c on r.cliente = c.id"
                        + " inner join vendedor v on r.vendedor = v.id "
                         + " order by id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                        + " r.id as Id"
                        + "    ,v.nombre as Nombre"
                        + " ,c.razonsocial as 'Razon Social'"
                        + " from relacaovendedor r"
                        + " inner join clientes c on r.cliente = c.id"
                        + " inner join vendedor v on r.vendedor = v.id "
                         + " order by id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGrid_2(string pOpcao,string pValorParametro)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (pOpcao=="todosvendedores")
            {
                _sql = "select"
                         + " r.id as Id"
                         + " ,v.nombre as Vendedor"
                         + " ,v.calendario as Calendario"
                         + " ,c.rut as Rut"
                         + " ,c.razonsocial as 'Razon Social'"
                         + " ,c.direccion as Direccion"
                         + " ,c.comuna as Comuna"
                         + " ,c.ciudade as Ciudad"
                         + " ,c.region as Region"
                         + " ,c.sucursal as Sucursal"                         
                         + " from relacaovendedor r"
                         + " inner join clientes c on r.cliente = c.id"
                         + " inner join vendedor v on r.vendedor = v.id";
            }
            else if(pOpcao=="vendedorpornome")
            {
                _sql = "select"
                         + " r.id as Id"
                         + " ,v.nombre as Vendedor"
                         + " ,v.calendario as Calendario"
                         + " ,c.rut as Rut"
                         + " ,c.razonsocial as 'Razon Social'"
                         + " ,c.direccion as Direccion"
                         + " ,c.comuna as Comuna"
                         + " ,c.ciudade as Ciudad"
                         + " ,c.region as Region"
                         + " ,c.sucursal as Sucursal"
                         + " from relacaovendedor r"
                         + " inner join clientes c on r.cliente = c.id"
                         + " inner join vendedor v on r.vendedor = v.id"
                         + " where v.id =" + pValorParametro;
            }
            else if (pOpcao == "todosClientes")
            {
                _sql = "select"
                         + " r.id as Id"
                         + " ,v.nombre as Vendedor"
                         + " ,c.rut as Rut"
                         + " ,c.razonsocial as 'Razon Social'"
                         + " ,c.direccion as Direccion"
                         + " ,c.comuna as Comuna"
                         + " ,c.ciudade as Ciudad"
                         + " ,c.region as Region"
                         + " ,c.sucursal as Sucursal"
                         + " from relacaovendedor r"
                         + " right join clientes c on r.cliente = c.id"
                         + " left join vendedor v on r.vendedor = v.id";
            }
            else if (pOpcao == "clientesSemVendedor")
            {
                _sql = "select"
                                      + " r.id as Id"
                                      + " ,c.rut as Rut"
                                      + " ,c.razonsocial as 'Razon Social'"
                                      + " ,c.direccion as Direccion"
                                      + " ,c.comuna as Comuna"
                                      + " ,c.ciudade as Ciudad"
                                      + " ,c.region as Region"
                                      + " ,c.sucursal as Sucursal"
                                      + " from relacaovendedor r"
                                      + " right join clientes c on r.cliente = c.id"
                                      + " where r.id is null";
            }   

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}