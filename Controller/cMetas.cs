﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cMetas
    {
        public mMetas Asignacionmetas(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mMetas metas = new mMetas();
            cMetas engineMetas = new cMetas();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from asignacionmetas where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            metas.id = Convert.ToInt32(id);
            metas.vendedor = (ds.Tables[0].Rows[0]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor"].ToString()) : null);
            metas.produto = (ds.Tables[0].Rows[0]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["produto"].ToString()) : null);
            metas.ano = ds.Tables[0].Rows[0]["ano"].ToString();
            metas.Enero = (ds.Tables[0].Rows[0]["Enero"].ToString()!=string.Empty?Convert.ToDecimal(ds.Tables[0].Rows[0]["Enero"].ToString()):0);
            metas.Febrero = (ds.Tables[0].Rows[0]["Febrero"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Febrero"].ToString()) : 0);
            metas.Marzo = (ds.Tables[0].Rows[0]["Marzo"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Marzo"].ToString()) : 0);
            metas.Abril = (ds.Tables[0].Rows[0]["Abril"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Abril"].ToString()) : 0);
            metas.Mayo = (ds.Tables[0].Rows[0]["Mayo"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Mayo"].ToString()) : 0);
            metas.Junio = (ds.Tables[0].Rows[0]["Junio"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Junio"].ToString()) : 0);
            metas.Julio = (ds.Tables[0].Rows[0]["Julio"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Julio"].ToString()) : 0);
            metas.Agosto = (ds.Tables[0].Rows[0]["Agosto"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Agosto"].ToString()) : 0);
            metas.Septiembre = (ds.Tables[0].Rows[0]["Septiembre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Septiembre"].ToString()) : 0);
            metas.Octubre = (ds.Tables[0].Rows[0]["Octubre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Octubre"].ToString()) : 0);
            metas.Noviembre = (ds.Tables[0].Rows[0]["Noviembre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Noviembre"].ToString()) : 0);
            metas.Diciembre = (ds.Tables[0].Rows[0]["Diciembre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["Diciembre"].ToString()) : 0);
            metas.Enero_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Enero_diashabilesdetrabajo"].ToString();
            metas.Febrero_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Febrero_diashabilesdetrabajo"].ToString();
            metas.Marzo_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Marzo_diashabilesdetrabajo"].ToString();
            metas.Abril_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Abril_diashabilesdetrabajo"].ToString();
            metas.Mayo_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Mayo_diashabilesdetrabajo"].ToString();
            metas.Junio_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Junio_diashabilesdetrabajo"].ToString();
            metas.Julio_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Julio_diashabilesdetrabajo"].ToString();
            metas.Agosto_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Agosto_diashabilesdetrabajo"].ToString();
            metas.Septiembre_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Septiembre_diashabilesdetrabajo"].ToString();
            metas.Octubre_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Octubre_diashabilesdetrabajo"].ToString();
            metas.Noviembre_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Noviembre_diashabilesdetrabajo"].ToString();
            metas.Diciembre_diashabilesdetrabajo = ds.Tables[0].Rows[0]["Diciembre_diashabilesdetrabajo"].ToString();
            return metas;
        }

        public List<mMetas> Asignacionmetas(int vendedor,int produto)
        {
            clsConexao banco = new clsConexao();
            mMetas metas = new mMetas();
            cMetas engineMetas = new cMetas();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            List<mMetas> lstMetas = null;
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM asignacionmetas where vendedor="+vendedor+" and produto="+produto+"  ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstMetas = new List<mMetas>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    metas = new mMetas();
                    metas.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    metas.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    metas.produto = (ds.Tables[0].Rows[i]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["produto"].ToString()) : null);
                    metas.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    metas.Enero = (ds.Tables[i].Rows[i]["Enero"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Enero"].ToString()) : 0);
                    metas.Febrero = (ds.Tables[i].Rows[i]["Febrero"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Febrero"].ToString()) : 0);
                    metas.Marzo = (ds.Tables[i].Rows[i]["Marzo"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Marzo"].ToString()) : 0);
                    metas.Abril = (ds.Tables[i].Rows[i]["Abril"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Abril"].ToString()) : 0);
                    metas.Mayo = (ds.Tables[i].Rows[i]["Mayo"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Mayo"].ToString()) : 0);
                    metas.Junio = (ds.Tables[i].Rows[i]["Junio"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Junio"].ToString()) : 0);
                    metas.Julio = (ds.Tables[i].Rows[i]["Julio"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Julio"].ToString()) : 0);
                    metas.Agosto = (ds.Tables[i].Rows[i]["Agosto"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Agosto"].ToString()) : 0);
                    metas.Septiembre = (ds.Tables[i].Rows[i]["Septiembre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Septiembre"].ToString()) : 0);
                    metas.Octubre = (ds.Tables[i].Rows[i]["Octubre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Octubre"].ToString()) : 0);
                    metas.Noviembre = (ds.Tables[i].Rows[i]["Noviembre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Noviembre"].ToString()) : 0);
                    metas.Diciembre = (ds.Tables[i].Rows[i]["Diciembre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Diciembre"].ToString()) : 0);
                    metas.Enero_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Enero_diashabilesdetrabajo"].ToString();
                    metas.Febrero_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Febrero_diashabilesdetrabajo"].ToString();
                    metas.Marzo_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Marzo_diashabilesdetrabajo"].ToString();
                    metas.Abril_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Abril_diashabilesdetrabajo"].ToString();
                    metas.Mayo_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Mayo_diashabilesdetrabajo"].ToString();
                    metas.Junio_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Junio_diashabilesdetrabajo"].ToString();
                    metas.Julio_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Julio_diashabilesdetrabajo"].ToString();
                    metas.Agosto_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Agosto_diashabilesdetrabajo"].ToString();
                    metas.Septiembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Septiembre_diashabilesdetrabajo"].ToString();
                    metas.Octubre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Octubre_diashabilesdetrabajo"].ToString();
                    metas.Noviembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Noviembre_diashabilesdetrabajo"].ToString();
                    metas.Diciembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Diciembre_diashabilesdetrabajo"].ToString();
                    lstMetas.Add(metas);
                }
            }

            return lstMetas;
        }


        public List<mMetas> Asignacionmetas()
        {
            clsConexao banco = new clsConexao();
            mMetas metas = new mMetas();
            cMetas engineMetas = new cMetas();
            cVendedor engineVendedor= new cVendedor();
            cProduto engineProduto = new cProduto();
            List<mMetas> lstMetas = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from asignacionmetas ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstMetas = new List<mMetas>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    metas = new mMetas();
                    metas.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    metas.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    metas.produto = (ds.Tables[0].Rows[i]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["produto"].ToString()) : null);
                    metas.ano = ds.Tables[0].Rows[i]["ano"].ToString();
                    metas.Enero = (ds.Tables[i].Rows[i]["Enero"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Enero"].ToString()) : 0);
                    metas.Febrero = (ds.Tables[i].Rows[i]["Febrero"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Febrero"].ToString()) : 0);
                    metas.Marzo = (ds.Tables[i].Rows[i]["Marzo"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Marzo"].ToString()) : 0);
                    metas.Abril = (ds.Tables[i].Rows[i]["Abril"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Abril"].ToString()) : 0);
                    metas.Mayo = (ds.Tables[i].Rows[i]["Mayo"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Mayo"].ToString()) : 0);
                    metas.Junio = (ds.Tables[i].Rows[i]["Junio"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Junio"].ToString()) : 0);
                    metas.Julio = (ds.Tables[i].Rows[i]["Julio"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Julio"].ToString()) : 0);
                    metas.Agosto = (ds.Tables[i].Rows[i]["Agosto"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Agosto"].ToString()) : 0);
                    metas.Septiembre = (ds.Tables[i].Rows[i]["Septiembre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Septiembre"].ToString()) : 0);
                    metas.Octubre = (ds.Tables[i].Rows[i]["Octubre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Octubre"].ToString()) : 0);
                    metas.Noviembre = (ds.Tables[i].Rows[i]["Noviembre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Noviembre"].ToString()) : 0);
                    metas.Diciembre = (ds.Tables[i].Rows[i]["Diciembre"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[i].Rows[i]["Diciembre"].ToString()) : 0);
                    metas.Enero_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Enero_diashabilesdetrabajo"].ToString();
                    metas.Febrero_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Febrero_diashabilesdetrabajo"].ToString();
                    metas.Marzo_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Marzo_diashabilesdetrabajo"].ToString();
                    metas.Abril_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Abril_diashabilesdetrabajo"].ToString();
                    metas.Mayo_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Mayo_diashabilesdetrabajo"].ToString();
                    metas.Junio_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Junio_diashabilesdetrabajo"].ToString();
                    metas.Julio_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Julio_diashabilesdetrabajo"].ToString();
                    metas.Agosto_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Agosto_diashabilesdetrabajo"].ToString();
                    metas.Septiembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Septiembre_diashabilesdetrabajo"].ToString();
                    metas.Octubre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Octubre_diashabilesdetrabajo"].ToString();
                    metas.Noviembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Noviembre_diashabilesdetrabajo"].ToString();
                    metas.Diciembre_diashabilesdetrabajo = ds.Tables[i].Rows[i]["Diciembre_diashabilesdetrabajo"].ToString();
                    lstMetas.Add(metas);
                }
            }

            return lstMetas;
        }

        public string Inserir(mMetas metas)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into asignacionmetas("
                                                + "vendedor"
                                                + ",produto"
                                                + ",ano"
                                                + ",Enero"
                                                + ",Febrero"
                                                + ",Marzo"
                                                + ",Abril"
                                                + ",Mayo"
                                                + ",Junio"
                                                + ",Julio"
                                                + ",Agosto"
                                                + ",Septiembre"
                                                + ",Octubre"
                                                + ",Noviembre"
                                                + ",Diciembre"
                                                + ",Enero_diashabilesdetrabajo"
                                                + ",Febrero_diashabilesdetrabajo"
                                                + ",Marzo_diashabilesdetrabajo"
                                                + ",Abril_diashabilesdetrabajo"
                                                + ",Mayo_diashabilesdetrabajo"
                                                + ",Junio_diashabilesdetrabajo"
                                                + ",Julio_diashabilesdetrabajo"
                                                + ",Agosto_diashabilesdetrabajo"
                                                + ",Septiembre_diashabilesdetrabajo"
                                                + ",Octubre_diashabilesdetrabajo"
                                                + ",Noviembre_diashabilesdetrabajo"
                                                + ",Diciembre_diashabilesdetrabajo"
                                                + ") values('"
                                                + (metas.vendedor != null ? metas.vendedor.id.ToString() : "0") + "'"
                                                + ",'" + (metas.produto!= null ? metas.produto.Id.ToString() : "0") + "'"
                                                + ",'" + metas.ano + "'"
                                                + ",'" + metas.Enero.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Febrero.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Marzo.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Abril.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Mayo.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Junio.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Julio.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Agosto.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Septiembre.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Octubre.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Noviembre.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Diciembre.ToString().Replace(",", ".") + "'"
                                                + ",'" + metas.Enero_diashabilesdetrabajo + "'"
                                                + ",'" + metas.Febrero_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Marzo_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Abril_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Mayo_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Junio_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Julio_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Agosto_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Septiembre_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Octubre_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Noviembre_diashabilesdetrabajo+ "'"
                                                + ",'" + metas.Diciembre_diashabilesdetrabajo+ "'"
                                                + ")");

            return banco.Selecionar("select  * from asignacionmetas order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mMetas metas)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update asignacionmetas set "
                                                + " vendedor='" + (metas.vendedor != null ? metas.vendedor.id.ToString() : "0") + "'"
                                                + " ,produto='" + (metas.produto != null ? metas.produto.Id.ToString() : "0") + "'"
                                                + " ,ano='" + metas.ano +"'"
                                                + " ,Enero='" + metas.Enero.ToString().Replace(",", ".") + "'"
                                                + " ,Febrero='" + metas.Febrero.ToString().Replace(",", ".") + "'"
                                                + " ,Marzo='" + metas.Marzo.ToString().Replace(",", ".") + "'"
                                                + " ,Abril='" + metas.Abril.ToString().Replace(",", ".") + "'"
                                                + " ,Mayo='" + metas.Mayo.ToString().Replace(",", ".") + "'"
                                                + " ,Junio='" + metas.Junio.ToString().Replace(",", ".") + "'"
                                                + " ,Julio='" + metas.Julio.ToString().Replace(",", ".") + "'"
                                                + " ,Agosto='" + metas.Agosto.ToString().Replace(",", ".") + "'"
                                                + " ,Septiembre='" + metas.Septiembre.ToString().Replace(",", ".") + "'"
                                                + " ,Octubre='" + metas.Octubre.ToString().Replace(",", ".") + "'"
                                                + " ,Noviembre='" + metas.Noviembre.ToString().Replace(",", ".") + "'"
                                                + " ,Diciembre='" + metas.Diciembre.ToString().Replace(",", ".") + "'"
                                                + " ,Enero_diashabilesdetrabajo='" + metas.Enero_diashabilesdetrabajo+ "'"
                                                + " ,Febrero_diashabilesdetrabajo='" + metas.Febrero_diashabilesdetrabajo+ "'"
                                                + " ,Marzo_diashabilesdetrabajo='" + metas.Marzo_diashabilesdetrabajo+ "'"
                                                + " ,Abril_diashabilesdetrabajo='" + metas.Abril_diashabilesdetrabajo+ "'"
                                                + " ,Mayo_diashabilesdetrabajo='" + metas.Mayo_diashabilesdetrabajo+ "'"
                                                + " ,Junio_diashabilesdetrabajo='" + metas.Junio_diashabilesdetrabajo+ "'"
                                                + " ,Julio_diashabilesdetrabajo='" + metas.Julio_diashabilesdetrabajo+ "'"
                                                + " ,Agosto_diashabilesdetrabajo='" + metas.Agosto_diashabilesdetrabajo+ "'"
                                                + " ,Septiembre_diashabilesdetrabajo='" + metas.Septiembre_diashabilesdetrabajo+ "'"
                                                + " ,Octubre_diashabilesdetrabajo='" + metas.Octubre_diashabilesdetrabajo+ "'"
                                                + " ,Noviembre_diashabilesdetrabajo='" + metas.Noviembre_diashabilesdetrabajo+ "'"
                                                + " ,Diciembre_diashabilesdetrabajo='" + metas.Diciembre_diashabilesdetrabajo+ "'"
                                                + " where id=" + metas.id.ToString());
        }
        public bool Deletar(mMetas metas)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from asignacionmetas  "
                                                + " where id=" + metas.id.ToString());
        }

        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select"
                       +" a.id"
                       +" ,v.nombre as Vendedor"
                       +" ,a.Enero"
                       +" ,a.Febrero"
                       +" ,a.Marzo"
                       +" ,a.Abril"
                       +" ,a.Mayo"
                       +" ,a.Junio"
                       +" ,a.Julio"
                       +" ,a.Agosto"
                       +" ,a.Septiembre"
                       +" ,a.Octubre"
                       +" ,a.Noviembre"
                       +" ,a.Diciembre"
                       +" from asignacionmetas a "
                       +" inner join vendedor v on a.vendedor = v.id "
                       +" inner join produto p on a.produto = p.id"
                       + " order by v.nombre limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,a.Enero"
                       + " ,a.Febrero"
                       + " ,a.Marzo"
                       + " ,a.Abril"
                       + " ,a.Mayo"
                       + " ,a.Junio"
                       + " ,a.Julio"
                       + " ,a.Agosto"
                       + " ,a.Septiembre"
                       + " ,a.Octubre"
                       + " ,a.Noviembre"
                       + " ,a.Diciembre"
                       + " from asignacionmetas a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " inner join produto p on a.produto = p.id"
                       + " order by v.nombre  ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }


        public DataSet PopularGrid()
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,a.Enero"
                       + " ,a.Febrero"
                       + " ,a.Marzo"
                       + " ,a.Abril"
                       + " ,a.Mayo"
                       + " ,a.Junio"
                       + " ,a.Julio"
                       + " ,a.Agosto"
                       + " ,a.Septiembre"
                       + " ,a.Octubre"
                       + " ,a.Noviembre"
                       + " ,a.Diciembre"
                       + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                       + " from asignacionmetas a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " inner join produto p on a.produto = p.id"
                       + " order by v.nombre  ";
            

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridProduto(int produto)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            _sql = "select "
                   + " a.id"
                   + " ,v.nombre as Vendedor"
                   + " ,a.Enero"
                   + " ,a.Febrero"
                   + " ,a.Marzo"
                   + " ,a.Abril"
                   + " ,a.Mayo"
                   + " ,a.Junio"
                   + " ,a.Julio"
                   + " ,a.Agosto"
                   + " ,a.Septiembre"
                   + " ,a.Octubre"
                   + " ,a.Noviembre"
                   + " ,a.Diciembre"
                   + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                   + " from asignacionmetas a "
                   + " inner join vendedor v on a.vendedor = v.id "
                   + " inner join produto p on a.produto = p.id"
                   + " where p.id=" + produto
                   + " order by v.nombre  ";


            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridFiltros(string pOpcao,string pValor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (pOpcao == "grupoprodutos")
            {
                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,p.nombre as Grupo"
                       + " ,a.Enero"
                       + " ,d.enero_diashabilesdetrabajo as 'Dias Enero'"
                       + " ,a.Febrero"
                       + " ,d.Febrero_diashabilesdetrabajo as 'Dias Febrero'"
                       + " ,a.Marzo"
                       + " ,d.Marzo_diashabilesdetrabajo as 'Dias Marzo'"
                       + " ,a.Abril"
                       + " ,d.Abril_diashabilesdetrabajo as 'Dias Abril'"
                       + " ,a.Mayo"
                       + " ,d.Mayo_diashabilesdetrabajo as 'Dias Mayo'"
                       + " ,a.Junio"
                       + " ,d.Junio_diashabilesdetrabajo as 'Dias Junio'"
                       + " ,a.Julio"
                       + " ,d.Julio_diashabilesdetrabajo as 'Dias Julio'"
                       + " ,a.Agosto"
                       + " ,d.Agosto_diashabilesdetrabajo as 'Dias Agosto'"
                       + " ,a.Septiembre"
                       + " ,d.Septiembre_diashabilesdetrabajo as 'Dias Septiembre'"
                       + " ,a.Octubre"
                       + " ,d.Octubre_diashabilesdetrabajo as 'Dias Octubre'"
                       + " ,a.Noviembre"
                       + " ,d.Noviembre_diashabilesdetrabajo as 'Dias Noviembre'"
                       + " ,a.Diciembre"
                       + " ,d.Diciembre_diashabilesdetrabajo as 'Dias Diciembre'"
                       + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                       + " from asignacionmetas a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " inner join produto p on a.produto = p.id"
                       + " inner join dias_uteis d"
                       + " where p.id=" + pValor
                       + " order by v.nombre  ";
            }
            else if (pOpcao == "grupovendedores")
            {
                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,p.nombre as Grupo"
                       + " ,a.Enero"
                       + " ,d.enero_diashabilesdetrabajo as 'Dias Enero'"
                       + " ,a.Febrero"
                       + " ,d.Febrero_diashabilesdetrabajo as 'Dias Febrero'"
                       + " ,a.Marzo"
                       + " ,d.Marzo_diashabilesdetrabajo as 'Dias Marzo'"
                       + " ,a.Abril"
                       + " ,d.Abril_diashabilesdetrabajo as 'Dias Abril'"
                       + " ,a.Mayo"
                       + " ,d.Mayo_diashabilesdetrabajo as 'Dias Mayo'"
                       + " ,a.Junio"
                       + " ,d.Junio_diashabilesdetrabajo as 'Dias Junio'"
                       + " ,a.Julio"
                       + " ,d.Julio_diashabilesdetrabajo as 'Dias Julio'"
                       + " ,a.Agosto"
                       + " ,d.Agosto_diashabilesdetrabajo as 'Dias Agosto'"
                       + " ,a.Septiembre"
                       + " ,d.Septiembre_diashabilesdetrabajo as 'Dias Septiembre'"
                       + " ,a.Octubre"
                       + " ,d.Octubre_diashabilesdetrabajo as 'Dias Octubre'"
                       + " ,a.Noviembre"
                       + " ,d.Noviembre_diashabilesdetrabajo as 'Dias Noviembre'"
                       + " ,a.Diciembre"
                       + " ,d.Diciembre_diashabilesdetrabajo as 'Dias Diciembre'"
                       + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                       + " from asignacionmetas a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " inner join produto p on a.produto = p.id"
                       + " inner join dias_uteis d"
                       + " where v.area=" + pValor
                       + " order by v.nombre  ";
            }
            else if (pOpcao == "vendedores")
            {
                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,p.nombre as Grupo"
                       + " ,a.Enero"
                       + " ,d.enero_diashabilesdetrabajo as 'Dias Enero'"
                       + " ,a.Febrero"
                       + " ,d.Febrero_diashabilesdetrabajo as 'Dias Febrero'"
                       + " ,a.Marzo"
                       + " ,d.Marzo_diashabilesdetrabajo as 'Dias Marzo'"
                       + " ,a.Abril"
                       + " ,d.Abril_diashabilesdetrabajo as 'Dias Abril'"
                       + " ,a.Mayo"
                       + " ,d.Mayo_diashabilesdetrabajo as 'Dias Mayo'"
                       + " ,a.Junio"
                       + " ,d.Junio_diashabilesdetrabajo as 'Dias Junio'"
                       + " ,a.Julio"
                       + " ,d.Julio_diashabilesdetrabajo as 'Dias Julio'"
                       + " ,a.Agosto"
                       + " ,d.Agosto_diashabilesdetrabajo as 'Dias Agosto'"
                       + " ,a.Septiembre"
                       + " ,d.Septiembre_diashabilesdetrabajo as 'Dias Septiembre'"
                       + " ,a.Octubre"
                       + " ,d.Octubre_diashabilesdetrabajo as 'Dias Octubre'"
                       + " ,a.Noviembre"
                       + " ,d.Noviembre_diashabilesdetrabajo as 'Dias Noviembre'"
                       + " ,a.Diciembre"
                       + " ,d.Diciembre_diashabilesdetrabajo as 'Dias Diciembre'"
                       + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                       + " from asignacionmetas a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " inner join produto p on a.produto = p.id"
                       + " inner join dias_uteis d"
                       + " where v.id=" + pValor
                       + " order by v.nombre  ";
            }    
            else if (pOpcao == "todos")
            {
                _sql = "select "
                      + " a.id"
                      + " ,v.nombre as Vendedor"
                      + " ,p.nombre as Grupo"
                       + " ,a.Enero"
                       + " ,d.enero_diashabilesdetrabajo as 'Dias Enero'"
                       + " ,a.Febrero"
                       + " ,d.Febrero_diashabilesdetrabajo as 'Dias Febrero'"
                       + " ,a.Marzo"
                       + " ,d.Marzo_diashabilesdetrabajo as 'Dias Marzo'"
                       + " ,a.Abril"
                       + " ,d.Abril_diashabilesdetrabajo as 'Dias Abril'"
                       + " ,a.Mayo"
                       + " ,d.Mayo_diashabilesdetrabajo as 'Dias Mayo'"
                       + " ,a.Junio"
                       + " ,d.Junio_diashabilesdetrabajo as 'Dias Junio'"
                       + " ,a.Julio"
                       + " ,d.Julio_diashabilesdetrabajo as 'Dias Julio'"
                       + " ,a.Agosto"
                       + " ,d.Agosto_diashabilesdetrabajo as 'Dias Agosto'"
                       + " ,a.Septiembre"
                       + " ,d.Septiembre_diashabilesdetrabajo as 'Dias Septiembre'"
                       + " ,a.Octubre"
                       + " ,d.Octubre_diashabilesdetrabajo as 'Dias Octubre'"
                       + " ,a.Noviembre"
                       + " ,d.Noviembre_diashabilesdetrabajo as 'Dias Noviembre'"
                       + " ,a.Diciembre"
                       + " ,d.Diciembre_diashabilesdetrabajo as 'Dias Diciembre'"
                      + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                      + " from asignacionmetas a "
                      + " inner join vendedor v on a.vendedor = v.id "
                      + " inner join produto p on a.produto = p.id"
                      + " inner join dias_uteis d"
                      + " order by v.nombre  ";
            }
            else if (pOpcao == "semmeta")
            {
                _sql = "select "
                      + " v.id"
                      + " ,v.nombre as Vendedor"
                      + " from asignacionmetas a "
                      + " right join vendedor v on a.vendedor = v.id "
                      + " where a.id is null"
                      + " order by v.nombre  ";
            }
            else if (pOpcao == "vend")
            {
                _sql = "select "
                      + " a.id"
                      + " ,p.nombre as Grupo"
                      + " ,v.nombre as Vendedor"
                       + " ,a.Enero"
                       + " ,d.enero_diashabilesdetrabajo as 'Dias Enero'"
                       + " ,a.Febrero"
                       + " ,d.Febrero_diashabilesdetrabajo as 'Dias Febrero'"
                       + " ,a.Marzo"
                       + " ,d.Marzo_diashabilesdetrabajo as 'Dias Marzo'"
                       + " ,a.Abril"
                       + " ,d.Abril_diashabilesdetrabajo as 'Dias Abril'"
                       + " ,a.Mayo"
                       + " ,d.Mayo_diashabilesdetrabajo as 'Dias Mayo'"
                       + " ,a.Junio"
                       + " ,d.Junio_diashabilesdetrabajo as 'Dias Junio'"
                       + " ,a.Julio"
                       + " ,d.Julio_diashabilesdetrabajo as 'Dias Julio'"
                       + " ,a.Agosto"
                       + " ,d.Agosto_diashabilesdetrabajo as 'Dias Agosto'"
                       + " ,a.Septiembre"
                       + " ,d.Septiembre_diashabilesdetrabajo as 'Dias Septiembre'"
                       + " ,a.Octubre"
                       + " ,d.Octubre_diashabilesdetrabajo as 'Dias Octubre'"
                       + " ,a.Noviembre"
                       + " ,d.Noviembre_diashabilesdetrabajo as 'Dias Noviembre'"
                       + " ,a.Diciembre"
                       + " ,d.Diciembre_diashabilesdetrabajo as 'Dias Diciembre'"
                      + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                      + " from asignacionmetas a "
                      + " inner join vendedor v on a.vendedor = v.id "
                      + " left join produto p on a.produto = p.id"
                      + " inner join dias_uteis d"
                      +  (pValor!=string.Empty? " where v.id=" + pValor :"")
                      + " order by p.nombre  ";
            }
            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        public DataSet PopularGridFiltros3(string pOpcao, string pValor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

           

                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,p.nombre as Producto"
                       + " ,a.Enero"
                       + " ,a.Febrero"
                       + " ,a.Marzo"
                       + " ,a.Abril"
                       + " ,a.Mayo"
                       + " ,a.Junio"
                       + " ,a.Julio"
                       + " ,a.Agosto"
                       + " ,a.Septiembre"
                       + " ,a.Octubre"
                       + " ,a.Noviembre"
                       + " ,a.Diciembre"
                       + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                       + " from asignacionmetas a "
                       + " inner join vendedor v on a.vendedor = v.id "
                       + " inner join produto p on a.produto = p.id"
                       + " inner join dias_uteis d"
                       + " where v.id=" + pValor
                       + " order by v.nombre  ";
            
            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridFiltros3(string pOpcao, string pValor,int idProducto)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select "
                   + " a.id"
                   + " ,v.nombre as Vendedor"
                   + " ,p.nombre as Producto"
                   + " ,a.Enero"
                   + " ,a.Febrero"
                   + " ,a.Marzo"
                   + " ,a.Abril"
                   + " ,a.Mayo"
                   + " ,a.Junio"
                   + " ,a.Julio"
                   + " ,a.Agosto"
                   + " ,a.Septiembre"
                   + " ,a.Octubre"
                   + " ,a.Noviembre"
                   + " ,a.Diciembre"
                   + " ,(a.Enero + a.Febrero + a.Marzo + a.Abril + a.Mayo + a.Junio + a.Julio + a.Agosto + a.Septiembre +a.Octubre + a.Noviembre + a.Diciembre) as total"
                   + " from asignacionmetas a "
                   + " inner join vendedor v on a.vendedor = v.id "
                   + " inner join produto p on a.produto = p.id"
                   + " inner join dias_uteis d"
                   + " where v.id=" + pValor
                   + " and p.id="+idProducto.ToString()
                   + " order by v.nombre  ";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        public DataSet PopularGridPorVendedor(string codigoVendedor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            _sql = "select m.id"
                    + " ,m.ano"
                    + " ,m.mes"
                    + " ,m.diashabilesdetrabajodelmes"
                    + " ,v.nombre"
                    + " ,m.meta"
                    + " FROM asignacionmetas m"
                    + " inner join vendedor v on m.vendedor=v.id ";
            
            if (codigoVendedor != "0")
                _sql += " where v.id=" + codigoVendedor;
                         
                            _sql+= " order by id desc";
            

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}
