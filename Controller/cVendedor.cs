﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cVendedor
    {
        public mVendedor RetornarVendedorUsuarioSenha(string usuario,string senha)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mVendedor Vendedor = new mVendedor();
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();
            cProduto engineProduto = new cProduto();

            DataSet ds = null;

            if (usuario == string.Empty || senha ==string.Empty)
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from vendedor where email='" + usuario.Replace("'", "") + "' and clave='" + senha.Replace("'", "") + "'");

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAVendedor.NOME
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Vendedor.id = Convert.ToInt32(ds.Tables[0].Rows[0]["id"].ToString());
                    Vendedor.calendario = (ds.Tables[0].Rows[0]["calendario"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[0]["calendario"].ToString()) : Convert.ToDateTime("1900-01-01"));
                    Vendedor.codigo = ds.Tables[0].Rows[0]["codigo"].ToString();
                    Vendedor.nombre = ds.Tables[0].Rows[0]["nombre"].ToString();
                    Vendedor.appelido = ds.Tables[0].Rows[0]["apellido"].ToString();
                    Vendedor.area = (ds.Tables[0].Rows[0]["area"].ToString() != string.Empty ? engineGrupoVendedores.RetornarGrupoVendedores(ds.Tables[0].Rows[0]["area"].ToString()) : null);
                    Vendedor.seccion = (ds.Tables[0].Rows[0]["seccion"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["seccion"].ToString()) : null);
                    Vendedor.meta = (ds.Tables[0].Rows[0]["meta"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["meta"].ToString()) : 0);
                    Vendedor.email = ds.Tables[0].Rows[0]["email"].ToString();
                    Vendedor.clave = ds.Tables[0].Rows[0]["clave"].ToString();
                    Vendedor.cargo = ds.Tables[0].Rows[0]["cargo"].ToString();
                    Vendedor.cartera_assignada = ds.Tables[0].Rows[0]["cartera_assignada"].ToString();

                    return Vendedor;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public mVendedor RetornarVendedor(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mVendedor Vendedor = new mVendedor();
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();
            cProduto engineProduto = new cProduto();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from vendedor where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELAVendedor.NOME
            Vendedor.id = Convert.ToInt32(id);
            Vendedor.calendario =(ds.Tables[0].Rows[0]["calendario"].ToString()!=string.Empty? Convert.ToDateTime(ds.Tables[0].Rows[0]["calendario"].ToString()):Convert.ToDateTime( "1900-01-01"));
            Vendedor.codigo = ds.Tables[0].Rows[0]["codigo"].ToString();
            Vendedor.nombre = ds.Tables[0].Rows[0]["nombre"].ToString();
            Vendedor.appelido = ds.Tables[0].Rows[0]["apellido"].ToString();
            Vendedor.area = ( ds.Tables[0].Rows[0]["area"].ToString() != string.Empty ? engineGrupoVendedores.RetornarGrupoVendedores( ds.Tables[0].Rows[0]["area"].ToString()) : null);
            Vendedor.seccion = (ds.Tables[0].Rows[0]["seccion"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["seccion"].ToString()) : null);
            Vendedor.meta = (ds.Tables[0].Rows[0]["meta"].ToString()!=string.Empty?Convert.ToDecimal(ds.Tables[0].Rows[0]["meta"].ToString()):0);
            Vendedor.email = ds.Tables[0].Rows[0]["email"].ToString();
            Vendedor.clave = ds.Tables[0].Rows[0]["clave"].ToString();
            Vendedor.cargo = ds.Tables[0].Rows[0]["cargo"].ToString();
            Vendedor.cartera_assignada = ds.Tables[0].Rows[0]["cartera_assignada"].ToString();

            return Vendedor;
        }

        public List<mVendedor> RetornarVendedor()
        {
            clsConexao banco = new clsConexao();
            cGrupoVendedores engineGrupoVendedores = new cGrupoVendedores();
            cProduto engineProduto = new cProduto();
            mVendedor Vendedor = new mVendedor();
            List<mVendedor> lstVendedor = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from vendedor ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstVendedor = new List<mVendedor>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    Vendedor = new mVendedor();
                    Vendedor.id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    Vendedor.calendario = (ds.Tables[0].Rows[i]["calendario"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["calendario"].ToString()) : Convert.ToDateTime("1900-01-01"));
                    Vendedor.codigo = ds.Tables[0].Rows[i]["codigo"].ToString();
                    Vendedor.nombre = ds.Tables[0].Rows[i]["nombre"].ToString();
                    Vendedor.appelido = ds.Tables[0].Rows[i]["apellido"].ToString();
                    Vendedor.area = (ds.Tables[0].Rows[i]["area"].ToString() != string.Empty ? engineGrupoVendedores.RetornarGrupoVendedores(ds.Tables[0].Rows[i]["area"].ToString()) : null);
                    Vendedor.seccion = (ds.Tables[0].Rows[i]["seccion"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["seccion"].ToString()) : null);
                    Vendedor.meta = (ds.Tables[0].Rows[i]["meta"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[i]["meta"].ToString()) : 0);
                    Vendedor.email = ds.Tables[0].Rows[i]["email"].ToString();
                    Vendedor.clave = ds.Tables[0].Rows[i]["clave"].ToString();
                    Vendedor.cargo = ds.Tables[0].Rows[i]["cargo"].ToString();
                    Vendedor.cartera_assignada = ds.Tables[0].Rows[i]["cartera_assignada"].ToString();
                    lstVendedor.Add(Vendedor);
                }
            }

            return lstVendedor;
        }

        public string Inserir(mVendedor Vendedor)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into vendedor("
                                                + "calendario"
                                                + ",codigo"
                                                + ",nombre"
                                                + ",apellido"
                                                + ",area"
                                                + ",seccion"
                                                + ",meta"
                                                + ",email"
                                                + ",clave"
                                                + ",cargo"
                                                + ",cartera_assignada"
                                                + ") values('"
                                                + Vendedor.calendario.ToString("yyyy-MM-dd") + "'"
                                                + ",'" + Vendedor.codigo + "'"
                                                + ",'" + Vendedor.nombre + "'"
                                                + ",'" + Vendedor.appelido + "'"
                                                + ",'" + Vendedor.area.Id + "'"
                                                + ",'" + (Vendedor.seccion!=null?Vendedor.seccion.Id.ToString():"0") + "'"
                                                + ",'" + Vendedor.meta.ToString().Replace(",",".") + "'"
                                                + ",'" + Vendedor.email + "'"
                                                + ",'" + Vendedor.clave + "'"
                                                + ",'" + Vendedor.cargo + "'"
                                                + ",'" + Vendedor.cartera_assignada + "'"
                                                + ")");

            return banco.Selecionar("select  * from vendedor order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mVendedor Vendedor)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update vendedor set "
                                                + " calendario='" + Vendedor.calendario.ToString("yyyy-MM-dd") + "'"
                                                + " ,codigo='" + Vendedor.codigo + "'"
                                                + " ,nombre='" + Vendedor.nombre + "'"
                                                + " ,apellido='" + Vendedor.appelido + "'"
                                                + " ,area='" + Vendedor.area.Id + "'"
                                                + " ,seccion='" + (Vendedor.seccion!=null?Vendedor.seccion.Id.ToString():"0") + "'"
                                                + " ,meta='" + Vendedor.meta.ToString().Replace(",",".") + "'"
                                                + " ,email='" + Vendedor.email + "'"
                                                + " ,clave='" + Vendedor.clave + "'"
                                                + " ,cargo='" + Vendedor.cargo + "'"
                                                + " ,cartera_assignada='" + Vendedor.cartera_assignada + "'"
                                                + " where id=" + Vendedor.id.ToString());
        }
        public bool Deletar(mVendedor Vendedor)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from vendedor  "
                                                + " where id=" + Vendedor.id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select "
                      + " v.id as Id"
                       + "  ,v.calendario as FECHA"
                       + "  ,v.codigo as CODIGO"
                       + "  ,v.nombre as NOMBRE"
                       + "  ,v.apellido as APELLIDO"
                       + "  ,p.nombre as AREA"
                       //+ "  ,gv.nomegrupovendedores as SECCION"
                       + "  ,v.meta as 'META'"
                       + " from vendedor v"
                       + " left join produto p on p.id=v.area"
                       + " left join grupovendedores gv on gv.id=v.seccion"
                         + " order by vendedor.id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                        + " v.id as Id"
                         + "  ,v.calendario as FECHA"
                         + "  ,v.codigo as CODIGO"
                         + "  ,v.nombre as NOMBRE"
                         + "  ,v.apellido as APELLIDO"
                         + "  ,p.nombre as AREA"
                         //+ "  ,gv.nomegrupovendedores as SECCION"
                         + "  ,v.meta as 'META'"
                         + " from vendedor v"
                         + " left join produto p on p.id=v.seccion"
                         + " left join grupovendedores gv on gv.id=v.area"
                         + " order by v.id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridComAsignadosOuNo(string asignados)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (asignados == "sim")
            {
                _sql = "select "
                      + " distinct(v.id) as Id"
                       + "  ,v.calendario as FECHA"
                       + "  ,v.codigo as CODIGO"
                       + "  ,v.nombre as NOMBRE"
                       + "  ,v.apellido as APELLIDO"
                       + "  ,v.meta as 'META'"
                       + " from vendedor v"
                       + " inner join relacaovendedor r on v.id=r.vendedor"
                         + " order by v.nombre ";
            }
            else if (asignados == "nao")
            {
                _sql = "select "
                        + " distinct(v.id) as Id"
                         + "  ,v.calendario as FECHA"
                         + "  ,v.codigo as CODIGO"
                         + "  ,v.nombre as NOMBRE"
                         + "  ,v.apellido as APELLIDO"
                         + "  ,v.meta as 'META'"
                         + " from vendedor v"
                         + " left join relacaovendedor r on r.vendedor = v.id"
                         + " where r.id is null"
                         + " order by v.id desc";
            }
            else
            {
                _sql = "select "
                    + " v.id as Id"
                    + "  ,v.calendario as FECHA"
                    + "  ,v.codigo as CODIGO"
                    + "  ,v.nombre as NOMBRE"
                    + "  ,v.apellido as APELLIDO"
                    + "  ,v.meta as 'META'"
                    + " from vendedor v"
                    + " order by v.nombre ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridComAsignadosOuNo_2(string asignados)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (asignados =="sim")
            {
                _sql = "select "
                      + " distinct(v.id) as Id"
                       + "  ,v.calendario as FECHA"
                       + "  ,v.codigo as CODIGO"
                       + "  ,v.nombre as NOMBRE"
                       + "  ,v.apellido as APELLIDO"
                       + "  ,v.meta as 'META'"
                       + "  ,r.cliente as 'cliente_id'"
                       + " from vendedor v"
                       + " inner join relacaovendedor r on v.id=r.vendedor"
                         + " order by v.nombre ";
            }
            else if (asignados == "nao")
            {
                _sql = "select "
                        + " distinct(v.id) as Id"
                         + "  ,v.calendario as FECHA"
                         + "  ,v.codigo as CODIGO"
                         + "  ,v.nombre as NOMBRE"
                         + "  ,v.apellido as APELLIDO"
                         + "  ,v.meta as 'META'"
                         + "  ,r.cliente as 'cliente_id'"
                         + " from vendedor v"
                         + " left join relacaovendedor r on r.vendedor = v.id"
                         + " where r.id is null"
                         + " order by v.id desc";
            }
            else
            {
                    _sql = "select "
                        + " v.id as Id"
                        + "  ,v.calendario as FECHA"
                        + "  ,v.codigo as CODIGO"
                        + "  ,v.nombre as NOMBRE"
                        + "  ,v.apellido as APELLIDO"
                        + "  ,v.meta as 'META'"
                        + "  ,r.cliente as 'cliente_id'"
                        + " from vendedor v"
                        + " order by v.nombre ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
        public DataSet PopularGrid_2()
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

                _sql = "select "
                        + " v.id as Id"
                         + "  ,codigo as CODIGO"
                         + "  ,nombre as NOMBRE"
                         + "  ,apellido as APELLIDO"
                         + "  ,area as AREA"
                         + "  ,seccion as SECCION"
                         + "  ,cargo as CARGO"
                         + "  ,calendario as FECHA"
                         //+ " ,if((select count(*) as total from asignacionmetas where vendedor=v.id),'S','N') as 'Meta'"
                         //+ " ,if((select count(*) as total from relacaovendedor where vendedor=v.id),'S','N') as 'Cartera Assignada'"
                         + " ,(select count(*) as total from asignacionmetas where vendedor=v.id ) as 'Meta'"
                         + " ,(select count(*) as total from relacaovendedor r inner join clientes c on r.cliente=c.id  where r.vendedor=v.id) as 'Cartera Assignada'"
                         + " from vendedor v"
                         + " order by v.nombre desc";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}