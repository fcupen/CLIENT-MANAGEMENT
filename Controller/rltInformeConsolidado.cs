﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class rltInformeConsolidado
    {
       public List<InformeConsolidadoDados> _listaDados = new List<InformeConsolidadoDados>();
         clsConexao conexao = new clsConexao();

         public rltInformeConsolidado(string pcodigoVendedor,string datainicial, string datafinal)
        {
            string _comando = string.Empty;
            DataSet ds = new DataSet();
            DataSet dscalculos = new DataSet();

            _comando = "select "
                        + " * from clientes " ;

            ds = conexao.Selecionar(_comando);

            if (ds.Tables[0].Rows.Count > 0)
            {
                int contador = 1;
                double totalcotizacion =0;
                foreach (DataRow linha in ds.Tables[0].Rows)
                {
                    //soma de cotizações trimestre
                    dscalculos = conexao.Selecionar("SELECT sum(cotizacion) FROM bdn b where b.vendedor_id="
                        + pcodigoVendedor
                        + " and b.fecha between '"+datainicial+" 00:00:00' and '"+datafinal+"23:59:59' ");
                    if(dscalculos!=null)
                    {
                        if(dscalculos.Tables[0].Rows.Count>0)
                        {
                            totalcotizacion=( dscalculos.Tables[0].Rows[0][0].ToString()!=string.Empty? Convert.ToDouble( dscalculos.Tables[0].Rows[0][0].ToString()):0);
                        }
                    }

                    //soma de brechas no trimestre
                    cBrecha engineBrecha = new cBrecha();
                    List<mBrecha> _listaBrecha = engineBrecha.RetornarBrechaPorData(Convert.ToInt32( pcodigoVendedor),datainicial,datafinal,Convert.ToInt32(linha["id"].ToString()));
                    double totalBrecha=0;
                    foreach (mBrecha item in _listaBrecha)
                    {
                        totalBrecha = Convert.ToDouble( item.valor.ToString("N2"));
                    }

                    //soma de licitacão no trimestre
                    double totallicitacao = 0;
                    dscalculos = conexao.Selecionar("SELECT sum(cotizacion) FROM bdp b where b.status='Licitacion' and b.vendedor_id=" 
                        + pcodigoVendedor
                        + " and b.fecha between '" + datainicial + " 00:00:00' and '" + datafinal + "23:59:59' ");
                    if (dscalculos != null)
                    {
                        if (dscalculos.Tables[0].Rows.Count > 0)
                        {
                            totallicitacao = (dscalculos.Tables[0].Rows[0][0].ToString() != string.Empty ? Convert.ToDouble(dscalculos.Tables[0].Rows[0][0].ToString()) : 0);
                        }
                    }

                    //somando visitas
                        int  janeiro_visitas = 0;
                        int  fevereiro_visitas = 0;
                        int  marco_visitas = 0;
                        int  abril_visitas = 0;
                        int  maio_visitas = 0;
                        int  junho_visitas = 0;
                        int  julho_visitas = 0;
                        int  agosto_visitas = 0;
                        int  setembro_visitas = 0;
                        int  outubro_visitas = 0;
                        int  novembro_visitas = 0;
                        int  dezembro = 0;
                    //obtendo o intervalo de meses
                        DateTime dtInicial = Convert.ToDateTime(datainicial);
                        DateTime dtfinal = Convert.ToDateTime(datafinal);
                        for (int i = dtInicial.Month; i <= dtInicial.Month; i++)
                        {
                            if (i == 1)
                                janeiro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 2)
                                fevereiro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 3)
                                marco_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 4)
                                abril_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 5)
                                maio_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 6)
                                junho_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 7)
                                julho_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 8)
                                agosto_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 9)
                                setembro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 10)
                                outubro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 11)
                                novembro_visitas = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 12)
                                dezembro = somarVisitasMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                        }



                    //somand faturados
                        double janeiro_faturado  = 0;
                        double fevereiro_faturado  = 0;
                        double marco_faturado  = 0;
                        double abril_faturado  = 0;
                        double maio_faturado  = 0;
                        double junho_faturado  = 0;
                        double julho_faturado  = 0;
                        double agosto_faturado  = 0;
                        double setembro_faturado  = 0;
                        double outubro_faturado  = 0;
                        double novembro_faturado  = 0;
                        double dezembro_faturado  = 0;
                        for (int i = dtInicial.Month; i <= dtInicial.Month; i++)
                        {
                            if (i == 1)
                                janeiro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 2)
                                fevereiro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 3)
                                marco_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 4)
                                abril_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 5)
                                maio_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 6)
                                junho_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 7)
                                julho_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 8)
                                agosto_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 9)
                                setembro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 10)
                                outubro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 11)
                                novembro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                            if (i == 12)
                                dezembro_faturado = somarFaturadosMes(i.ToString(), linha["id"].ToString(), pcodigoVendedor);
                        }



                    _listaDados.Add(new InformeConsolidadoDados(
                        contador.ToString(),
                        linha["razonsocial"].ToString(),
                        totalcotizacion.ToString("N2"),
                        totalBrecha.ToString("N2"),
                        totallicitacao.ToString("N2"),
                        janeiro_visitas.ToString(),
                        fevereiro_visitas.ToString(),
                        marco_visitas.ToString(),
                        abril_visitas.ToString(),
                        maio_visitas.ToString(),
                        junho_visitas.ToString(),
                        julho_visitas.ToString(),
                        agosto_visitas.ToString(),
                        setembro_visitas.ToString(),
                        outubro_visitas.ToString(),
                        novembro_visitas.ToString(),
                        dezembro.ToString(),
                        janeiro_faturado.ToString(),
            fevereiro_faturado.ToString(),
            marco_faturado.ToString(),
            abril_faturado.ToString(),
            maio_faturado.ToString(),
            junho_faturado.ToString(),
            julho_faturado.ToString(),
            agosto_faturado.ToString(),
            setembro_faturado.ToString(),
            outubro_faturado.ToString(),
            novembro_faturado.ToString(),
            dezembro_faturado.ToString()
                        ));
                }
            }
                      
        }

         private double somarFaturadosMes(string mes, string pCliente, string pVendedor)
         {
             string mesescrito = string.Empty;
             if (mes == "1")
                 mesescrito = "Enero";
             else if (mes == "2")
                 mesescrito = "Febrero";
             else if (mes == "3")
                 mesescrito = "Marzo";
             else if (mes == "4")
                 mesescrito = "Abril";
             else if (mes == "5")
                 mesescrito = "Mayo";
             else if (mes == "6")
                 mesescrito = "Junio";
             else if (mes == "7")
                 mesescrito = "Julio";
             else if (mes == "8")
                 mesescrito = "Agosto";
             else if (mes == "9")
                 mesescrito = "Septiembre";
             else if (mes == "10")
                 mesescrito = "Octubre";
             else if (mes == "11")
                 mesescrito = "Noviembre";
             else if (mes == "12")
                 mesescrito = "Diciembre";
             else
                 mesescrito = mes;

             string ConsultaSQL =string.Empty;
             DataSet ds = null;
             clsConexao banco = new clsConexao();
             double totalvisitas = 0;
             for (int i = 1; i < 31; i++)
             {
                 ConsultaSQL = "select  sum(d" + i.ToString() + "_valor) as total from ventas_dias where mes='" 
                     + mesescrito + "' and ano='" + DateTime.Now.Year.ToString() +
                     "' and cliente_id=" + pCliente + " and vendedor_id=" + pVendedor ;

                 ds = banco.Selecionar(ConsultaSQL);
                 if (ds != null)
                     totalvisitas += Convert.ToDouble((ds.Tables[0].Rows[0][0].ToString()!=string.Empty?ds.Tables[0].Rows[0][0]:0));
             }
             return totalvisitas;
         }

         private int somarVisitasMes(string mes, string pCliente, string pVendedor)
         {
             string mesescrito = string.Empty;
             if (mes == "1")
                 mesescrito = "Enero";
             else if (mes == "2")
                 mesescrito = "Febrero";
             else if (mes == "3")
                 mesescrito = "Marzo";
             else if (mes == "4")
                 mesescrito = "Abril";
             else if (mes == "5")
                 mesescrito = "Mayo";
             else if (mes == "6")
                 mesescrito = "Junio";
             else if (mes == "7")
                 mesescrito = "Julio";
             else if (mes == "8")
                 mesescrito = "Agosto";
             else if (mes == "9")
                 mesescrito = "Septiembre";
             else if (mes == "10")
                 mesescrito = "Octubre";
             else if (mes == "11")
                 mesescrito = "Noviembre";
             else if (mes == "12")
                 mesescrito = "Diciembre";
             else
                 mesescrito = mes;

             string ConsultaSQL = string.Empty;
             DataSet ds = null;
             clsConexao banco = new clsConexao();
             int totalvisitas = 0;
             for (int i = 1; i < 31; i++)
             {
                 ConsultaSQL = "select * from informediario where mes='" + mesescrito + "' and ano='" + DateTime.Now.Year.ToString() +
                     "' and cliente_id=" + pCliente + " and vendedor_id=" + pVendedor + " and "
                     + "d" + i.ToString() + "_valor<>'F' and d" + i.ToString() + "_valor<>'P' and d" + i.ToString() + "_valor<>''";

                 ds = banco.Selecionar(ConsultaSQL);
                 if(ds!=null)
                    totalvisitas += ds.Tables[0].Rows.Count;
             }
             return totalvisitas;
         }
        public List<InformeConsolidadoDados> GetDados()
        {
            return _listaDados;
        }

        public class InformeConsolidadoDados
        {
            
            #region Atributos
            public string codigo { get; set; }
            public string cliente { get; set; }
            public string cotizacao { get; set; }
            public string brecha { get; set; }
            public string licitacao { get; set; }
            public string janeiro_visitas { get; set; }
            public string fevereiro_visitas { get; set; }
            public string marco_visitas { get; set; }
            public string abril_visitas { get; set; }
            public string maio_visitas { get; set; }
            public string junho_visitas { get; set; }
            public string julho_visitas { get; set; }
            public string agosto_visitas { get; set; }
            public string setembro_visitas { get; set; }
            public string outubro_visitas { get; set; }
            public string novembro_visitas { get; set; }
            public string dezembro { get; set; }
            public string janeiro_faturado { get; set; }
            public string fevereiro_faturado { get; set; }
            public string marco_faturado { get; set; }
            public string abril_faturado { get; set; }
            public string maio_faturado { get; set; }
            public string junho_faturado { get; set; }
            public string julho_faturado { get; set; }
            public string agosto_faturado { get; set; }
            public string setembro_faturado { get; set; }
            public string outubro_faturado { get; set; }
            public string novembro_faturado { get; set; }
            public string dezembro_faturado { get; set; }
            #endregion




            public InformeConsolidadoDados(
                               string pcodigo ,
                                string pcliente ,
                                string pcotizacao ,
                                string pbrecha ,
                                string plicitacao ,
                                string pjaneiro_visitas ,
                                string pfevereiro_visitas ,
                                string pmarco_visitas ,
                                string pabril_visitas ,
                                string pmaio_visitas ,
                                string pjunho_visitas ,
                                string pjulho_visitas ,
                                string pagosto_visitas ,
                                string psetembro_visitas ,
                                string poutubro_visitas ,
                                string pnovembro_visitas ,
                                string pdezembro ,
                                string pjaneiro_faturado ,
                                string pfevereiro_faturado ,
                                string pmarco_faturado ,
                                string pabril_faturado ,
                                string pmaio_faturado ,
                                string pjunho_faturado ,
                                string pjulho_faturado ,
                                string pagosto_faturado ,
                                string psetembro_faturado ,
                                string poutubro_faturado ,
                                string pnovembro_faturado ,
                                string pdezembro_faturado 
                )
            {

                //string 
                    codigo=pcodigo ;
                    cliente=pcliente ;
                    cotizacao=pcotizacao ;
                    brecha=pbrecha ;
                    licitacao=plicitacao ;
                    janeiro_visitas=pjaneiro_visitas ;
                    fevereiro_visitas=pfevereiro_visitas ;
                    marco_visitas=pmarco_visitas ;
                    abril_visitas=pabril_visitas ;
                    maio_visitas=pmaio_visitas ;
                    junho_visitas=pjunho_visitas ;
                    julho_visitas=pjulho_visitas ;
                    agosto_visitas=pagosto_visitas ;
                    setembro_visitas=psetembro_visitas ;
                    outubro_visitas=poutubro_visitas ;
                    novembro_visitas=pnovembro_visitas ;
                    dezembro=pdezembro ;
                    janeiro_faturado=pjaneiro_faturado ;
                    fevereiro_faturado=pfevereiro_faturado ;
                    marco_faturado=pmarco_faturado ;
                    abril_faturado=pabril_faturado ;
                    maio_faturado=pmaio_faturado ;
                    junho_faturado=pjunho_faturado ;
                    julho_faturado=pjulho_faturado ;
                    agosto_faturado=pagosto_faturado ;
                    setembro_faturado=psetembro_faturado ;
                    outubro_faturado=poutubro_faturado ;
                    novembro_faturado=pnovembro_faturado ;
                    dezembro_faturado = pdezembro_faturado; 
                
            }
        }
    }
}