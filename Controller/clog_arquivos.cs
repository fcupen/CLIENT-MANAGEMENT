﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class clog_arquivos
    {
        public mlog_arquivo Retornarlog_arquivo(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mlog_arquivo log_arquivo = new mlog_arquivo();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from log_arquivo where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            log_arquivo.Id = Convert.ToInt32(id);
            log_arquivo.vendedor = (ds.Tables[0].Rows[0]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor_id"].ToString()) : null);
            log_arquivo.data = (ds.Tables[0].Rows[0]["data"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[0]["data"].ToString()) : DateTime.Now);
            log_arquivo.arquivo = ds.Tables[0].Rows[0]["arquivo"].ToString();
            log_arquivo.tela = ds.Tables[0].Rows[0]["tela"].ToString();
            return log_arquivo;
        }

        public List<mlog_arquivo> Retornarlog_arquivo(int vendedor)
        {
            clsConexao banco = new clsConexao();
            mlog_arquivo log_arquivo = new mlog_arquivo();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            List<mlog_arquivo> lstlog_arquivo = null;
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM log_arquivo where vendedor_id=" + vendedor );

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstlog_arquivo = new List<mlog_arquivo>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    log_arquivo = new mlog_arquivo();
                    log_arquivo.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    log_arquivo.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    log_arquivo.data = (ds.Tables[0].Rows[i]["data"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["data"].ToString()) : DateTime.Now);
                    log_arquivo.arquivo = ds.Tables[0].Rows[i]["arquivo"].ToString();
                    log_arquivo.tela = ds.Tables[0].Rows[i]["tela"].ToString();
                    lstlog_arquivo.Add(log_arquivo);
                }
            }

            return lstlog_arquivo;
        }

        public List<mlog_arquivo> Retornarlog_arquivoPorData(int vendedor, string datainicial, string datafinal,string tela)
        {
            clsConexao banco = new clsConexao();
            mlog_arquivo log_arquivo = new mlog_arquivo();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            List<mlog_arquivo> lstlog_arquivo = null;
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM log_arquivo where vendedor_id=" + vendedor +
                (tela!=string.Empty? " and tela='" + tela + "'":"")
                + " and data between '" + datainicial + " 00:00:00' and '" + datafinal + " 23:59:59' ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstlog_arquivo = new List<mlog_arquivo>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    log_arquivo = new mlog_arquivo();
                    log_arquivo.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    log_arquivo.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    log_arquivo.data = (ds.Tables[0].Rows[i]["data"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["data"].ToString()) : DateTime.Now);
                    log_arquivo.arquivo = ds.Tables[0].Rows[i]["arquivo"].ToString();
                    log_arquivo.tela = ds.Tables[0].Rows[i]["tela"].ToString();
                    lstlog_arquivo.Add(log_arquivo);
                }
            }

            return lstlog_arquivo;
        }

        public List<mlog_arquivo> Retornarlog_arquivo()
        {
            clsConexao banco = new clsConexao();
            mlog_arquivo log_arquivo = new mlog_arquivo();
            cVendedor engineVendedor = new cVendedor();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            List<mlog_arquivo> lstlog_arquivo = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from log_arquivo ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstlog_arquivo = new List<mlog_arquivo>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    log_arquivo = new mlog_arquivo();
                    log_arquivo.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    log_arquivo.vendedor = (ds.Tables[0].Rows[i]["vendedor_id"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor_id"].ToString()) : null);
                    log_arquivo.data = (ds.Tables[0].Rows[i]["data"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["data"].ToString()) : DateTime.Now);
                    log_arquivo.arquivo = ds.Tables[0].Rows[i]["arquivo"].ToString();
                    log_arquivo.tela = ds.Tables[0].Rows[i]["tela"].ToString();
                    lstlog_arquivo.Add(log_arquivo);
                }
            }

            return lstlog_arquivo;
        }

        public string Inserir(mlog_arquivo log_arquivo)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into log_arquivo("
                                                + "vendedor_id"
                                                + ",arquivo"
                                                + ",tela"
                                                + ",data"
                                                + ") values('"
                                                + (log_arquivo.vendedor != null ? log_arquivo.vendedor.id.ToString() : "0") + "'"
                                                + ",'" + log_arquivo.arquivo + "'"
                                                + ",'" + log_arquivo.tela + "'"
                                                + ",'" + log_arquivo.data.ToString("yyyy-MM-dd") + "'"
                                                + ")");

            return banco.Selecionar("select  * from log_arquivo order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mlog_arquivo log_arquivo)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update log_arquivo set "
                                                + " vendedor_id='" + (log_arquivo.vendedor != null ? log_arquivo.vendedor.id.ToString() : "0") + "'"
                                                + " ,arquivo='" + log_arquivo.arquivo + "'"
                                                + " ,tela='" + log_arquivo.tela  + "'"
                                                + " ,data='" + log_arquivo.data.ToString("yyyy-MM-dd") + "'"
                                                + " where id=" + log_arquivo.Id.ToString());
        }
        public bool Deletar(mlog_arquivo log_arquivo)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from log_arquivo  "
                                                + " where id=" + log_arquivo.Id.ToString());
        }

        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select"
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,a.tela"
                       + " ,a.data"
                       + " ,a.arquivo"
                       + " from log_arquivo a "
                       + " inner join vendedor v on a.vendedor_id = v.id "
                       + " order by v.nombre limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                       + " a.id"
                       + " ,v.nombre as Vendedor"
                       + " ,a.tela"
                       + " ,a.data"
                       + " ,a.arquivo"
                       + " from log_arquivo a "
                       + " inner join vendedor v on a.vendedor_id = v.id "
                       + " order by v.nombre  ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}