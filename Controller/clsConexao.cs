﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;


namespace Clients_Management.Controller
{
    public class clsConexao
    {
        private MySqlConnection objConexao = null;
        
        public MySqlConnection ObjConexaoprop
        {
            get { return objConexao; }
            set { objConexao = value; }
        }

        private string strConexao = WebConfigurationManager.ConnectionStrings["Clients_ManagementConnectionString"].ToString();

        public bool Conectar()
        {
            try
            {
                if (objConexao == null)
                    objConexao = new MySqlConnection(strConexao);
                objConexao.Open();
                return true;
            }
            catch (Exception ex)
            {
                //Alert.Show(ex.Message.ToString());
                return false;
            }
        }

        public bool Desconectar()
        {
            try
            {
                objConexao.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Alert.Show(ex.Message.ToString());
                return false;
            }
        }
        public DataSet Selecionar(string pComando)
        {
            DataSet ds = new DataSet();
            try
            {
                if (Conectar())
                {
                    MySqlDataAdapter objAdapter = new
                    MySqlDataAdapter(pComando, objConexao);
                    objAdapter.Fill(ds);
                    return ds;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Alert.Show(ex.Message.ToString());
                return null;
            }
            finally
            {
                Desconectar();
            }
        }
        public bool CreateUpdateDelete(string _pComando)
        {
            DataSet ds = new DataSet();
            try
            {
                if (Conectar())
                {
                    MySqlCommand objComando = new MySqlCommand(_pComando, objConexao);
                    objComando.ExecuteNonQuery();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //Alert.Show(ex.Message);
                return false;
            }
            finally
            {
                Desconectar();
            }
        }
    }
}