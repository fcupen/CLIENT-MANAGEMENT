﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cBDP
    {
        public mBDP RetornaBDP(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mBDP BDP = new mBDP();
            cClientes engineCliente = new cClientes();
            cProduto engineProduto = new cProduto();
            cVendedor engineVendedor = new cVendedor();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from bdp where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            BDP.Id = Convert.ToInt32(id);
            BDP.fecha = (ds.Tables[0].Rows[0]["fecha"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[0]["fecha"].ToString()) : Convert.ToDateTime("1900-01-01"));
            BDP.cotizacion = ds.Tables[0].Rows[0]["cotizacion"].ToString();
            BDP.instituicion = (ds.Tables[0].Rows[0]["instituicion"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[0]["instituicion"].ToString()) : null);
            BDP.produto = (ds.Tables[0].Rows[0]["grupo_produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["grupo_produto"].ToString()) : null);
            BDP.vendedor = (ds.Tables[0].Rows[0]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[0]["vendedor"].ToString()) : null);
            BDP.contacto = ds.Tables[0].Rows[0]["contacto"].ToString();
            BDP.descripcion_productos = ds.Tables[0].Rows[0]["descripcion_productos"].ToString();
            BDP.totalneto = (ds.Tables[0].Rows[0]["totalneto"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[0]["totalneto"].ToString()) : 0);
            BDP.status = ds.Tables[0].Rows[0]["status"].ToString();
            return BDP;
        }

        public List<mBDP> RetornaBDP()
        {
            clsConexao banco = new clsConexao();
            mBDP BDP = new mBDP();
            cClientes engineCliente = new cClientes();
            cProduto engineProduto = new cProduto();
            cVendedor engineVendedor = new cVendedor();
            List<mBDP> lstMBDP = null;
            DataSet ds = null;

            ds = banco.Selecionar("SELECT * FROM bdp ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstMBDP = new List<mBDP>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    BDP = new mBDP();
                    BDP.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    BDP.fecha = (ds.Tables[0].Rows[i]["fecha"].ToString() != string.Empty ? Convert.ToDateTime(ds.Tables[0].Rows[i]["fecha"].ToString()) : Convert.ToDateTime("1900-01-01"));
                    BDP.cotizacion = ds.Tables[0].Rows[i]["cotizacion"].ToString();
                    BDP.produto = (ds.Tables[0].Rows[i]["grupo_produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["grupo_produto"].ToString()) : null);
                    BDP.instituicion = (ds.Tables[0].Rows[i]["instituicion"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["instituicion"].ToString()) : null);
                    BDP.vendedor = (ds.Tables[0].Rows[i]["vendedor"].ToString() != string.Empty ? engineVendedor.RetornarVendedor(ds.Tables[0].Rows[i]["vendedor"].ToString()) : null);
                    BDP.contacto = ds.Tables[0].Rows[i]["contacto"].ToString();
                    BDP.descripcion_productos = ds.Tables[0].Rows[i]["descripcion_productos"].ToString();
                    BDP.totalneto = (ds.Tables[0].Rows[i]["totalneto"].ToString() != string.Empty ? Convert.ToDecimal(ds.Tables[0].Rows[i]["totalneto"].ToString()) : 0);
                    BDP.status = ds.Tables[0].Rows[i]["status"].ToString();
                    lstMBDP.Add(BDP);
                }
            }

            return lstMBDP;
        }

        public string Inserir(mBDP BDP)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into bdp("
                                                + "fecha"
                                                + ",cotizacion"
                                                + ",instituicion"
                                                + ",contacto"
                                                + ",descripcion_productos"
                                                + ",totalneto"
                                                + ",status"
                                                + ",grupo_produto"
                                                + ",vendedor"      
                                                + ") values('"
                                                + BDP.fecha.ToString("yyyy-MM-dd") + "'"
                                                + ",'" + BDP.cotizacion + "'"
                                                + ",'" + (BDP.instituicion != null ? BDP.instituicion.id.ToString() : "0") + "'"
                                                + ",'" + BDP.contacto + "'"
                                                + ",'" + BDP.descripcion_productos + "'"
                                                + ",'" + BDP.totalneto.ToString().Replace(",", ".") + "'"
                                                + ",'" + BDP.status + "'"
                                                + ",'" + (BDP.produto != null ? BDP.produto.Id.ToString() : "0") + "'"
                                                + ",'" + (BDP.vendedor != null ? BDP.vendedor.id.ToString() : "0") + "'"
                                                + ")");

            return banco.Selecionar("select  * from bdp order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mBDP BDP)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update bdp set "
                                                + " fecha='" + BDP.fecha.ToString("yyyy-MM-dd") + "'"
                                                + " ,instituicion='" + (BDP.instituicion != null ? BDP.instituicion.id.ToString() : "0") + "'"
                                                + " ,cotizacion='" + BDP.cotizacion + "'"
                                                + " ,contacto='" + BDP.contacto + "'"
                                                + " ,descripcion_productos='" + BDP.descripcion_productos + "'"
                                                + " ,status='" + BDP.status + "'"
                                                + " ,totalneto='" + BDP.totalneto.ToString().Replace(",", ".") + "'"
                                                + " ,grupo_produto='" + (BDP.produto != null ? BDP.produto.Id.ToString() : "0") + "'"
                                                + " ,vendedor='" + (BDP.vendedor != null ? BDP.vendedor.id.ToString() : "0") + "'"
                                                + " where id=" + BDP.Id.ToString());
        }
        public bool Deletar(mBDP BDP)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from bdp  "
                                                + " where id=" + BDP.Id.ToString());
        }

        public DataSet PopularGrid(int limite,string idVendedor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select"
                        + " b.id"
                        + " ,b.fecha"
                        + " ,c.razonsocial as 'razon social'"
                        + " ,p.nombre as Producto"
                        + " ,b.cotizacion"
                        + " ,b.contacto"
                        + " ,b.descripcion_productos"
                        + " ,ROUND(b.totalneto) as 'total neto'"
                        + " ,b.status"
                        + " ,v.nombre as Vendedor"
                        + " from bdp b"
                        + " inner join clientes c"
                        + " on b.instituicion = c.id"
                        + " inner join produto p on b.grupo_produto = p.id"
                        + " inner join vendedor v on b.vendedor = v.id"
                        + " where b.vendedor="+idVendedor
                       + " order by c.razonsocial limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                      + " b.id"
                        + " ,b.fecha"
                        + " ,c.razonsocial as 'razon social'"
                        + " ,p.nombre as Producto"
                        + " ,b.cotizacion"
                        + " ,b.contacto"
                        + " ,b.descripcion_productos"
                        + " ,ROUND(b.totalneto) as 'total neto'"
                        + " ,b.status"
                        + " ,v.nombre as Vendedor"
                        + " from bdp b"
                        + " inner join clientes c"
                        + " on b.instituicion = c.id"
                        + " inner join produto p on b.grupo_produto = p.id"
                        + " inner join vendedor v on b.vendedor = v.id"
                        + " where b.vendedor=" + idVendedor
                       + " order by c.razonsocial  ";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGrid(string idVendedor,int idProducto)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

                _sql = "select "
                      + " b.id"
                        + " ,b.fecha"
                        + " ,c.razonsocial"
                        + " ,p.nombre as Produto"
                        + " ,b.cotizacion"
                        + " ,b.contacto"
                        + " ,b.descripcion_productos"
                        + " ,b.totalneto"
                        + " ,b.status"
                        + " from bdp b"
                        + " inner join clientes c"
                        + " on b.instituicion = c.id"
                        + " inner join produto p on b.grupo_produto = p.id"
                        + " where b.vendedor=" + idVendedor
                        + " and b.grupo_produto=" + idProducto
                       + " order by c.razonsocial  ";
            

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGridFiltros(string opcao,string idVendedor)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;



            _sql = "select "
                  + " b.id"
                    + " ,b.fecha"
                    + " ,c.razonsocial"
                    + " ,p.nombre as Produto"
                    + " ,b.cotizacion"
                    + " ,b.contacto"
                    + " ,b.descripcion_productos"
                    + " ,b.totalneto"
                    + " ,b.status"
                    + " from bdp b"
                    + " inner join clientes c"
                    + " on b.instituicion = c.id"
                    + " inner join produto p on b.grupo_produto = p.id"
                    + " where b.vendedor="+idVendedor;

            if (opcao != "Todas")
                _sql += " and b.status='" + opcao + "'";

            _sql += " order by c.razonsocial  ";
            

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

        public DataSet PopularGrid_2(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;


            //_sql = "select"
            //        + " b.fecha as 'FECHA'"
            //        + " ,b.cotizacion as 'COTIZACIÓN'"
            //        + " ,c.razonsocial 'INSTITUCIÓN'"
            //        + " ,b.contacto as 'CONTACTO'"
            //        + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS'"
            //        + " ,b.totalneto as 'TOTAL NETO'"
            //        + " ,b.status as 'ESTADO'"
            //        + " from BDP b"
            //        + " inner join clientes c"
            //        + " on b.instituicion = c.id";
            _sql = "select "
                     + " b.fecha as 'FECHA' "
                     + " ,b.cotizacion as 'COTIZACIÓN' "
                     + " ,c.razonsocial 'INSTITUCIÓN' "
                     + " ,b.contacto as 'CONTACTO' "
                     + " ,b.descripcion_productos as 'DESCRIPCIÓN PRODUCTOS' "
                     + " ,b.totalneto as 'TOTAL NETO' "
                     + " ,b.status as 'ESTADO' "
                     + " ,if((select TIMESTAMPDIFF(MONTH, b.fecha, now()) as meses)>p.horizonte,'S','N') as Expirado "
                     + " from BDP b "
                     + " inner join clientes c on b.instituicion = c.id "
                     + " inner join produto p on b.grupo_produto=p.id"
                     + " order by b.totalneto asc;";

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }
    }
}