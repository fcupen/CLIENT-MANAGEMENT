﻿using Clients_Management.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Clients_Management.Controller
{
    public class cRelacaoCliente
    {
        public mRelacaoCliente Retornarrelacaocliente(string id)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mRelacaoCliente relacaocliente = new mRelacaoCliente();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();

            DataSet ds = null;

            if (id == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from relacaocliente where id=" + id);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELArelacaocliente.NOME
            relacaocliente.Id = Convert.ToInt32(id);
            relacaocliente.produto = (ds.Tables[0].Rows[0]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["produto"].ToString()) : null);
            relacaocliente.cliente = (ds.Tables[0].Rows[0]["cliente"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[0]["cliente"].ToString()) : null);

            return relacaocliente;
        }

        public mRelacaoCliente RetornarRelacaoVendedorPorProdutoCliente(string produto, string cliente)
        {
            clsConexao banco = new clsConexao();

            //ALTERAR NOME DA TABELA, COLOCAR A LETRA m ANTES DO NOME ONDE POSSUI
            mRelacaoCliente RelacaoCliente = new mRelacaoCliente();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();

            DataSet ds = null;

            if (produto == "0" || cliente == "0")
                return null;

            //ALTEREAR NOME DA TABELA
            ds = banco.Selecionar("select * from relacaocliente where produto=" + produto + " and cliente=" + cliente);

            //SEGUIR O MODELO NOME DA TABELA . CAMPO
            //EX: TABELARelacaoVendedor.NOME
            if (ds.Tables[0].Rows.Count > 0)
            {
                RelacaoCliente.Id = Convert.ToInt32(ds.Tables[0].Rows[0]["id"].ToString());
                RelacaoCliente.produto = (ds.Tables[0].Rows[0]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[0]["produto"].ToString()) : null);
                RelacaoCliente.cliente = (ds.Tables[0].Rows[0]["cliente"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[0]["cliente"].ToString()) : null);
                return RelacaoCliente;
            }
            else
            {
                return null;
            }
        }

        public List<mRelacaoCliente> Retornarrelacaocliente()
        {
            clsConexao banco = new clsConexao();
            mRelacaoCliente relacaocliente = new mRelacaoCliente();
            cProduto engineProduto = new cProduto();
            cClientes engineCliente = new cClientes();
            List<mRelacaoCliente> lstrelacaocliente = null;
            DataSet ds = null;

            ds = banco.Selecionar("select * from relacaocliente ");

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstrelacaocliente = new List<mRelacaoCliente>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //mesma coisa da função de cima
                    relacaocliente = new mRelacaoCliente();
                    relacaocliente.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["id"].ToString());
                    relacaocliente.produto = (ds.Tables[0].Rows[i]["produto"].ToString() != string.Empty ? engineProduto.RetornarProduto(ds.Tables[0].Rows[i]["produto"].ToString()) : null);
                    relacaocliente.cliente = (ds.Tables[0].Rows[i]["cliente"].ToString() != string.Empty ? engineCliente.RetornarClientes(ds.Tables[0].Rows[i]["cliente"].ToString()) : null);
                    lstrelacaocliente.Add(relacaocliente);
                }
            }

            return lstrelacaocliente;
        }
        
        public string Inserir(mRelacaoCliente relacaocliente)
        {

            clsConexao banco = new clsConexao();

            banco.CreateUpdateDelete("Insert into relacaocliente("
                                                + "produto"
                                                + ",cliente"
                                                + ") values('"
                                                + (relacaocliente.produto != null ? relacaocliente.produto.Id.ToString() : "0") + "'"
                                                + ",'" + (relacaocliente.cliente != null ? relacaocliente.cliente.id.ToString() : "0") + "'"
                                                + ")");

            return banco.Selecionar("select  * from relacaocliente order by id desc limit 1").Tables[0].Rows[0]["id"].ToString();

        }

        public bool Update(mRelacaoCliente relacaocliente)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("update relacaocliente set "
                                                + " produto='" + (relacaocliente.produto != null ? relacaocliente.produto.Id.ToString() : "0") + "'"
                                                + " ,cliente='" + (relacaocliente.cliente != null ? relacaocliente.cliente.id.ToString() : "0") + "'"
                                                + " where id=" + relacaocliente.Id.ToString());
        }
        public bool Deletar(mRelacaoCliente relacaocliente)
        {
            clsConexao banco = new clsConexao();
            return banco.CreateUpdateDelete("delete from relacaocliente  "
                                                + " where id=" + relacaocliente.Id.ToString());
        }
        public DataSet PopularGrid(int limite)
        {
            clsConexao banco = new clsConexao();
            DataSet dsConsulta = new DataSet();
            string _sql = string.Empty;

            if (limite > 0)
            {
                _sql = "select "
                        + " r.id as Id"
                        + "    ,v.nombre as Nombre"
                        + " ,c.razonsocial as 'Razon Social'"
                        + " from relacaocliente r"
                        + " inner join clientes c on r.cliente = c.id"
                        + " inner join produto v on r.produto = v.id "
                         + " order by id desc limit " + limite.ToString();
            }
            else
            {
                _sql = "select "
                        + " r.id as Id"
                        + "    ,v.nombre as Nombre"
                        + " ,c.razonsocial as 'Razon Social'"
                        + " from relacaocliente r"
                        + " inner join clientes c on r.cliente = c.id"
                        + " inner join produto v on r.produto = v.id "
                         + " order by id desc";
            }

            dsConsulta = banco.Selecionar(_sql);

            return dsConsulta;
        }

       
    }
}